<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'F:\Dewald\Appture\AppturePay\Globeflight\DSV\Integration\WSDL\Shipping\DB847C94.wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'F:\Dewald\Appture\AppturePay\Globeflight\DSV\Integration\WSDL\Shipping\DB847C94.wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \AppturePay\DSV\ClassMap::get(),
];
/**
 * Samples for Submit ServiceType
 */
$submit = new \AppturePay\DSV\ServiceType\Submit($options);
/**
 * Sample call for SubmitShipmentTMS operation/method
 */
if ($submit->SubmitShipmentTMS(new \AppturePay\DSV\StructType\SubmitShipmentTMS()) !== false) {
    print_r($submit->getResult());
} else {
    print_r($submit->getLastError());
}
/**
 * Sample call for SubmitCancelTMS operation/method
 */
if ($submit->SubmitCancelTMS(new \AppturePay\DSV\StructType\SubmitCancelTMS()) !== false) {
    print_r($submit->getResult());
} else {
    print_r($submit->getLastError());
}
/**
 * Samples for Retrieve ServiceType
 */
$retrieve = new \AppturePay\DSV\ServiceType\Retrieve($options);
/**
 * Sample call for RetrieveShipmentLabelsTMS operation/method
 */
if ($retrieve->RetrieveShipmentLabelsTMS(new \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMS()) !== false) {
    print_r($retrieve->getResult());
} else {
    print_r($retrieve->getLastError());
}
/**
 * Samples for Validate ServiceType
 */
$validate = new \AppturePay\DSV\ServiceType\Validate($options);
/**
 * Sample call for ValidateAddress operation/method
 */
if ($validate->ValidateAddress(new \AppturePay\DSV\StructType\ValidateAddress()) !== false) {
    print_r($validate->getResult());
} else {
    print_r($validate->getLastError());
}
