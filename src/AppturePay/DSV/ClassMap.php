<?php

declare(strict_types=1);

namespace AppturePay\DSV;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'SubmitShipmentTMS' => '\\AppturePay\\DSV\\StructType\\SubmitShipmentTMS',
            'SubmitShipmentTMSResponse' => '\\AppturePay\\DSV\\StructType\\SubmitShipmentTMSResponse',
            'ShipmentServiceResultTMS' => '\\AppturePay\\DSV\\StructType\\ShipmentServiceResultTMS',
            'ShipmentServiceResult' => '\\AppturePay\\DSV\\StructType\\ShipmentServiceResult',
            'SubmitCancelTMS' => '\\AppturePay\\DSV\\StructType\\SubmitCancelTMS',
            'SubmitCancelTMSResponse' => '\\AppturePay\\DSV\\StructType\\SubmitCancelTMSResponse',
            'RetrieveShipmentLabelsTMS' => '\\AppturePay\\DSV\\StructType\\RetrieveShipmentLabelsTMS',
            'RetrieveShipmentLabelsRequest' => '\\AppturePay\\DSV\\StructType\\RetrieveShipmentLabelsRequest',
            'RetrieveShipmentLabelsTMSResponse' => '\\AppturePay\\DSV\\StructType\\RetrieveShipmentLabelsTMSResponse',
            'ValidateAddress' => '\\AppturePay\\DSV\\StructType\\ValidateAddress',
            'ValidateAddressRequest' => '\\AppturePay\\DSV\\StructType\\ValidateAddressRequest',
            'ValidateAddressResponse' => '\\AppturePay\\DSV\\StructType\\ValidateAddressResponse',
            'ArrayOfValidAddress' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfValidAddress',
            'ValidAddress' => '\\AppturePay\\DSV\\StructType\\ValidAddress',
            'ShipmentType' => '\\AppturePay\\DSV\\StructType\\ShipmentType',
            'EdiCustomerDepartmentTypeComplex' => '\\AppturePay\\DSV\\StructType\\EdiCustomerDepartmentTypeComplex_1',
            'FileType' => '\\AppturePay\\DSV\\StructType\\FileType_1',
            'TotalsType' => '\\AppturePay\\DSV\\StructType\\TotalsType_1',
            'DimensionType' => '\\AppturePay\\DSV\\StructType\\DimensionType_1',
            'TransportType' => '\\AppturePay\\DSV\\StructType\\TransportType',
            'SeafreightType' => '\\AppturePay\\DSV\\StructType\\SeafreightType',
            'AirfreightType' => '\\AppturePay\\DSV\\StructType\\AirfreightType',
            'CustomItemsType' => '\\AppturePay\\DSV\\StructType\\CustomItemsType',
            'AmountsType' => '\\AppturePay\\DSV\\StructType\\AmountsType',
            'CodesType' => '\\AppturePay\\DSV\\StructType\\CodesType',
            'ParametersType' => '\\AppturePay\\DSV\\StructType\\ParametersType_1',
            'ExtraParametersType' => '\\AppturePay\\DSV\\StructType\\ExtraParametersType_1',
            'CashInfoType' => '\\AppturePay\\DSV\\StructType\\CashInfoType',
            'ExtraAmountType' => '\\AppturePay\\DSV\\StructType\\ExtraAmountType_1',
            'FreightChargeType' => '\\AppturePay\\DSV\\StructType\\FreightChargeType',
            'MeansOfTransportType' => '\\AppturePay\\DSV\\StructType\\MeansOfTransportType',
            'CarrierType' => '\\AppturePay\\DSV\\StructType\\CarrierType',
            'EmissionType' => '\\AppturePay\\DSV\\StructType\\EmissionType',
            'TextKeysType' => '\\AppturePay\\DSV\\StructType\\TextKeysType_1',
            'DateTimeZonesType' => '\\AppturePay\\DSV\\StructType\\DateTimeZonesType',
            'AddressType' => '\\AppturePay\\DSV\\StructType\\AddressType_1',
            'AdditionalType' => '\\AppturePay\\DSV\\StructType\\AdditionalType_1',
            'AddressDetailsType' => '\\AppturePay\\DSV\\StructType\\AddressDetailsType_1',
            'ContactInformationType' => '\\AppturePay\\DSV\\StructType\\ContactInformationType_1',
            'CTcoordinatesType' => '\\AppturePay\\DSV\\StructType\\CTcoordinatesType',
            'TimeFramesType' => '\\AppturePay\\DSV\\StructType\\TimeFramesType',
            'ExtraReferenceType' => '\\AppturePay\\DSV\\StructType\\ExtraReferenceType',
            'ReferenceCodeTypeComplex' => '\\AppturePay\\DSV\\StructType\\ReferenceCodeTypeComplex',
            'ReturnReferenceType' => '\\AppturePay\\DSV\\StructType\\ReturnReferenceType',
            'AdditionalServicesType' => '\\AppturePay\\DSV\\StructType\\AdditionalServicesType_1',
            'ConNoteType' => '\\AppturePay\\DSV\\StructType\\ConNoteType',
            'LinkType' => '\\AppturePay\\DSV\\StructType\\LinkType',
            'FreeTextType' => '\\AppturePay\\DSV\\StructType\\FreeTextType',
            'CustomsDocumentsType' => '\\AppturePay\\DSV\\StructType\\CustomsDocumentsType',
            'TransportInstructionType' => '\\AppturePay\\DSV\\StructType\\TransportInstructionType_1',
            'PlanCharacteristicType' => '\\AppturePay\\DSV\\StructType\\PlanCharacteristicType_1',
            'PlanningType' => '\\AppturePay\\DSV\\StructType\\PlanningType',
            'TripNumberTypeComplex' => '\\AppturePay\\DSV\\StructType\\TripNumberTypeComplex',
            'ShippingUnitType' => '\\AppturePay\\DSV\\StructType\\ShippingUnitType',
            'UnitDimensionType' => '\\AppturePay\\DSV\\StructType\\UnitDimensionType',
            'CarrierReferenceType' => '\\AppturePay\\DSV\\StructType\\CarrierReferenceType',
            'ShippingUnitDetailsType' => '\\AppturePay\\DSV\\StructType\\ShippingUnitDetailsType',
            'GoodsLineType' => '\\AppturePay\\DSV\\StructType\\GoodsLineType_1',
            'DangerousGoodsType' => '\\AppturePay\\DSV\\StructType\\DangerousGoodsType_1',
            'CustomLineItemsTms' => '\\AppturePay\\DSV\\StructType\\CustomLineItemsTms',
            'SubQuantityType' => '\\AppturePay\\DSV\\StructType\\SubQuantityType',
            'MarksAndNumbersType' => '\\AppturePay\\DSV\\StructType\\MarksAndNumbersType',
            'GoodsDescriptionType' => '\\AppturePay\\DSV\\StructType\\GoodsDescriptionType',
            'DangerousGoodsSpecificationType' => '\\AppturePay\\DSV\\StructType\\DangerousGoodsSpecificationType',
            'TechnicalNamesType' => '\\AppturePay\\DSV\\StructType\\TechnicalNamesType',
            'ContainerType' => '\\AppturePay\\DSV\\StructType\\ContainerType',
            'CostAndChargeType' => '\\AppturePay\\DSV\\StructType\\CostAndChargeType',
            'TailorMadeType' => '\\AppturePay\\DSV\\StructType\\TailorMadeType',
            'ServiceMessage' => '\\AppturePay\\DSV\\StructType\\ServiceMessage',
            'ConnectionInfo' => '\\AppturePay\\DSV\\StructType\\ConnectionInfo',
            'EdiParametersType' => '\\AppturePay\\DSV\\StructType\\EdiParametersType_1',
            'LoggingType' => '\\AppturePay\\DSV\\StructType\\LoggingType_1',
            'FileHeaderType' => '\\AppturePay\\DSV\\StructType\\FileHeaderType_1',
            'InvoiceLineType' => '\\AppturePay\\DSV\\StructType\\InvoiceLineType_1',
            'ActivityCodeTypeComplex' => '\\AppturePay\\DSV\\StructType\\ActivityCodeTypeComplex_1',
            'VatType' => '\\AppturePay\\DSV\\StructType\\VatType_1',
            'WeightCalculationType' => '\\AppturePay\\DSV\\StructType\\WeightCalculationType_1',
            'TariffBaseType' => '\\AppturePay\\DSV\\StructType\\TariffBaseType_1',
            'TariffSpecificationType' => '\\AppturePay\\DSV\\StructType\\TariffSpecificationType_1',
            'ChildType' => '\\AppturePay\\DSV\\StructType\\ChildType_1',
            'ArrayOfChildType' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfChildType',
            'ArrayOfLoggingType' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfLoggingType',
            'ArrayOfExtraAmountType' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfExtraAmountType',
            'ArrayOfInvoiceLineType' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfInvoiceLineType',
            'ArrayOfTariffBaseType' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfTariffBaseType',
            'ArrayOfShippingUnitType' => '\\AppturePay\\DSV\\ArrayType\\ArrayOfShippingUnitType',
            'ServiceMessageConnectionInfo' => '\\AppturePay\\DSV\\StructType\\ServiceMessageConnectionInfo',
            'StatusMessage' => '\\AppturePay\\DSV\\StructType\\StatusMessage',
            'fileHeader' => '\\AppturePay\\DSV\\StructType\\FileHeader',
            'trackingAndTracing' => '\\AppturePay\\DSV\\StructType\\TrackingAndTracing',
        ];
    }
}
