<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitCancelTMSResponse StructType
 * @subpackage Structs
 */
class SubmitCancelTMSResponse extends AbstractStructBase
{
    /**
     * The SubmitCancelTMSResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShipmentServiceResultTMS|null
     */
    protected ?\AppturePay\DSV\StructType\ShipmentServiceResultTMS $SubmitCancelTMSResult = null;
    /**
     * Constructor method for SubmitCancelTMSResponse
     * @uses SubmitCancelTMSResponse::setSubmitCancelTMSResult()
     * @param \AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitCancelTMSResult
     */
    public function __construct(?\AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitCancelTMSResult = null)
    {
        $this
            ->setSubmitCancelTMSResult($submitCancelTMSResult);
    }
    /**
     * Get SubmitCancelTMSResult value
     * @return \AppturePay\DSV\StructType\ShipmentServiceResultTMS|null
     */
    public function getSubmitCancelTMSResult(): ?\AppturePay\DSV\StructType\ShipmentServiceResultTMS
    {
        return $this->SubmitCancelTMSResult;
    }
    /**
     * Set SubmitCancelTMSResult value
     * @param \AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitCancelTMSResult
     * @return \AppturePay\DSV\StructType\SubmitCancelTMSResponse
     */
    public function setSubmitCancelTMSResult(?\AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitCancelTMSResult = null): self
    {
        $this->SubmitCancelTMSResult = $submitCancelTMSResult;
        
        return $this;
    }
}
