<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateAddressRequest StructType
 * @subpackage Structs
 */
class ValidateAddressRequest extends AbstractStructBase
{
    /**
     * The NoOfReturnAddresses
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $NoOfReturnAddresses;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The CountryCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $CountryCode = null;
    /**
     * The Province
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Province = null;
    /**
     * The Town
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Town = null;
    /**
     * The Suburb
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Suburb = null;
    /**
     * The PostalCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $PostalCode = null;
    /**
     * The Latitude
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Latitude = null;
    /**
     * The Longitude
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Longitude = null;
    /**
     * Constructor method for ValidateAddressRequest
     * @uses ValidateAddressRequest::setNoOfReturnAddresses()
     * @uses ValidateAddressRequest::setCountry()
     * @uses ValidateAddressRequest::setCountryCode()
     * @uses ValidateAddressRequest::setProvince()
     * @uses ValidateAddressRequest::setTown()
     * @uses ValidateAddressRequest::setSuburb()
     * @uses ValidateAddressRequest::setPostalCode()
     * @uses ValidateAddressRequest::setLatitude()
     * @uses ValidateAddressRequest::setLongitude()
     * @param int $noOfReturnAddresses
     * @param string $country
     * @param string $countryCode
     * @param string $province
     * @param string $town
     * @param string $suburb
     * @param string $postalCode
     * @param string $latitude
     * @param string $longitude
     */
    public function __construct(int $noOfReturnAddresses, ?string $country = null, ?string $countryCode = null, ?string $province = null, ?string $town = null, ?string $suburb = null, ?string $postalCode = null, ?string $latitude = null, ?string $longitude = null)
    {
        $this
            ->setNoOfReturnAddresses($noOfReturnAddresses)
            ->setCountry($country)
            ->setCountryCode($countryCode)
            ->setProvince($province)
            ->setTown($town)
            ->setSuburb($suburb)
            ->setPostalCode($postalCode)
            ->setLatitude($latitude)
            ->setLongitude($longitude);
    }
    /**
     * Get NoOfReturnAddresses value
     * @return int
     */
    public function getNoOfReturnAddresses(): int
    {
        return $this->NoOfReturnAddresses;
    }
    /**
     * Set NoOfReturnAddresses value
     * @param int $noOfReturnAddresses
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setNoOfReturnAddresses(int $noOfReturnAddresses): self
    {
        // validation for constraint: int
        if (!is_null($noOfReturnAddresses) && !(is_int($noOfReturnAddresses) || ctype_digit($noOfReturnAddresses))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($noOfReturnAddresses, true), gettype($noOfReturnAddresses)), __LINE__);
        }
        $this->NoOfReturnAddresses = $noOfReturnAddresses;
        
        return $this;
    }
    /**
     * Get Country value
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->Country;
    }
    /**
     * Set Country value
     * @param string $country
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        $this->Country = $country;
        
        return $this;
    }
    /**
     * Get CountryCode value
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->CountryCode;
    }
    /**
     * Set CountryCode value
     * @param string $countryCode
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setCountryCode(?string $countryCode = null): self
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->CountryCode = $countryCode;
        
        return $this;
    }
    /**
     * Get Province value
     * @return string|null
     */
    public function getProvince(): ?string
    {
        return $this->Province;
    }
    /**
     * Set Province value
     * @param string $province
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setProvince(?string $province = null): self
    {
        // validation for constraint: string
        if (!is_null($province) && !is_string($province)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($province, true), gettype($province)), __LINE__);
        }
        $this->Province = $province;
        
        return $this;
    }
    /**
     * Get Town value
     * @return string|null
     */
    public function getTown(): ?string
    {
        return $this->Town;
    }
    /**
     * Set Town value
     * @param string $town
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setTown(?string $town = null): self
    {
        // validation for constraint: string
        if (!is_null($town) && !is_string($town)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($town, true), gettype($town)), __LINE__);
        }
        $this->Town = $town;
        
        return $this;
    }
    /**
     * Get Suburb value
     * @return string|null
     */
    public function getSuburb(): ?string
    {
        return $this->Suburb;
    }
    /**
     * Set Suburb value
     * @param string $suburb
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setSuburb(?string $suburb = null): self
    {
        // validation for constraint: string
        if (!is_null($suburb) && !is_string($suburb)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($suburb, true), gettype($suburb)), __LINE__);
        }
        $this->Suburb = $suburb;
        
        return $this;
    }
    /**
     * Get PostalCode value
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->PostalCode;
    }
    /**
     * Set PostalCode value
     * @param string $postalCode
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setPostalCode(?string $postalCode = null): self
    {
        // validation for constraint: string
        if (!is_null($postalCode) && !is_string($postalCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postalCode, true), gettype($postalCode)), __LINE__);
        }
        $this->PostalCode = $postalCode;
        
        return $this;
    }
    /**
     * Get Latitude value
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return $this->Latitude;
    }
    /**
     * Set Latitude value
     * @param string $latitude
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setLatitude(?string $latitude = null): self
    {
        // validation for constraint: string
        if (!is_null($latitude) && !is_string($latitude)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($latitude, true), gettype($latitude)), __LINE__);
        }
        $this->Latitude = $latitude;
        
        return $this;
    }
    /**
     * Get Longitude value
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return $this->Longitude;
    }
    /**
     * Set Longitude value
     * @param string $longitude
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest
     */
    public function setLongitude(?string $longitude = null): self
    {
        // validation for constraint: string
        if (!is_null($longitude) && !is_string($longitude)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($longitude, true), gettype($longitude)), __LINE__);
        }
        $this->Longitude = $longitude;
        
        return $this;
    }
}
