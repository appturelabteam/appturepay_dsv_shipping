<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitShipmentTMS StructType
 * @subpackage Structs
 */
class SubmitShipmentTMS extends AbstractStructBase
{
    /**
     * The shipment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShipmentType|null
     */
    protected ?\AppturePay\DSV\StructType\ShipmentType $shipment = null;
    /**
     * Constructor method for SubmitShipmentTMS
     * @uses SubmitShipmentTMS::setShipment()
     * @param \AppturePay\DSV\StructType\ShipmentType $shipment
     */
    public function __construct(?\AppturePay\DSV\StructType\ShipmentType $shipment = null)
    {
        $this
            ->setShipment($shipment);
    }
    /**
     * Get shipment value
     * @return \AppturePay\DSV\StructType\ShipmentType|null
     */
    public function getShipment(): ?\AppturePay\DSV\StructType\ShipmentType
    {
        return $this->shipment;
    }
    /**
     * Set shipment value
     * @param \AppturePay\DSV\StructType\ShipmentType $shipment
     * @return \AppturePay\DSV\StructType\SubmitShipmentTMS
     */
    public function setShipment(?\AppturePay\DSV\StructType\ShipmentType $shipment = null): self
    {
        $this->shipment = $shipment;
        
        return $this;
    }
}
