<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ReferenceCodeTypeComplex StructType
 * @subpackage Structs
 */
class ReferenceCodeTypeComplex extends AbstractStructBase
{
    /**
     * The _
     * @var int|null
     */
    protected ?int $_ = null;
    /**
     * The fixedType
     * @var int|null
     */
    protected ?int $fixedType = null;
    /**
     * Constructor method for ReferenceCodeTypeComplex
     * @uses ReferenceCodeTypeComplex::set_()
     * @uses ReferenceCodeTypeComplex::setFixedType()
     * @param int $_
     * @param int $fixedType
     */
    public function __construct(?int $_ = null, ?int $fixedType = null)
    {
        $this
            ->set_($_)
            ->setFixedType($fixedType);
    }
    /**
     * Get _ value
     * @return int|null
     */
    public function get_(): ?int
    {
        return $this->_;
    }
    /**
     * Set _ value
     * @param int $_
     * @return \AppturePay\DSV\StructType\ReferenceCodeTypeComplex
     */
    public function set_(?int $_ = null): self
    {
        // validation for constraint: int
        if (!is_null($_) && !(is_int($_) || ctype_digit($_))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($_, true), gettype($_)), __LINE__);
        }
        $this->_ = $_;
        
        return $this;
    }
    /**
     * Get fixedType value
     * @return int|null
     */
    public function getFixedType(): ?int
    {
        return $this->fixedType;
    }
    /**
     * Set fixedType value
     * @param int $fixedType
     * @return \AppturePay\DSV\StructType\ReferenceCodeTypeComplex
     */
    public function setFixedType(?int $fixedType = null): self
    {
        // validation for constraint: int
        if (!is_null($fixedType) && !(is_int($fixedType) || ctype_digit($fixedType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($fixedType, true), gettype($fixedType)), __LINE__);
        }
        $this->fixedType = $fixedType;
        
        return $this;
    }
}
