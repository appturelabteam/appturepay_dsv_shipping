<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RetrieveShipmentLabelsTMSResponse StructType
 * @subpackage Structs
 */
class RetrieveShipmentLabelsTMSResponse extends AbstractStructBase
{
    /**
     * The ResponseCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $ResponseCode;
    /**
     * The RequestDateTimne
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var string
     */
    protected string $RequestDateTimne;
    /**
     * The ResponseMessage
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ResponseMessage = null;
    /**
     * The LabelCreated
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - nillable: true
     * @var string
     */
    protected ?string $LabelCreated;
    /**
     * The Shipment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Shipment = null;
    /**
     * The LabelType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $LabelType = null;
    /**
     * The NoOfLabels
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - nillable: true
     * @var int
     */
    protected ?int $NoOfLabels;
    /**
     * The Label
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Label = null;
    /**
     * The RetrieveShipmentLabelsTMSResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse|null
     */
    protected ?\AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse $RetrieveShipmentLabelsTMSResult = null;
    /**
     * Constructor method for RetrieveShipmentLabelsTMSResponse
     * @uses RetrieveShipmentLabelsTMSResponse::setResponseCode()
     * @uses RetrieveShipmentLabelsTMSResponse::setRequestDateTimne()
     * @uses RetrieveShipmentLabelsTMSResponse::setResponseMessage()
     * @uses RetrieveShipmentLabelsTMSResponse::setLabelCreated()
     * @uses RetrieveShipmentLabelsTMSResponse::setShipment()
     * @uses RetrieveShipmentLabelsTMSResponse::setLabelType()
     * @uses RetrieveShipmentLabelsTMSResponse::setNoOfLabels()
     * @uses RetrieveShipmentLabelsTMSResponse::setLabel()
     * @uses RetrieveShipmentLabelsTMSResponse::setRetrieveShipmentLabelsTMSResult()
     * @param int $responseCode
     * @param string $requestDateTimne
     * @param string $responseMessage
     * @param string $labelCreated
     * @param string $shipment
     * @param string $labelType
     * @param int $noOfLabels
     * @param string $label
     * @param \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse $retrieveShipmentLabelsTMSResult
     */
    public function __construct(int $responseCode, string $requestDateTimne, ?string $responseMessage = null, ?string $labelCreated, ?string $shipment = null, ?string $labelType = null, ?int $noOfLabels, ?string $label = null, ?\AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse $retrieveShipmentLabelsTMSResult = null)
    {
        $this
            ->setResponseCode($responseCode)
            ->setRequestDateTimne($requestDateTimne)
            ->setResponseMessage($responseMessage)
            ->setLabelCreated($labelCreated)
            ->setShipment($shipment)
            ->setLabelType($labelType)
            ->setNoOfLabels($noOfLabels)
            ->setLabel($label)
            ->setRetrieveShipmentLabelsTMSResult($retrieveShipmentLabelsTMSResult);
    }
    /**
     * Get ResponseCode value
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->ResponseCode;
    }
    /**
     * Set ResponseCode value
     * @param int $responseCode
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setResponseCode(int $responseCode): self
    {
        // validation for constraint: int
        if (!is_null($responseCode) && !(is_int($responseCode) || ctype_digit($responseCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($responseCode, true), gettype($responseCode)), __LINE__);
        }
        $this->ResponseCode = $responseCode;
        
        return $this;
    }
    /**
     * Get RequestDateTimne value
     * @return string
     */
    public function getRequestDateTimne(): string
    {
        return $this->RequestDateTimne;
    }
    /**
     * Set RequestDateTimne value
     * @param string $requestDateTimne
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setRequestDateTimne(string $requestDateTimne): self
    {
        // validation for constraint: string
        if (!is_null($requestDateTimne) && !is_string($requestDateTimne)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($requestDateTimne, true), gettype($requestDateTimne)), __LINE__);
        }
        $this->RequestDateTimne = $requestDateTimne;
        
        return $this;
    }
    /**
     * Get ResponseMessage value
     * @return string|null
     */
    public function getResponseMessage(): ?string
    {
        return $this->ResponseMessage;
    }
    /**
     * Set ResponseMessage value
     * @param string $responseMessage
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setResponseMessage(?string $responseMessage = null): self
    {
        // validation for constraint: string
        if (!is_null($responseMessage) && !is_string($responseMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseMessage, true), gettype($responseMessage)), __LINE__);
        }
        $this->ResponseMessage = $responseMessage;
        
        return $this;
    }
    /**
     * Get LabelCreated value
     * @return string
     */
    public function getLabelCreated(): string
    {
        return $this->LabelCreated;
    }
    /**
     * Set LabelCreated value
     * @param string $labelCreated
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setLabelCreated(?string $labelCreated): self
    {
        // validation for constraint: string
        if (!is_null($labelCreated) && !is_string($labelCreated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($labelCreated, true), gettype($labelCreated)), __LINE__);
        }
        $this->LabelCreated = $labelCreated;
        
        return $this;
    }
    /**
     * Get Shipment value
     * @return string|null
     */
    public function getShipment(): ?string
    {
        return $this->Shipment;
    }
    /**
     * Set Shipment value
     * @param string $shipment
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setShipment(?string $shipment = null): self
    {
        // validation for constraint: string
        if (!is_null($shipment) && !is_string($shipment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipment, true), gettype($shipment)), __LINE__);
        }
        $this->Shipment = $shipment;
        
        return $this;
    }
    /**
     * Get LabelType value
     * @return string|null
     */
    public function getLabelType(): ?string
    {
        return $this->LabelType;
    }
    /**
     * Set LabelType value
     * @param string $labelType
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setLabelType(?string $labelType = null): self
    {
        // validation for constraint: string
        if (!is_null($labelType) && !is_string($labelType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($labelType, true), gettype($labelType)), __LINE__);
        }
        $this->LabelType = $labelType;
        
        return $this;
    }
    /**
     * Get NoOfLabels value
     * @return int
     */
    public function getNoOfLabels(): int
    {
        return $this->NoOfLabels;
    }
    /**
     * Set NoOfLabels value
     * @param int $noOfLabels
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setNoOfLabels(?int $noOfLabels): self
    {
        // validation for constraint: int
        if (!is_null($noOfLabels) && !(is_int($noOfLabels) || ctype_digit($noOfLabels))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($noOfLabels, true), gettype($noOfLabels)), __LINE__);
        }
        $this->NoOfLabels = $noOfLabels;
        
        return $this;
    }
    /**
     * Get Label value
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->Label;
    }
    /**
     * Set Label value
     * @param string $label
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setLabel(?string $label = null): self
    {
        // validation for constraint: string
        if (!is_null($label) && !is_string($label)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($label, true), gettype($label)), __LINE__);
        }
        $this->Label = $label;
        
        return $this;
    }
    /**
     * Get RetrieveShipmentLabelsTMSResult value
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse|null
     */
    public function getRetrieveShipmentLabelsTMSResult(): ?\AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
    {
        return $this->RetrieveShipmentLabelsTMSResult;
    }
    /**
     * Set RetrieveShipmentLabelsTMSResult value
     * @param \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse $retrieveShipmentLabelsTMSResult
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function setRetrieveShipmentLabelsTMSResult(?\AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse $retrieveShipmentLabelsTMSResult = null): self
    {
        $this->RetrieveShipmentLabelsTMSResult = $retrieveShipmentLabelsTMSResult;
        
        return $this;
    }
}
