<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CostAndChargeType StructType
 * @subpackage Structs
 */
class CostAndChargeType extends AbstractStructBase
{
    /**
     * The transactionType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transactionType = null;
    /**
     * The addressType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $addressType = null;
    /**
     * The activityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $activityCode = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for CostAndChargeType
     * @uses CostAndChargeType::setTransactionType()
     * @uses CostAndChargeType::setAddressType()
     * @uses CostAndChargeType::setActivityCode()
     * @uses CostAndChargeType::setCurrencyCode()
     * @uses CostAndChargeType::setAmount()
     * @uses CostAndChargeType::setDescription()
     * @uses CostAndChargeType::setType()
     * @param string $transactionType
     * @param int $addressType
     * @param int $activityCode
     * @param string $currencyCode
     * @param float $amount
     * @param string $description
     * @param string $type
     */
    public function __construct(?string $transactionType = null, ?int $addressType = null, ?int $activityCode = null, ?string $currencyCode = null, ?float $amount = null, ?string $description = null, ?string $type = null)
    {
        $this
            ->setTransactionType($transactionType)
            ->setAddressType($addressType)
            ->setActivityCode($activityCode)
            ->setCurrencyCode($currencyCode)
            ->setAmount($amount)
            ->setDescription($description)
            ->setType($type);
    }
    /**
     * Get transactionType value
     * @return string|null
     */
    public function getTransactionType(): ?string
    {
        return $this->transactionType;
    }
    /**
     * Set transactionType value
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $transactionType
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setTransactionType(?string $transactionType = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\TransactionTypeType_1::valueIsValid($transactionType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\TransactionTypeType_1', is_array($transactionType) ? implode(', ', $transactionType) : var_export($transactionType, true), implode(', ', \AppturePay\DSV\EnumType\TransactionTypeType_1::getValidValues())), __LINE__);
        }
        $this->transactionType = $transactionType;
        
        return $this;
    }
    /**
     * Get addressType value
     * @return int|null
     */
    public function getAddressType(): ?int
    {
        return $this->addressType;
    }
    /**
     * Set addressType value
     * @param int $addressType
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setAddressType(?int $addressType = null): self
    {
        // validation for constraint: int
        if (!is_null($addressType) && !(is_int($addressType) || ctype_digit($addressType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($addressType, true), gettype($addressType)), __LINE__);
        }
        $this->addressType = $addressType;
        
        return $this;
    }
    /**
     * Get activityCode value
     * @return int|null
     */
    public function getActivityCode(): ?int
    {
        return $this->activityCode;
    }
    /**
     * Set activityCode value
     * @param int $activityCode
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setActivityCode(?int $activityCode = null): self
    {
        // validation for constraint: int
        if (!is_null($activityCode) && !(is_int($activityCode) || ctype_digit($activityCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activityCode, true), gettype($activityCode)), __LINE__);
        }
        $this->activityCode = $activityCode;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\CostAndChargeType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
