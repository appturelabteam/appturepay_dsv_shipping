<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ActivityCodeTypeComplex StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ActivityCodeTypeComplex
 * @subpackage Structs
 */
class ActivityCodeTypeComplex_1 extends AbstractStructBase
{
    /**
     * The _
     * @var int|null
     */
    protected ?int $_ = null;
    /**
     * The group
     * @var string|null
     */
    protected ?string $group = null;
    /**
     * The groupField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $groupField = null;
    /**
     * The valueField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $valueField = null;
    /**
     * Constructor method for ActivityCodeTypeComplex
     * @uses ActivityCodeTypeComplex_1::set_()
     * @uses ActivityCodeTypeComplex_1::setGroup()
     * @uses ActivityCodeTypeComplex_1::setGroupField()
     * @uses ActivityCodeTypeComplex_1::setValueField()
     * @param int $_
     * @param string $group
     * @param string $groupField
     * @param string $valueField
     */
    public function __construct(?int $_ = null, ?string $group = null, ?string $groupField = null, ?string $valueField = null)
    {
        $this
            ->set_($_)
            ->setGroup($group)
            ->setGroupField($groupField)
            ->setValueField($valueField);
    }
    /**
     * Get _ value
     * @return int|null
     */
    public function get_(): ?int
    {
        return $this->_;
    }
    /**
     * Set _ value
     * @param int $_
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1
     */
    public function set_(?int $_ = null): self
    {
        // validation for constraint: int
        if (!is_null($_) && !(is_int($_) || ctype_digit($_))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($_, true), gettype($_)), __LINE__);
        }
        $this->_ = $_;
        
        return $this;
    }
    /**
     * Get group value
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }
    /**
     * Set group value
     * @param string $group
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1
     */
    public function setGroup(?string $group = null): self
    {
        // validation for constraint: string
        if (!is_null($group) && !is_string($group)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($group, true), gettype($group)), __LINE__);
        }
        $this->group = $group;
        
        return $this;
    }
    /**
     * Get groupField value
     * @return string|null
     */
    public function getGroupField(): ?string
    {
        return $this->groupField;
    }
    /**
     * Set groupField value
     * @param string $groupField
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1
     */
    public function setGroupField(?string $groupField = null): self
    {
        // validation for constraint: string
        if (!is_null($groupField) && !is_string($groupField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($groupField, true), gettype($groupField)), __LINE__);
        }
        $this->groupField = $groupField;
        
        return $this;
    }
    /**
     * Get valueField value
     * @return string|null
     */
    public function getValueField(): ?string
    {
        return $this->valueField;
    }
    /**
     * Set valueField value
     * @param string $valueField
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1
     */
    public function setValueField(?string $valueField = null): self
    {
        // validation for constraint: string
        if (!is_null($valueField) && !is_string($valueField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($valueField, true), gettype($valueField)), __LINE__);
        }
        $this->valueField = $valueField;
        
        return $this;
    }
}
