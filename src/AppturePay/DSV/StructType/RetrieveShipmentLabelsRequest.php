<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RetrieveShipmentLabelsRequest StructType
 * @subpackage Structs
 */
class RetrieveShipmentLabelsRequest extends AbstractStructBase
{
    /**
     * The Shipment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Shipment = null;
    /**
     * The PrimaryReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $PrimaryReference = null;
    /**
     * The RelationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $RelationNumber = null;
    /**
     * The LabelType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $LabelType = null;
    /**
     * Constructor method for RetrieveShipmentLabelsRequest
     * @uses RetrieveShipmentLabelsRequest::setShipment()
     * @uses RetrieveShipmentLabelsRequest::setPrimaryReference()
     * @uses RetrieveShipmentLabelsRequest::setRelationNumber()
     * @uses RetrieveShipmentLabelsRequest::setLabelType()
     * @param string $shipment
     * @param string $primaryReference
     * @param string $relationNumber
     * @param string $labelType
     */
    public function __construct(?string $shipment = null, ?string $primaryReference = null, ?string $relationNumber = null, ?string $labelType = null)
    {
        $this
            ->setShipment($shipment)
            ->setPrimaryReference($primaryReference)
            ->setRelationNumber($relationNumber)
            ->setLabelType($labelType);
    }
    /**
     * Get Shipment value
     * @return string|null
     */
    public function getShipment(): ?string
    {
        return $this->Shipment;
    }
    /**
     * Set Shipment value
     * @param string $shipment
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest
     */
    public function setShipment(?string $shipment = null): self
    {
        // validation for constraint: string
        if (!is_null($shipment) && !is_string($shipment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipment, true), gettype($shipment)), __LINE__);
        }
        $this->Shipment = $shipment;
        
        return $this;
    }
    /**
     * Get PrimaryReference value
     * @return string|null
     */
    public function getPrimaryReference(): ?string
    {
        return $this->PrimaryReference;
    }
    /**
     * Set PrimaryReference value
     * @param string $primaryReference
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest
     */
    public function setPrimaryReference(?string $primaryReference = null): self
    {
        // validation for constraint: string
        if (!is_null($primaryReference) && !is_string($primaryReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($primaryReference, true), gettype($primaryReference)), __LINE__);
        }
        $this->PrimaryReference = $primaryReference;
        
        return $this;
    }
    /**
     * Get RelationNumber value
     * @return string|null
     */
    public function getRelationNumber(): ?string
    {
        return $this->RelationNumber;
    }
    /**
     * Set RelationNumber value
     * @param string $relationNumber
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest
     */
    public function setRelationNumber(?string $relationNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($relationNumber) && !is_string($relationNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->RelationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get LabelType value
     * @return string|null
     */
    public function getLabelType(): ?string
    {
        return $this->LabelType;
    }
    /**
     * Set LabelType value
     * @param string $labelType
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest
     */
    public function setLabelType(?string $labelType = null): self
    {
        // validation for constraint: string
        if (!is_null($labelType) && !is_string($labelType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($labelType, true), gettype($labelType)), __LINE__);
        }
        $this->LabelType = $labelType;
        
        return $this;
    }
}
