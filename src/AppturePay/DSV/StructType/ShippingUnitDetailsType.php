<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShippingUnitDetailsType StructType
 * @subpackage Structs
 */
class ShippingUnitDetailsType extends AbstractStructBase
{
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The packageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCode = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The netWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $netWeight = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The volumeCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $volumeCode = null;
    /**
     * The cubicMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cubicMeters = null;
    /**
     * The loadingMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadingMeters = null;
    /**
     * The goodsValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $goodsValue = null;
    /**
     * The loadIndex
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadIndex = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for ShippingUnitDetailsType
     * @uses ShippingUnitDetailsType::setQuantity()
     * @uses ShippingUnitDetailsType::setPackageCode()
     * @uses ShippingUnitDetailsType::setGrossWeight()
     * @uses ShippingUnitDetailsType::setNetWeight()
     * @uses ShippingUnitDetailsType::setVolume()
     * @uses ShippingUnitDetailsType::setVolumeCode()
     * @uses ShippingUnitDetailsType::setCubicMeters()
     * @uses ShippingUnitDetailsType::setLoadingMeters()
     * @uses ShippingUnitDetailsType::setGoodsValue()
     * @uses ShippingUnitDetailsType::setLoadIndex()
     * @uses ShippingUnitDetailsType::setType()
     * @param float $quantity
     * @param string $packageCode
     * @param float $grossWeight
     * @param float $netWeight
     * @param float $volume
     * @param string $volumeCode
     * @param float $cubicMeters
     * @param float $loadingMeters
     * @param float $goodsValue
     * @param float $loadIndex
     * @param string $type
     */
    public function __construct(?float $quantity = null, ?string $packageCode = null, ?float $grossWeight = null, ?float $netWeight = null, ?float $volume = null, ?string $volumeCode = null, ?float $cubicMeters = null, ?float $loadingMeters = null, ?float $goodsValue = null, ?float $loadIndex = null, ?string $type = null)
    {
        $this
            ->setQuantity($quantity)
            ->setPackageCode($packageCode)
            ->setGrossWeight($grossWeight)
            ->setNetWeight($netWeight)
            ->setVolume($volume)
            ->setVolumeCode($volumeCode)
            ->setCubicMeters($cubicMeters)
            ->setLoadingMeters($loadingMeters)
            ->setGoodsValue($goodsValue)
            ->setLoadIndex($loadIndex)
            ->setType($type);
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get packageCode value
     * @return string|null
     */
    public function getPackageCode(): ?string
    {
        return $this->packageCode;
    }
    /**
     * Set packageCode value
     * @param string $packageCode
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setPackageCode(?string $packageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCode) && !is_string($packageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCode, true), gettype($packageCode)), __LINE__);
        }
        $this->packageCode = $packageCode;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get netWeight value
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    /**
     * Set netWeight value
     * @param float $netWeight
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setNetWeight(?float $netWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($netWeight) && !(is_float($netWeight) || is_numeric($netWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($netWeight, true), gettype($netWeight)), __LINE__);
        }
        $this->netWeight = $netWeight;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get volumeCode value
     * @return string|null
     */
    public function getVolumeCode(): ?string
    {
        return $this->volumeCode;
    }
    /**
     * Set volumeCode value
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $volumeCode
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setVolumeCode(?string $volumeCode = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\VolumeCodeType_1::valueIsValid($volumeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\VolumeCodeType_1', is_array($volumeCode) ? implode(', ', $volumeCode) : var_export($volumeCode, true), implode(', ', \AppturePay\DSV\EnumType\VolumeCodeType_1::getValidValues())), __LINE__);
        }
        $this->volumeCode = $volumeCode;
        
        return $this;
    }
    /**
     * Get cubicMeters value
     * @return float|null
     */
    public function getCubicMeters(): ?float
    {
        return $this->cubicMeters;
    }
    /**
     * Set cubicMeters value
     * @param float $cubicMeters
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setCubicMeters(?float $cubicMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($cubicMeters) && !(is_float($cubicMeters) || is_numeric($cubicMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cubicMeters, true), gettype($cubicMeters)), __LINE__);
        }
        $this->cubicMeters = $cubicMeters;
        
        return $this;
    }
    /**
     * Get loadingMeters value
     * @return float|null
     */
    public function getLoadingMeters(): ?float
    {
        return $this->loadingMeters;
    }
    /**
     * Set loadingMeters value
     * @param float $loadingMeters
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setLoadingMeters(?float $loadingMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($loadingMeters) && !(is_float($loadingMeters) || is_numeric($loadingMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadingMeters, true), gettype($loadingMeters)), __LINE__);
        }
        $this->loadingMeters = $loadingMeters;
        
        return $this;
    }
    /**
     * Get goodsValue value
     * @return float|null
     */
    public function getGoodsValue(): ?float
    {
        return $this->goodsValue;
    }
    /**
     * Set goodsValue value
     * @param float $goodsValue
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setGoodsValue(?float $goodsValue = null): self
    {
        // validation for constraint: float
        if (!is_null($goodsValue) && !(is_float($goodsValue) || is_numeric($goodsValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsValue, true), gettype($goodsValue)), __LINE__);
        }
        $this->goodsValue = $goodsValue;
        
        return $this;
    }
    /**
     * Get loadIndex value
     * @return float|null
     */
    public function getLoadIndex(): ?float
    {
        return $this->loadIndex;
    }
    /**
     * Set loadIndex value
     * @param float $loadIndex
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setLoadIndex(?float $loadIndex = null): self
    {
        // validation for constraint: float
        if (!is_null($loadIndex) && !(is_float($loadIndex) || is_numeric($loadIndex))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadIndex, true), gettype($loadIndex)), __LINE__);
        }
        $this->loadIndex = $loadIndex;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
