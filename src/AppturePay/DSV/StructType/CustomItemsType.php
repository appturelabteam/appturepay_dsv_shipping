<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CustomItemsType StructType
 * @subpackage Structs
 */
class CustomItemsType extends AbstractStructBase
{
    /**
     * The manifestNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $manifestNumber = null;
    /**
     * The documentCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentCode = null;
    /**
     * The documentNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentNumber = null;
    /**
     * The documentIssuingOffice
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentIssuingOffice = null;
    /**
     * The specialCustomsInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $specialCustomsInformation = null;
    /**
     * The documentItemNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $documentItemNumber = null;
    /**
     * The countryOfOrigin
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryOfOrigin = null;
    /**
     * The countryOfDespatch
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryOfDespatch = null;
    /**
     * The countryOfDestination
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryOfDestination = null;
    /**
     * The documentIssuingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentIssuingDate = null;
    /**
     * Constructor method for CustomItemsType
     * @uses CustomItemsType::setManifestNumber()
     * @uses CustomItemsType::setDocumentCode()
     * @uses CustomItemsType::setDocumentNumber()
     * @uses CustomItemsType::setDocumentIssuingOffice()
     * @uses CustomItemsType::setSpecialCustomsInformation()
     * @uses CustomItemsType::setDocumentItemNumber()
     * @uses CustomItemsType::setCountryOfOrigin()
     * @uses CustomItemsType::setCountryOfDespatch()
     * @uses CustomItemsType::setCountryOfDestination()
     * @uses CustomItemsType::setDocumentIssuingDate()
     * @param string $manifestNumber
     * @param string $documentCode
     * @param string $documentNumber
     * @param string $documentIssuingOffice
     * @param string $specialCustomsInformation
     * @param int $documentItemNumber
     * @param string $countryOfOrigin
     * @param string $countryOfDespatch
     * @param string $countryOfDestination
     * @param string $documentIssuingDate
     */
    public function __construct(?string $manifestNumber = null, ?string $documentCode = null, ?string $documentNumber = null, ?string $documentIssuingOffice = null, ?string $specialCustomsInformation = null, ?int $documentItemNumber = null, ?string $countryOfOrigin = null, ?string $countryOfDespatch = null, ?string $countryOfDestination = null, ?string $documentIssuingDate = null)
    {
        $this
            ->setManifestNumber($manifestNumber)
            ->setDocumentCode($documentCode)
            ->setDocumentNumber($documentNumber)
            ->setDocumentIssuingOffice($documentIssuingOffice)
            ->setSpecialCustomsInformation($specialCustomsInformation)
            ->setDocumentItemNumber($documentItemNumber)
            ->setCountryOfOrigin($countryOfOrigin)
            ->setCountryOfDespatch($countryOfDespatch)
            ->setCountryOfDestination($countryOfDestination)
            ->setDocumentIssuingDate($documentIssuingDate);
    }
    /**
     * Get manifestNumber value
     * @return string|null
     */
    public function getManifestNumber(): ?string
    {
        return $this->manifestNumber;
    }
    /**
     * Set manifestNumber value
     * @param string $manifestNumber
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setManifestNumber(?string $manifestNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($manifestNumber) && !is_string($manifestNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($manifestNumber, true), gettype($manifestNumber)), __LINE__);
        }
        $this->manifestNumber = $manifestNumber;
        
        return $this;
    }
    /**
     * Get documentCode value
     * @return string|null
     */
    public function getDocumentCode(): ?string
    {
        return $this->documentCode;
    }
    /**
     * Set documentCode value
     * @param string $documentCode
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setDocumentCode(?string $documentCode = null): self
    {
        // validation for constraint: string
        if (!is_null($documentCode) && !is_string($documentCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentCode, true), gettype($documentCode)), __LINE__);
        }
        $this->documentCode = $documentCode;
        
        return $this;
    }
    /**
     * Get documentNumber value
     * @return string|null
     */
    public function getDocumentNumber(): ?string
    {
        return $this->documentNumber;
    }
    /**
     * Set documentNumber value
     * @param string $documentNumber
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setDocumentNumber(?string $documentNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($documentNumber) && !is_string($documentNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentNumber, true), gettype($documentNumber)), __LINE__);
        }
        $this->documentNumber = $documentNumber;
        
        return $this;
    }
    /**
     * Get documentIssuingOffice value
     * @return string|null
     */
    public function getDocumentIssuingOffice(): ?string
    {
        return $this->documentIssuingOffice;
    }
    /**
     * Set documentIssuingOffice value
     * @param string $documentIssuingOffice
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setDocumentIssuingOffice(?string $documentIssuingOffice = null): self
    {
        // validation for constraint: string
        if (!is_null($documentIssuingOffice) && !is_string($documentIssuingOffice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentIssuingOffice, true), gettype($documentIssuingOffice)), __LINE__);
        }
        $this->documentIssuingOffice = $documentIssuingOffice;
        
        return $this;
    }
    /**
     * Get specialCustomsInformation value
     * @return string|null
     */
    public function getSpecialCustomsInformation(): ?string
    {
        return $this->specialCustomsInformation;
    }
    /**
     * Set specialCustomsInformation value
     * @param string $specialCustomsInformation
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setSpecialCustomsInformation(?string $specialCustomsInformation = null): self
    {
        // validation for constraint: string
        if (!is_null($specialCustomsInformation) && !is_string($specialCustomsInformation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specialCustomsInformation, true), gettype($specialCustomsInformation)), __LINE__);
        }
        $this->specialCustomsInformation = $specialCustomsInformation;
        
        return $this;
    }
    /**
     * Get documentItemNumber value
     * @return int|null
     */
    public function getDocumentItemNumber(): ?int
    {
        return $this->documentItemNumber;
    }
    /**
     * Set documentItemNumber value
     * @param int $documentItemNumber
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setDocumentItemNumber(?int $documentItemNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($documentItemNumber) && !(is_int($documentItemNumber) || ctype_digit($documentItemNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($documentItemNumber, true), gettype($documentItemNumber)), __LINE__);
        }
        $this->documentItemNumber = $documentItemNumber;
        
        return $this;
    }
    /**
     * Get countryOfOrigin value
     * @return string|null
     */
    public function getCountryOfOrigin(): ?string
    {
        return $this->countryOfOrigin;
    }
    /**
     * Set countryOfOrigin value
     * @param string $countryOfOrigin
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setCountryOfOrigin(?string $countryOfOrigin = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfOrigin) && !is_string($countryOfOrigin)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfOrigin, true), gettype($countryOfOrigin)), __LINE__);
        }
        $this->countryOfOrigin = $countryOfOrigin;
        
        return $this;
    }
    /**
     * Get countryOfDespatch value
     * @return string|null
     */
    public function getCountryOfDespatch(): ?string
    {
        return $this->countryOfDespatch;
    }
    /**
     * Set countryOfDespatch value
     * @param string $countryOfDespatch
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setCountryOfDespatch(?string $countryOfDespatch = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfDespatch) && !is_string($countryOfDespatch)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfDespatch, true), gettype($countryOfDespatch)), __LINE__);
        }
        $this->countryOfDespatch = $countryOfDespatch;
        
        return $this;
    }
    /**
     * Get countryOfDestination value
     * @return string|null
     */
    public function getCountryOfDestination(): ?string
    {
        return $this->countryOfDestination;
    }
    /**
     * Set countryOfDestination value
     * @param string $countryOfDestination
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setCountryOfDestination(?string $countryOfDestination = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfDestination) && !is_string($countryOfDestination)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfDestination, true), gettype($countryOfDestination)), __LINE__);
        }
        $this->countryOfDestination = $countryOfDestination;
        
        return $this;
    }
    /**
     * Get documentIssuingDate value
     * @return string|null
     */
    public function getDocumentIssuingDate(): ?string
    {
        return $this->documentIssuingDate;
    }
    /**
     * Set documentIssuingDate value
     * @param string $documentIssuingDate
     * @return \AppturePay\DSV\StructType\CustomItemsType
     */
    public function setDocumentIssuingDate(?string $documentIssuingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($documentIssuingDate) && !is_string($documentIssuingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentIssuingDate, true), gettype($documentIssuingDate)), __LINE__);
        }
        $this->documentIssuingDate = $documentIssuingDate;
        
        return $this;
    }
}
