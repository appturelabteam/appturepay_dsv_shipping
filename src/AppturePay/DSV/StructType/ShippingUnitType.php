<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShippingUnitType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ShippingUnitType
 * @subpackage Structs
 */
class ShippingUnitType extends AbstractStructBase
{
    /**
     * The containedInMSU
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $containedInMSU = null;
    /**
     * The externalUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalUnit = null;
    /**
     * The qualityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $qualityCode = null;
    /**
     * The dimension
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\UnitDimensionType|null
     */
    protected ?\AppturePay\DSV\StructType\UnitDimensionType $dimension = null;
    /**
     * The carrierReference
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CarrierReferenceType[]
     */
    protected ?array $carrierReference = null;
    /**
     * The shippingUnitDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShippingUnitDetailsType[]
     */
    protected ?array $shippingUnitDetails = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The shippingUnitNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $shippingUnitNumber = null;
    /**
     * The externalUnitField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $externalUnitField = null;
    /**
     * The shippingUnitNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $shippingUnitNumberField = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for ShippingUnitType
     * @uses ShippingUnitType::setContainedInMSU()
     * @uses ShippingUnitType::setExternalUnit()
     * @uses ShippingUnitType::setQualityCode()
     * @uses ShippingUnitType::setDimension()
     * @uses ShippingUnitType::setCarrierReference()
     * @uses ShippingUnitType::setShippingUnitDetails()
     * @uses ShippingUnitType::setType()
     * @uses ShippingUnitType::setShippingUnitNumber()
     * @uses ShippingUnitType::setExternalUnitField()
     * @uses ShippingUnitType::setShippingUnitNumberField()
     * @uses ShippingUnitType::setTypeField()
     * @param string $containedInMSU
     * @param string $externalUnit
     * @param string $qualityCode
     * @param \AppturePay\DSV\StructType\UnitDimensionType $dimension
     * @param \AppturePay\DSV\StructType\CarrierReferenceType[] $carrierReference
     * @param \AppturePay\DSV\StructType\ShippingUnitDetailsType[] $shippingUnitDetails
     * @param string $type
     * @param string $shippingUnitNumber
     * @param string $externalUnitField
     * @param string $shippingUnitNumberField
     * @param string $typeField
     */
    public function __construct(?string $containedInMSU = null, ?string $externalUnit = null, ?string $qualityCode = null, ?\AppturePay\DSV\StructType\UnitDimensionType $dimension = null, ?array $carrierReference = null, ?array $shippingUnitDetails = null, ?string $type = null, ?string $shippingUnitNumber = null, ?string $externalUnitField = null, ?string $shippingUnitNumberField = null, ?string $typeField = null)
    {
        $this
            ->setContainedInMSU($containedInMSU)
            ->setExternalUnit($externalUnit)
            ->setQualityCode($qualityCode)
            ->setDimension($dimension)
            ->setCarrierReference($carrierReference)
            ->setShippingUnitDetails($shippingUnitDetails)
            ->setType($type)
            ->setShippingUnitNumber($shippingUnitNumber)
            ->setExternalUnitField($externalUnitField)
            ->setShippingUnitNumberField($shippingUnitNumberField)
            ->setTypeField($typeField);
    }
    /**
     * Get containedInMSU value
     * @return string|null
     */
    public function getContainedInMSU(): ?string
    {
        return $this->containedInMSU;
    }
    /**
     * Set containedInMSU value
     * @param string $containedInMSU
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setContainedInMSU(?string $containedInMSU = null): self
    {
        // validation for constraint: string
        if (!is_null($containedInMSU) && !is_string($containedInMSU)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($containedInMSU, true), gettype($containedInMSU)), __LINE__);
        }
        $this->containedInMSU = $containedInMSU;
        
        return $this;
    }
    /**
     * Get externalUnit value
     * @return string|null
     */
    public function getExternalUnit(): ?string
    {
        return $this->externalUnit;
    }
    /**
     * Set externalUnit value
     * @param string $externalUnit
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setExternalUnit(?string $externalUnit = null): self
    {
        // validation for constraint: string
        if (!is_null($externalUnit) && !is_string($externalUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalUnit, true), gettype($externalUnit)), __LINE__);
        }
        $this->externalUnit = $externalUnit;
        
        return $this;
    }
    /**
     * Get qualityCode value
     * @return string|null
     */
    public function getQualityCode(): ?string
    {
        return $this->qualityCode;
    }
    /**
     * Set qualityCode value
     * @param string $qualityCode
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setQualityCode(?string $qualityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($qualityCode) && !is_string($qualityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($qualityCode, true), gettype($qualityCode)), __LINE__);
        }
        $this->qualityCode = $qualityCode;
        
        return $this;
    }
    /**
     * Get dimension value
     * @return \AppturePay\DSV\StructType\UnitDimensionType|null
     */
    public function getDimension(): ?\AppturePay\DSV\StructType\UnitDimensionType
    {
        return $this->dimension;
    }
    /**
     * Set dimension value
     * @param \AppturePay\DSV\StructType\UnitDimensionType $dimension
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setDimension(?\AppturePay\DSV\StructType\UnitDimensionType $dimension = null): self
    {
        $this->dimension = $dimension;
        
        return $this;
    }
    /**
     * Get carrierReference value
     * @return \AppturePay\DSV\StructType\CarrierReferenceType[]
     */
    public function getCarrierReference(): ?array
    {
        return $this->carrierReference;
    }
    /**
     * This method is responsible for validating the values passed to the setCarrierReference method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCarrierReference method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCarrierReferenceForArrayConstraintsFromSetCarrierReference(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $shippingUnitTypeCarrierReferenceItem) {
            // validation for constraint: itemType
            if (!$shippingUnitTypeCarrierReferenceItem instanceof \AppturePay\DSV\StructType\CarrierReferenceType) {
                $invalidValues[] = is_object($shippingUnitTypeCarrierReferenceItem) ? get_class($shippingUnitTypeCarrierReferenceItem) : sprintf('%s(%s)', gettype($shippingUnitTypeCarrierReferenceItem), var_export($shippingUnitTypeCarrierReferenceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The carrierReference property can only contain items of type \AppturePay\DSV\StructType\CarrierReferenceType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set carrierReference value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CarrierReferenceType[] $carrierReference
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setCarrierReference(?array $carrierReference = null): self
    {
        // validation for constraint: array
        if ('' !== ($carrierReferenceArrayErrorMessage = self::validateCarrierReferenceForArrayConstraintsFromSetCarrierReference($carrierReference))) {
            throw new InvalidArgumentException($carrierReferenceArrayErrorMessage, __LINE__);
        }
        $this->carrierReference = $carrierReference;
        
        return $this;
    }
    /**
     * Add item to carrierReference value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CarrierReferenceType $item
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function addToCarrierReference(\AppturePay\DSV\StructType\CarrierReferenceType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\CarrierReferenceType) {
            throw new InvalidArgumentException(sprintf('The carrierReference property can only contain items of type \AppturePay\DSV\StructType\CarrierReferenceType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->carrierReference[] = $item;
        
        return $this;
    }
    /**
     * Get shippingUnitDetails value
     * @return \AppturePay\DSV\StructType\ShippingUnitDetailsType[]
     */
    public function getShippingUnitDetails(): ?array
    {
        return $this->shippingUnitDetails;
    }
    /**
     * This method is responsible for validating the values passed to the setShippingUnitDetails method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShippingUnitDetails method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShippingUnitDetailsForArrayConstraintsFromSetShippingUnitDetails(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $shippingUnitTypeShippingUnitDetailsItem) {
            // validation for constraint: itemType
            if (!$shippingUnitTypeShippingUnitDetailsItem instanceof \AppturePay\DSV\StructType\ShippingUnitDetailsType) {
                $invalidValues[] = is_object($shippingUnitTypeShippingUnitDetailsItem) ? get_class($shippingUnitTypeShippingUnitDetailsItem) : sprintf('%s(%s)', gettype($shippingUnitTypeShippingUnitDetailsItem), var_export($shippingUnitTypeShippingUnitDetailsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shippingUnitDetails property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitDetailsType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set shippingUnitDetails value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitDetailsType[] $shippingUnitDetails
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setShippingUnitDetails(?array $shippingUnitDetails = null): self
    {
        // validation for constraint: array
        if ('' !== ($shippingUnitDetailsArrayErrorMessage = self::validateShippingUnitDetailsForArrayConstraintsFromSetShippingUnitDetails($shippingUnitDetails))) {
            throw new InvalidArgumentException($shippingUnitDetailsArrayErrorMessage, __LINE__);
        }
        $this->shippingUnitDetails = $shippingUnitDetails;
        
        return $this;
    }
    /**
     * Add item to shippingUnitDetails value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitDetailsType $item
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function addToShippingUnitDetails(\AppturePay\DSV\StructType\ShippingUnitDetailsType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ShippingUnitDetailsType) {
            throw new InvalidArgumentException(sprintf('The shippingUnitDetails property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitDetailsType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shippingUnitDetails[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get shippingUnitNumber value
     * @return string|null
     */
    public function getShippingUnitNumber(): ?string
    {
        return $this->shippingUnitNumber;
    }
    /**
     * Set shippingUnitNumber value
     * @param string $shippingUnitNumber
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setShippingUnitNumber(?string $shippingUnitNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($shippingUnitNumber) && !is_string($shippingUnitNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingUnitNumber, true), gettype($shippingUnitNumber)), __LINE__);
        }
        $this->shippingUnitNumber = $shippingUnitNumber;
        
        return $this;
    }
    /**
     * Get externalUnitField value
     * @return string|null
     */
    public function getExternalUnitField(): ?string
    {
        return $this->externalUnitField;
    }
    /**
     * Set externalUnitField value
     * @param string $externalUnitField
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setExternalUnitField(?string $externalUnitField = null): self
    {
        // validation for constraint: string
        if (!is_null($externalUnitField) && !is_string($externalUnitField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalUnitField, true), gettype($externalUnitField)), __LINE__);
        }
        $this->externalUnitField = $externalUnitField;
        
        return $this;
    }
    /**
     * Get shippingUnitNumberField value
     * @return string|null
     */
    public function getShippingUnitNumberField(): ?string
    {
        return $this->shippingUnitNumberField;
    }
    /**
     * Set shippingUnitNumberField value
     * @param string $shippingUnitNumberField
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setShippingUnitNumberField(?string $shippingUnitNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($shippingUnitNumberField) && !is_string($shippingUnitNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingUnitNumberField, true), gettype($shippingUnitNumberField)), __LINE__);
        }
        $this->shippingUnitNumberField = $shippingUnitNumberField;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\ShippingUnitType
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
