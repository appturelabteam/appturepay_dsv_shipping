<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for VatType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:VatType
 * @subpackage Structs
 */
class VatType_1 extends AbstractStructBase
{
    /**
     * The countryCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryCode = null;
    /**
     * The typeOfVat
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $typeOfVat = null;
    /**
     * The vatCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $vatCode = null;
    /**
     * The vatAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $vatAmount = null;
    /**
     * The countryCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $countryCodeField = null;
    /**
     * The typeOfVatField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeOfVatField = null;
    /**
     * The vatAmountField
     * @var float|null
     */
    protected ?float $vatAmountField = null;
    /**
     * The vatAmountFieldSpecified
     * @var bool|null
     */
    protected ?bool $vatAmountFieldSpecified = null;
    /**
     * The vatCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $vatCodeField = null;
    /**
     * Constructor method for VatType
     * @uses VatType_1::setCountryCode()
     * @uses VatType_1::setTypeOfVat()
     * @uses VatType_1::setVatCode()
     * @uses VatType_1::setVatAmount()
     * @uses VatType_1::setCountryCodeField()
     * @uses VatType_1::setTypeOfVatField()
     * @uses VatType_1::setVatAmountField()
     * @uses VatType_1::setVatAmountFieldSpecified()
     * @uses VatType_1::setVatCodeField()
     * @param string $countryCode
     * @param int $typeOfVat
     * @param int $vatCode
     * @param float $vatAmount
     * @param string $countryCodeField
     * @param string $typeOfVatField
     * @param float $vatAmountField
     * @param bool $vatAmountFieldSpecified
     * @param string $vatCodeField
     */
    public function __construct(?string $countryCode = null, ?int $typeOfVat = null, ?int $vatCode = null, ?float $vatAmount = null, ?string $countryCodeField = null, ?string $typeOfVatField = null, ?float $vatAmountField = null, ?bool $vatAmountFieldSpecified = null, ?string $vatCodeField = null)
    {
        $this
            ->setCountryCode($countryCode)
            ->setTypeOfVat($typeOfVat)
            ->setVatCode($vatCode)
            ->setVatAmount($vatAmount)
            ->setCountryCodeField($countryCodeField)
            ->setTypeOfVatField($typeOfVatField)
            ->setVatAmountField($vatAmountField)
            ->setVatAmountFieldSpecified($vatAmountFieldSpecified)
            ->setVatCodeField($vatCodeField);
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setCountryCode(?string $countryCode = null): self
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        
        return $this;
    }
    /**
     * Get typeOfVat value
     * @return int|null
     */
    public function getTypeOfVat(): ?int
    {
        return $this->typeOfVat;
    }
    /**
     * Set typeOfVat value
     * @param int $typeOfVat
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setTypeOfVat(?int $typeOfVat = null): self
    {
        // validation for constraint: int
        if (!is_null($typeOfVat) && !(is_int($typeOfVat) || ctype_digit($typeOfVat))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($typeOfVat, true), gettype($typeOfVat)), __LINE__);
        }
        $this->typeOfVat = $typeOfVat;
        
        return $this;
    }
    /**
     * Get vatCode value
     * @return int|null
     */
    public function getVatCode(): ?int
    {
        return $this->vatCode;
    }
    /**
     * Set vatCode value
     * @param int $vatCode
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setVatCode(?int $vatCode = null): self
    {
        // validation for constraint: int
        if (!is_null($vatCode) && !(is_int($vatCode) || ctype_digit($vatCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($vatCode, true), gettype($vatCode)), __LINE__);
        }
        $this->vatCode = $vatCode;
        
        return $this;
    }
    /**
     * Get vatAmount value
     * @return float|null
     */
    public function getVatAmount(): ?float
    {
        return $this->vatAmount;
    }
    /**
     * Set vatAmount value
     * @param float $vatAmount
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setVatAmount(?float $vatAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($vatAmount) && !(is_float($vatAmount) || is_numeric($vatAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vatAmount, true), gettype($vatAmount)), __LINE__);
        }
        $this->vatAmount = $vatAmount;
        
        return $this;
    }
    /**
     * Get countryCodeField value
     * @return string|null
     */
    public function getCountryCodeField(): ?string
    {
        return $this->countryCodeField;
    }
    /**
     * Set countryCodeField value
     * @param string $countryCodeField
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setCountryCodeField(?string $countryCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($countryCodeField) && !is_string($countryCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCodeField, true), gettype($countryCodeField)), __LINE__);
        }
        $this->countryCodeField = $countryCodeField;
        
        return $this;
    }
    /**
     * Get typeOfVatField value
     * @return string|null
     */
    public function getTypeOfVatField(): ?string
    {
        return $this->typeOfVatField;
    }
    /**
     * Set typeOfVatField value
     * @param string $typeOfVatField
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setTypeOfVatField(?string $typeOfVatField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeOfVatField) && !is_string($typeOfVatField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeOfVatField, true), gettype($typeOfVatField)), __LINE__);
        }
        $this->typeOfVatField = $typeOfVatField;
        
        return $this;
    }
    /**
     * Get vatAmountField value
     * @return float|null
     */
    public function getVatAmountField(): ?float
    {
        return $this->vatAmountField;
    }
    /**
     * Set vatAmountField value
     * @param float $vatAmountField
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setVatAmountField(?float $vatAmountField = null): self
    {
        // validation for constraint: float
        if (!is_null($vatAmountField) && !(is_float($vatAmountField) || is_numeric($vatAmountField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vatAmountField, true), gettype($vatAmountField)), __LINE__);
        }
        $this->vatAmountField = $vatAmountField;
        
        return $this;
    }
    /**
     * Get vatAmountFieldSpecified value
     * @return bool|null
     */
    public function getVatAmountFieldSpecified(): ?bool
    {
        return $this->vatAmountFieldSpecified;
    }
    /**
     * Set vatAmountFieldSpecified value
     * @param bool $vatAmountFieldSpecified
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setVatAmountFieldSpecified(?bool $vatAmountFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($vatAmountFieldSpecified) && !is_bool($vatAmountFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($vatAmountFieldSpecified, true), gettype($vatAmountFieldSpecified)), __LINE__);
        }
        $this->vatAmountFieldSpecified = $vatAmountFieldSpecified;
        
        return $this;
    }
    /**
     * Get vatCodeField value
     * @return string|null
     */
    public function getVatCodeField(): ?string
    {
        return $this->vatCodeField;
    }
    /**
     * Set vatCodeField value
     * @param string $vatCodeField
     * @return \AppturePay\DSV\StructType\VatType_1
     */
    public function setVatCodeField(?string $vatCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($vatCodeField) && !is_string($vatCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vatCodeField, true), gettype($vatCodeField)), __LINE__);
        }
        $this->vatCodeField = $vatCodeField;
        
        return $this;
    }
}
