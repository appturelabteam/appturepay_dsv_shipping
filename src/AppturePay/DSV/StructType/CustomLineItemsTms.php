<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CustomLineItemsTms StructType
 * @subpackage Structs
 */
class CustomLineItemsTms extends AbstractStructBase
{
    /**
     * The taricCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $taricCode = null;
    /**
     * The documentCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentCode = null;
    /**
     * The documentNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentNumber = null;
    /**
     * The documentIssuingOffice
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentIssuingOffice = null;
    /**
     * The countryOfOrigin
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $countryOfOrigin = null;
    /**
     * The documentIssuingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentIssuingDate = null;
    /**
     * The statisticalQuantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $statisticalQuantity = null;
    /**
     * Constructor method for CustomLineItemsTms
     * @uses CustomLineItemsTms::setTaricCode()
     * @uses CustomLineItemsTms::setDocumentCode()
     * @uses CustomLineItemsTms::setDocumentNumber()
     * @uses CustomLineItemsTms::setDocumentIssuingOffice()
     * @uses CustomLineItemsTms::setCountryOfOrigin()
     * @uses CustomLineItemsTms::setDocumentIssuingDate()
     * @uses CustomLineItemsTms::setStatisticalQuantity()
     * @param string $taricCode
     * @param string $documentCode
     * @param string $documentNumber
     * @param string $documentIssuingOffice
     * @param string $countryOfOrigin
     * @param string $documentIssuingDate
     * @param int $statisticalQuantity
     */
    public function __construct(?string $taricCode = null, ?string $documentCode = null, ?string $documentNumber = null, ?string $documentIssuingOffice = null, ?string $countryOfOrigin = null, ?string $documentIssuingDate = null, ?int $statisticalQuantity = null)
    {
        $this
            ->setTaricCode($taricCode)
            ->setDocumentCode($documentCode)
            ->setDocumentNumber($documentNumber)
            ->setDocumentIssuingOffice($documentIssuingOffice)
            ->setCountryOfOrigin($countryOfOrigin)
            ->setDocumentIssuingDate($documentIssuingDate)
            ->setStatisticalQuantity($statisticalQuantity);
    }
    /**
     * Get taricCode value
     * @return string|null
     */
    public function getTaricCode(): ?string
    {
        return $this->taricCode;
    }
    /**
     * Set taricCode value
     * @param string $taricCode
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setTaricCode(?string $taricCode = null): self
    {
        // validation for constraint: string
        if (!is_null($taricCode) && !is_string($taricCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($taricCode, true), gettype($taricCode)), __LINE__);
        }
        $this->taricCode = $taricCode;
        
        return $this;
    }
    /**
     * Get documentCode value
     * @return string|null
     */
    public function getDocumentCode(): ?string
    {
        return $this->documentCode;
    }
    /**
     * Set documentCode value
     * @param string $documentCode
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setDocumentCode(?string $documentCode = null): self
    {
        // validation for constraint: string
        if (!is_null($documentCode) && !is_string($documentCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentCode, true), gettype($documentCode)), __LINE__);
        }
        $this->documentCode = $documentCode;
        
        return $this;
    }
    /**
     * Get documentNumber value
     * @return string|null
     */
    public function getDocumentNumber(): ?string
    {
        return $this->documentNumber;
    }
    /**
     * Set documentNumber value
     * @param string $documentNumber
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setDocumentNumber(?string $documentNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($documentNumber) && !is_string($documentNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentNumber, true), gettype($documentNumber)), __LINE__);
        }
        $this->documentNumber = $documentNumber;
        
        return $this;
    }
    /**
     * Get documentIssuingOffice value
     * @return string|null
     */
    public function getDocumentIssuingOffice(): ?string
    {
        return $this->documentIssuingOffice;
    }
    /**
     * Set documentIssuingOffice value
     * @param string $documentIssuingOffice
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setDocumentIssuingOffice(?string $documentIssuingOffice = null): self
    {
        // validation for constraint: string
        if (!is_null($documentIssuingOffice) && !is_string($documentIssuingOffice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentIssuingOffice, true), gettype($documentIssuingOffice)), __LINE__);
        }
        $this->documentIssuingOffice = $documentIssuingOffice;
        
        return $this;
    }
    /**
     * Get countryOfOrigin value
     * @return string|null
     */
    public function getCountryOfOrigin(): ?string
    {
        return $this->countryOfOrigin;
    }
    /**
     * Set countryOfOrigin value
     * @param string $countryOfOrigin
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setCountryOfOrigin(?string $countryOfOrigin = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfOrigin) && !is_string($countryOfOrigin)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfOrigin, true), gettype($countryOfOrigin)), __LINE__);
        }
        $this->countryOfOrigin = $countryOfOrigin;
        
        return $this;
    }
    /**
     * Get documentIssuingDate value
     * @return string|null
     */
    public function getDocumentIssuingDate(): ?string
    {
        return $this->documentIssuingDate;
    }
    /**
     * Set documentIssuingDate value
     * @param string $documentIssuingDate
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setDocumentIssuingDate(?string $documentIssuingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($documentIssuingDate) && !is_string($documentIssuingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentIssuingDate, true), gettype($documentIssuingDate)), __LINE__);
        }
        $this->documentIssuingDate = $documentIssuingDate;
        
        return $this;
    }
    /**
     * Get statisticalQuantity value
     * @return int|null
     */
    public function getStatisticalQuantity(): ?int
    {
        return $this->statisticalQuantity;
    }
    /**
     * Set statisticalQuantity value
     * @param int $statisticalQuantity
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms
     */
    public function setStatisticalQuantity(?int $statisticalQuantity = null): self
    {
        // validation for constraint: int
        if (!is_null($statisticalQuantity) && !(is_int($statisticalQuantity) || ctype_digit($statisticalQuantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($statisticalQuantity, true), gettype($statisticalQuantity)), __LINE__);
        }
        $this->statisticalQuantity = $statisticalQuantity;
        
        return $this;
    }
}
