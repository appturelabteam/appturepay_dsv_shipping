<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RetrieveShipmentLabelsTMS StructType
 * @subpackage Structs
 */
class RetrieveShipmentLabelsTMS extends AbstractStructBase
{
    /**
     * The labelRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest|null
     */
    protected ?\AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest $labelRequest = null;
    /**
     * Constructor method for RetrieveShipmentLabelsTMS
     * @uses RetrieveShipmentLabelsTMS::setLabelRequest()
     * @param \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest $labelRequest
     */
    public function __construct(?\AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest $labelRequest = null)
    {
        $this
            ->setLabelRequest($labelRequest);
    }
    /**
     * Get labelRequest value
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest|null
     */
    public function getLabelRequest(): ?\AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest
    {
        return $this->labelRequest;
    }
    /**
     * Set labelRequest value
     * @param \AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest $labelRequest
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMS
     */
    public function setLabelRequest(?\AppturePay\DSV\StructType\RetrieveShipmentLabelsRequest $labelRequest = null): self
    {
        $this->labelRequest = $labelRequest;
        
        return $this;
    }
}
