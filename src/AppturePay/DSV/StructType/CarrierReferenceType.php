<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CarrierReferenceType StructType
 * @subpackage Structs
 */
class CarrierReferenceType extends AbstractStructBase
{
    /**
     * The carrier
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CarrierType|null
     */
    protected ?\AppturePay\DSV\StructType\CarrierType $carrier = null;
    /**
     * The externalUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalUnit = null;
    /**
     * The checkMethod
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $checkMethod = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for CarrierReferenceType
     * @uses CarrierReferenceType::setCarrier()
     * @uses CarrierReferenceType::setExternalUnit()
     * @uses CarrierReferenceType::setCheckMethod()
     * @uses CarrierReferenceType::setType()
     * @param \AppturePay\DSV\StructType\CarrierType $carrier
     * @param string $externalUnit
     * @param int $checkMethod
     * @param string $type
     */
    public function __construct(?\AppturePay\DSV\StructType\CarrierType $carrier = null, ?string $externalUnit = null, ?int $checkMethod = null, ?string $type = null)
    {
        $this
            ->setCarrier($carrier)
            ->setExternalUnit($externalUnit)
            ->setCheckMethod($checkMethod)
            ->setType($type);
    }
    /**
     * Get carrier value
     * @return \AppturePay\DSV\StructType\CarrierType|null
     */
    public function getCarrier(): ?\AppturePay\DSV\StructType\CarrierType
    {
        return $this->carrier;
    }
    /**
     * Set carrier value
     * @param \AppturePay\DSV\StructType\CarrierType $carrier
     * @return \AppturePay\DSV\StructType\CarrierReferenceType
     */
    public function setCarrier(?\AppturePay\DSV\StructType\CarrierType $carrier = null): self
    {
        $this->carrier = $carrier;
        
        return $this;
    }
    /**
     * Get externalUnit value
     * @return string|null
     */
    public function getExternalUnit(): ?string
    {
        return $this->externalUnit;
    }
    /**
     * Set externalUnit value
     * @param string $externalUnit
     * @return \AppturePay\DSV\StructType\CarrierReferenceType
     */
    public function setExternalUnit(?string $externalUnit = null): self
    {
        // validation for constraint: string
        if (!is_null($externalUnit) && !is_string($externalUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalUnit, true), gettype($externalUnit)), __LINE__);
        }
        $this->externalUnit = $externalUnit;
        
        return $this;
    }
    /**
     * Get checkMethod value
     * @return int|null
     */
    public function getCheckMethod(): ?int
    {
        return $this->checkMethod;
    }
    /**
     * Set checkMethod value
     * @param int $checkMethod
     * @return \AppturePay\DSV\StructType\CarrierReferenceType
     */
    public function setCheckMethod(?int $checkMethod = null): self
    {
        // validation for constraint: int
        if (!is_null($checkMethod) && !(is_int($checkMethod) || ctype_digit($checkMethod))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($checkMethod, true), gettype($checkMethod)), __LINE__);
        }
        $this->checkMethod = $checkMethod;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\CarrierReferenceType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
