<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TimeFramesType StructType
 * @subpackage Structs
 */
class TimeFramesType extends AbstractStructBase
{
    /**
     * The fromTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromTime = null;
    /**
     * The tillTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tillTime = null;
    /**
     * The tailorFillerA10
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tailorFillerA10 = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for TimeFramesType
     * @uses TimeFramesType::setFromTime()
     * @uses TimeFramesType::setTillTime()
     * @uses TimeFramesType::setTailorFillerA10()
     * @uses TimeFramesType::setType()
     * @param string $fromTime
     * @param string $tillTime
     * @param string $tailorFillerA10
     * @param string $type
     */
    public function __construct(?string $fromTime = null, ?string $tillTime = null, ?string $tailorFillerA10 = null, ?string $type = null)
    {
        $this
            ->setFromTime($fromTime)
            ->setTillTime($tillTime)
            ->setTailorFillerA10($tailorFillerA10)
            ->setType($type);
    }
    /**
     * Get fromTime value
     * @return string|null
     */
    public function getFromTime(): ?string
    {
        return $this->fromTime;
    }
    /**
     * Set fromTime value
     * @param string $fromTime
     * @return \AppturePay\DSV\StructType\TimeFramesType
     */
    public function setFromTime(?string $fromTime = null): self
    {
        // validation for constraint: string
        if (!is_null($fromTime) && !is_string($fromTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromTime, true), gettype($fromTime)), __LINE__);
        }
        $this->fromTime = $fromTime;
        
        return $this;
    }
    /**
     * Get tillTime value
     * @return string|null
     */
    public function getTillTime(): ?string
    {
        return $this->tillTime;
    }
    /**
     * Set tillTime value
     * @param string $tillTime
     * @return \AppturePay\DSV\StructType\TimeFramesType
     */
    public function setTillTime(?string $tillTime = null): self
    {
        // validation for constraint: string
        if (!is_null($tillTime) && !is_string($tillTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tillTime, true), gettype($tillTime)), __LINE__);
        }
        $this->tillTime = $tillTime;
        
        return $this;
    }
    /**
     * Get tailorFillerA10 value
     * @return string|null
     */
    public function getTailorFillerA10(): ?string
    {
        return $this->tailorFillerA10;
    }
    /**
     * Set tailorFillerA10 value
     * @param string $tailorFillerA10
     * @return \AppturePay\DSV\StructType\TimeFramesType
     */
    public function setTailorFillerA10(?string $tailorFillerA10 = null): self
    {
        // validation for constraint: string
        if (!is_null($tailorFillerA10) && !is_string($tailorFillerA10)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tailorFillerA10, true), gettype($tailorFillerA10)), __LINE__);
        }
        $this->tailorFillerA10 = $tailorFillerA10;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TimeFramesType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
