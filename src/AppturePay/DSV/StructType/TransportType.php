<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TransportType StructType
 * @subpackage Structs
 */
class TransportType extends AbstractStructBase
{
    /**
     * The deliveryTermAgent
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $deliveryTermAgent = null;
    /**
     * The domesticTransport
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $domesticTransport = null;
    /**
     * The domesticTransportAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $domesticTransportAmount = null;
    /**
     * The CollectiveCMR
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $CollectiveCMR = null;
    /**
     * The distanceManually
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $distanceManually = null;
    /**
     * The distance
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $distance = null;
    /**
     * The containerPlanning
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $containerPlanning = null;
    /**
     * Constructor method for TransportType
     * @uses TransportType::setDeliveryTermAgent()
     * @uses TransportType::setDomesticTransport()
     * @uses TransportType::setDomesticTransportAmount()
     * @uses TransportType::setCollectiveCMR()
     * @uses TransportType::setDistanceManually()
     * @uses TransportType::setDistance()
     * @uses TransportType::setContainerPlanning()
     * @param string $deliveryTermAgent
     * @param string $domesticTransport
     * @param float $domesticTransportAmount
     * @param string $collectiveCMR
     * @param string $distanceManually
     * @param int $distance
     * @param string $containerPlanning
     */
    public function __construct(?string $deliveryTermAgent = null, ?string $domesticTransport = null, ?float $domesticTransportAmount = null, ?string $collectiveCMR = null, ?string $distanceManually = null, ?int $distance = null, ?string $containerPlanning = null)
    {
        $this
            ->setDeliveryTermAgent($deliveryTermAgent)
            ->setDomesticTransport($domesticTransport)
            ->setDomesticTransportAmount($domesticTransportAmount)
            ->setCollectiveCMR($collectiveCMR)
            ->setDistanceManually($distanceManually)
            ->setDistance($distance)
            ->setContainerPlanning($containerPlanning);
    }
    /**
     * Get deliveryTermAgent value
     * @return string|null
     */
    public function getDeliveryTermAgent(): ?string
    {
        return $this->deliveryTermAgent;
    }
    /**
     * Set deliveryTermAgent value
     * @param string $deliveryTermAgent
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setDeliveryTermAgent(?string $deliveryTermAgent = null): self
    {
        // validation for constraint: string
        if (!is_null($deliveryTermAgent) && !is_string($deliveryTermAgent)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryTermAgent, true), gettype($deliveryTermAgent)), __LINE__);
        }
        $this->deliveryTermAgent = $deliveryTermAgent;
        
        return $this;
    }
    /**
     * Get domesticTransport value
     * @return string|null
     */
    public function getDomesticTransport(): ?string
    {
        return $this->domesticTransport;
    }
    /**
     * Set domesticTransport value
     * @uses \AppturePay\DSV\EnumType\DomesticTransportType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\DomesticTransportType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $domesticTransport
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setDomesticTransport(?string $domesticTransport = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\DomesticTransportType::valueIsValid($domesticTransport)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\DomesticTransportType', is_array($domesticTransport) ? implode(', ', $domesticTransport) : var_export($domesticTransport, true), implode(', ', \AppturePay\DSV\EnumType\DomesticTransportType::getValidValues())), __LINE__);
        }
        $this->domesticTransport = $domesticTransport;
        
        return $this;
    }
    /**
     * Get domesticTransportAmount value
     * @return float|null
     */
    public function getDomesticTransportAmount(): ?float
    {
        return $this->domesticTransportAmount;
    }
    /**
     * Set domesticTransportAmount value
     * @param float $domesticTransportAmount
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setDomesticTransportAmount(?float $domesticTransportAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($domesticTransportAmount) && !(is_float($domesticTransportAmount) || is_numeric($domesticTransportAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($domesticTransportAmount, true), gettype($domesticTransportAmount)), __LINE__);
        }
        $this->domesticTransportAmount = $domesticTransportAmount;
        
        return $this;
    }
    /**
     * Get CollectiveCMR value
     * @return string|null
     */
    public function getCollectiveCMR(): ?string
    {
        return $this->CollectiveCMR;
    }
    /**
     * Set CollectiveCMR value
     * @param string $collectiveCMR
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setCollectiveCMR(?string $collectiveCMR = null): self
    {
        // validation for constraint: string
        if (!is_null($collectiveCMR) && !is_string($collectiveCMR)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($collectiveCMR, true), gettype($collectiveCMR)), __LINE__);
        }
        $this->CollectiveCMR = $collectiveCMR;
        
        return $this;
    }
    /**
     * Get distanceManually value
     * @return string|null
     */
    public function getDistanceManually(): ?string
    {
        return $this->distanceManually;
    }
    /**
     * Set distanceManually value
     * @param string $distanceManually
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setDistanceManually(?string $distanceManually = null): self
    {
        // validation for constraint: string
        if (!is_null($distanceManually) && !is_string($distanceManually)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($distanceManually, true), gettype($distanceManually)), __LINE__);
        }
        $this->distanceManually = $distanceManually;
        
        return $this;
    }
    /**
     * Get distance value
     * @return int|null
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }
    /**
     * Set distance value
     * @param int $distance
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setDistance(?int $distance = null): self
    {
        // validation for constraint: int
        if (!is_null($distance) && !(is_int($distance) || ctype_digit($distance))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($distance, true), gettype($distance)), __LINE__);
        }
        $this->distance = $distance;
        
        return $this;
    }
    /**
     * Get containerPlanning value
     * @return string|null
     */
    public function getContainerPlanning(): ?string
    {
        return $this->containerPlanning;
    }
    /**
     * Set containerPlanning value
     * @param string $containerPlanning
     * @return \AppturePay\DSV\StructType\TransportType
     */
    public function setContainerPlanning(?string $containerPlanning = null): self
    {
        // validation for constraint: string
        if (!is_null($containerPlanning) && !is_string($containerPlanning)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($containerPlanning, true), gettype($containerPlanning)), __LINE__);
        }
        $this->containerPlanning = $containerPlanning;
        
        return $this;
    }
}
