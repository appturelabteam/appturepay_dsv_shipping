<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ServiceMessage StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ServiceMessage
 * @subpackage Structs
 */
class ServiceMessage extends AbstractStructBase
{
    /**
     * The ErrorCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var string
     */
    protected string $ErrorCode;
    /**
     * The version
     * Meta information extracted from the WSDL
     * - use: required
     * @var float
     */
    protected float $version;
    /**
     * The DateTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DateTime = null;
    /**
     * The MasterId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $MasterId = null;
    /**
     * The ServiceName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ServiceName = null;
    /**
     * The OperationName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $OperationName = null;
    /**
     * The ConnectionInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ConnectionInfo|null
     */
    protected ?\AppturePay\DSV\StructType\ConnectionInfo $ConnectionInfo = null;
    /**
     * The ErrorText
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ErrorText = null;
    /**
     * The RootCause
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $RootCause = null;
    /**
     * The connectionInfoField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ServiceMessageConnectionInfo|null
     */
    protected ?\AppturePay\DSV\StructType\ServiceMessageConnectionInfo $connectionInfoField = null;
    /**
     * The dateTimeField
     * @var string|null
     */
    protected ?string $dateTimeField = null;
    /**
     * The errorCodeField
     * @var string|null
     */
    protected ?string $errorCodeField = null;
    /**
     * The errorTextField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $errorTextField = null;
    /**
     * The masterIdField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $masterIdField = null;
    /**
     * The operationNameField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $operationNameField = null;
    /**
     * The rootCauseField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $rootCauseField = null;
    /**
     * The serviceNameField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $serviceNameField = null;
    /**
     * The versionField
     * @var float|null
     */
    protected ?float $versionField = null;
    /**
     * Constructor method for ServiceMessage
     * @uses ServiceMessage::setErrorCode()
     * @uses ServiceMessage::setVersion()
     * @uses ServiceMessage::setDateTime()
     * @uses ServiceMessage::setMasterId()
     * @uses ServiceMessage::setServiceName()
     * @uses ServiceMessage::setOperationName()
     * @uses ServiceMessage::setConnectionInfo()
     * @uses ServiceMessage::setErrorText()
     * @uses ServiceMessage::setRootCause()
     * @uses ServiceMessage::setConnectionInfoField()
     * @uses ServiceMessage::setDateTimeField()
     * @uses ServiceMessage::setErrorCodeField()
     * @uses ServiceMessage::setErrorTextField()
     * @uses ServiceMessage::setMasterIdField()
     * @uses ServiceMessage::setOperationNameField()
     * @uses ServiceMessage::setRootCauseField()
     * @uses ServiceMessage::setServiceNameField()
     * @uses ServiceMessage::setVersionField()
     * @param string $errorCode
     * @param float $version
     * @param string $dateTime
     * @param string $masterId
     * @param string $serviceName
     * @param string $operationName
     * @param \AppturePay\DSV\StructType\ConnectionInfo $connectionInfo
     * @param string $errorText
     * @param string $rootCause
     * @param \AppturePay\DSV\StructType\ServiceMessageConnectionInfo $connectionInfoField
     * @param string $dateTimeField
     * @param string $errorCodeField
     * @param string $errorTextField
     * @param string $masterIdField
     * @param string $operationNameField
     * @param string $rootCauseField
     * @param string $serviceNameField
     * @param float $versionField
     */
    public function __construct(string $errorCode, float $version, ?string $dateTime = null, ?string $masterId = null, ?string $serviceName = null, ?string $operationName = null, ?\AppturePay\DSV\StructType\ConnectionInfo $connectionInfo = null, ?string $errorText = null, ?string $rootCause = null, ?\AppturePay\DSV\StructType\ServiceMessageConnectionInfo $connectionInfoField = null, ?string $dateTimeField = null, ?string $errorCodeField = null, ?string $errorTextField = null, ?string $masterIdField = null, ?string $operationNameField = null, ?string $rootCauseField = null, ?string $serviceNameField = null, ?float $versionField = null)
    {
        $this
            ->setErrorCode($errorCode)
            ->setVersion($version)
            ->setDateTime($dateTime)
            ->setMasterId($masterId)
            ->setServiceName($serviceName)
            ->setOperationName($operationName)
            ->setConnectionInfo($connectionInfo)
            ->setErrorText($errorText)
            ->setRootCause($rootCause)
            ->setConnectionInfoField($connectionInfoField)
            ->setDateTimeField($dateTimeField)
            ->setErrorCodeField($errorCodeField)
            ->setErrorTextField($errorTextField)
            ->setMasterIdField($masterIdField)
            ->setOperationNameField($operationNameField)
            ->setRootCauseField($rootCauseField)
            ->setServiceNameField($serviceNameField)
            ->setVersionField($versionField);
    }
    /**
     * Get ErrorCode value
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->ErrorCode;
    }
    /**
     * Set ErrorCode value
     * @param string $errorCode
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setErrorCode(string $errorCode): self
    {
        // validation for constraint: string
        if (!is_null($errorCode) && !is_string($errorCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorCode, true), gettype($errorCode)), __LINE__);
        }
        $this->ErrorCode = $errorCode;
        
        return $this;
    }
    /**
     * Get version value
     * @return float
     */
    public function getVersion(): float
    {
        return $this->version;
    }
    /**
     * Set version value
     * @param float $version
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setVersion(float $version): self
    {
        // validation for constraint: float
        if (!is_null($version) && !(is_float($version) || is_numeric($version))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        $this->version = $version;
        
        return $this;
    }
    /**
     * Get DateTime value
     * @return string|null
     */
    public function getDateTime(): ?string
    {
        return $this->DateTime;
    }
    /**
     * Set DateTime value
     * @param string $dateTime
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setDateTime(?string $dateTime = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTime) && !is_string($dateTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTime, true), gettype($dateTime)), __LINE__);
        }
        $this->DateTime = $dateTime;
        
        return $this;
    }
    /**
     * Get MasterId value
     * @return string|null
     */
    public function getMasterId(): ?string
    {
        return $this->MasterId;
    }
    /**
     * Set MasterId value
     * @param string $masterId
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setMasterId(?string $masterId = null): self
    {
        // validation for constraint: string
        if (!is_null($masterId) && !is_string($masterId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($masterId, true), gettype($masterId)), __LINE__);
        }
        $this->MasterId = $masterId;
        
        return $this;
    }
    /**
     * Get ServiceName value
     * @return string|null
     */
    public function getServiceName(): ?string
    {
        return $this->ServiceName;
    }
    /**
     * Set ServiceName value
     * @param string $serviceName
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setServiceName(?string $serviceName = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceName) && !is_string($serviceName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceName, true), gettype($serviceName)), __LINE__);
        }
        $this->ServiceName = $serviceName;
        
        return $this;
    }
    /**
     * Get OperationName value
     * @return string|null
     */
    public function getOperationName(): ?string
    {
        return $this->OperationName;
    }
    /**
     * Set OperationName value
     * @param string $operationName
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setOperationName(?string $operationName = null): self
    {
        // validation for constraint: string
        if (!is_null($operationName) && !is_string($operationName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($operationName, true), gettype($operationName)), __LINE__);
        }
        $this->OperationName = $operationName;
        
        return $this;
    }
    /**
     * Get ConnectionInfo value
     * @return \AppturePay\DSV\StructType\ConnectionInfo|null
     */
    public function getConnectionInfo(): ?\AppturePay\DSV\StructType\ConnectionInfo
    {
        return $this->ConnectionInfo;
    }
    /**
     * Set ConnectionInfo value
     * @param \AppturePay\DSV\StructType\ConnectionInfo $connectionInfo
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setConnectionInfo(?\AppturePay\DSV\StructType\ConnectionInfo $connectionInfo = null): self
    {
        $this->ConnectionInfo = $connectionInfo;
        
        return $this;
    }
    /**
     * Get ErrorText value
     * @return string|null
     */
    public function getErrorText(): ?string
    {
        return $this->ErrorText;
    }
    /**
     * Set ErrorText value
     * @param string $errorText
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setErrorText(?string $errorText = null): self
    {
        // validation for constraint: string
        if (!is_null($errorText) && !is_string($errorText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorText, true), gettype($errorText)), __LINE__);
        }
        $this->ErrorText = $errorText;
        
        return $this;
    }
    /**
     * Get RootCause value
     * @return string|null
     */
    public function getRootCause(): ?string
    {
        return $this->RootCause;
    }
    /**
     * Set RootCause value
     * @param string $rootCause
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setRootCause(?string $rootCause = null): self
    {
        // validation for constraint: string
        if (!is_null($rootCause) && !is_string($rootCause)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($rootCause, true), gettype($rootCause)), __LINE__);
        }
        $this->RootCause = $rootCause;
        
        return $this;
    }
    /**
     * Get connectionInfoField value
     * @return \AppturePay\DSV\StructType\ServiceMessageConnectionInfo|null
     */
    public function getConnectionInfoField(): ?\AppturePay\DSV\StructType\ServiceMessageConnectionInfo
    {
        return $this->connectionInfoField;
    }
    /**
     * Set connectionInfoField value
     * @param \AppturePay\DSV\StructType\ServiceMessageConnectionInfo $connectionInfoField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setConnectionInfoField(?\AppturePay\DSV\StructType\ServiceMessageConnectionInfo $connectionInfoField = null): self
    {
        $this->connectionInfoField = $connectionInfoField;
        
        return $this;
    }
    /**
     * Get dateTimeField value
     * @return string|null
     */
    public function getDateTimeField(): ?string
    {
        return $this->dateTimeField;
    }
    /**
     * Set dateTimeField value
     * @param string $dateTimeField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setDateTimeField(?string $dateTimeField = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeField) && !is_string($dateTimeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeField, true), gettype($dateTimeField)), __LINE__);
        }
        $this->dateTimeField = $dateTimeField;
        
        return $this;
    }
    /**
     * Get errorCodeField value
     * @return string|null
     */
    public function getErrorCodeField(): ?string
    {
        return $this->errorCodeField;
    }
    /**
     * Set errorCodeField value
     * @param string $errorCodeField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setErrorCodeField(?string $errorCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($errorCodeField) && !is_string($errorCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorCodeField, true), gettype($errorCodeField)), __LINE__);
        }
        $this->errorCodeField = $errorCodeField;
        
        return $this;
    }
    /**
     * Get errorTextField value
     * @return string|null
     */
    public function getErrorTextField(): ?string
    {
        return $this->errorTextField;
    }
    /**
     * Set errorTextField value
     * @param string $errorTextField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setErrorTextField(?string $errorTextField = null): self
    {
        // validation for constraint: string
        if (!is_null($errorTextField) && !is_string($errorTextField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorTextField, true), gettype($errorTextField)), __LINE__);
        }
        $this->errorTextField = $errorTextField;
        
        return $this;
    }
    /**
     * Get masterIdField value
     * @return string|null
     */
    public function getMasterIdField(): ?string
    {
        return $this->masterIdField;
    }
    /**
     * Set masterIdField value
     * @param string $masterIdField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setMasterIdField(?string $masterIdField = null): self
    {
        // validation for constraint: string
        if (!is_null($masterIdField) && !is_string($masterIdField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($masterIdField, true), gettype($masterIdField)), __LINE__);
        }
        $this->masterIdField = $masterIdField;
        
        return $this;
    }
    /**
     * Get operationNameField value
     * @return string|null
     */
    public function getOperationNameField(): ?string
    {
        return $this->operationNameField;
    }
    /**
     * Set operationNameField value
     * @param string $operationNameField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setOperationNameField(?string $operationNameField = null): self
    {
        // validation for constraint: string
        if (!is_null($operationNameField) && !is_string($operationNameField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($operationNameField, true), gettype($operationNameField)), __LINE__);
        }
        $this->operationNameField = $operationNameField;
        
        return $this;
    }
    /**
     * Get rootCauseField value
     * @return string|null
     */
    public function getRootCauseField(): ?string
    {
        return $this->rootCauseField;
    }
    /**
     * Set rootCauseField value
     * @param string $rootCauseField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setRootCauseField(?string $rootCauseField = null): self
    {
        // validation for constraint: string
        if (!is_null($rootCauseField) && !is_string($rootCauseField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($rootCauseField, true), gettype($rootCauseField)), __LINE__);
        }
        $this->rootCauseField = $rootCauseField;
        
        return $this;
    }
    /**
     * Get serviceNameField value
     * @return string|null
     */
    public function getServiceNameField(): ?string
    {
        return $this->serviceNameField;
    }
    /**
     * Set serviceNameField value
     * @param string $serviceNameField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setServiceNameField(?string $serviceNameField = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceNameField) && !is_string($serviceNameField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceNameField, true), gettype($serviceNameField)), __LINE__);
        }
        $this->serviceNameField = $serviceNameField;
        
        return $this;
    }
    /**
     * Get versionField value
     * @return float|null
     */
    public function getVersionField(): ?float
    {
        return $this->versionField;
    }
    /**
     * Set versionField value
     * @param float $versionField
     * @return \AppturePay\DSV\StructType\ServiceMessage
     */
    public function setVersionField(?float $versionField = null): self
    {
        // validation for constraint: float
        if (!is_null($versionField) && !(is_float($versionField) || is_numeric($versionField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($versionField, true), gettype($versionField)), __LINE__);
        }
        $this->versionField = $versionField;
        
        return $this;
    }
}
