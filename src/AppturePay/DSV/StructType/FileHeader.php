<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for fileHeader StructType
 * @subpackage Structs
 */
class FileHeader extends AbstractStructBase
{
    /**
     * The trackingAndTracing
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TrackingAndTracing|null
     */
    protected ?\AppturePay\DSV\StructType\TrackingAndTracing $trackingAndTracing = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for fileHeader
     * @uses FileHeader::setTrackingAndTracing()
     * @uses FileHeader::setType()
     * @param \AppturePay\DSV\StructType\TrackingAndTracing $trackingAndTracing
     * @param string $type
     */
    public function __construct(?\AppturePay\DSV\StructType\TrackingAndTracing $trackingAndTracing = null, ?string $type = null)
    {
        $this
            ->setTrackingAndTracing($trackingAndTracing)
            ->setType($type);
    }
    /**
     * Get trackingAndTracing value
     * @return \AppturePay\DSV\StructType\TrackingAndTracing|null
     */
    public function getTrackingAndTracing(): ?\AppturePay\DSV\StructType\TrackingAndTracing
    {
        return $this->trackingAndTracing;
    }
    /**
     * Set trackingAndTracing value
     * @param \AppturePay\DSV\StructType\TrackingAndTracing $trackingAndTracing
     * @return \AppturePay\DSV\StructType\FileHeader
     */
    public function setTrackingAndTracing(?\AppturePay\DSV\StructType\TrackingAndTracing $trackingAndTracing = null): self
    {
        $this->trackingAndTracing = $trackingAndTracing;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FileHeader
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
