<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ChildType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ChildType
 * @subpackage Structs
 */
class ChildType_1 extends AbstractStructBase
{
    /**
     * The ediReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediReference = null;
    /**
     * The referenceIndication
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceIndication = null;
    /**
     * The internalNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $internalNumber = null;
    /**
     * The ediFunction1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediFunction1 = null;
    /**
     * The logging
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\LoggingType_1[]
     */
    protected ?array $logging = null;
    /**
     * The file
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FileType_1|null
     */
    protected ?\AppturePay\DSV\StructType\FileType_1 $file = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The ediFunction1Field
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $ediFunction1Field = null;
    /**
     * The ediReferenceField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $ediReferenceField = null;
    /**
     * The fileField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\FileType_1|null
     */
    protected ?\AppturePay\DSV\StructType\FileType_1 $fileField = null;
    /**
     * The internalNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $internalNumberField = null;
    /**
     * The loggingField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfLoggingType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField = null;
    /**
     * The referenceIndicationField
     * @var string|null
     */
    protected ?string $referenceIndicationField = null;
    /**
     * The referenceIndicationFieldSpecified
     * @var bool|null
     */
    protected ?bool $referenceIndicationFieldSpecified = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for ChildType
     * @uses ChildType_1::setEdiReference()
     * @uses ChildType_1::setReferenceIndication()
     * @uses ChildType_1::setInternalNumber()
     * @uses ChildType_1::setEdiFunction1()
     * @uses ChildType_1::setLogging()
     * @uses ChildType_1::setFile()
     * @uses ChildType_1::setType()
     * @uses ChildType_1::setEdiFunction1Field()
     * @uses ChildType_1::setEdiReferenceField()
     * @uses ChildType_1::setFileField()
     * @uses ChildType_1::setInternalNumberField()
     * @uses ChildType_1::setLoggingField()
     * @uses ChildType_1::setReferenceIndicationField()
     * @uses ChildType_1::setReferenceIndicationFieldSpecified()
     * @uses ChildType_1::setTypeField()
     * @param string $ediReference
     * @param string $referenceIndication
     * @param int $internalNumber
     * @param string $ediFunction1
     * @param \AppturePay\DSV\StructType\LoggingType_1[] $logging
     * @param \AppturePay\DSV\StructType\FileType_1 $file
     * @param string $type
     * @param string $ediFunction1Field
     * @param string $ediReferenceField
     * @param \AppturePay\DSV\StructType\FileType_1 $fileField
     * @param string $internalNumberField
     * @param \AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField
     * @param string $referenceIndicationField
     * @param bool $referenceIndicationFieldSpecified
     * @param string $typeField
     */
    public function __construct(?string $ediReference = null, ?string $referenceIndication = null, ?int $internalNumber = null, ?string $ediFunction1 = null, ?array $logging = null, ?\AppturePay\DSV\StructType\FileType_1 $file = null, ?string $type = null, ?string $ediFunction1Field = null, ?string $ediReferenceField = null, ?\AppturePay\DSV\StructType\FileType_1 $fileField = null, ?string $internalNumberField = null, ?\AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField = null, ?string $referenceIndicationField = null, ?bool $referenceIndicationFieldSpecified = null, ?string $typeField = null)
    {
        $this
            ->setEdiReference($ediReference)
            ->setReferenceIndication($referenceIndication)
            ->setInternalNumber($internalNumber)
            ->setEdiFunction1($ediFunction1)
            ->setLogging($logging)
            ->setFile($file)
            ->setType($type)
            ->setEdiFunction1Field($ediFunction1Field)
            ->setEdiReferenceField($ediReferenceField)
            ->setFileField($fileField)
            ->setInternalNumberField($internalNumberField)
            ->setLoggingField($loggingField)
            ->setReferenceIndicationField($referenceIndicationField)
            ->setReferenceIndicationFieldSpecified($referenceIndicationFieldSpecified)
            ->setTypeField($typeField);
    }
    /**
     * Get ediReference value
     * @return string|null
     */
    public function getEdiReference(): ?string
    {
        return $this->ediReference;
    }
    /**
     * Set ediReference value
     * @param string $ediReference
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setEdiReference(?string $ediReference = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReference) && !is_string($ediReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReference, true), gettype($ediReference)), __LINE__);
        }
        $this->ediReference = $ediReference;
        
        return $this;
    }
    /**
     * Get referenceIndication value
     * @return string|null
     */
    public function getReferenceIndication(): ?string
    {
        return $this->referenceIndication;
    }
    /**
     * Set referenceIndication value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndication
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setReferenceIndication(?string $referenceIndication = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid($referenceIndication)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType_1', is_array($referenceIndication) ? implode(', ', $referenceIndication) : var_export($referenceIndication, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues())), __LINE__);
        }
        $this->referenceIndication = $referenceIndication;
        
        return $this;
    }
    /**
     * Get internalNumber value
     * @return int|null
     */
    public function getInternalNumber(): ?int
    {
        return $this->internalNumber;
    }
    /**
     * Set internalNumber value
     * @param int $internalNumber
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setInternalNumber(?int $internalNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($internalNumber) && !(is_int($internalNumber) || ctype_digit($internalNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($internalNumber, true), gettype($internalNumber)), __LINE__);
        }
        $this->internalNumber = $internalNumber;
        
        return $this;
    }
    /**
     * Get ediFunction1 value
     * @return string|null
     */
    public function getEdiFunction1(): ?string
    {
        return $this->ediFunction1;
    }
    /**
     * Set ediFunction1 value
     * @param string $ediFunction1
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setEdiFunction1(?string $ediFunction1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1) && !is_string($ediFunction1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1, true), gettype($ediFunction1)), __LINE__);
        }
        $this->ediFunction1 = $ediFunction1;
        
        return $this;
    }
    /**
     * Get logging value
     * @return \AppturePay\DSV\StructType\LoggingType_1[]
     */
    public function getLogging(): ?array
    {
        return $this->logging;
    }
    /**
     * This method is responsible for validating the values passed to the setLogging method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLogging method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLoggingForArrayConstraintsFromSetLogging(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $childTypeLoggingItem) {
            // validation for constraint: itemType
            if (!$childTypeLoggingItem instanceof \AppturePay\DSV\StructType\LoggingType_1) {
                $invalidValues[] = is_object($childTypeLoggingItem) ? get_class($childTypeLoggingItem) : sprintf('%s(%s)', gettype($childTypeLoggingItem), var_export($childTypeLoggingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType_1[] $logging
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setLogging(?array $logging = null): self
    {
        // validation for constraint: array
        if ('' !== ($loggingArrayErrorMessage = self::validateLoggingForArrayConstraintsFromSetLogging($logging))) {
            throw new InvalidArgumentException($loggingArrayErrorMessage, __LINE__);
        }
        $this->logging = $logging;
        
        return $this;
    }
    /**
     * Add item to logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType_1 $item
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function addToLogging(\AppturePay\DSV\StructType\LoggingType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\LoggingType_1) {
            throw new InvalidArgumentException(sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->logging[] = $item;
        
        return $this;
    }
    /**
     * Get file value
     * @return \AppturePay\DSV\StructType\FileType_1|null
     */
    public function getFile(): ?\AppturePay\DSV\StructType\FileType_1
    {
        return $this->file;
    }
    /**
     * Set file value
     * @param \AppturePay\DSV\StructType\FileType_1 $file
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setFile(?\AppturePay\DSV\StructType\FileType_1 $file = null): self
    {
        $this->file = $file;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get ediFunction1Field value
     * @return string|null
     */
    public function getEdiFunction1Field(): ?string
    {
        return $this->ediFunction1Field;
    }
    /**
     * Set ediFunction1Field value
     * @param string $ediFunction1Field
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setEdiFunction1Field(?string $ediFunction1Field = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1Field) && !is_string($ediFunction1Field)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1Field, true), gettype($ediFunction1Field)), __LINE__);
        }
        $this->ediFunction1Field = $ediFunction1Field;
        
        return $this;
    }
    /**
     * Get ediReferenceField value
     * @return string|null
     */
    public function getEdiReferenceField(): ?string
    {
        return $this->ediReferenceField;
    }
    /**
     * Set ediReferenceField value
     * @param string $ediReferenceField
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setEdiReferenceField(?string $ediReferenceField = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReferenceField) && !is_string($ediReferenceField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReferenceField, true), gettype($ediReferenceField)), __LINE__);
        }
        $this->ediReferenceField = $ediReferenceField;
        
        return $this;
    }
    /**
     * Get fileField value
     * @return \AppturePay\DSV\StructType\FileType_1|null
     */
    public function getFileField(): ?\AppturePay\DSV\StructType\FileType_1
    {
        return $this->fileField;
    }
    /**
     * Set fileField value
     * @param \AppturePay\DSV\StructType\FileType_1 $fileField
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setFileField(?\AppturePay\DSV\StructType\FileType_1 $fileField = null): self
    {
        $this->fileField = $fileField;
        
        return $this;
    }
    /**
     * Get internalNumberField value
     * @return string|null
     */
    public function getInternalNumberField(): ?string
    {
        return $this->internalNumberField;
    }
    /**
     * Set internalNumberField value
     * @param string $internalNumberField
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setInternalNumberField(?string $internalNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($internalNumberField) && !is_string($internalNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($internalNumberField, true), gettype($internalNumberField)), __LINE__);
        }
        $this->internalNumberField = $internalNumberField;
        
        return $this;
    }
    /**
     * Get loggingField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfLoggingType|null
     */
    public function getLoggingField(): ?\AppturePay\DSV\ArrayType\ArrayOfLoggingType
    {
        return $this->loggingField;
    }
    /**
     * Set loggingField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setLoggingField(?\AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField = null): self
    {
        $this->loggingField = $loggingField;
        
        return $this;
    }
    /**
     * Get referenceIndicationField value
     * @return string|null
     */
    public function getReferenceIndicationField(): ?string
    {
        return $this->referenceIndicationField;
    }
    /**
     * Set referenceIndicationField value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndicationField
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setReferenceIndicationField(?string $referenceIndicationField = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid($referenceIndicationField)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType_1', is_array($referenceIndicationField) ? implode(', ', $referenceIndicationField) : var_export($referenceIndicationField, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues())), __LINE__);
        }
        $this->referenceIndicationField = $referenceIndicationField;
        
        return $this;
    }
    /**
     * Get referenceIndicationFieldSpecified value
     * @return bool|null
     */
    public function getReferenceIndicationFieldSpecified(): ?bool
    {
        return $this->referenceIndicationFieldSpecified;
    }
    /**
     * Set referenceIndicationFieldSpecified value
     * @param bool $referenceIndicationFieldSpecified
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setReferenceIndicationFieldSpecified(?bool $referenceIndicationFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($referenceIndicationFieldSpecified) && !is_bool($referenceIndicationFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($referenceIndicationFieldSpecified, true), gettype($referenceIndicationFieldSpecified)), __LINE__);
        }
        $this->referenceIndicationFieldSpecified = $referenceIndicationFieldSpecified;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\ChildType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
