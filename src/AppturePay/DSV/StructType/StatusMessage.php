<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StatusMessage StructType
 * @subpackage Structs
 */
class StatusMessage extends AbstractStructBase
{
    /**
     * The ediCustomerNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediCustomerNumber = null;
    /**
     * The ediCustomerDepartment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1|null
     */
    protected ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1 $ediCustomerDepartment = null;
    /**
     * The ediParm1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediParm1 = null;
    /**
     * The ediParm2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediParm2 = null;
    /**
     * The transmitter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transmitter = null;
    /**
     * The receiver
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $receiver = null;
    /**
     * The ediReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediReference = null;
    /**
     * The referenceIndication
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceIndication = null;
    /**
     * The ediFunction1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediFunction1 = null;
    /**
     * The ediCustomerSearchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediCustomerSearchName = null;
    /**
     * The employeesInitials
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $employeesInitials = null;
    /**
     * The dateTimeZone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTimeZone = null;
    /**
     * The fileHeader
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FileHeader|null
     */
    protected ?\AppturePay\DSV\StructType\FileHeader $fileHeader = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for StatusMessage
     * @uses StatusMessage::setEdiCustomerNumber()
     * @uses StatusMessage::setEdiCustomerDepartment()
     * @uses StatusMessage::setEdiParm1()
     * @uses StatusMessage::setEdiParm2()
     * @uses StatusMessage::setTransmitter()
     * @uses StatusMessage::setReceiver()
     * @uses StatusMessage::setEdiReference()
     * @uses StatusMessage::setReferenceIndication()
     * @uses StatusMessage::setEdiFunction1()
     * @uses StatusMessage::setEdiCustomerSearchName()
     * @uses StatusMessage::setEmployeesInitials()
     * @uses StatusMessage::setDateTimeZone()
     * @uses StatusMessage::setFileHeader()
     * @uses StatusMessage::setType()
     * @param string $ediCustomerNumber
     * @param \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1 $ediCustomerDepartment
     * @param string $ediParm1
     * @param string $ediParm2
     * @param string $transmitter
     * @param string $receiver
     * @param string $ediReference
     * @param string $referenceIndication
     * @param string $ediFunction1
     * @param string $ediCustomerSearchName
     * @param string $employeesInitials
     * @param string $dateTimeZone
     * @param \AppturePay\DSV\StructType\FileHeader $fileHeader
     * @param string $type
     */
    public function __construct(?string $ediCustomerNumber = null, ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1 $ediCustomerDepartment = null, ?string $ediParm1 = null, ?string $ediParm2 = null, ?string $transmitter = null, ?string $receiver = null, ?string $ediReference = null, ?string $referenceIndication = null, ?string $ediFunction1 = null, ?string $ediCustomerSearchName = null, ?string $employeesInitials = null, ?string $dateTimeZone = null, ?\AppturePay\DSV\StructType\FileHeader $fileHeader = null, ?string $type = null)
    {
        $this
            ->setEdiCustomerNumber($ediCustomerNumber)
            ->setEdiCustomerDepartment($ediCustomerDepartment)
            ->setEdiParm1($ediParm1)
            ->setEdiParm2($ediParm2)
            ->setTransmitter($transmitter)
            ->setReceiver($receiver)
            ->setEdiReference($ediReference)
            ->setReferenceIndication($referenceIndication)
            ->setEdiFunction1($ediFunction1)
            ->setEdiCustomerSearchName($ediCustomerSearchName)
            ->setEmployeesInitials($employeesInitials)
            ->setDateTimeZone($dateTimeZone)
            ->setFileHeader($fileHeader)
            ->setType($type);
    }
    /**
     * Get ediCustomerNumber value
     * @return string|null
     */
    public function getEdiCustomerNumber(): ?string
    {
        return $this->ediCustomerNumber;
    }
    /**
     * Set ediCustomerNumber value
     * @param string $ediCustomerNumber
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiCustomerNumber(?string $ediCustomerNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($ediCustomerNumber) && !is_string($ediCustomerNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediCustomerNumber, true), gettype($ediCustomerNumber)), __LINE__);
        }
        $this->ediCustomerNumber = $ediCustomerNumber;
        
        return $this;
    }
    /**
     * Get ediCustomerDepartment value
     * @return \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1|null
     */
    public function getEdiCustomerDepartment(): ?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1
    {
        return $this->ediCustomerDepartment;
    }
    /**
     * Set ediCustomerDepartment value
     * @param \AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1 $ediCustomerDepartment
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiCustomerDepartment(?\AppturePay\DSV\StructType\EdiCustomerDepartmentTypeComplex_1 $ediCustomerDepartment = null): self
    {
        $this->ediCustomerDepartment = $ediCustomerDepartment;
        
        return $this;
    }
    /**
     * Get ediParm1 value
     * @return string|null
     */
    public function getEdiParm1(): ?string
    {
        return $this->ediParm1;
    }
    /**
     * Set ediParm1 value
     * @param string $ediParm1
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiParm1(?string $ediParm1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediParm1) && !is_string($ediParm1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediParm1, true), gettype($ediParm1)), __LINE__);
        }
        $this->ediParm1 = $ediParm1;
        
        return $this;
    }
    /**
     * Get ediParm2 value
     * @return string|null
     */
    public function getEdiParm2(): ?string
    {
        return $this->ediParm2;
    }
    /**
     * Set ediParm2 value
     * @param string $ediParm2
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiParm2(?string $ediParm2 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediParm2) && !is_string($ediParm2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediParm2, true), gettype($ediParm2)), __LINE__);
        }
        $this->ediParm2 = $ediParm2;
        
        return $this;
    }
    /**
     * Get transmitter value
     * @return string|null
     */
    public function getTransmitter(): ?string
    {
        return $this->transmitter;
    }
    /**
     * Set transmitter value
     * @param string $transmitter
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setTransmitter(?string $transmitter = null): self
    {
        // validation for constraint: string
        if (!is_null($transmitter) && !is_string($transmitter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transmitter, true), gettype($transmitter)), __LINE__);
        }
        $this->transmitter = $transmitter;
        
        return $this;
    }
    /**
     * Get receiver value
     * @return string|null
     */
    public function getReceiver(): ?string
    {
        return $this->receiver;
    }
    /**
     * Set receiver value
     * @param string $receiver
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setReceiver(?string $receiver = null): self
    {
        // validation for constraint: string
        if (!is_null($receiver) && !is_string($receiver)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiver, true), gettype($receiver)), __LINE__);
        }
        $this->receiver = $receiver;
        
        return $this;
    }
    /**
     * Get ediReference value
     * @return string|null
     */
    public function getEdiReference(): ?string
    {
        return $this->ediReference;
    }
    /**
     * Set ediReference value
     * @param string $ediReference
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiReference(?string $ediReference = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReference) && !is_string($ediReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReference, true), gettype($ediReference)), __LINE__);
        }
        $this->ediReference = $ediReference;
        
        return $this;
    }
    /**
     * Get referenceIndication value
     * @return string|null
     */
    public function getReferenceIndication(): ?string
    {
        return $this->referenceIndication;
    }
    /**
     * Set referenceIndication value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndication
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setReferenceIndication(?string $referenceIndication = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid($referenceIndication)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType_1', is_array($referenceIndication) ? implode(', ', $referenceIndication) : var_export($referenceIndication, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues())), __LINE__);
        }
        $this->referenceIndication = $referenceIndication;
        
        return $this;
    }
    /**
     * Get ediFunction1 value
     * @return string|null
     */
    public function getEdiFunction1(): ?string
    {
        return $this->ediFunction1;
    }
    /**
     * Set ediFunction1 value
     * @param string $ediFunction1
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiFunction1(?string $ediFunction1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1) && !is_string($ediFunction1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1, true), gettype($ediFunction1)), __LINE__);
        }
        $this->ediFunction1 = $ediFunction1;
        
        return $this;
    }
    /**
     * Get ediCustomerSearchName value
     * @return string|null
     */
    public function getEdiCustomerSearchName(): ?string
    {
        return $this->ediCustomerSearchName;
    }
    /**
     * Set ediCustomerSearchName value
     * @param string $ediCustomerSearchName
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEdiCustomerSearchName(?string $ediCustomerSearchName = null): self
    {
        // validation for constraint: string
        if (!is_null($ediCustomerSearchName) && !is_string($ediCustomerSearchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediCustomerSearchName, true), gettype($ediCustomerSearchName)), __LINE__);
        }
        $this->ediCustomerSearchName = $ediCustomerSearchName;
        
        return $this;
    }
    /**
     * Get employeesInitials value
     * @return string|null
     */
    public function getEmployeesInitials(): ?string
    {
        return $this->employeesInitials;
    }
    /**
     * Set employeesInitials value
     * @param string $employeesInitials
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setEmployeesInitials(?string $employeesInitials = null): self
    {
        // validation for constraint: string
        if (!is_null($employeesInitials) && !is_string($employeesInitials)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($employeesInitials, true), gettype($employeesInitials)), __LINE__);
        }
        $this->employeesInitials = $employeesInitials;
        
        return $this;
    }
    /**
     * Get dateTimeZone value
     * @return string|null
     */
    public function getDateTimeZone(): ?string
    {
        return $this->dateTimeZone;
    }
    /**
     * Set dateTimeZone value
     * @param string $dateTimeZone
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setDateTimeZone(?string $dateTimeZone = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZone) && !is_string($dateTimeZone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZone, true), gettype($dateTimeZone)), __LINE__);
        }
        $this->dateTimeZone = $dateTimeZone;
        
        return $this;
    }
    /**
     * Get fileHeader value
     * @return \AppturePay\DSV\StructType\FileHeader|null
     */
    public function getFileHeader(): ?\AppturePay\DSV\StructType\FileHeader
    {
        return $this->fileHeader;
    }
    /**
     * Set fileHeader value
     * @param \AppturePay\DSV\StructType\FileHeader $fileHeader
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setFileHeader(?\AppturePay\DSV\StructType\FileHeader $fileHeader = null): self
    {
        $this->fileHeader = $fileHeader;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\StatusMessage
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
