<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateAddress StructType
 * @subpackage Structs
 */
class ValidateAddress extends AbstractStructBase
{
    /**
     * The validateAddressRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ValidateAddressRequest|null
     */
    protected ?\AppturePay\DSV\StructType\ValidateAddressRequest $validateAddressRequest = null;
    /**
     * Constructor method for ValidateAddress
     * @uses ValidateAddress::setValidateAddressRequest()
     * @param \AppturePay\DSV\StructType\ValidateAddressRequest $validateAddressRequest
     */
    public function __construct(?\AppturePay\DSV\StructType\ValidateAddressRequest $validateAddressRequest = null)
    {
        $this
            ->setValidateAddressRequest($validateAddressRequest);
    }
    /**
     * Get validateAddressRequest value
     * @return \AppturePay\DSV\StructType\ValidateAddressRequest|null
     */
    public function getValidateAddressRequest(): ?\AppturePay\DSV\StructType\ValidateAddressRequest
    {
        return $this->validateAddressRequest;
    }
    /**
     * Set validateAddressRequest value
     * @param \AppturePay\DSV\StructType\ValidateAddressRequest $validateAddressRequest
     * @return \AppturePay\DSV\StructType\ValidateAddress
     */
    public function setValidateAddressRequest(?\AppturePay\DSV\StructType\ValidateAddressRequest $validateAddressRequest = null): self
    {
        $this->validateAddressRequest = $validateAddressRequest;
        
        return $this;
    }
}
