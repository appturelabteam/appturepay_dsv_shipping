<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CarrierType StructType
 * @subpackage Structs
 */
class CarrierType extends AbstractStructBase
{
    /**
     * The searchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchName = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The searchString
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchString = null;
    /**
     * The vatIdNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $vatIdNumber = null;
    /**
     * The ILNNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ILNNumber = null;
    /**
     * Constructor method for CarrierType
     * @uses CarrierType::setSearchName()
     * @uses CarrierType::setRelationNumber()
     * @uses CarrierType::setSearchString()
     * @uses CarrierType::setVatIdNumber()
     * @uses CarrierType::setILNNumber()
     * @param string $searchName
     * @param int $relationNumber
     * @param string $searchString
     * @param string $vatIdNumber
     * @param string $iLNNumber
     */
    public function __construct(?string $searchName = null, ?int $relationNumber = null, ?string $searchString = null, ?string $vatIdNumber = null, ?string $iLNNumber = null)
    {
        $this
            ->setSearchName($searchName)
            ->setRelationNumber($relationNumber)
            ->setSearchString($searchString)
            ->setVatIdNumber($vatIdNumber)
            ->setILNNumber($iLNNumber);
    }
    /**
     * Get searchName value
     * @return string|null
     */
    public function getSearchName(): ?string
    {
        return $this->searchName;
    }
    /**
     * Set searchName value
     * @param string $searchName
     * @return \AppturePay\DSV\StructType\CarrierType
     */
    public function setSearchName(?string $searchName = null): self
    {
        // validation for constraint: string
        if (!is_null($searchName) && !is_string($searchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchName, true), gettype($searchName)), __LINE__);
        }
        $this->searchName = $searchName;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\CarrierType
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get searchString value
     * @return string|null
     */
    public function getSearchString(): ?string
    {
        return $this->searchString;
    }
    /**
     * Set searchString value
     * @param string $searchString
     * @return \AppturePay\DSV\StructType\CarrierType
     */
    public function setSearchString(?string $searchString = null): self
    {
        // validation for constraint: string
        if (!is_null($searchString) && !is_string($searchString)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchString, true), gettype($searchString)), __LINE__);
        }
        $this->searchString = $searchString;
        
        return $this;
    }
    /**
     * Get vatIdNumber value
     * @return string|null
     */
    public function getVatIdNumber(): ?string
    {
        return $this->vatIdNumber;
    }
    /**
     * Set vatIdNumber value
     * @param string $vatIdNumber
     * @return \AppturePay\DSV\StructType\CarrierType
     */
    public function setVatIdNumber(?string $vatIdNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($vatIdNumber) && !is_string($vatIdNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vatIdNumber, true), gettype($vatIdNumber)), __LINE__);
        }
        $this->vatIdNumber = $vatIdNumber;
        
        return $this;
    }
    /**
     * Get ILNNumber value
     * @return string|null
     */
    public function getILNNumber(): ?string
    {
        return $this->ILNNumber;
    }
    /**
     * Set ILNNumber value
     * @param string $iLNNumber
     * @return \AppturePay\DSV\StructType\CarrierType
     */
    public function setILNNumber(?string $iLNNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($iLNNumber) && !is_string($iLNNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iLNNumber, true), gettype($iLNNumber)), __LINE__);
        }
        $this->ILNNumber = $iLNNumber;
        
        return $this;
    }
}
