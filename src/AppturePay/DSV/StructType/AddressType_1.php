<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddressType StructType
 * @subpackage Structs
 */
class AddressType_1 extends AbstractStructBase
{
    /**
     * The addressType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $addressType = null;
    /**
     * The searchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchName = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The searchString
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchString = null;
    /**
     * The additional
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AdditionalType_1|null
     */
    protected ?\AppturePay\DSV\StructType\AdditionalType_1 $additional = null;
    /**
     * The addressDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AddressDetailsType_1|null
     */
    protected ?\AppturePay\DSV\StructType\AddressDetailsType_1 $addressDetails = null;
    /**
     * The contactInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ContactInformationType_1|null
     */
    protected ?\AppturePay\DSV\StructType\ContactInformationType_1 $contactInformation = null;
    /**
     * The coordinates
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CTcoordinatesType[]
     */
    protected ?array $coordinates = null;
    /**
     * The timeFrames
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TimeFramesType[]
     */
    protected ?array $timeFrames = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for AddressType
     * @uses AddressType_1::setAddressType()
     * @uses AddressType_1::setSearchName()
     * @uses AddressType_1::setRelationNumber()
     * @uses AddressType_1::setSearchString()
     * @uses AddressType_1::setAdditional()
     * @uses AddressType_1::setAddressDetails()
     * @uses AddressType_1::setContactInformation()
     * @uses AddressType_1::setCoordinates()
     * @uses AddressType_1::setTimeFrames()
     * @uses AddressType_1::setType()
     * @param int $addressType
     * @param string $searchName
     * @param int $relationNumber
     * @param string $searchString
     * @param \AppturePay\DSV\StructType\AdditionalType_1 $additional
     * @param \AppturePay\DSV\StructType\AddressDetailsType_1 $addressDetails
     * @param \AppturePay\DSV\StructType\ContactInformationType_1 $contactInformation
     * @param \AppturePay\DSV\StructType\CTcoordinatesType[] $coordinates
     * @param \AppturePay\DSV\StructType\TimeFramesType[] $timeFrames
     * @param string $type
     */
    public function __construct(?int $addressType = null, ?string $searchName = null, ?int $relationNumber = null, ?string $searchString = null, ?\AppturePay\DSV\StructType\AdditionalType_1 $additional = null, ?\AppturePay\DSV\StructType\AddressDetailsType_1 $addressDetails = null, ?\AppturePay\DSV\StructType\ContactInformationType_1 $contactInformation = null, ?array $coordinates = null, ?array $timeFrames = null, ?string $type = null)
    {
        $this
            ->setAddressType($addressType)
            ->setSearchName($searchName)
            ->setRelationNumber($relationNumber)
            ->setSearchString($searchString)
            ->setAdditional($additional)
            ->setAddressDetails($addressDetails)
            ->setContactInformation($contactInformation)
            ->setCoordinates($coordinates)
            ->setTimeFrames($timeFrames)
            ->setType($type);
    }
    /**
     * Get addressType value
     * @return int|null
     */
    public function getAddressType(): ?int
    {
        return $this->addressType;
    }
    /**
     * Set addressType value
     * @param int $addressType
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setAddressType(?int $addressType = null): self
    {
        // validation for constraint: int
        if (!is_null($addressType) && !(is_int($addressType) || ctype_digit($addressType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($addressType, true), gettype($addressType)), __LINE__);
        }
        $this->addressType = $addressType;
        
        return $this;
    }
    /**
     * Get searchName value
     * @return string|null
     */
    public function getSearchName(): ?string
    {
        return $this->searchName;
    }
    /**
     * Set searchName value
     * @param string $searchName
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setSearchName(?string $searchName = null): self
    {
        // validation for constraint: string
        if (!is_null($searchName) && !is_string($searchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchName, true), gettype($searchName)), __LINE__);
        }
        $this->searchName = $searchName;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get searchString value
     * @return string|null
     */
    public function getSearchString(): ?string
    {
        return $this->searchString;
    }
    /**
     * Set searchString value
     * @param string $searchString
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setSearchString(?string $searchString = null): self
    {
        // validation for constraint: string
        if (!is_null($searchString) && !is_string($searchString)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchString, true), gettype($searchString)), __LINE__);
        }
        $this->searchString = $searchString;
        
        return $this;
    }
    /**
     * Get additional value
     * @return \AppturePay\DSV\StructType\AdditionalType_1|null
     */
    public function getAdditional(): ?\AppturePay\DSV\StructType\AdditionalType_1
    {
        return $this->additional;
    }
    /**
     * Set additional value
     * @param \AppturePay\DSV\StructType\AdditionalType_1 $additional
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setAdditional(?\AppturePay\DSV\StructType\AdditionalType_1 $additional = null): self
    {
        $this->additional = $additional;
        
        return $this;
    }
    /**
     * Get addressDetails value
     * @return \AppturePay\DSV\StructType\AddressDetailsType_1|null
     */
    public function getAddressDetails(): ?\AppturePay\DSV\StructType\AddressDetailsType_1
    {
        return $this->addressDetails;
    }
    /**
     * Set addressDetails value
     * @param \AppturePay\DSV\StructType\AddressDetailsType_1 $addressDetails
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setAddressDetails(?\AppturePay\DSV\StructType\AddressDetailsType_1 $addressDetails = null): self
    {
        $this->addressDetails = $addressDetails;
        
        return $this;
    }
    /**
     * Get contactInformation value
     * @return \AppturePay\DSV\StructType\ContactInformationType_1|null
     */
    public function getContactInformation(): ?\AppturePay\DSV\StructType\ContactInformationType_1
    {
        return $this->contactInformation;
    }
    /**
     * Set contactInformation value
     * @param \AppturePay\DSV\StructType\ContactInformationType_1 $contactInformation
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setContactInformation(?\AppturePay\DSV\StructType\ContactInformationType_1 $contactInformation = null): self
    {
        $this->contactInformation = $contactInformation;
        
        return $this;
    }
    /**
     * Get coordinates value
     * @return \AppturePay\DSV\StructType\CTcoordinatesType[]
     */
    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }
    /**
     * This method is responsible for validating the values passed to the setCoordinates method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCoordinates method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCoordinatesForArrayConstraintsFromSetCoordinates(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $addressTypeCoordinatesItem) {
            // validation for constraint: itemType
            if (!$addressTypeCoordinatesItem instanceof \AppturePay\DSV\StructType\CTcoordinatesType) {
                $invalidValues[] = is_object($addressTypeCoordinatesItem) ? get_class($addressTypeCoordinatesItem) : sprintf('%s(%s)', gettype($addressTypeCoordinatesItem), var_export($addressTypeCoordinatesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The coordinates property can only contain items of type \AppturePay\DSV\StructType\CTcoordinatesType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set coordinates value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CTcoordinatesType[] $coordinates
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setCoordinates(?array $coordinates = null): self
    {
        // validation for constraint: array
        if ('' !== ($coordinatesArrayErrorMessage = self::validateCoordinatesForArrayConstraintsFromSetCoordinates($coordinates))) {
            throw new InvalidArgumentException($coordinatesArrayErrorMessage, __LINE__);
        }
        $this->coordinates = $coordinates;
        
        return $this;
    }
    /**
     * Add item to coordinates value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CTcoordinatesType $item
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function addToCoordinates(\AppturePay\DSV\StructType\CTcoordinatesType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\CTcoordinatesType) {
            throw new InvalidArgumentException(sprintf('The coordinates property can only contain items of type \AppturePay\DSV\StructType\CTcoordinatesType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->coordinates[] = $item;
        
        return $this;
    }
    /**
     * Get timeFrames value
     * @return \AppturePay\DSV\StructType\TimeFramesType[]
     */
    public function getTimeFrames(): ?array
    {
        return $this->timeFrames;
    }
    /**
     * This method is responsible for validating the values passed to the setTimeFrames method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTimeFrames method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTimeFramesForArrayConstraintsFromSetTimeFrames(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $addressTypeTimeFramesItem) {
            // validation for constraint: itemType
            if (!$addressTypeTimeFramesItem instanceof \AppturePay\DSV\StructType\TimeFramesType) {
                $invalidValues[] = is_object($addressTypeTimeFramesItem) ? get_class($addressTypeTimeFramesItem) : sprintf('%s(%s)', gettype($addressTypeTimeFramesItem), var_export($addressTypeTimeFramesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The timeFrames property can only contain items of type \AppturePay\DSV\StructType\TimeFramesType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set timeFrames value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TimeFramesType[] $timeFrames
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setTimeFrames(?array $timeFrames = null): self
    {
        // validation for constraint: array
        if ('' !== ($timeFramesArrayErrorMessage = self::validateTimeFramesForArrayConstraintsFromSetTimeFrames($timeFrames))) {
            throw new InvalidArgumentException($timeFramesArrayErrorMessage, __LINE__);
        }
        $this->timeFrames = $timeFrames;
        
        return $this;
    }
    /**
     * Add item to timeFrames value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TimeFramesType $item
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function addToTimeFrames(\AppturePay\DSV\StructType\TimeFramesType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TimeFramesType) {
            throw new InvalidArgumentException(sprintf('The timeFrames property can only contain items of type \AppturePay\DSV\StructType\TimeFramesType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->timeFrames[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\AddressType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
