<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExtraReferenceType StructType
 * @subpackage Structs
 */
class ExtraReferenceType extends AbstractStructBase
{
    /**
     * The referenceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ReferenceCodeTypeComplex|null
     */
    protected ?\AppturePay\DSV\StructType\ReferenceCodeTypeComplex $referenceCode = null;
    /**
     * The referenceText
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceText = null;
    /**
     * The tailorFiller20
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tailorFiller20 = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for ExtraReferenceType
     * @uses ExtraReferenceType::setReferenceCode()
     * @uses ExtraReferenceType::setReferenceText()
     * @uses ExtraReferenceType::setTailorFiller20()
     * @uses ExtraReferenceType::setType()
     * @param \AppturePay\DSV\StructType\ReferenceCodeTypeComplex $referenceCode
     * @param string $referenceText
     * @param string $tailorFiller20
     * @param string $type
     */
    public function __construct(?\AppturePay\DSV\StructType\ReferenceCodeTypeComplex $referenceCode = null, ?string $referenceText = null, ?string $tailorFiller20 = null, ?string $type = null)
    {
        $this
            ->setReferenceCode($referenceCode)
            ->setReferenceText($referenceText)
            ->setTailorFiller20($tailorFiller20)
            ->setType($type);
    }
    /**
     * Get referenceCode value
     * @return \AppturePay\DSV\StructType\ReferenceCodeTypeComplex|null
     */
    public function getReferenceCode(): ?\AppturePay\DSV\StructType\ReferenceCodeTypeComplex
    {
        return $this->referenceCode;
    }
    /**
     * Set referenceCode value
     * @param \AppturePay\DSV\StructType\ReferenceCodeTypeComplex $referenceCode
     * @return \AppturePay\DSV\StructType\ExtraReferenceType
     */
    public function setReferenceCode(?\AppturePay\DSV\StructType\ReferenceCodeTypeComplex $referenceCode = null): self
    {
        $this->referenceCode = $referenceCode;
        
        return $this;
    }
    /**
     * Get referenceText value
     * @return string|null
     */
    public function getReferenceText(): ?string
    {
        return $this->referenceText;
    }
    /**
     * Set referenceText value
     * @param string $referenceText
     * @return \AppturePay\DSV\StructType\ExtraReferenceType
     */
    public function setReferenceText(?string $referenceText = null): self
    {
        // validation for constraint: string
        if (!is_null($referenceText) && !is_string($referenceText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceText, true), gettype($referenceText)), __LINE__);
        }
        $this->referenceText = $referenceText;
        
        return $this;
    }
    /**
     * Get tailorFiller20 value
     * @return string|null
     */
    public function getTailorFiller20(): ?string
    {
        return $this->tailorFiller20;
    }
    /**
     * Set tailorFiller20 value
     * @param string $tailorFiller20
     * @return \AppturePay\DSV\StructType\ExtraReferenceType
     */
    public function setTailorFiller20(?string $tailorFiller20 = null): self
    {
        // validation for constraint: string
        if (!is_null($tailorFiller20) && !is_string($tailorFiller20)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tailorFiller20, true), gettype($tailorFiller20)), __LINE__);
        }
        $this->tailorFiller20 = $tailorFiller20;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ExtraReferenceType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
