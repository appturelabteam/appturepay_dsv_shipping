<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CashInfoType StructType
 * @subpackage Structs
 */
class CashInfoType extends AbstractStructBase
{
    /**
     * The sequenceNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $sequenceNumber = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The text
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $text = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for CashInfoType
     * @uses CashInfoType::setSequenceNumber()
     * @uses CashInfoType::setCurrencyCode()
     * @uses CashInfoType::setAmount()
     * @uses CashInfoType::setText()
     * @uses CashInfoType::setType()
     * @param string $sequenceNumber
     * @param string $currencyCode
     * @param float $amount
     * @param string $text
     * @param string $type
     */
    public function __construct(?string $sequenceNumber = null, ?string $currencyCode = null, ?float $amount = null, ?string $text = null, ?string $type = null)
    {
        $this
            ->setSequenceNumber($sequenceNumber)
            ->setCurrencyCode($currencyCode)
            ->setAmount($amount)
            ->setText($text)
            ->setType($type);
    }
    /**
     * Get sequenceNumber value
     * @return string|null
     */
    public function getSequenceNumber(): ?string
    {
        return $this->sequenceNumber;
    }
    /**
     * Set sequenceNumber value
     * @uses \AppturePay\DSV\EnumType\CodSequenceNumber::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\CodSequenceNumber::getValidValues()
     * @throws InvalidArgumentException
     * @param string $sequenceNumber
     * @return \AppturePay\DSV\StructType\CashInfoType
     */
    public function setSequenceNumber(?string $sequenceNumber = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\CodSequenceNumber::valueIsValid($sequenceNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\CodSequenceNumber', is_array($sequenceNumber) ? implode(', ', $sequenceNumber) : var_export($sequenceNumber, true), implode(', ', \AppturePay\DSV\EnumType\CodSequenceNumber::getValidValues())), __LINE__);
        }
        $this->sequenceNumber = $sequenceNumber;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\CashInfoType
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\CashInfoType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get text value
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }
    /**
     * Set text value
     * @param string $text
     * @return \AppturePay\DSV\StructType\CashInfoType
     */
    public function setText(?string $text = null): self
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($text, true), gettype($text)), __LINE__);
        }
        $this->text = $text;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\CashInfoType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
