<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitShipmentTMSResponse StructType
 * @subpackage Structs
 */
class SubmitShipmentTMSResponse extends AbstractStructBase
{
    /**
     * The SubmitShipmentTMSResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShipmentServiceResultTMS|null
     */
    protected ?\AppturePay\DSV\StructType\ShipmentServiceResultTMS $SubmitShipmentTMSResult = null;
    /**
     * Constructor method for SubmitShipmentTMSResponse
     * @uses SubmitShipmentTMSResponse::setSubmitShipmentTMSResult()
     * @param \AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitShipmentTMSResult
     */
    public function __construct(?\AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitShipmentTMSResult = null)
    {
        $this
            ->setSubmitShipmentTMSResult($submitShipmentTMSResult);
    }
    /**
     * Get SubmitShipmentTMSResult value
     * @return \AppturePay\DSV\StructType\ShipmentServiceResultTMS|null
     */
    public function getSubmitShipmentTMSResult(): ?\AppturePay\DSV\StructType\ShipmentServiceResultTMS
    {
        return $this->SubmitShipmentTMSResult;
    }
    /**
     * Set SubmitShipmentTMSResult value
     * @param \AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitShipmentTMSResult
     * @return \AppturePay\DSV\StructType\SubmitShipmentTMSResponse
     */
    public function setSubmitShipmentTMSResult(?\AppturePay\DSV\StructType\ShipmentServiceResultTMS $submitShipmentTMSResult = null): self
    {
        $this->SubmitShipmentTMSResult = $submitShipmentTMSResult;
        
        return $this;
    }
}
