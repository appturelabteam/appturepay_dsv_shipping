<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FreeTextType StructType
 * @subpackage Structs
 */
class FreeTextType extends AbstractStructBase
{
    /**
     * The typeOfFreeText
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $typeOfFreeText = null;
    /**
     * The text
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $text = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for FreeTextType
     * @uses FreeTextType::setTypeOfFreeText()
     * @uses FreeTextType::setText()
     * @uses FreeTextType::setType()
     * @param string $typeOfFreeText
     * @param string $text
     * @param string $type
     */
    public function __construct(?string $typeOfFreeText = null, ?string $text = null, ?string $type = null)
    {
        $this
            ->setTypeOfFreeText($typeOfFreeText)
            ->setText($text)
            ->setType($type);
    }
    /**
     * Get typeOfFreeText value
     * @return string|null
     */
    public function getTypeOfFreeText(): ?string
    {
        return $this->typeOfFreeText;
    }
    /**
     * Set typeOfFreeText value
     * @param string $typeOfFreeText
     * @return \AppturePay\DSV\StructType\FreeTextType
     */
    public function setTypeOfFreeText(?string $typeOfFreeText = null): self
    {
        // validation for constraint: string
        if (!is_null($typeOfFreeText) && !is_string($typeOfFreeText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeOfFreeText, true), gettype($typeOfFreeText)), __LINE__);
        }
        $this->typeOfFreeText = $typeOfFreeText;
        
        return $this;
    }
    /**
     * Get text value
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }
    /**
     * Set text value
     * @param string $text
     * @return \AppturePay\DSV\StructType\FreeTextType
     */
    public function setText(?string $text = null): self
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($text, true), gettype($text)), __LINE__);
        }
        $this->text = $text;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FreeTextType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
