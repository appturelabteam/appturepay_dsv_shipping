<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MarksAndNumbersType StructType
 * @subpackage Structs
 */
class MarksAndNumbersType extends AbstractStructBase
{
    /**
     * The marksAndNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $marksAndNumber = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for MarksAndNumbersType
     * @uses MarksAndNumbersType::setMarksAndNumber()
     * @uses MarksAndNumbersType::setType()
     * @param string $marksAndNumber
     * @param string $type
     */
    public function __construct(?string $marksAndNumber = null, ?string $type = null)
    {
        $this
            ->setMarksAndNumber($marksAndNumber)
            ->setType($type);
    }
    /**
     * Get marksAndNumber value
     * @return string|null
     */
    public function getMarksAndNumber(): ?string
    {
        return $this->marksAndNumber;
    }
    /**
     * Set marksAndNumber value
     * @param string $marksAndNumber
     * @return \AppturePay\DSV\StructType\MarksAndNumbersType
     */
    public function setMarksAndNumber(?string $marksAndNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($marksAndNumber) && !is_string($marksAndNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($marksAndNumber, true), gettype($marksAndNumber)), __LINE__);
        }
        $this->marksAndNumber = $marksAndNumber;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\MarksAndNumbersType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
