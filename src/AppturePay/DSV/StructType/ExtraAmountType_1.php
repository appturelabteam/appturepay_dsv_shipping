<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExtraAmountType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ExtraAmountType
 * @subpackage Structs
 */
class ExtraAmountType_1 extends AbstractStructBase
{
    /**
     * The amountType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $amountType = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The amountField
     * @var float|null
     */
    protected ?float $amountField = null;
    /**
     * The amountFieldSpecified
     * @var bool|null
     */
    protected ?bool $amountFieldSpecified = null;
    /**
     * The amountTypeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $amountTypeField = null;
    /**
     * The currencyCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $currencyCodeField = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for ExtraAmountType
     * @uses ExtraAmountType_1::setAmountType()
     * @uses ExtraAmountType_1::setCurrencyCode()
     * @uses ExtraAmountType_1::setAmount()
     * @uses ExtraAmountType_1::setType()
     * @uses ExtraAmountType_1::setAmountField()
     * @uses ExtraAmountType_1::setAmountFieldSpecified()
     * @uses ExtraAmountType_1::setAmountTypeField()
     * @uses ExtraAmountType_1::setCurrencyCodeField()
     * @uses ExtraAmountType_1::setTypeField()
     * @param int $amountType
     * @param string $currencyCode
     * @param float $amount
     * @param string $type
     * @param float $amountField
     * @param bool $amountFieldSpecified
     * @param string $amountTypeField
     * @param string $currencyCodeField
     * @param string $typeField
     */
    public function __construct(?int $amountType = null, ?string $currencyCode = null, ?float $amount = null, ?string $type = null, ?float $amountField = null, ?bool $amountFieldSpecified = null, ?string $amountTypeField = null, ?string $currencyCodeField = null, ?string $typeField = null)
    {
        $this
            ->setAmountType($amountType)
            ->setCurrencyCode($currencyCode)
            ->setAmount($amount)
            ->setType($type)
            ->setAmountField($amountField)
            ->setAmountFieldSpecified($amountFieldSpecified)
            ->setAmountTypeField($amountTypeField)
            ->setCurrencyCodeField($currencyCodeField)
            ->setTypeField($typeField);
    }
    /**
     * Get amountType value
     * @return int|null
     */
    public function getAmountType(): ?int
    {
        return $this->amountType;
    }
    /**
     * Set amountType value
     * @param int $amountType
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setAmountType(?int $amountType = null): self
    {
        // validation for constraint: int
        if (!is_null($amountType) && !(is_int($amountType) || ctype_digit($amountType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($amountType, true), gettype($amountType)), __LINE__);
        }
        $this->amountType = $amountType;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get amountField value
     * @return float|null
     */
    public function getAmountField(): ?float
    {
        return $this->amountField;
    }
    /**
     * Set amountField value
     * @param float $amountField
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setAmountField(?float $amountField = null): self
    {
        // validation for constraint: float
        if (!is_null($amountField) && !(is_float($amountField) || is_numeric($amountField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amountField, true), gettype($amountField)), __LINE__);
        }
        $this->amountField = $amountField;
        
        return $this;
    }
    /**
     * Get amountFieldSpecified value
     * @return bool|null
     */
    public function getAmountFieldSpecified(): ?bool
    {
        return $this->amountFieldSpecified;
    }
    /**
     * Set amountFieldSpecified value
     * @param bool $amountFieldSpecified
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setAmountFieldSpecified(?bool $amountFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($amountFieldSpecified) && !is_bool($amountFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($amountFieldSpecified, true), gettype($amountFieldSpecified)), __LINE__);
        }
        $this->amountFieldSpecified = $amountFieldSpecified;
        
        return $this;
    }
    /**
     * Get amountTypeField value
     * @return string|null
     */
    public function getAmountTypeField(): ?string
    {
        return $this->amountTypeField;
    }
    /**
     * Set amountTypeField value
     * @param string $amountTypeField
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setAmountTypeField(?string $amountTypeField = null): self
    {
        // validation for constraint: string
        if (!is_null($amountTypeField) && !is_string($amountTypeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($amountTypeField, true), gettype($amountTypeField)), __LINE__);
        }
        $this->amountTypeField = $amountTypeField;
        
        return $this;
    }
    /**
     * Get currencyCodeField value
     * @return string|null
     */
    public function getCurrencyCodeField(): ?string
    {
        return $this->currencyCodeField;
    }
    /**
     * Set currencyCodeField value
     * @param string $currencyCodeField
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setCurrencyCodeField(?string $currencyCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCodeField) && !is_string($currencyCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCodeField, true), gettype($currencyCodeField)), __LINE__);
        }
        $this->currencyCodeField = $currencyCodeField;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
