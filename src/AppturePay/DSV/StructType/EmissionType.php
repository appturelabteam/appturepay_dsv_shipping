<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EmissionType StructType
 * @subpackage Structs
 */
class EmissionType extends AbstractStructBase
{
    /**
     * The emissionType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $emissionType = null;
    /**
     * The estimatedEmissionValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $estimatedEmissionValue = null;
    /**
     * The actualEmissionValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $actualEmissionValue = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for EmissionType
     * @uses EmissionType::setEmissionType()
     * @uses EmissionType::setEstimatedEmissionValue()
     * @uses EmissionType::setActualEmissionValue()
     * @uses EmissionType::setType()
     * @param string $emissionType
     * @param float $estimatedEmissionValue
     * @param float $actualEmissionValue
     * @param string $type
     */
    public function __construct(?string $emissionType = null, ?float $estimatedEmissionValue = null, ?float $actualEmissionValue = null, ?string $type = null)
    {
        $this
            ->setEmissionType($emissionType)
            ->setEstimatedEmissionValue($estimatedEmissionValue)
            ->setActualEmissionValue($actualEmissionValue)
            ->setType($type);
    }
    /**
     * Get emissionType value
     * @return string|null
     */
    public function getEmissionType(): ?string
    {
        return $this->emissionType;
    }
    /**
     * Set emissionType value
     * @param string $emissionType
     * @return \AppturePay\DSV\StructType\EmissionType
     */
    public function setEmissionType(?string $emissionType = null): self
    {
        // validation for constraint: string
        if (!is_null($emissionType) && !is_string($emissionType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($emissionType, true), gettype($emissionType)), __LINE__);
        }
        $this->emissionType = $emissionType;
        
        return $this;
    }
    /**
     * Get estimatedEmissionValue value
     * @return float|null
     */
    public function getEstimatedEmissionValue(): ?float
    {
        return $this->estimatedEmissionValue;
    }
    /**
     * Set estimatedEmissionValue value
     * @param float $estimatedEmissionValue
     * @return \AppturePay\DSV\StructType\EmissionType
     */
    public function setEstimatedEmissionValue(?float $estimatedEmissionValue = null): self
    {
        // validation for constraint: float
        if (!is_null($estimatedEmissionValue) && !(is_float($estimatedEmissionValue) || is_numeric($estimatedEmissionValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($estimatedEmissionValue, true), gettype($estimatedEmissionValue)), __LINE__);
        }
        $this->estimatedEmissionValue = $estimatedEmissionValue;
        
        return $this;
    }
    /**
     * Get actualEmissionValue value
     * @return float|null
     */
    public function getActualEmissionValue(): ?float
    {
        return $this->actualEmissionValue;
    }
    /**
     * Set actualEmissionValue value
     * @param float $actualEmissionValue
     * @return \AppturePay\DSV\StructType\EmissionType
     */
    public function setActualEmissionValue(?float $actualEmissionValue = null): self
    {
        // validation for constraint: float
        if (!is_null($actualEmissionValue) && !(is_float($actualEmissionValue) || is_numeric($actualEmissionValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($actualEmissionValue, true), gettype($actualEmissionValue)), __LINE__);
        }
        $this->actualEmissionValue = $actualEmissionValue;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\EmissionType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
