<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EdiParametersType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:EdiParametersType
 * @subpackage Structs
 */
class EdiParametersType_1 extends AbstractStructBase
{
    /**
     * The transmitter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transmitter = null;
    /**
     * The receiver
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $receiver = null;
    /**
     * The ediReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediReference = null;
    /**
     * The referenceIndication
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceIndication = null;
    /**
     * The internalNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $internalNumber = null;
    /**
     * The ediFunction1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ediFunction1 = null;
    /**
     * The dateTimeZone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTimeZone = null;
    /**
     * The messageReferenceNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $messageReferenceNumber = null;
    /**
     * The logging
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\LoggingType_1[]
     */
    protected ?array $logging = null;
    /**
     * The fileHeader
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FileHeaderType_1|null
     */
    protected ?\AppturePay\DSV\StructType\FileHeaderType_1 $fileHeader = null;
    /**
     * The child
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ChildType_1[]
     */
    protected ?array $child = null;
    /**
     * The status
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The childField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfChildType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfChildType $childField = null;
    /**
     * The dateTimeZoneField
     * @var string|null
     */
    protected ?string $dateTimeZoneField = null;
    /**
     * The dateTimeZoneStringFieldSpecified
     * @var bool|null
     */
    protected ?bool $dateTimeZoneStringFieldSpecified = null;
    /**
     * The ediFunction1Field
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $ediFunction1Field = null;
    /**
     * The ediReferenceField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $ediReferenceField = null;
    /**
     * The fileHeaderField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\FileHeaderType_1|null
     */
    protected ?\AppturePay\DSV\StructType\FileHeaderType_1 $fileHeaderField = null;
    /**
     * The internalNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $internalNumberField = null;
    /**
     * The loggingField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfLoggingType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField = null;
    /**
     * The messageReferenceNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $messageReferenceNumberField = null;
    /**
     * The receiverField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $receiverField = null;
    /**
     * The referenceIndicationField
     * @var string|null
     */
    protected ?string $referenceIndicationField = null;
    /**
     * The referenceIndicationFieldSpecified
     * @var bool|null
     */
    protected ?bool $referenceIndicationFieldSpecified = null;
    /**
     * The statusField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $statusField = null;
    /**
     * The transmitterField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $transmitterField = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for EdiParametersType
     * @uses EdiParametersType_1::setTransmitter()
     * @uses EdiParametersType_1::setReceiver()
     * @uses EdiParametersType_1::setEdiReference()
     * @uses EdiParametersType_1::setReferenceIndication()
     * @uses EdiParametersType_1::setInternalNumber()
     * @uses EdiParametersType_1::setEdiFunction1()
     * @uses EdiParametersType_1::setDateTimeZone()
     * @uses EdiParametersType_1::setMessageReferenceNumber()
     * @uses EdiParametersType_1::setLogging()
     * @uses EdiParametersType_1::setFileHeader()
     * @uses EdiParametersType_1::setChild()
     * @uses EdiParametersType_1::setStatus()
     * @uses EdiParametersType_1::setType()
     * @uses EdiParametersType_1::setChildField()
     * @uses EdiParametersType_1::setDateTimeZoneField()
     * @uses EdiParametersType_1::setDateTimeZoneStringFieldSpecified()
     * @uses EdiParametersType_1::setEdiFunction1Field()
     * @uses EdiParametersType_1::setEdiReferenceField()
     * @uses EdiParametersType_1::setFileHeaderField()
     * @uses EdiParametersType_1::setInternalNumberField()
     * @uses EdiParametersType_1::setLoggingField()
     * @uses EdiParametersType_1::setMessageReferenceNumberField()
     * @uses EdiParametersType_1::setReceiverField()
     * @uses EdiParametersType_1::setReferenceIndicationField()
     * @uses EdiParametersType_1::setReferenceIndicationFieldSpecified()
     * @uses EdiParametersType_1::setStatusField()
     * @uses EdiParametersType_1::setTransmitterField()
     * @uses EdiParametersType_1::setTypeField()
     * @param string $transmitter
     * @param string $receiver
     * @param string $ediReference
     * @param string $referenceIndication
     * @param int $internalNumber
     * @param string $ediFunction1
     * @param string $dateTimeZone
     * @param string $messageReferenceNumber
     * @param \AppturePay\DSV\StructType\LoggingType_1[] $logging
     * @param \AppturePay\DSV\StructType\FileHeaderType_1 $fileHeader
     * @param \AppturePay\DSV\StructType\ChildType_1[] $child
     * @param string $status
     * @param string $type
     * @param \AppturePay\DSV\ArrayType\ArrayOfChildType $childField
     * @param string $dateTimeZoneField
     * @param bool $dateTimeZoneStringFieldSpecified
     * @param string $ediFunction1Field
     * @param string $ediReferenceField
     * @param \AppturePay\DSV\StructType\FileHeaderType_1 $fileHeaderField
     * @param string $internalNumberField
     * @param \AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField
     * @param string $messageReferenceNumberField
     * @param string $receiverField
     * @param string $referenceIndicationField
     * @param bool $referenceIndicationFieldSpecified
     * @param string $statusField
     * @param string $transmitterField
     * @param string $typeField
     */
    public function __construct(?string $transmitter = null, ?string $receiver = null, ?string $ediReference = null, ?string $referenceIndication = null, ?int $internalNumber = null, ?string $ediFunction1 = null, ?string $dateTimeZone = null, ?string $messageReferenceNumber = null, ?array $logging = null, ?\AppturePay\DSV\StructType\FileHeaderType_1 $fileHeader = null, ?array $child = null, ?string $status = null, ?string $type = null, ?\AppturePay\DSV\ArrayType\ArrayOfChildType $childField = null, ?string $dateTimeZoneField = null, ?bool $dateTimeZoneStringFieldSpecified = null, ?string $ediFunction1Field = null, ?string $ediReferenceField = null, ?\AppturePay\DSV\StructType\FileHeaderType_1 $fileHeaderField = null, ?string $internalNumberField = null, ?\AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField = null, ?string $messageReferenceNumberField = null, ?string $receiverField = null, ?string $referenceIndicationField = null, ?bool $referenceIndicationFieldSpecified = null, ?string $statusField = null, ?string $transmitterField = null, ?string $typeField = null)
    {
        $this
            ->setTransmitter($transmitter)
            ->setReceiver($receiver)
            ->setEdiReference($ediReference)
            ->setReferenceIndication($referenceIndication)
            ->setInternalNumber($internalNumber)
            ->setEdiFunction1($ediFunction1)
            ->setDateTimeZone($dateTimeZone)
            ->setMessageReferenceNumber($messageReferenceNumber)
            ->setLogging($logging)
            ->setFileHeader($fileHeader)
            ->setChild($child)
            ->setStatus($status)
            ->setType($type)
            ->setChildField($childField)
            ->setDateTimeZoneField($dateTimeZoneField)
            ->setDateTimeZoneStringFieldSpecified($dateTimeZoneStringFieldSpecified)
            ->setEdiFunction1Field($ediFunction1Field)
            ->setEdiReferenceField($ediReferenceField)
            ->setFileHeaderField($fileHeaderField)
            ->setInternalNumberField($internalNumberField)
            ->setLoggingField($loggingField)
            ->setMessageReferenceNumberField($messageReferenceNumberField)
            ->setReceiverField($receiverField)
            ->setReferenceIndicationField($referenceIndicationField)
            ->setReferenceIndicationFieldSpecified($referenceIndicationFieldSpecified)
            ->setStatusField($statusField)
            ->setTransmitterField($transmitterField)
            ->setTypeField($typeField);
    }
    /**
     * Get transmitter value
     * @return string|null
     */
    public function getTransmitter(): ?string
    {
        return $this->transmitter;
    }
    /**
     * Set transmitter value
     * @param string $transmitter
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setTransmitter(?string $transmitter = null): self
    {
        // validation for constraint: string
        if (!is_null($transmitter) && !is_string($transmitter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transmitter, true), gettype($transmitter)), __LINE__);
        }
        $this->transmitter = $transmitter;
        
        return $this;
    }
    /**
     * Get receiver value
     * @return string|null
     */
    public function getReceiver(): ?string
    {
        return $this->receiver;
    }
    /**
     * Set receiver value
     * @param string $receiver
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setReceiver(?string $receiver = null): self
    {
        // validation for constraint: string
        if (!is_null($receiver) && !is_string($receiver)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiver, true), gettype($receiver)), __LINE__);
        }
        $this->receiver = $receiver;
        
        return $this;
    }
    /**
     * Get ediReference value
     * @return string|null
     */
    public function getEdiReference(): ?string
    {
        return $this->ediReference;
    }
    /**
     * Set ediReference value
     * @param string $ediReference
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setEdiReference(?string $ediReference = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReference) && !is_string($ediReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReference, true), gettype($ediReference)), __LINE__);
        }
        $this->ediReference = $ediReference;
        
        return $this;
    }
    /**
     * Get referenceIndication value
     * @return string|null
     */
    public function getReferenceIndication(): ?string
    {
        return $this->referenceIndication;
    }
    /**
     * Set referenceIndication value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndication
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setReferenceIndication(?string $referenceIndication = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid($referenceIndication)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType_1', is_array($referenceIndication) ? implode(', ', $referenceIndication) : var_export($referenceIndication, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues())), __LINE__);
        }
        $this->referenceIndication = $referenceIndication;
        
        return $this;
    }
    /**
     * Get internalNumber value
     * @return int|null
     */
    public function getInternalNumber(): ?int
    {
        return $this->internalNumber;
    }
    /**
     * Set internalNumber value
     * @param int $internalNumber
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setInternalNumber(?int $internalNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($internalNumber) && !(is_int($internalNumber) || ctype_digit($internalNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($internalNumber, true), gettype($internalNumber)), __LINE__);
        }
        $this->internalNumber = $internalNumber;
        
        return $this;
    }
    /**
     * Get ediFunction1 value
     * @return string|null
     */
    public function getEdiFunction1(): ?string
    {
        return $this->ediFunction1;
    }
    /**
     * Set ediFunction1 value
     * @param string $ediFunction1
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setEdiFunction1(?string $ediFunction1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1) && !is_string($ediFunction1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1, true), gettype($ediFunction1)), __LINE__);
        }
        $this->ediFunction1 = $ediFunction1;
        
        return $this;
    }
    /**
     * Get dateTimeZone value
     * @return string|null
     */
    public function getDateTimeZone(): ?string
    {
        return $this->dateTimeZone;
    }
    /**
     * Set dateTimeZone value
     * @param string $dateTimeZone
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setDateTimeZone(?string $dateTimeZone = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZone) && !is_string($dateTimeZone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZone, true), gettype($dateTimeZone)), __LINE__);
        }
        $this->dateTimeZone = $dateTimeZone;
        
        return $this;
    }
    /**
     * Get messageReferenceNumber value
     * @return string|null
     */
    public function getMessageReferenceNumber(): ?string
    {
        return $this->messageReferenceNumber;
    }
    /**
     * Set messageReferenceNumber value
     * @param string $messageReferenceNumber
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setMessageReferenceNumber(?string $messageReferenceNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($messageReferenceNumber) && !is_string($messageReferenceNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageReferenceNumber, true), gettype($messageReferenceNumber)), __LINE__);
        }
        $this->messageReferenceNumber = $messageReferenceNumber;
        
        return $this;
    }
    /**
     * Get logging value
     * @return \AppturePay\DSV\StructType\LoggingType_1[]
     */
    public function getLogging(): ?array
    {
        return $this->logging;
    }
    /**
     * This method is responsible for validating the values passed to the setLogging method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLogging method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLoggingForArrayConstraintsFromSetLogging(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $ediParametersTypeLoggingItem) {
            // validation for constraint: itemType
            if (!$ediParametersTypeLoggingItem instanceof \AppturePay\DSV\StructType\LoggingType_1) {
                $invalidValues[] = is_object($ediParametersTypeLoggingItem) ? get_class($ediParametersTypeLoggingItem) : sprintf('%s(%s)', gettype($ediParametersTypeLoggingItem), var_export($ediParametersTypeLoggingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType_1[] $logging
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setLogging(?array $logging = null): self
    {
        // validation for constraint: array
        if ('' !== ($loggingArrayErrorMessage = self::validateLoggingForArrayConstraintsFromSetLogging($logging))) {
            throw new InvalidArgumentException($loggingArrayErrorMessage, __LINE__);
        }
        $this->logging = $logging;
        
        return $this;
    }
    /**
     * Add item to logging value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType_1 $item
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function addToLogging(\AppturePay\DSV\StructType\LoggingType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\LoggingType_1) {
            throw new InvalidArgumentException(sprintf('The logging property can only contain items of type \AppturePay\DSV\StructType\LoggingType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->logging[] = $item;
        
        return $this;
    }
    /**
     * Get fileHeader value
     * @return \AppturePay\DSV\StructType\FileHeaderType_1|null
     */
    public function getFileHeader(): ?\AppturePay\DSV\StructType\FileHeaderType_1
    {
        return $this->fileHeader;
    }
    /**
     * Set fileHeader value
     * @param \AppturePay\DSV\StructType\FileHeaderType_1 $fileHeader
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setFileHeader(?\AppturePay\DSV\StructType\FileHeaderType_1 $fileHeader = null): self
    {
        $this->fileHeader = $fileHeader;
        
        return $this;
    }
    /**
     * Get child value
     * @return \AppturePay\DSV\StructType\ChildType_1[]
     */
    public function getChild(): ?array
    {
        return $this->child;
    }
    /**
     * This method is responsible for validating the values passed to the setChild method
     * This method is willingly generated in order to preserve the one-line inline validation within the setChild method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateChildForArrayConstraintsFromSetChild(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $ediParametersTypeChildItem) {
            // validation for constraint: itemType
            if (!$ediParametersTypeChildItem instanceof \AppturePay\DSV\StructType\ChildType_1) {
                $invalidValues[] = is_object($ediParametersTypeChildItem) ? get_class($ediParametersTypeChildItem) : sprintf('%s(%s)', gettype($ediParametersTypeChildItem), var_export($ediParametersTypeChildItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The child property can only contain items of type \AppturePay\DSV\StructType\ChildType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set child value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ChildType_1[] $child
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setChild(?array $child = null): self
    {
        // validation for constraint: array
        if ('' !== ($childArrayErrorMessage = self::validateChildForArrayConstraintsFromSetChild($child))) {
            throw new InvalidArgumentException($childArrayErrorMessage, __LINE__);
        }
        $this->child = $child;
        
        return $this;
    }
    /**
     * Add item to child value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ChildType_1 $item
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function addToChild(\AppturePay\DSV\StructType\ChildType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ChildType_1) {
            throw new InvalidArgumentException(sprintf('The child property can only contain items of type \AppturePay\DSV\StructType\ChildType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->child[] = $item;
        
        return $this;
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param string $status
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->status = $status;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get childField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfChildType|null
     */
    public function getChildField(): ?\AppturePay\DSV\ArrayType\ArrayOfChildType
    {
        return $this->childField;
    }
    /**
     * Set childField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfChildType $childField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setChildField(?\AppturePay\DSV\ArrayType\ArrayOfChildType $childField = null): self
    {
        $this->childField = $childField;
        
        return $this;
    }
    /**
     * Get dateTimeZoneField value
     * @return string|null
     */
    public function getDateTimeZoneField(): ?string
    {
        return $this->dateTimeZoneField;
    }
    /**
     * Set dateTimeZoneField value
     * @param string $dateTimeZoneField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setDateTimeZoneField(?string $dateTimeZoneField = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZoneField) && !is_string($dateTimeZoneField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZoneField, true), gettype($dateTimeZoneField)), __LINE__);
        }
        $this->dateTimeZoneField = $dateTimeZoneField;
        
        return $this;
    }
    /**
     * Get dateTimeZoneStringFieldSpecified value
     * @return bool|null
     */
    public function getDateTimeZoneStringFieldSpecified(): ?bool
    {
        return $this->dateTimeZoneStringFieldSpecified;
    }
    /**
     * Set dateTimeZoneStringFieldSpecified value
     * @param bool $dateTimeZoneStringFieldSpecified
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setDateTimeZoneStringFieldSpecified(?bool $dateTimeZoneStringFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($dateTimeZoneStringFieldSpecified) && !is_bool($dateTimeZoneStringFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($dateTimeZoneStringFieldSpecified, true), gettype($dateTimeZoneStringFieldSpecified)), __LINE__);
        }
        $this->dateTimeZoneStringFieldSpecified = $dateTimeZoneStringFieldSpecified;
        
        return $this;
    }
    /**
     * Get ediFunction1Field value
     * @return string|null
     */
    public function getEdiFunction1Field(): ?string
    {
        return $this->ediFunction1Field;
    }
    /**
     * Set ediFunction1Field value
     * @param string $ediFunction1Field
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setEdiFunction1Field(?string $ediFunction1Field = null): self
    {
        // validation for constraint: string
        if (!is_null($ediFunction1Field) && !is_string($ediFunction1Field)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediFunction1Field, true), gettype($ediFunction1Field)), __LINE__);
        }
        $this->ediFunction1Field = $ediFunction1Field;
        
        return $this;
    }
    /**
     * Get ediReferenceField value
     * @return string|null
     */
    public function getEdiReferenceField(): ?string
    {
        return $this->ediReferenceField;
    }
    /**
     * Set ediReferenceField value
     * @param string $ediReferenceField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setEdiReferenceField(?string $ediReferenceField = null): self
    {
        // validation for constraint: string
        if (!is_null($ediReferenceField) && !is_string($ediReferenceField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ediReferenceField, true), gettype($ediReferenceField)), __LINE__);
        }
        $this->ediReferenceField = $ediReferenceField;
        
        return $this;
    }
    /**
     * Get fileHeaderField value
     * @return \AppturePay\DSV\StructType\FileHeaderType_1|null
     */
    public function getFileHeaderField(): ?\AppturePay\DSV\StructType\FileHeaderType_1
    {
        return $this->fileHeaderField;
    }
    /**
     * Set fileHeaderField value
     * @param \AppturePay\DSV\StructType\FileHeaderType_1 $fileHeaderField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setFileHeaderField(?\AppturePay\DSV\StructType\FileHeaderType_1 $fileHeaderField = null): self
    {
        $this->fileHeaderField = $fileHeaderField;
        
        return $this;
    }
    /**
     * Get internalNumberField value
     * @return string|null
     */
    public function getInternalNumberField(): ?string
    {
        return $this->internalNumberField;
    }
    /**
     * Set internalNumberField value
     * @param string $internalNumberField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setInternalNumberField(?string $internalNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($internalNumberField) && !is_string($internalNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($internalNumberField, true), gettype($internalNumberField)), __LINE__);
        }
        $this->internalNumberField = $internalNumberField;
        
        return $this;
    }
    /**
     * Get loggingField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfLoggingType|null
     */
    public function getLoggingField(): ?\AppturePay\DSV\ArrayType\ArrayOfLoggingType
    {
        return $this->loggingField;
    }
    /**
     * Set loggingField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setLoggingField(?\AppturePay\DSV\ArrayType\ArrayOfLoggingType $loggingField = null): self
    {
        $this->loggingField = $loggingField;
        
        return $this;
    }
    /**
     * Get messageReferenceNumberField value
     * @return string|null
     */
    public function getMessageReferenceNumberField(): ?string
    {
        return $this->messageReferenceNumberField;
    }
    /**
     * Set messageReferenceNumberField value
     * @param string $messageReferenceNumberField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setMessageReferenceNumberField(?string $messageReferenceNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($messageReferenceNumberField) && !is_string($messageReferenceNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageReferenceNumberField, true), gettype($messageReferenceNumberField)), __LINE__);
        }
        $this->messageReferenceNumberField = $messageReferenceNumberField;
        
        return $this;
    }
    /**
     * Get receiverField value
     * @return string|null
     */
    public function getReceiverField(): ?string
    {
        return $this->receiverField;
    }
    /**
     * Set receiverField value
     * @param string $receiverField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setReceiverField(?string $receiverField = null): self
    {
        // validation for constraint: string
        if (!is_null($receiverField) && !is_string($receiverField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverField, true), gettype($receiverField)), __LINE__);
        }
        $this->receiverField = $receiverField;
        
        return $this;
    }
    /**
     * Get referenceIndicationField value
     * @return string|null
     */
    public function getReferenceIndicationField(): ?string
    {
        return $this->referenceIndicationField;
    }
    /**
     * Set referenceIndicationField value
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $referenceIndicationField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setReferenceIndicationField(?string $referenceIndicationField = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\ReferenceIndicationType_1::valueIsValid($referenceIndicationField)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\ReferenceIndicationType_1', is_array($referenceIndicationField) ? implode(', ', $referenceIndicationField) : var_export($referenceIndicationField, true), implode(', ', \AppturePay\DSV\EnumType\ReferenceIndicationType_1::getValidValues())), __LINE__);
        }
        $this->referenceIndicationField = $referenceIndicationField;
        
        return $this;
    }
    /**
     * Get referenceIndicationFieldSpecified value
     * @return bool|null
     */
    public function getReferenceIndicationFieldSpecified(): ?bool
    {
        return $this->referenceIndicationFieldSpecified;
    }
    /**
     * Set referenceIndicationFieldSpecified value
     * @param bool $referenceIndicationFieldSpecified
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setReferenceIndicationFieldSpecified(?bool $referenceIndicationFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($referenceIndicationFieldSpecified) && !is_bool($referenceIndicationFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($referenceIndicationFieldSpecified, true), gettype($referenceIndicationFieldSpecified)), __LINE__);
        }
        $this->referenceIndicationFieldSpecified = $referenceIndicationFieldSpecified;
        
        return $this;
    }
    /**
     * Get statusField value
     * @return string|null
     */
    public function getStatusField(): ?string
    {
        return $this->statusField;
    }
    /**
     * Set statusField value
     * @param string $statusField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setStatusField(?string $statusField = null): self
    {
        // validation for constraint: string
        if (!is_null($statusField) && !is_string($statusField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusField, true), gettype($statusField)), __LINE__);
        }
        $this->statusField = $statusField;
        
        return $this;
    }
    /**
     * Get transmitterField value
     * @return string|null
     */
    public function getTransmitterField(): ?string
    {
        return $this->transmitterField;
    }
    /**
     * Set transmitterField value
     * @param string $transmitterField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setTransmitterField(?string $transmitterField = null): self
    {
        // validation for constraint: string
        if (!is_null($transmitterField) && !is_string($transmitterField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transmitterField, true), gettype($transmitterField)), __LINE__);
        }
        $this->transmitterField = $transmitterField;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\EdiParametersType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
