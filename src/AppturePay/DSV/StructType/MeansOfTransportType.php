<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MeansOfTransportType StructType
 * @subpackage Structs
 */
class MeansOfTransportType extends AbstractStructBase
{
    /**
     * The displaySequence
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $displaySequence = null;
    /**
     * The motMode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $motMode = null;
    /**
     * The motType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $motType = null;
    /**
     * The motID
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $motID = null;
    /**
     * The motName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $motName = null;
    /**
     * The motIdentity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $motIdentity = null;
    /**
     * The motOrigin
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $motOrigin = null;
    /**
     * The motDestination
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $motDestination = null;
    /**
     * The carrier
     * Meta information extracted from the WSDL
     * - choice: carrier | shipbroker
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CarrierType|null
     */
    protected ?\AppturePay\DSV\StructType\CarrierType $carrier = null;
    /**
     * The shipbroker
     * Meta information extracted from the WSDL
     * - choice: carrier | shipbroker
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CarrierType|null
     */
    protected ?\AppturePay\DSV\StructType\CarrierType $shipbroker = null;
    /**
     * The shedLocationBerth
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $shedLocationBerth = null;
    /**
     * The ETD
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ETD = null;
    /**
     * The ATD
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ATD = null;
    /**
     * The ETA
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ETA = null;
    /**
     * The ATA
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ATA = null;
    /**
     * The motNationality
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $motNationality = null;
    /**
     * The equipmentEmissionID
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $equipmentEmissionID = null;
    /**
     * The emission
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\EmissionType[]
     */
    protected ?array $emission = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for MeansOfTransportType
     * @uses MeansOfTransportType::setDisplaySequence()
     * @uses MeansOfTransportType::setMotMode()
     * @uses MeansOfTransportType::setMotType()
     * @uses MeansOfTransportType::setMotID()
     * @uses MeansOfTransportType::setMotName()
     * @uses MeansOfTransportType::setMotIdentity()
     * @uses MeansOfTransportType::setMotOrigin()
     * @uses MeansOfTransportType::setMotDestination()
     * @uses MeansOfTransportType::setCarrier()
     * @uses MeansOfTransportType::setShipbroker()
     * @uses MeansOfTransportType::setShedLocationBerth()
     * @uses MeansOfTransportType::setETD()
     * @uses MeansOfTransportType::setATD()
     * @uses MeansOfTransportType::setETA()
     * @uses MeansOfTransportType::setATA()
     * @uses MeansOfTransportType::setMotNationality()
     * @uses MeansOfTransportType::setEquipmentEmissionID()
     * @uses MeansOfTransportType::setEmission()
     * @uses MeansOfTransportType::setType()
     * @param int $displaySequence
     * @param string $motMode
     * @param int $motType
     * @param int $motID
     * @param string $motName
     * @param string $motIdentity
     * @param string $motOrigin
     * @param string $motDestination
     * @param \AppturePay\DSV\StructType\CarrierType $carrier
     * @param \AppturePay\DSV\StructType\CarrierType $shipbroker
     * @param string $shedLocationBerth
     * @param string $eTD
     * @param string $aTD
     * @param string $eTA
     * @param string $aTA
     * @param string $motNationality
     * @param string $equipmentEmissionID
     * @param \AppturePay\DSV\StructType\EmissionType[] $emission
     * @param string $type
     */
    public function __construct(?int $displaySequence = null, ?string $motMode = null, ?int $motType = null, ?int $motID = null, ?string $motName = null, ?string $motIdentity = null, ?string $motOrigin = null, ?string $motDestination = null, ?\AppturePay\DSV\StructType\CarrierType $carrier = null, ?\AppturePay\DSV\StructType\CarrierType $shipbroker = null, ?string $shedLocationBerth = null, ?string $eTD = null, ?string $aTD = null, ?string $eTA = null, ?string $aTA = null, ?string $motNationality = null, ?string $equipmentEmissionID = null, ?array $emission = null, ?string $type = null)
    {
        $this
            ->setDisplaySequence($displaySequence)
            ->setMotMode($motMode)
            ->setMotType($motType)
            ->setMotID($motID)
            ->setMotName($motName)
            ->setMotIdentity($motIdentity)
            ->setMotOrigin($motOrigin)
            ->setMotDestination($motDestination)
            ->setCarrier($carrier)
            ->setShipbroker($shipbroker)
            ->setShedLocationBerth($shedLocationBerth)
            ->setETD($eTD)
            ->setATD($aTD)
            ->setETA($eTA)
            ->setATA($aTA)
            ->setMotNationality($motNationality)
            ->setEquipmentEmissionID($equipmentEmissionID)
            ->setEmission($emission)
            ->setType($type);
    }
    /**
     * Get displaySequence value
     * @return int|null
     */
    public function getDisplaySequence(): ?int
    {
        return $this->displaySequence;
    }
    /**
     * Set displaySequence value
     * @param int $displaySequence
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setDisplaySequence(?int $displaySequence = null): self
    {
        // validation for constraint: int
        if (!is_null($displaySequence) && !(is_int($displaySequence) || ctype_digit($displaySequence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($displaySequence, true), gettype($displaySequence)), __LINE__);
        }
        $this->displaySequence = $displaySequence;
        
        return $this;
    }
    /**
     * Get motMode value
     * @return string|null
     */
    public function getMotMode(): ?string
    {
        return $this->motMode;
    }
    /**
     * Set motMode value
     * @uses \AppturePay\DSV\EnumType\MotModeType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\MotModeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $motMode
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotMode(?string $motMode = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\MotModeType::valueIsValid($motMode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\MotModeType', is_array($motMode) ? implode(', ', $motMode) : var_export($motMode, true), implode(', ', \AppturePay\DSV\EnumType\MotModeType::getValidValues())), __LINE__);
        }
        $this->motMode = $motMode;
        
        return $this;
    }
    /**
     * Get motType value
     * @return int|null
     */
    public function getMotType(): ?int
    {
        return $this->motType;
    }
    /**
     * Set motType value
     * @param int $motType
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotType(?int $motType = null): self
    {
        // validation for constraint: int
        if (!is_null($motType) && !(is_int($motType) || ctype_digit($motType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($motType, true), gettype($motType)), __LINE__);
        }
        $this->motType = $motType;
        
        return $this;
    }
    /**
     * Get motID value
     * @return int|null
     */
    public function getMotID(): ?int
    {
        return $this->motID;
    }
    /**
     * Set motID value
     * @param int $motID
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotID(?int $motID = null): self
    {
        // validation for constraint: int
        if (!is_null($motID) && !(is_int($motID) || ctype_digit($motID))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($motID, true), gettype($motID)), __LINE__);
        }
        $this->motID = $motID;
        
        return $this;
    }
    /**
     * Get motName value
     * @return string|null
     */
    public function getMotName(): ?string
    {
        return $this->motName;
    }
    /**
     * Set motName value
     * @param string $motName
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotName(?string $motName = null): self
    {
        // validation for constraint: string
        if (!is_null($motName) && !is_string($motName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($motName, true), gettype($motName)), __LINE__);
        }
        $this->motName = $motName;
        
        return $this;
    }
    /**
     * Get motIdentity value
     * @return string|null
     */
    public function getMotIdentity(): ?string
    {
        return $this->motIdentity;
    }
    /**
     * Set motIdentity value
     * @param string $motIdentity
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotIdentity(?string $motIdentity = null): self
    {
        // validation for constraint: string
        if (!is_null($motIdentity) && !is_string($motIdentity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($motIdentity, true), gettype($motIdentity)), __LINE__);
        }
        $this->motIdentity = $motIdentity;
        
        return $this;
    }
    /**
     * Get motOrigin value
     * @return string|null
     */
    public function getMotOrigin(): ?string
    {
        return $this->motOrigin;
    }
    /**
     * Set motOrigin value
     * @param string $motOrigin
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotOrigin(?string $motOrigin = null): self
    {
        // validation for constraint: string
        if (!is_null($motOrigin) && !is_string($motOrigin)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($motOrigin, true), gettype($motOrigin)), __LINE__);
        }
        $this->motOrigin = $motOrigin;
        
        return $this;
    }
    /**
     * Get motDestination value
     * @return string|null
     */
    public function getMotDestination(): ?string
    {
        return $this->motDestination;
    }
    /**
     * Set motDestination value
     * @param string $motDestination
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotDestination(?string $motDestination = null): self
    {
        // validation for constraint: string
        if (!is_null($motDestination) && !is_string($motDestination)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($motDestination, true), gettype($motDestination)), __LINE__);
        }
        $this->motDestination = $motDestination;
        
        return $this;
    }
    /**
     * Get carrier value
     * @return \AppturePay\DSV\StructType\CarrierType|null
     */
    public function getCarrier(): ?\AppturePay\DSV\StructType\CarrierType
    {
        return isset($this->carrier) ? $this->carrier : null;
    }
    /**
     * This method is responsible for validating the value passed to the setCarrier method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCarrier method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateCarrierForChoiceConstraintsFromSetCarrier($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'shipbroker',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property carrier can\'t be set as the property %s is already set. Only one property must be set among these properties: carrier, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set carrier value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CarrierType $carrier
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setCarrier(?\AppturePay\DSV\StructType\CarrierType $carrier = null): self
    {
        // validation for constraint: choice(carrier, shipbroker)
        if ('' !== ($carrierChoiceErrorMessage = self::validateCarrierForChoiceConstraintsFromSetCarrier($carrier))) {
            throw new InvalidArgumentException($carrierChoiceErrorMessage, __LINE__);
        }
        if (is_null($carrier) || (is_array($carrier) && empty($carrier))) {
            unset($this->carrier);
        } else {
            $this->carrier = $carrier;
        }
        
        return $this;
    }
    /**
     * Get shipbroker value
     * @return \AppturePay\DSV\StructType\CarrierType|null
     */
    public function getShipbroker(): ?\AppturePay\DSV\StructType\CarrierType
    {
        return isset($this->shipbroker) ? $this->shipbroker : null;
    }
    /**
     * This method is responsible for validating the value passed to the setShipbroker method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShipbroker method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateShipbrokerForChoiceConstraintsFromSetShipbroker($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'carrier',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property shipbroker can\'t be set as the property %s is already set. Only one property must be set among these properties: shipbroker, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set shipbroker value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CarrierType $shipbroker
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setShipbroker(?\AppturePay\DSV\StructType\CarrierType $shipbroker = null): self
    {
        // validation for constraint: choice(carrier, shipbroker)
        if ('' !== ($shipbrokerChoiceErrorMessage = self::validateShipbrokerForChoiceConstraintsFromSetShipbroker($shipbroker))) {
            throw new InvalidArgumentException($shipbrokerChoiceErrorMessage, __LINE__);
        }
        if (is_null($shipbroker) || (is_array($shipbroker) && empty($shipbroker))) {
            unset($this->shipbroker);
        } else {
            $this->shipbroker = $shipbroker;
        }
        
        return $this;
    }
    /**
     * Get shedLocationBerth value
     * @return string|null
     */
    public function getShedLocationBerth(): ?string
    {
        return $this->shedLocationBerth;
    }
    /**
     * Set shedLocationBerth value
     * @param string $shedLocationBerth
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setShedLocationBerth(?string $shedLocationBerth = null): self
    {
        // validation for constraint: string
        if (!is_null($shedLocationBerth) && !is_string($shedLocationBerth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shedLocationBerth, true), gettype($shedLocationBerth)), __LINE__);
        }
        $this->shedLocationBerth = $shedLocationBerth;
        
        return $this;
    }
    /**
     * Get ETD value
     * @return string|null
     */
    public function getETD(): ?string
    {
        return $this->ETD;
    }
    /**
     * Set ETD value
     * @param string $eTD
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setETD(?string $eTD = null): self
    {
        // validation for constraint: string
        if (!is_null($eTD) && !is_string($eTD)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($eTD, true), gettype($eTD)), __LINE__);
        }
        $this->ETD = $eTD;
        
        return $this;
    }
    /**
     * Get ATD value
     * @return string|null
     */
    public function getATD(): ?string
    {
        return $this->ATD;
    }
    /**
     * Set ATD value
     * @param string $aTD
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setATD(?string $aTD = null): self
    {
        // validation for constraint: string
        if (!is_null($aTD) && !is_string($aTD)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aTD, true), gettype($aTD)), __LINE__);
        }
        $this->ATD = $aTD;
        
        return $this;
    }
    /**
     * Get ETA value
     * @return string|null
     */
    public function getETA(): ?string
    {
        return $this->ETA;
    }
    /**
     * Set ETA value
     * @param string $eTA
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setETA(?string $eTA = null): self
    {
        // validation for constraint: string
        if (!is_null($eTA) && !is_string($eTA)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($eTA, true), gettype($eTA)), __LINE__);
        }
        $this->ETA = $eTA;
        
        return $this;
    }
    /**
     * Get ATA value
     * @return string|null
     */
    public function getATA(): ?string
    {
        return $this->ATA;
    }
    /**
     * Set ATA value
     * @param string $aTA
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setATA(?string $aTA = null): self
    {
        // validation for constraint: string
        if (!is_null($aTA) && !is_string($aTA)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aTA, true), gettype($aTA)), __LINE__);
        }
        $this->ATA = $aTA;
        
        return $this;
    }
    /**
     * Get motNationality value
     * @return string|null
     */
    public function getMotNationality(): ?string
    {
        return $this->motNationality;
    }
    /**
     * Set motNationality value
     * @param string $motNationality
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setMotNationality(?string $motNationality = null): self
    {
        // validation for constraint: string
        if (!is_null($motNationality) && !is_string($motNationality)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($motNationality, true), gettype($motNationality)), __LINE__);
        }
        $this->motNationality = $motNationality;
        
        return $this;
    }
    /**
     * Get equipmentEmissionID value
     * @return string|null
     */
    public function getEquipmentEmissionID(): ?string
    {
        return $this->equipmentEmissionID;
    }
    /**
     * Set equipmentEmissionID value
     * @param string $equipmentEmissionID
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setEquipmentEmissionID(?string $equipmentEmissionID = null): self
    {
        // validation for constraint: string
        if (!is_null($equipmentEmissionID) && !is_string($equipmentEmissionID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($equipmentEmissionID, true), gettype($equipmentEmissionID)), __LINE__);
        }
        $this->equipmentEmissionID = $equipmentEmissionID;
        
        return $this;
    }
    /**
     * Get emission value
     * @return \AppturePay\DSV\StructType\EmissionType[]
     */
    public function getEmission(): ?array
    {
        return $this->emission;
    }
    /**
     * This method is responsible for validating the values passed to the setEmission method
     * This method is willingly generated in order to preserve the one-line inline validation within the setEmission method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateEmissionForArrayConstraintsFromSetEmission(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $meansOfTransportTypeEmissionItem) {
            // validation for constraint: itemType
            if (!$meansOfTransportTypeEmissionItem instanceof \AppturePay\DSV\StructType\EmissionType) {
                $invalidValues[] = is_object($meansOfTransportTypeEmissionItem) ? get_class($meansOfTransportTypeEmissionItem) : sprintf('%s(%s)', gettype($meansOfTransportTypeEmissionItem), var_export($meansOfTransportTypeEmissionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The emission property can only contain items of type \AppturePay\DSV\StructType\EmissionType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set emission value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\EmissionType[] $emission
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setEmission(?array $emission = null): self
    {
        // validation for constraint: array
        if ('' !== ($emissionArrayErrorMessage = self::validateEmissionForArrayConstraintsFromSetEmission($emission))) {
            throw new InvalidArgumentException($emissionArrayErrorMessage, __LINE__);
        }
        $this->emission = $emission;
        
        return $this;
    }
    /**
     * Add item to emission value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\EmissionType $item
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function addToEmission(\AppturePay\DSV\StructType\EmissionType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\EmissionType) {
            throw new InvalidArgumentException(sprintf('The emission property can only contain items of type \AppturePay\DSV\StructType\EmissionType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->emission[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\MeansOfTransportType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
