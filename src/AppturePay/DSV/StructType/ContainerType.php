<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ContainerType StructType
 * @subpackage Structs
 */
class ContainerType extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ID = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The sealNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $sealNumber = null;
    /**
     * The ventilation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ventilation = null;
    /**
     * The airHumidity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $airHumidity = null;
    /**
     * The temperatureCelsius
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $temperatureCelsius = null;
    /**
     * The temperatureFahrenheit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $temperatureFahrenheit = null;
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The goodsValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $goodsValue = null;
    /**
     * Constructor method for ContainerType
     * @uses ContainerType::setID()
     * @uses ContainerType::setType()
     * @uses ContainerType::setSealNumber()
     * @uses ContainerType::setVentilation()
     * @uses ContainerType::setAirHumidity()
     * @uses ContainerType::setTemperatureCelsius()
     * @uses ContainerType::setTemperatureFahrenheit()
     * @uses ContainerType::setQuantity()
     * @uses ContainerType::setGrossWeight()
     * @uses ContainerType::setVolume()
     * @uses ContainerType::setGoodsValue()
     * @param string $iD
     * @param string $type
     * @param string $sealNumber
     * @param string $ventilation
     * @param float $airHumidity
     * @param float $temperatureCelsius
     * @param float $temperatureFahrenheit
     * @param float $quantity
     * @param float $grossWeight
     * @param float $volume
     * @param float $goodsValue
     */
    public function __construct(?string $iD = null, ?string $type = null, ?string $sealNumber = null, ?string $ventilation = null, ?float $airHumidity = null, ?float $temperatureCelsius = null, ?float $temperatureFahrenheit = null, ?float $quantity = null, ?float $grossWeight = null, ?float $volume = null, ?float $goodsValue = null)
    {
        $this
            ->setID($iD)
            ->setType($type)
            ->setSealNumber($sealNumber)
            ->setVentilation($ventilation)
            ->setAirHumidity($airHumidity)
            ->setTemperatureCelsius($temperatureCelsius)
            ->setTemperatureFahrenheit($temperatureFahrenheit)
            ->setQuantity($quantity)
            ->setGrossWeight($grossWeight)
            ->setVolume($volume)
            ->setGoodsValue($goodsValue);
    }
    /**
     * Get ID value
     * @return string|null
     */
    public function getID(): ?string
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param string $iD
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setID(?string $iD = null): self
    {
        // validation for constraint: string
        if (!is_null($iD) && !is_string($iD)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get sealNumber value
     * @return string|null
     */
    public function getSealNumber(): ?string
    {
        return $this->sealNumber;
    }
    /**
     * Set sealNumber value
     * @param string $sealNumber
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setSealNumber(?string $sealNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($sealNumber) && !is_string($sealNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sealNumber, true), gettype($sealNumber)), __LINE__);
        }
        $this->sealNumber = $sealNumber;
        
        return $this;
    }
    /**
     * Get ventilation value
     * @return string|null
     */
    public function getVentilation(): ?string
    {
        return $this->ventilation;
    }
    /**
     * Set ventilation value
     * @param string $ventilation
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setVentilation(?string $ventilation = null): self
    {
        // validation for constraint: string
        if (!is_null($ventilation) && !is_string($ventilation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ventilation, true), gettype($ventilation)), __LINE__);
        }
        $this->ventilation = $ventilation;
        
        return $this;
    }
    /**
     * Get airHumidity value
     * @return float|null
     */
    public function getAirHumidity(): ?float
    {
        return $this->airHumidity;
    }
    /**
     * Set airHumidity value
     * @param float $airHumidity
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setAirHumidity(?float $airHumidity = null): self
    {
        // validation for constraint: float
        if (!is_null($airHumidity) && !(is_float($airHumidity) || is_numeric($airHumidity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($airHumidity, true), gettype($airHumidity)), __LINE__);
        }
        $this->airHumidity = $airHumidity;
        
        return $this;
    }
    /**
     * Get temperatureCelsius value
     * @return float|null
     */
    public function getTemperatureCelsius(): ?float
    {
        return $this->temperatureCelsius;
    }
    /**
     * Set temperatureCelsius value
     * @param float $temperatureCelsius
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setTemperatureCelsius(?float $temperatureCelsius = null): self
    {
        // validation for constraint: float
        if (!is_null($temperatureCelsius) && !(is_float($temperatureCelsius) || is_numeric($temperatureCelsius))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($temperatureCelsius, true), gettype($temperatureCelsius)), __LINE__);
        }
        $this->temperatureCelsius = $temperatureCelsius;
        
        return $this;
    }
    /**
     * Get temperatureFahrenheit value
     * @return float|null
     */
    public function getTemperatureFahrenheit(): ?float
    {
        return $this->temperatureFahrenheit;
    }
    /**
     * Set temperatureFahrenheit value
     * @param float $temperatureFahrenheit
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setTemperatureFahrenheit(?float $temperatureFahrenheit = null): self
    {
        // validation for constraint: float
        if (!is_null($temperatureFahrenheit) && !(is_float($temperatureFahrenheit) || is_numeric($temperatureFahrenheit))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($temperatureFahrenheit, true), gettype($temperatureFahrenheit)), __LINE__);
        }
        $this->temperatureFahrenheit = $temperatureFahrenheit;
        
        return $this;
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get goodsValue value
     * @return float|null
     */
    public function getGoodsValue(): ?float
    {
        return $this->goodsValue;
    }
    /**
     * Set goodsValue value
     * @param float $goodsValue
     * @return \AppturePay\DSV\StructType\ContainerType
     */
    public function setGoodsValue(?float $goodsValue = null): self
    {
        // validation for constraint: float
        if (!is_null($goodsValue) && !(is_float($goodsValue) || is_numeric($goodsValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsValue, true), gettype($goodsValue)), __LINE__);
        }
        $this->goodsValue = $goodsValue;
        
        return $this;
    }
}
