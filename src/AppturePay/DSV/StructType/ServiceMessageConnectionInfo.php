<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ServiceMessageConnectionInfo StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ServiceMessageConnectionInfo
 * @subpackage Structs
 */
class ServiceMessageConnectionInfo extends AbstractStructBase
{
    /**
     * The urlField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $urlField = null;
    /**
     * The userField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $userField = null;
    /**
     * Constructor method for ServiceMessageConnectionInfo
     * @uses ServiceMessageConnectionInfo::setUrlField()
     * @uses ServiceMessageConnectionInfo::setUserField()
     * @param string $urlField
     * @param string $userField
     */
    public function __construct(?string $urlField = null, ?string $userField = null)
    {
        $this
            ->setUrlField($urlField)
            ->setUserField($userField);
    }
    /**
     * Get urlField value
     * @return string|null
     */
    public function getUrlField(): ?string
    {
        return $this->urlField;
    }
    /**
     * Set urlField value
     * @param string $urlField
     * @return \AppturePay\DSV\StructType\ServiceMessageConnectionInfo
     */
    public function setUrlField(?string $urlField = null): self
    {
        // validation for constraint: string
        if (!is_null($urlField) && !is_string($urlField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($urlField, true), gettype($urlField)), __LINE__);
        }
        $this->urlField = $urlField;
        
        return $this;
    }
    /**
     * Get userField value
     * @return string|null
     */
    public function getUserField(): ?string
    {
        return $this->userField;
    }
    /**
     * Set userField value
     * @param string $userField
     * @return \AppturePay\DSV\StructType\ServiceMessageConnectionInfo
     */
    public function setUserField(?string $userField = null): self
    {
        // validation for constraint: string
        if (!is_null($userField) && !is_string($userField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userField, true), gettype($userField)), __LINE__);
        }
        $this->userField = $userField;
        
        return $this;
    }
}
