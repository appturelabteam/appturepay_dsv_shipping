<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for InvoiceLineType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:InvoiceLineType
 * @subpackage Structs
 */
class InvoiceLineType_1 extends AbstractStructBase
{
    /**
     * The activityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1|null
     */
    protected ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCode = null;
    /**
     * The sequentialNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $sequentialNumber = null;
    /**
     * The transactionType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $transactionType = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The searchName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchName = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The vat
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\VatType_1|null
     */
    protected ?\AppturePay\DSV\StructType\VatType_1 $vat = null;
    /**
     * The weightCalculation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\WeightCalculationType_1|null
     */
    protected ?\AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculation = null;
    /**
     * The tariffBase
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TariffBaseType_1[]
     */
    protected ?array $tariffBase = null;
    /**
     * The tariffSpecification
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TariffSpecificationType_1|null
     */
    protected ?\AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecification = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The activityCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1|null
     */
    protected ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCodeField = null;
    /**
     * The amountField
     * @var float|null
     */
    protected ?float $amountField = null;
    /**
     * The amountFieldSpecified
     * @var bool|null
     */
    protected ?bool $amountFieldSpecified = null;
    /**
     * The currencyCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $currencyCodeField = null;
    /**
     * The descriptionField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $descriptionField = null;
    /**
     * The relationNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $relationNumberField = null;
    /**
     * The searchNameField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $searchNameField = null;
    /**
     * The sequentialNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $sequentialNumberField = null;
    /**
     * The tariffBaseField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfTariffBaseType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfTariffBaseType $tariffBaseField = null;
    /**
     * The tariffSpecificationField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\TariffSpecificationType_1|null
     */
    protected ?\AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecificationField = null;
    /**
     * The transactionTypeField
     * @var string|null
     */
    protected ?string $transactionTypeField = null;
    /**
     * The transactionTypeFieldSpecified
     * @var bool|null
     */
    protected ?bool $transactionTypeFieldSpecified = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * The vatField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\VatType_1|null
     */
    protected ?\AppturePay\DSV\StructType\VatType_1 $vatField = null;
    /**
     * The weightCalculationField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\StructType\WeightCalculationType_1|null
     */
    protected ?\AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculationField = null;
    /**
     * Constructor method for InvoiceLineType
     * @uses InvoiceLineType_1::setActivityCode()
     * @uses InvoiceLineType_1::setSequentialNumber()
     * @uses InvoiceLineType_1::setTransactionType()
     * @uses InvoiceLineType_1::setRelationNumber()
     * @uses InvoiceLineType_1::setSearchName()
     * @uses InvoiceLineType_1::setAmount()
     * @uses InvoiceLineType_1::setCurrencyCode()
     * @uses InvoiceLineType_1::setDescription()
     * @uses InvoiceLineType_1::setVat()
     * @uses InvoiceLineType_1::setWeightCalculation()
     * @uses InvoiceLineType_1::setTariffBase()
     * @uses InvoiceLineType_1::setTariffSpecification()
     * @uses InvoiceLineType_1::setType()
     * @uses InvoiceLineType_1::setActivityCodeField()
     * @uses InvoiceLineType_1::setAmountField()
     * @uses InvoiceLineType_1::setAmountFieldSpecified()
     * @uses InvoiceLineType_1::setCurrencyCodeField()
     * @uses InvoiceLineType_1::setDescriptionField()
     * @uses InvoiceLineType_1::setRelationNumberField()
     * @uses InvoiceLineType_1::setSearchNameField()
     * @uses InvoiceLineType_1::setSequentialNumberField()
     * @uses InvoiceLineType_1::setTariffBaseField()
     * @uses InvoiceLineType_1::setTariffSpecificationField()
     * @uses InvoiceLineType_1::setTransactionTypeField()
     * @uses InvoiceLineType_1::setTransactionTypeFieldSpecified()
     * @uses InvoiceLineType_1::setTypeField()
     * @uses InvoiceLineType_1::setVatField()
     * @uses InvoiceLineType_1::setWeightCalculationField()
     * @param \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCode
     * @param int $sequentialNumber
     * @param string $transactionType
     * @param int $relationNumber
     * @param string $searchName
     * @param float $amount
     * @param string $currencyCode
     * @param string $description
     * @param \AppturePay\DSV\StructType\VatType_1 $vat
     * @param \AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculation
     * @param \AppturePay\DSV\StructType\TariffBaseType_1[] $tariffBase
     * @param \AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecification
     * @param string $type
     * @param \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCodeField
     * @param float $amountField
     * @param bool $amountFieldSpecified
     * @param string $currencyCodeField
     * @param string $descriptionField
     * @param string $relationNumberField
     * @param string $searchNameField
     * @param string $sequentialNumberField
     * @param \AppturePay\DSV\ArrayType\ArrayOfTariffBaseType $tariffBaseField
     * @param \AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecificationField
     * @param string $transactionTypeField
     * @param bool $transactionTypeFieldSpecified
     * @param string $typeField
     * @param \AppturePay\DSV\StructType\VatType_1 $vatField
     * @param \AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculationField
     */
    public function __construct(?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCode = null, ?int $sequentialNumber = null, ?string $transactionType = null, ?int $relationNumber = null, ?string $searchName = null, ?float $amount = null, ?string $currencyCode = null, ?string $description = null, ?\AppturePay\DSV\StructType\VatType_1 $vat = null, ?\AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculation = null, ?array $tariffBase = null, ?\AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecification = null, ?string $type = null, ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCodeField = null, ?float $amountField = null, ?bool $amountFieldSpecified = null, ?string $currencyCodeField = null, ?string $descriptionField = null, ?string $relationNumberField = null, ?string $searchNameField = null, ?string $sequentialNumberField = null, ?\AppturePay\DSV\ArrayType\ArrayOfTariffBaseType $tariffBaseField = null, ?\AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecificationField = null, ?string $transactionTypeField = null, ?bool $transactionTypeFieldSpecified = null, ?string $typeField = null, ?\AppturePay\DSV\StructType\VatType_1 $vatField = null, ?\AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculationField = null)
    {
        $this
            ->setActivityCode($activityCode)
            ->setSequentialNumber($sequentialNumber)
            ->setTransactionType($transactionType)
            ->setRelationNumber($relationNumber)
            ->setSearchName($searchName)
            ->setAmount($amount)
            ->setCurrencyCode($currencyCode)
            ->setDescription($description)
            ->setVat($vat)
            ->setWeightCalculation($weightCalculation)
            ->setTariffBase($tariffBase)
            ->setTariffSpecification($tariffSpecification)
            ->setType($type)
            ->setActivityCodeField($activityCodeField)
            ->setAmountField($amountField)
            ->setAmountFieldSpecified($amountFieldSpecified)
            ->setCurrencyCodeField($currencyCodeField)
            ->setDescriptionField($descriptionField)
            ->setRelationNumberField($relationNumberField)
            ->setSearchNameField($searchNameField)
            ->setSequentialNumberField($sequentialNumberField)
            ->setTariffBaseField($tariffBaseField)
            ->setTariffSpecificationField($tariffSpecificationField)
            ->setTransactionTypeField($transactionTypeField)
            ->setTransactionTypeFieldSpecified($transactionTypeFieldSpecified)
            ->setTypeField($typeField)
            ->setVatField($vatField)
            ->setWeightCalculationField($weightCalculationField);
    }
    /**
     * Get activityCode value
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1|null
     */
    public function getActivityCode(): ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1
    {
        return $this->activityCode;
    }
    /**
     * Set activityCode value
     * @param \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCode
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setActivityCode(?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCode = null): self
    {
        $this->activityCode = $activityCode;
        
        return $this;
    }
    /**
     * Get sequentialNumber value
     * @return int|null
     */
    public function getSequentialNumber(): ?int
    {
        return $this->sequentialNumber;
    }
    /**
     * Set sequentialNumber value
     * @param int $sequentialNumber
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setSequentialNumber(?int $sequentialNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($sequentialNumber) && !(is_int($sequentialNumber) || ctype_digit($sequentialNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($sequentialNumber, true), gettype($sequentialNumber)), __LINE__);
        }
        $this->sequentialNumber = $sequentialNumber;
        
        return $this;
    }
    /**
     * Get transactionType value
     * @return string|null
     */
    public function getTransactionType(): ?string
    {
        return $this->transactionType;
    }
    /**
     * Set transactionType value
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $transactionType
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTransactionType(?string $transactionType = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\TransactionTypeType_1::valueIsValid($transactionType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\TransactionTypeType_1', is_array($transactionType) ? implode(', ', $transactionType) : var_export($transactionType, true), implode(', ', \AppturePay\DSV\EnumType\TransactionTypeType_1::getValidValues())), __LINE__);
        }
        $this->transactionType = $transactionType;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get searchName value
     * @return string|null
     */
    public function getSearchName(): ?string
    {
        return $this->searchName;
    }
    /**
     * Set searchName value
     * @param string $searchName
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setSearchName(?string $searchName = null): self
    {
        // validation for constraint: string
        if (!is_null($searchName) && !is_string($searchName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchName, true), gettype($searchName)), __LINE__);
        }
        $this->searchName = $searchName;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get vat value
     * @return \AppturePay\DSV\StructType\VatType_1|null
     */
    public function getVat(): ?\AppturePay\DSV\StructType\VatType_1
    {
        return $this->vat;
    }
    /**
     * Set vat value
     * @param \AppturePay\DSV\StructType\VatType_1 $vat
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setVat(?\AppturePay\DSV\StructType\VatType_1 $vat = null): self
    {
        $this->vat = $vat;
        
        return $this;
    }
    /**
     * Get weightCalculation value
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1|null
     */
    public function getWeightCalculation(): ?\AppturePay\DSV\StructType\WeightCalculationType_1
    {
        return $this->weightCalculation;
    }
    /**
     * Set weightCalculation value
     * @param \AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculation
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setWeightCalculation(?\AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculation = null): self
    {
        $this->weightCalculation = $weightCalculation;
        
        return $this;
    }
    /**
     * Get tariffBase value
     * @return \AppturePay\DSV\StructType\TariffBaseType_1[]
     */
    public function getTariffBase(): ?array
    {
        return $this->tariffBase;
    }
    /**
     * This method is responsible for validating the values passed to the setTariffBase method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTariffBase method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTariffBaseForArrayConstraintsFromSetTariffBase(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $invoiceLineTypeTariffBaseItem) {
            // validation for constraint: itemType
            if (!$invoiceLineTypeTariffBaseItem instanceof \AppturePay\DSV\StructType\TariffBaseType_1) {
                $invalidValues[] = is_object($invoiceLineTypeTariffBaseItem) ? get_class($invoiceLineTypeTariffBaseItem) : sprintf('%s(%s)', gettype($invoiceLineTypeTariffBaseItem), var_export($invoiceLineTypeTariffBaseItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The tariffBase property can only contain items of type \AppturePay\DSV\StructType\TariffBaseType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set tariffBase value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TariffBaseType_1[] $tariffBase
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTariffBase(?array $tariffBase = null): self
    {
        // validation for constraint: array
        if ('' !== ($tariffBaseArrayErrorMessage = self::validateTariffBaseForArrayConstraintsFromSetTariffBase($tariffBase))) {
            throw new InvalidArgumentException($tariffBaseArrayErrorMessage, __LINE__);
        }
        $this->tariffBase = $tariffBase;
        
        return $this;
    }
    /**
     * Add item to tariffBase value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TariffBaseType_1 $item
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function addToTariffBase(\AppturePay\DSV\StructType\TariffBaseType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TariffBaseType_1) {
            throw new InvalidArgumentException(sprintf('The tariffBase property can only contain items of type \AppturePay\DSV\StructType\TariffBaseType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->tariffBase[] = $item;
        
        return $this;
    }
    /**
     * Get tariffSpecification value
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1|null
     */
    public function getTariffSpecification(): ?\AppturePay\DSV\StructType\TariffSpecificationType_1
    {
        return $this->tariffSpecification;
    }
    /**
     * Set tariffSpecification value
     * @param \AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecification
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTariffSpecification(?\AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecification = null): self
    {
        $this->tariffSpecification = $tariffSpecification;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get activityCodeField value
     * @return \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1|null
     */
    public function getActivityCodeField(): ?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1
    {
        return $this->activityCodeField;
    }
    /**
     * Set activityCodeField value
     * @param \AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCodeField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setActivityCodeField(?\AppturePay\DSV\StructType\ActivityCodeTypeComplex_1 $activityCodeField = null): self
    {
        $this->activityCodeField = $activityCodeField;
        
        return $this;
    }
    /**
     * Get amountField value
     * @return float|null
     */
    public function getAmountField(): ?float
    {
        return $this->amountField;
    }
    /**
     * Set amountField value
     * @param float $amountField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setAmountField(?float $amountField = null): self
    {
        // validation for constraint: float
        if (!is_null($amountField) && !(is_float($amountField) || is_numeric($amountField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amountField, true), gettype($amountField)), __LINE__);
        }
        $this->amountField = $amountField;
        
        return $this;
    }
    /**
     * Get amountFieldSpecified value
     * @return bool|null
     */
    public function getAmountFieldSpecified(): ?bool
    {
        return $this->amountFieldSpecified;
    }
    /**
     * Set amountFieldSpecified value
     * @param bool $amountFieldSpecified
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setAmountFieldSpecified(?bool $amountFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($amountFieldSpecified) && !is_bool($amountFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($amountFieldSpecified, true), gettype($amountFieldSpecified)), __LINE__);
        }
        $this->amountFieldSpecified = $amountFieldSpecified;
        
        return $this;
    }
    /**
     * Get currencyCodeField value
     * @return string|null
     */
    public function getCurrencyCodeField(): ?string
    {
        return $this->currencyCodeField;
    }
    /**
     * Set currencyCodeField value
     * @param string $currencyCodeField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setCurrencyCodeField(?string $currencyCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCodeField) && !is_string($currencyCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCodeField, true), gettype($currencyCodeField)), __LINE__);
        }
        $this->currencyCodeField = $currencyCodeField;
        
        return $this;
    }
    /**
     * Get descriptionField value
     * @return string|null
     */
    public function getDescriptionField(): ?string
    {
        return $this->descriptionField;
    }
    /**
     * Set descriptionField value
     * @param string $descriptionField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setDescriptionField(?string $descriptionField = null): self
    {
        // validation for constraint: string
        if (!is_null($descriptionField) && !is_string($descriptionField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($descriptionField, true), gettype($descriptionField)), __LINE__);
        }
        $this->descriptionField = $descriptionField;
        
        return $this;
    }
    /**
     * Get relationNumberField value
     * @return string|null
     */
    public function getRelationNumberField(): ?string
    {
        return $this->relationNumberField;
    }
    /**
     * Set relationNumberField value
     * @param string $relationNumberField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setRelationNumberField(?string $relationNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($relationNumberField) && !is_string($relationNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($relationNumberField, true), gettype($relationNumberField)), __LINE__);
        }
        $this->relationNumberField = $relationNumberField;
        
        return $this;
    }
    /**
     * Get searchNameField value
     * @return string|null
     */
    public function getSearchNameField(): ?string
    {
        return $this->searchNameField;
    }
    /**
     * Set searchNameField value
     * @param string $searchNameField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setSearchNameField(?string $searchNameField = null): self
    {
        // validation for constraint: string
        if (!is_null($searchNameField) && !is_string($searchNameField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchNameField, true), gettype($searchNameField)), __LINE__);
        }
        $this->searchNameField = $searchNameField;
        
        return $this;
    }
    /**
     * Get sequentialNumberField value
     * @return string|null
     */
    public function getSequentialNumberField(): ?string
    {
        return $this->sequentialNumberField;
    }
    /**
     * Set sequentialNumberField value
     * @param string $sequentialNumberField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setSequentialNumberField(?string $sequentialNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($sequentialNumberField) && !is_string($sequentialNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sequentialNumberField, true), gettype($sequentialNumberField)), __LINE__);
        }
        $this->sequentialNumberField = $sequentialNumberField;
        
        return $this;
    }
    /**
     * Get tariffBaseField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfTariffBaseType|null
     */
    public function getTariffBaseField(): ?\AppturePay\DSV\ArrayType\ArrayOfTariffBaseType
    {
        return $this->tariffBaseField;
    }
    /**
     * Set tariffBaseField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfTariffBaseType $tariffBaseField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTariffBaseField(?\AppturePay\DSV\ArrayType\ArrayOfTariffBaseType $tariffBaseField = null): self
    {
        $this->tariffBaseField = $tariffBaseField;
        
        return $this;
    }
    /**
     * Get tariffSpecificationField value
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1|null
     */
    public function getTariffSpecificationField(): ?\AppturePay\DSV\StructType\TariffSpecificationType_1
    {
        return $this->tariffSpecificationField;
    }
    /**
     * Set tariffSpecificationField value
     * @param \AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecificationField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTariffSpecificationField(?\AppturePay\DSV\StructType\TariffSpecificationType_1 $tariffSpecificationField = null): self
    {
        $this->tariffSpecificationField = $tariffSpecificationField;
        
        return $this;
    }
    /**
     * Get transactionTypeField value
     * @return string|null
     */
    public function getTransactionTypeField(): ?string
    {
        return $this->transactionTypeField;
    }
    /**
     * Set transactionTypeField value
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\TransactionTypeType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $transactionTypeField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTransactionTypeField(?string $transactionTypeField = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\TransactionTypeType_1::valueIsValid($transactionTypeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\TransactionTypeType_1', is_array($transactionTypeField) ? implode(', ', $transactionTypeField) : var_export($transactionTypeField, true), implode(', ', \AppturePay\DSV\EnumType\TransactionTypeType_1::getValidValues())), __LINE__);
        }
        $this->transactionTypeField = $transactionTypeField;
        
        return $this;
    }
    /**
     * Get transactionTypeFieldSpecified value
     * @return bool|null
     */
    public function getTransactionTypeFieldSpecified(): ?bool
    {
        return $this->transactionTypeFieldSpecified;
    }
    /**
     * Set transactionTypeFieldSpecified value
     * @param bool $transactionTypeFieldSpecified
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTransactionTypeFieldSpecified(?bool $transactionTypeFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($transactionTypeFieldSpecified) && !is_bool($transactionTypeFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($transactionTypeFieldSpecified, true), gettype($transactionTypeFieldSpecified)), __LINE__);
        }
        $this->transactionTypeFieldSpecified = $transactionTypeFieldSpecified;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
    /**
     * Get vatField value
     * @return \AppturePay\DSV\StructType\VatType_1|null
     */
    public function getVatField(): ?\AppturePay\DSV\StructType\VatType_1
    {
        return $this->vatField;
    }
    /**
     * Set vatField value
     * @param \AppturePay\DSV\StructType\VatType_1 $vatField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setVatField(?\AppturePay\DSV\StructType\VatType_1 $vatField = null): self
    {
        $this->vatField = $vatField;
        
        return $this;
    }
    /**
     * Get weightCalculationField value
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1|null
     */
    public function getWeightCalculationField(): ?\AppturePay\DSV\StructType\WeightCalculationType_1
    {
        return $this->weightCalculationField;
    }
    /**
     * Set weightCalculationField value
     * @param \AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculationField
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1
     */
    public function setWeightCalculationField(?\AppturePay\DSV\StructType\WeightCalculationType_1 $weightCalculationField = null): self
    {
        $this->weightCalculationField = $weightCalculationField;
        
        return $this;
    }
}
