<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DangerousGoodsSpecificationType StructType
 * @subpackage Structs
 */
class DangerousGoodsSpecificationType extends AbstractStructBase
{
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The dangerousGoodsPackageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dangerousGoodsPackageCode = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The netWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $netWeight = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The goodsAmountInUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $goodsAmountInUnit = null;
    /**
     * The totalQuantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $totalQuantity = null;
    /**
     * The dangerousGoodsUNNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $dangerousGoodsUNNumber = null;
    /**
     * The dangerousGoodsUNSuffix
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $dangerousGoodsUNSuffix = null;
    /**
     * The unClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $unClass = null;
    /**
     * The dangerousGoodsName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dangerousGoodsName = null;
    /**
     * The technicalDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $technicalDescription = null;
    /**
     * The subsidiaryRiskClassI
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $subsidiaryRiskClassI = null;
    /**
     * The subsidiaryRiskClassII
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $subsidiaryRiskClassII = null;
    /**
     * The subsidiaryRiskClassIII
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $subsidiaryRiskClassIII = null;
    /**
     * The ADRClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ADRClass = null;
    /**
     * The ADRCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ADRCode = null;
    /**
     * The RIDClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $RIDClass = null;
    /**
     * The RIDCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $RIDCode = null;
    /**
     * The ADNClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ADNClass = null;
    /**
     * The ADNCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ADNCode = null;
    /**
     * The ICAClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ICAClass = null;
    /**
     * The ICACode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ICACode = null;
    /**
     * The IMDClass
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $IMDClass = null;
    /**
     * The IMDCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $IMDCode = null;
    /**
     * The packagingGroup
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packagingGroup = null;
    /**
     * The goodsUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $goodsUnit = null;
    /**
     * The points
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $points = null;
    /**
     * The emergencyTemperature
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $emergencyTemperature = null;
    /**
     * The controlTemperature
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $controlTemperature = null;
    /**
     * The flashPoint
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $flashPoint = null;
    /**
     * The emergencyScheduleNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $emergencyScheduleNumber = null;
    /**
     * The tunnelCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tunnelCode = null;
    /**
     * The provisions
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $provisions = null;
    /**
     * The allowedOnRailIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $allowedOnRailIndicator = null;
    /**
     * The limitedQuantityIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $limitedQuantityIndicator = null;
    /**
     * The expectedQuantityIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $expectedQuantityIndicator = null;
    /**
     * The harmfulIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $harmfulIndicator = null;
    /**
     * The wasteIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $wasteIndicator = null;
    /**
     * The emptyIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $emptyIndicator = null;
    /**
     * The marinePollutantIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $marinePollutantIndicator = null;
    /**
     * The marinePollutantNameIndicator
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $marinePollutantNameIndicator = null;
    /**
     * The maritimeFirstAidGuide
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $maritimeFirstAidGuide = null;
    /**
     * The innerPackageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $innerPackageCode = null;
    /**
     * The additionalInformation
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $additionalInformation = null;
    /**
     * The technicalNames
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TechnicalNamesType[]
     */
    protected ?array $technicalNames = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for DangerousGoodsSpecificationType
     * @uses DangerousGoodsSpecificationType::setQuantity()
     * @uses DangerousGoodsSpecificationType::setDangerousGoodsPackageCode()
     * @uses DangerousGoodsSpecificationType::setGrossWeight()
     * @uses DangerousGoodsSpecificationType::setNetWeight()
     * @uses DangerousGoodsSpecificationType::setVolume()
     * @uses DangerousGoodsSpecificationType::setGoodsAmountInUnit()
     * @uses DangerousGoodsSpecificationType::setTotalQuantity()
     * @uses DangerousGoodsSpecificationType::setDangerousGoodsUNNumber()
     * @uses DangerousGoodsSpecificationType::setDangerousGoodsUNSuffix()
     * @uses DangerousGoodsSpecificationType::setUnClass()
     * @uses DangerousGoodsSpecificationType::setDangerousGoodsName()
     * @uses DangerousGoodsSpecificationType::setTechnicalDescription()
     * @uses DangerousGoodsSpecificationType::setSubsidiaryRiskClassI()
     * @uses DangerousGoodsSpecificationType::setSubsidiaryRiskClassII()
     * @uses DangerousGoodsSpecificationType::setSubsidiaryRiskClassIII()
     * @uses DangerousGoodsSpecificationType::setADRClass()
     * @uses DangerousGoodsSpecificationType::setADRCode()
     * @uses DangerousGoodsSpecificationType::setRIDClass()
     * @uses DangerousGoodsSpecificationType::setRIDCode()
     * @uses DangerousGoodsSpecificationType::setADNClass()
     * @uses DangerousGoodsSpecificationType::setADNCode()
     * @uses DangerousGoodsSpecificationType::setICAClass()
     * @uses DangerousGoodsSpecificationType::setICACode()
     * @uses DangerousGoodsSpecificationType::setIMDClass()
     * @uses DangerousGoodsSpecificationType::setIMDCode()
     * @uses DangerousGoodsSpecificationType::setPackagingGroup()
     * @uses DangerousGoodsSpecificationType::setGoodsUnit()
     * @uses DangerousGoodsSpecificationType::setPoints()
     * @uses DangerousGoodsSpecificationType::setEmergencyTemperature()
     * @uses DangerousGoodsSpecificationType::setControlTemperature()
     * @uses DangerousGoodsSpecificationType::setFlashPoint()
     * @uses DangerousGoodsSpecificationType::setEmergencyScheduleNumber()
     * @uses DangerousGoodsSpecificationType::setTunnelCode()
     * @uses DangerousGoodsSpecificationType::setProvisions()
     * @uses DangerousGoodsSpecificationType::setAllowedOnRailIndicator()
     * @uses DangerousGoodsSpecificationType::setLimitedQuantityIndicator()
     * @uses DangerousGoodsSpecificationType::setExpectedQuantityIndicator()
     * @uses DangerousGoodsSpecificationType::setHarmfulIndicator()
     * @uses DangerousGoodsSpecificationType::setWasteIndicator()
     * @uses DangerousGoodsSpecificationType::setEmptyIndicator()
     * @uses DangerousGoodsSpecificationType::setMarinePollutantIndicator()
     * @uses DangerousGoodsSpecificationType::setMarinePollutantNameIndicator()
     * @uses DangerousGoodsSpecificationType::setMaritimeFirstAidGuide()
     * @uses DangerousGoodsSpecificationType::setInnerPackageCode()
     * @uses DangerousGoodsSpecificationType::setAdditionalInformation()
     * @uses DangerousGoodsSpecificationType::setTechnicalNames()
     * @uses DangerousGoodsSpecificationType::setType()
     * @param float $quantity
     * @param string $dangerousGoodsPackageCode
     * @param float $grossWeight
     * @param float $netWeight
     * @param float $volume
     * @param float $goodsAmountInUnit
     * @param float $totalQuantity
     * @param int $dangerousGoodsUNNumber
     * @param int $dangerousGoodsUNSuffix
     * @param string $unClass
     * @param string $dangerousGoodsName
     * @param string $technicalDescription
     * @param string $subsidiaryRiskClassI
     * @param string $subsidiaryRiskClassII
     * @param string $subsidiaryRiskClassIII
     * @param string $aDRClass
     * @param string $aDRCode
     * @param string $rIDClass
     * @param string $rIDCode
     * @param string $aDNClass
     * @param string $aDNCode
     * @param string $iCAClass
     * @param string $iCACode
     * @param string $iMDClass
     * @param string $iMDCode
     * @param string $packagingGroup
     * @param int $goodsUnit
     * @param float $points
     * @param string $emergencyTemperature
     * @param string $controlTemperature
     * @param string $flashPoint
     * @param string $emergencyScheduleNumber
     * @param string $tunnelCode
     * @param string $provisions
     * @param string $allowedOnRailIndicator
     * @param string $limitedQuantityIndicator
     * @param string $expectedQuantityIndicator
     * @param string $harmfulIndicator
     * @param string $wasteIndicator
     * @param string $emptyIndicator
     * @param string $marinePollutantIndicator
     * @param string $marinePollutantNameIndicator
     * @param string $maritimeFirstAidGuide
     * @param string $innerPackageCode
     * @param string $additionalInformation
     * @param \AppturePay\DSV\StructType\TechnicalNamesType[] $technicalNames
     * @param string $type
     */
    public function __construct(?float $quantity = null, ?string $dangerousGoodsPackageCode = null, ?float $grossWeight = null, ?float $netWeight = null, ?float $volume = null, ?float $goodsAmountInUnit = null, ?float $totalQuantity = null, ?int $dangerousGoodsUNNumber = null, ?int $dangerousGoodsUNSuffix = null, ?string $unClass = null, ?string $dangerousGoodsName = null, ?string $technicalDescription = null, ?string $subsidiaryRiskClassI = null, ?string $subsidiaryRiskClassII = null, ?string $subsidiaryRiskClassIII = null, ?string $aDRClass = null, ?string $aDRCode = null, ?string $rIDClass = null, ?string $rIDCode = null, ?string $aDNClass = null, ?string $aDNCode = null, ?string $iCAClass = null, ?string $iCACode = null, ?string $iMDClass = null, ?string $iMDCode = null, ?string $packagingGroup = null, ?int $goodsUnit = null, ?float $points = null, ?string $emergencyTemperature = null, ?string $controlTemperature = null, ?string $flashPoint = null, ?string $emergencyScheduleNumber = null, ?string $tunnelCode = null, ?string $provisions = null, ?string $allowedOnRailIndicator = null, ?string $limitedQuantityIndicator = null, ?string $expectedQuantityIndicator = null, ?string $harmfulIndicator = null, ?string $wasteIndicator = null, ?string $emptyIndicator = null, ?string $marinePollutantIndicator = null, ?string $marinePollutantNameIndicator = null, ?string $maritimeFirstAidGuide = null, ?string $innerPackageCode = null, ?string $additionalInformation = null, ?array $technicalNames = null, ?string $type = null)
    {
        $this
            ->setQuantity($quantity)
            ->setDangerousGoodsPackageCode($dangerousGoodsPackageCode)
            ->setGrossWeight($grossWeight)
            ->setNetWeight($netWeight)
            ->setVolume($volume)
            ->setGoodsAmountInUnit($goodsAmountInUnit)
            ->setTotalQuantity($totalQuantity)
            ->setDangerousGoodsUNNumber($dangerousGoodsUNNumber)
            ->setDangerousGoodsUNSuffix($dangerousGoodsUNSuffix)
            ->setUnClass($unClass)
            ->setDangerousGoodsName($dangerousGoodsName)
            ->setTechnicalDescription($technicalDescription)
            ->setSubsidiaryRiskClassI($subsidiaryRiskClassI)
            ->setSubsidiaryRiskClassII($subsidiaryRiskClassII)
            ->setSubsidiaryRiskClassIII($subsidiaryRiskClassIII)
            ->setADRClass($aDRClass)
            ->setADRCode($aDRCode)
            ->setRIDClass($rIDClass)
            ->setRIDCode($rIDCode)
            ->setADNClass($aDNClass)
            ->setADNCode($aDNCode)
            ->setICAClass($iCAClass)
            ->setICACode($iCACode)
            ->setIMDClass($iMDClass)
            ->setIMDCode($iMDCode)
            ->setPackagingGroup($packagingGroup)
            ->setGoodsUnit($goodsUnit)
            ->setPoints($points)
            ->setEmergencyTemperature($emergencyTemperature)
            ->setControlTemperature($controlTemperature)
            ->setFlashPoint($flashPoint)
            ->setEmergencyScheduleNumber($emergencyScheduleNumber)
            ->setTunnelCode($tunnelCode)
            ->setProvisions($provisions)
            ->setAllowedOnRailIndicator($allowedOnRailIndicator)
            ->setLimitedQuantityIndicator($limitedQuantityIndicator)
            ->setExpectedQuantityIndicator($expectedQuantityIndicator)
            ->setHarmfulIndicator($harmfulIndicator)
            ->setWasteIndicator($wasteIndicator)
            ->setEmptyIndicator($emptyIndicator)
            ->setMarinePollutantIndicator($marinePollutantIndicator)
            ->setMarinePollutantNameIndicator($marinePollutantNameIndicator)
            ->setMaritimeFirstAidGuide($maritimeFirstAidGuide)
            ->setInnerPackageCode($innerPackageCode)
            ->setAdditionalInformation($additionalInformation)
            ->setTechnicalNames($technicalNames)
            ->setType($type);
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get dangerousGoodsPackageCode value
     * @return string|null
     */
    public function getDangerousGoodsPackageCode(): ?string
    {
        return $this->dangerousGoodsPackageCode;
    }
    /**
     * Set dangerousGoodsPackageCode value
     * @param string $dangerousGoodsPackageCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setDangerousGoodsPackageCode(?string $dangerousGoodsPackageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($dangerousGoodsPackageCode) && !is_string($dangerousGoodsPackageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dangerousGoodsPackageCode, true), gettype($dangerousGoodsPackageCode)), __LINE__);
        }
        $this->dangerousGoodsPackageCode = $dangerousGoodsPackageCode;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get netWeight value
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    /**
     * Set netWeight value
     * @param float $netWeight
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setNetWeight(?float $netWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($netWeight) && !(is_float($netWeight) || is_numeric($netWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($netWeight, true), gettype($netWeight)), __LINE__);
        }
        $this->netWeight = $netWeight;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get goodsAmountInUnit value
     * @return float|null
     */
    public function getGoodsAmountInUnit(): ?float
    {
        return $this->goodsAmountInUnit;
    }
    /**
     * Set goodsAmountInUnit value
     * @param float $goodsAmountInUnit
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setGoodsAmountInUnit(?float $goodsAmountInUnit = null): self
    {
        // validation for constraint: float
        if (!is_null($goodsAmountInUnit) && !(is_float($goodsAmountInUnit) || is_numeric($goodsAmountInUnit))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsAmountInUnit, true), gettype($goodsAmountInUnit)), __LINE__);
        }
        $this->goodsAmountInUnit = $goodsAmountInUnit;
        
        return $this;
    }
    /**
     * Get totalQuantity value
     * @return float|null
     */
    public function getTotalQuantity(): ?float
    {
        return $this->totalQuantity;
    }
    /**
     * Set totalQuantity value
     * @param float $totalQuantity
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setTotalQuantity(?float $totalQuantity = null): self
    {
        // validation for constraint: float
        if (!is_null($totalQuantity) && !(is_float($totalQuantity) || is_numeric($totalQuantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($totalQuantity, true), gettype($totalQuantity)), __LINE__);
        }
        $this->totalQuantity = $totalQuantity;
        
        return $this;
    }
    /**
     * Get dangerousGoodsUNNumber value
     * @return int|null
     */
    public function getDangerousGoodsUNNumber(): ?int
    {
        return $this->dangerousGoodsUNNumber;
    }
    /**
     * Set dangerousGoodsUNNumber value
     * @param int $dangerousGoodsUNNumber
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setDangerousGoodsUNNumber(?int $dangerousGoodsUNNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($dangerousGoodsUNNumber) && !(is_int($dangerousGoodsUNNumber) || ctype_digit($dangerousGoodsUNNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dangerousGoodsUNNumber, true), gettype($dangerousGoodsUNNumber)), __LINE__);
        }
        $this->dangerousGoodsUNNumber = $dangerousGoodsUNNumber;
        
        return $this;
    }
    /**
     * Get dangerousGoodsUNSuffix value
     * @return int|null
     */
    public function getDangerousGoodsUNSuffix(): ?int
    {
        return $this->dangerousGoodsUNSuffix;
    }
    /**
     * Set dangerousGoodsUNSuffix value
     * @param int $dangerousGoodsUNSuffix
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setDangerousGoodsUNSuffix(?int $dangerousGoodsUNSuffix = null): self
    {
        // validation for constraint: int
        if (!is_null($dangerousGoodsUNSuffix) && !(is_int($dangerousGoodsUNSuffix) || ctype_digit($dangerousGoodsUNSuffix))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dangerousGoodsUNSuffix, true), gettype($dangerousGoodsUNSuffix)), __LINE__);
        }
        $this->dangerousGoodsUNSuffix = $dangerousGoodsUNSuffix;
        
        return $this;
    }
    /**
     * Get unClass value
     * @return string|null
     */
    public function getUnClass(): ?string
    {
        return $this->unClass;
    }
    /**
     * Set unClass value
     * @param string $unClass
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setUnClass(?string $unClass = null): self
    {
        // validation for constraint: string
        if (!is_null($unClass) && !is_string($unClass)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($unClass, true), gettype($unClass)), __LINE__);
        }
        $this->unClass = $unClass;
        
        return $this;
    }
    /**
     * Get dangerousGoodsName value
     * @return string|null
     */
    public function getDangerousGoodsName(): ?string
    {
        return $this->dangerousGoodsName;
    }
    /**
     * Set dangerousGoodsName value
     * @param string $dangerousGoodsName
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setDangerousGoodsName(?string $dangerousGoodsName = null): self
    {
        // validation for constraint: string
        if (!is_null($dangerousGoodsName) && !is_string($dangerousGoodsName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dangerousGoodsName, true), gettype($dangerousGoodsName)), __LINE__);
        }
        $this->dangerousGoodsName = $dangerousGoodsName;
        
        return $this;
    }
    /**
     * Get technicalDescription value
     * @return string|null
     */
    public function getTechnicalDescription(): ?string
    {
        return $this->technicalDescription;
    }
    /**
     * Set technicalDescription value
     * @param string $technicalDescription
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setTechnicalDescription(?string $technicalDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($technicalDescription) && !is_string($technicalDescription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($technicalDescription, true), gettype($technicalDescription)), __LINE__);
        }
        $this->technicalDescription = $technicalDescription;
        
        return $this;
    }
    /**
     * Get subsidiaryRiskClassI value
     * @return string|null
     */
    public function getSubsidiaryRiskClassI(): ?string
    {
        return $this->subsidiaryRiskClassI;
    }
    /**
     * Set subsidiaryRiskClassI value
     * @param string $subsidiaryRiskClassI
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setSubsidiaryRiskClassI(?string $subsidiaryRiskClassI = null): self
    {
        // validation for constraint: string
        if (!is_null($subsidiaryRiskClassI) && !is_string($subsidiaryRiskClassI)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subsidiaryRiskClassI, true), gettype($subsidiaryRiskClassI)), __LINE__);
        }
        $this->subsidiaryRiskClassI = $subsidiaryRiskClassI;
        
        return $this;
    }
    /**
     * Get subsidiaryRiskClassII value
     * @return string|null
     */
    public function getSubsidiaryRiskClassII(): ?string
    {
        return $this->subsidiaryRiskClassII;
    }
    /**
     * Set subsidiaryRiskClassII value
     * @param string $subsidiaryRiskClassII
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setSubsidiaryRiskClassII(?string $subsidiaryRiskClassII = null): self
    {
        // validation for constraint: string
        if (!is_null($subsidiaryRiskClassII) && !is_string($subsidiaryRiskClassII)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subsidiaryRiskClassII, true), gettype($subsidiaryRiskClassII)), __LINE__);
        }
        $this->subsidiaryRiskClassII = $subsidiaryRiskClassII;
        
        return $this;
    }
    /**
     * Get subsidiaryRiskClassIII value
     * @return string|null
     */
    public function getSubsidiaryRiskClassIII(): ?string
    {
        return $this->subsidiaryRiskClassIII;
    }
    /**
     * Set subsidiaryRiskClassIII value
     * @param string $subsidiaryRiskClassIII
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setSubsidiaryRiskClassIII(?string $subsidiaryRiskClassIII = null): self
    {
        // validation for constraint: string
        if (!is_null($subsidiaryRiskClassIII) && !is_string($subsidiaryRiskClassIII)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subsidiaryRiskClassIII, true), gettype($subsidiaryRiskClassIII)), __LINE__);
        }
        $this->subsidiaryRiskClassIII = $subsidiaryRiskClassIII;
        
        return $this;
    }
    /**
     * Get ADRClass value
     * @return string|null
     */
    public function getADRClass(): ?string
    {
        return $this->ADRClass;
    }
    /**
     * Set ADRClass value
     * @param string $aDRClass
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setADRClass(?string $aDRClass = null): self
    {
        // validation for constraint: string
        if (!is_null($aDRClass) && !is_string($aDRClass)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aDRClass, true), gettype($aDRClass)), __LINE__);
        }
        $this->ADRClass = $aDRClass;
        
        return $this;
    }
    /**
     * Get ADRCode value
     * @return string|null
     */
    public function getADRCode(): ?string
    {
        return $this->ADRCode;
    }
    /**
     * Set ADRCode value
     * @param string $aDRCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setADRCode(?string $aDRCode = null): self
    {
        // validation for constraint: string
        if (!is_null($aDRCode) && !is_string($aDRCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aDRCode, true), gettype($aDRCode)), __LINE__);
        }
        $this->ADRCode = $aDRCode;
        
        return $this;
    }
    /**
     * Get RIDClass value
     * @return string|null
     */
    public function getRIDClass(): ?string
    {
        return $this->RIDClass;
    }
    /**
     * Set RIDClass value
     * @param string $rIDClass
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setRIDClass(?string $rIDClass = null): self
    {
        // validation for constraint: string
        if (!is_null($rIDClass) && !is_string($rIDClass)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($rIDClass, true), gettype($rIDClass)), __LINE__);
        }
        $this->RIDClass = $rIDClass;
        
        return $this;
    }
    /**
     * Get RIDCode value
     * @return string|null
     */
    public function getRIDCode(): ?string
    {
        return $this->RIDCode;
    }
    /**
     * Set RIDCode value
     * @param string $rIDCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setRIDCode(?string $rIDCode = null): self
    {
        // validation for constraint: string
        if (!is_null($rIDCode) && !is_string($rIDCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($rIDCode, true), gettype($rIDCode)), __LINE__);
        }
        $this->RIDCode = $rIDCode;
        
        return $this;
    }
    /**
     * Get ADNClass value
     * @return string|null
     */
    public function getADNClass(): ?string
    {
        return $this->ADNClass;
    }
    /**
     * Set ADNClass value
     * @param string $aDNClass
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setADNClass(?string $aDNClass = null): self
    {
        // validation for constraint: string
        if (!is_null($aDNClass) && !is_string($aDNClass)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aDNClass, true), gettype($aDNClass)), __LINE__);
        }
        $this->ADNClass = $aDNClass;
        
        return $this;
    }
    /**
     * Get ADNCode value
     * @return string|null
     */
    public function getADNCode(): ?string
    {
        return $this->ADNCode;
    }
    /**
     * Set ADNCode value
     * @param string $aDNCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setADNCode(?string $aDNCode = null): self
    {
        // validation for constraint: string
        if (!is_null($aDNCode) && !is_string($aDNCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aDNCode, true), gettype($aDNCode)), __LINE__);
        }
        $this->ADNCode = $aDNCode;
        
        return $this;
    }
    /**
     * Get ICAClass value
     * @return string|null
     */
    public function getICAClass(): ?string
    {
        return $this->ICAClass;
    }
    /**
     * Set ICAClass value
     * @param string $iCAClass
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setICAClass(?string $iCAClass = null): self
    {
        // validation for constraint: string
        if (!is_null($iCAClass) && !is_string($iCAClass)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iCAClass, true), gettype($iCAClass)), __LINE__);
        }
        $this->ICAClass = $iCAClass;
        
        return $this;
    }
    /**
     * Get ICACode value
     * @return string|null
     */
    public function getICACode(): ?string
    {
        return $this->ICACode;
    }
    /**
     * Set ICACode value
     * @param string $iCACode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setICACode(?string $iCACode = null): self
    {
        // validation for constraint: string
        if (!is_null($iCACode) && !is_string($iCACode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iCACode, true), gettype($iCACode)), __LINE__);
        }
        $this->ICACode = $iCACode;
        
        return $this;
    }
    /**
     * Get IMDClass value
     * @return string|null
     */
    public function getIMDClass(): ?string
    {
        return $this->IMDClass;
    }
    /**
     * Set IMDClass value
     * @param string $iMDClass
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setIMDClass(?string $iMDClass = null): self
    {
        // validation for constraint: string
        if (!is_null($iMDClass) && !is_string($iMDClass)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iMDClass, true), gettype($iMDClass)), __LINE__);
        }
        $this->IMDClass = $iMDClass;
        
        return $this;
    }
    /**
     * Get IMDCode value
     * @return string|null
     */
    public function getIMDCode(): ?string
    {
        return $this->IMDCode;
    }
    /**
     * Set IMDCode value
     * @param string $iMDCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setIMDCode(?string $iMDCode = null): self
    {
        // validation for constraint: string
        if (!is_null($iMDCode) && !is_string($iMDCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iMDCode, true), gettype($iMDCode)), __LINE__);
        }
        $this->IMDCode = $iMDCode;
        
        return $this;
    }
    /**
     * Get packagingGroup value
     * @return string|null
     */
    public function getPackagingGroup(): ?string
    {
        return $this->packagingGroup;
    }
    /**
     * Set packagingGroup value
     * @param string $packagingGroup
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setPackagingGroup(?string $packagingGroup = null): self
    {
        // validation for constraint: string
        if (!is_null($packagingGroup) && !is_string($packagingGroup)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packagingGroup, true), gettype($packagingGroup)), __LINE__);
        }
        $this->packagingGroup = $packagingGroup;
        
        return $this;
    }
    /**
     * Get goodsUnit value
     * @return int|null
     */
    public function getGoodsUnit(): ?int
    {
        return $this->goodsUnit;
    }
    /**
     * Set goodsUnit value
     * @param int $goodsUnit
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setGoodsUnit(?int $goodsUnit = null): self
    {
        // validation for constraint: int
        if (!is_null($goodsUnit) && !(is_int($goodsUnit) || ctype_digit($goodsUnit))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($goodsUnit, true), gettype($goodsUnit)), __LINE__);
        }
        $this->goodsUnit = $goodsUnit;
        
        return $this;
    }
    /**
     * Get points value
     * @return float|null
     */
    public function getPoints(): ?float
    {
        return $this->points;
    }
    /**
     * Set points value
     * @param float $points
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setPoints(?float $points = null): self
    {
        // validation for constraint: float
        if (!is_null($points) && !(is_float($points) || is_numeric($points))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($points, true), gettype($points)), __LINE__);
        }
        $this->points = $points;
        
        return $this;
    }
    /**
     * Get emergencyTemperature value
     * @return string|null
     */
    public function getEmergencyTemperature(): ?string
    {
        return $this->emergencyTemperature;
    }
    /**
     * Set emergencyTemperature value
     * @param string $emergencyTemperature
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setEmergencyTemperature(?string $emergencyTemperature = null): self
    {
        // validation for constraint: string
        if (!is_null($emergencyTemperature) && !is_string($emergencyTemperature)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($emergencyTemperature, true), gettype($emergencyTemperature)), __LINE__);
        }
        $this->emergencyTemperature = $emergencyTemperature;
        
        return $this;
    }
    /**
     * Get controlTemperature value
     * @return string|null
     */
    public function getControlTemperature(): ?string
    {
        return $this->controlTemperature;
    }
    /**
     * Set controlTemperature value
     * @param string $controlTemperature
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setControlTemperature(?string $controlTemperature = null): self
    {
        // validation for constraint: string
        if (!is_null($controlTemperature) && !is_string($controlTemperature)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($controlTemperature, true), gettype($controlTemperature)), __LINE__);
        }
        $this->controlTemperature = $controlTemperature;
        
        return $this;
    }
    /**
     * Get flashPoint value
     * @return string|null
     */
    public function getFlashPoint(): ?string
    {
        return $this->flashPoint;
    }
    /**
     * Set flashPoint value
     * @param string $flashPoint
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setFlashPoint(?string $flashPoint = null): self
    {
        // validation for constraint: string
        if (!is_null($flashPoint) && !is_string($flashPoint)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($flashPoint, true), gettype($flashPoint)), __LINE__);
        }
        $this->flashPoint = $flashPoint;
        
        return $this;
    }
    /**
     * Get emergencyScheduleNumber value
     * @return string|null
     */
    public function getEmergencyScheduleNumber(): ?string
    {
        return $this->emergencyScheduleNumber;
    }
    /**
     * Set emergencyScheduleNumber value
     * @param string $emergencyScheduleNumber
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setEmergencyScheduleNumber(?string $emergencyScheduleNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($emergencyScheduleNumber) && !is_string($emergencyScheduleNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($emergencyScheduleNumber, true), gettype($emergencyScheduleNumber)), __LINE__);
        }
        $this->emergencyScheduleNumber = $emergencyScheduleNumber;
        
        return $this;
    }
    /**
     * Get tunnelCode value
     * @return string|null
     */
    public function getTunnelCode(): ?string
    {
        return $this->tunnelCode;
    }
    /**
     * Set tunnelCode value
     * @param string $tunnelCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setTunnelCode(?string $tunnelCode = null): self
    {
        // validation for constraint: string
        if (!is_null($tunnelCode) && !is_string($tunnelCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tunnelCode, true), gettype($tunnelCode)), __LINE__);
        }
        $this->tunnelCode = $tunnelCode;
        
        return $this;
    }
    /**
     * Get provisions value
     * @return string|null
     */
    public function getProvisions(): ?string
    {
        return $this->provisions;
    }
    /**
     * Set provisions value
     * @param string $provisions
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setProvisions(?string $provisions = null): self
    {
        // validation for constraint: string
        if (!is_null($provisions) && !is_string($provisions)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($provisions, true), gettype($provisions)), __LINE__);
        }
        $this->provisions = $provisions;
        
        return $this;
    }
    /**
     * Get allowedOnRailIndicator value
     * @return string|null
     */
    public function getAllowedOnRailIndicator(): ?string
    {
        return $this->allowedOnRailIndicator;
    }
    /**
     * Set allowedOnRailIndicator value
     * @param string $allowedOnRailIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setAllowedOnRailIndicator(?string $allowedOnRailIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($allowedOnRailIndicator) && !is_string($allowedOnRailIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($allowedOnRailIndicator, true), gettype($allowedOnRailIndicator)), __LINE__);
        }
        $this->allowedOnRailIndicator = $allowedOnRailIndicator;
        
        return $this;
    }
    /**
     * Get limitedQuantityIndicator value
     * @return string|null
     */
    public function getLimitedQuantityIndicator(): ?string
    {
        return $this->limitedQuantityIndicator;
    }
    /**
     * Set limitedQuantityIndicator value
     * @param string $limitedQuantityIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setLimitedQuantityIndicator(?string $limitedQuantityIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($limitedQuantityIndicator) && !is_string($limitedQuantityIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($limitedQuantityIndicator, true), gettype($limitedQuantityIndicator)), __LINE__);
        }
        $this->limitedQuantityIndicator = $limitedQuantityIndicator;
        
        return $this;
    }
    /**
     * Get expectedQuantityIndicator value
     * @return string|null
     */
    public function getExpectedQuantityIndicator(): ?string
    {
        return $this->expectedQuantityIndicator;
    }
    /**
     * Set expectedQuantityIndicator value
     * @param string $expectedQuantityIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setExpectedQuantityIndicator(?string $expectedQuantityIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($expectedQuantityIndicator) && !is_string($expectedQuantityIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expectedQuantityIndicator, true), gettype($expectedQuantityIndicator)), __LINE__);
        }
        $this->expectedQuantityIndicator = $expectedQuantityIndicator;
        
        return $this;
    }
    /**
     * Get harmfulIndicator value
     * @return string|null
     */
    public function getHarmfulIndicator(): ?string
    {
        return $this->harmfulIndicator;
    }
    /**
     * Set harmfulIndicator value
     * @param string $harmfulIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setHarmfulIndicator(?string $harmfulIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($harmfulIndicator) && !is_string($harmfulIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($harmfulIndicator, true), gettype($harmfulIndicator)), __LINE__);
        }
        $this->harmfulIndicator = $harmfulIndicator;
        
        return $this;
    }
    /**
     * Get wasteIndicator value
     * @return string|null
     */
    public function getWasteIndicator(): ?string
    {
        return $this->wasteIndicator;
    }
    /**
     * Set wasteIndicator value
     * @param string $wasteIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setWasteIndicator(?string $wasteIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($wasteIndicator) && !is_string($wasteIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wasteIndicator, true), gettype($wasteIndicator)), __LINE__);
        }
        $this->wasteIndicator = $wasteIndicator;
        
        return $this;
    }
    /**
     * Get emptyIndicator value
     * @return string|null
     */
    public function getEmptyIndicator(): ?string
    {
        return $this->emptyIndicator;
    }
    /**
     * Set emptyIndicator value
     * @param string $emptyIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setEmptyIndicator(?string $emptyIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($emptyIndicator) && !is_string($emptyIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($emptyIndicator, true), gettype($emptyIndicator)), __LINE__);
        }
        $this->emptyIndicator = $emptyIndicator;
        
        return $this;
    }
    /**
     * Get marinePollutantIndicator value
     * @return string|null
     */
    public function getMarinePollutantIndicator(): ?string
    {
        return $this->marinePollutantIndicator;
    }
    /**
     * Set marinePollutantIndicator value
     * @param string $marinePollutantIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setMarinePollutantIndicator(?string $marinePollutantIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($marinePollutantIndicator) && !is_string($marinePollutantIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($marinePollutantIndicator, true), gettype($marinePollutantIndicator)), __LINE__);
        }
        $this->marinePollutantIndicator = $marinePollutantIndicator;
        
        return $this;
    }
    /**
     * Get marinePollutantNameIndicator value
     * @return string|null
     */
    public function getMarinePollutantNameIndicator(): ?string
    {
        return $this->marinePollutantNameIndicator;
    }
    /**
     * Set marinePollutantNameIndicator value
     * @param string $marinePollutantNameIndicator
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setMarinePollutantNameIndicator(?string $marinePollutantNameIndicator = null): self
    {
        // validation for constraint: string
        if (!is_null($marinePollutantNameIndicator) && !is_string($marinePollutantNameIndicator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($marinePollutantNameIndicator, true), gettype($marinePollutantNameIndicator)), __LINE__);
        }
        $this->marinePollutantNameIndicator = $marinePollutantNameIndicator;
        
        return $this;
    }
    /**
     * Get maritimeFirstAidGuide value
     * @return string|null
     */
    public function getMaritimeFirstAidGuide(): ?string
    {
        return $this->maritimeFirstAidGuide;
    }
    /**
     * Set maritimeFirstAidGuide value
     * @param string $maritimeFirstAidGuide
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setMaritimeFirstAidGuide(?string $maritimeFirstAidGuide = null): self
    {
        // validation for constraint: string
        if (!is_null($maritimeFirstAidGuide) && !is_string($maritimeFirstAidGuide)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($maritimeFirstAidGuide, true), gettype($maritimeFirstAidGuide)), __LINE__);
        }
        $this->maritimeFirstAidGuide = $maritimeFirstAidGuide;
        
        return $this;
    }
    /**
     * Get innerPackageCode value
     * @return string|null
     */
    public function getInnerPackageCode(): ?string
    {
        return $this->innerPackageCode;
    }
    /**
     * Set innerPackageCode value
     * @param string $innerPackageCode
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setInnerPackageCode(?string $innerPackageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($innerPackageCode) && !is_string($innerPackageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($innerPackageCode, true), gettype($innerPackageCode)), __LINE__);
        }
        $this->innerPackageCode = $innerPackageCode;
        
        return $this;
    }
    /**
     * Get additionalInformation value
     * @return string|null
     */
    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }
    /**
     * Set additionalInformation value
     * @param string $additionalInformation
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setAdditionalInformation(?string $additionalInformation = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalInformation) && !is_string($additionalInformation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalInformation, true), gettype($additionalInformation)), __LINE__);
        }
        $this->additionalInformation = $additionalInformation;
        
        return $this;
    }
    /**
     * Get technicalNames value
     * @return \AppturePay\DSV\StructType\TechnicalNamesType[]
     */
    public function getTechnicalNames(): ?array
    {
        return $this->technicalNames;
    }
    /**
     * This method is responsible for validating the values passed to the setTechnicalNames method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTechnicalNames method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTechnicalNamesForArrayConstraintsFromSetTechnicalNames(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $dangerousGoodsSpecificationTypeTechnicalNamesItem) {
            // validation for constraint: itemType
            if (!$dangerousGoodsSpecificationTypeTechnicalNamesItem instanceof \AppturePay\DSV\StructType\TechnicalNamesType) {
                $invalidValues[] = is_object($dangerousGoodsSpecificationTypeTechnicalNamesItem) ? get_class($dangerousGoodsSpecificationTypeTechnicalNamesItem) : sprintf('%s(%s)', gettype($dangerousGoodsSpecificationTypeTechnicalNamesItem), var_export($dangerousGoodsSpecificationTypeTechnicalNamesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The technicalNames property can only contain items of type \AppturePay\DSV\StructType\TechnicalNamesType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set technicalNames value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TechnicalNamesType[] $technicalNames
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setTechnicalNames(?array $technicalNames = null): self
    {
        // validation for constraint: array
        if ('' !== ($technicalNamesArrayErrorMessage = self::validateTechnicalNamesForArrayConstraintsFromSetTechnicalNames($technicalNames))) {
            throw new InvalidArgumentException($technicalNamesArrayErrorMessage, __LINE__);
        }
        $this->technicalNames = $technicalNames;
        
        return $this;
    }
    /**
     * Add item to technicalNames value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TechnicalNamesType $item
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function addToTechnicalNames(\AppturePay\DSV\StructType\TechnicalNamesType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TechnicalNamesType) {
            throw new InvalidArgumentException(sprintf('The technicalNames property can only contain items of type \AppturePay\DSV\StructType\TechnicalNamesType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->technicalNames[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
