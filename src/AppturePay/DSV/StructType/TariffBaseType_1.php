<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TariffBaseType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:TariffBaseType
 * @subpackage Structs
 */
class TariffBaseType_1 extends AbstractStructBase
{
    /**
     * The base
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $base = null;
    /**
     * The round
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $round = null;
    /**
     * The rate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $rate = null;
    /**
     * The unit
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $unit = null;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $value = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The percentage
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $percentage = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The baseField
     * @var float|null
     */
    protected ?float $baseField = null;
    /**
     * The baseFieldSpecified
     * @var bool|null
     */
    protected ?bool $baseFieldSpecified = null;
    /**
     * The currencyCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $currencyCodeField = null;
    /**
     * The percentageField
     * @var float|null
     */
    protected ?float $percentageField = null;
    /**
     * The percentageFieldSpecified
     * @var bool|null
     */
    protected ?bool $percentageFieldSpecified = null;
    /**
     * The rateField
     * @var float|null
     */
    protected ?float $rateField = null;
    /**
     * The rateFieldSpecified
     * @var bool|null
     */
    protected ?bool $rateFieldSpecified = null;
    /**
     * The roundField
     * @var float|null
     */
    protected ?float $roundField = null;
    /**
     * The roundFieldSpecified
     * @var bool|null
     */
    protected ?bool $roundFieldSpecified = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * The unitField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $unitField = null;
    /**
     * The valueField
     * @var float|null
     */
    protected ?float $valueField = null;
    /**
     * The valueFieldSpecified
     * @var bool|null
     */
    protected ?bool $valueFieldSpecified = null;
    /**
     * Constructor method for TariffBaseType
     * @uses TariffBaseType_1::setBase()
     * @uses TariffBaseType_1::setRound()
     * @uses TariffBaseType_1::setRate()
     * @uses TariffBaseType_1::setUnit()
     * @uses TariffBaseType_1::setValue()
     * @uses TariffBaseType_1::setCurrencyCode()
     * @uses TariffBaseType_1::setPercentage()
     * @uses TariffBaseType_1::setType()
     * @uses TariffBaseType_1::setBaseField()
     * @uses TariffBaseType_1::setBaseFieldSpecified()
     * @uses TariffBaseType_1::setCurrencyCodeField()
     * @uses TariffBaseType_1::setPercentageField()
     * @uses TariffBaseType_1::setPercentageFieldSpecified()
     * @uses TariffBaseType_1::setRateField()
     * @uses TariffBaseType_1::setRateFieldSpecified()
     * @uses TariffBaseType_1::setRoundField()
     * @uses TariffBaseType_1::setRoundFieldSpecified()
     * @uses TariffBaseType_1::setTypeField()
     * @uses TariffBaseType_1::setUnitField()
     * @uses TariffBaseType_1::setValueField()
     * @uses TariffBaseType_1::setValueFieldSpecified()
     * @param float $base
     * @param float $round
     * @param float $rate
     * @param int $unit
     * @param float $value
     * @param string $currencyCode
     * @param float $percentage
     * @param string $type
     * @param float $baseField
     * @param bool $baseFieldSpecified
     * @param string $currencyCodeField
     * @param float $percentageField
     * @param bool $percentageFieldSpecified
     * @param float $rateField
     * @param bool $rateFieldSpecified
     * @param float $roundField
     * @param bool $roundFieldSpecified
     * @param string $typeField
     * @param string $unitField
     * @param float $valueField
     * @param bool $valueFieldSpecified
     */
    public function __construct(?float $base = null, ?float $round = null, ?float $rate = null, ?int $unit = null, ?float $value = null, ?string $currencyCode = null, ?float $percentage = null, ?string $type = null, ?float $baseField = null, ?bool $baseFieldSpecified = null, ?string $currencyCodeField = null, ?float $percentageField = null, ?bool $percentageFieldSpecified = null, ?float $rateField = null, ?bool $rateFieldSpecified = null, ?float $roundField = null, ?bool $roundFieldSpecified = null, ?string $typeField = null, ?string $unitField = null, ?float $valueField = null, ?bool $valueFieldSpecified = null)
    {
        $this
            ->setBase($base)
            ->setRound($round)
            ->setRate($rate)
            ->setUnit($unit)
            ->setValue($value)
            ->setCurrencyCode($currencyCode)
            ->setPercentage($percentage)
            ->setType($type)
            ->setBaseField($baseField)
            ->setBaseFieldSpecified($baseFieldSpecified)
            ->setCurrencyCodeField($currencyCodeField)
            ->setPercentageField($percentageField)
            ->setPercentageFieldSpecified($percentageFieldSpecified)
            ->setRateField($rateField)
            ->setRateFieldSpecified($rateFieldSpecified)
            ->setRoundField($roundField)
            ->setRoundFieldSpecified($roundFieldSpecified)
            ->setTypeField($typeField)
            ->setUnitField($unitField)
            ->setValueField($valueField)
            ->setValueFieldSpecified($valueFieldSpecified);
    }
    /**
     * Get base value
     * @return float|null
     */
    public function getBase(): ?float
    {
        return $this->base;
    }
    /**
     * Set base value
     * @param float $base
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setBase(?float $base = null): self
    {
        // validation for constraint: float
        if (!is_null($base) && !(is_float($base) || is_numeric($base))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($base, true), gettype($base)), __LINE__);
        }
        $this->base = $base;
        
        return $this;
    }
    /**
     * Get round value
     * @return float|null
     */
    public function getRound(): ?float
    {
        return $this->round;
    }
    /**
     * Set round value
     * @param float $round
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setRound(?float $round = null): self
    {
        // validation for constraint: float
        if (!is_null($round) && !(is_float($round) || is_numeric($round))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($round, true), gettype($round)), __LINE__);
        }
        $this->round = $round;
        
        return $this;
    }
    /**
     * Get rate value
     * @return float|null
     */
    public function getRate(): ?float
    {
        return $this->rate;
    }
    /**
     * Set rate value
     * @param float $rate
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setRate(?float $rate = null): self
    {
        // validation for constraint: float
        if (!is_null($rate) && !(is_float($rate) || is_numeric($rate))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($rate, true), gettype($rate)), __LINE__);
        }
        $this->rate = $rate;
        
        return $this;
    }
    /**
     * Get unit value
     * @return int|null
     */
    public function getUnit(): ?int
    {
        return $this->unit;
    }
    /**
     * Set unit value
     * @param int $unit
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setUnit(?int $unit = null): self
    {
        // validation for constraint: int
        if (!is_null($unit) && !(is_int($unit) || ctype_digit($unit))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($unit, true), gettype($unit)), __LINE__);
        }
        $this->unit = $unit;
        
        return $this;
    }
    /**
     * Get value value
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param float $value
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setValue(?float $value = null): self
    {
        // validation for constraint: float
        if (!is_null($value) && !(is_float($value) || is_numeric($value))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get percentage value
     * @return float|null
     */
    public function getPercentage(): ?float
    {
        return $this->percentage;
    }
    /**
     * Set percentage value
     * @param float $percentage
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setPercentage(?float $percentage = null): self
    {
        // validation for constraint: float
        if (!is_null($percentage) && !(is_float($percentage) || is_numeric($percentage))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($percentage, true), gettype($percentage)), __LINE__);
        }
        $this->percentage = $percentage;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get baseField value
     * @return float|null
     */
    public function getBaseField(): ?float
    {
        return $this->baseField;
    }
    /**
     * Set baseField value
     * @param float $baseField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setBaseField(?float $baseField = null): self
    {
        // validation for constraint: float
        if (!is_null($baseField) && !(is_float($baseField) || is_numeric($baseField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($baseField, true), gettype($baseField)), __LINE__);
        }
        $this->baseField = $baseField;
        
        return $this;
    }
    /**
     * Get baseFieldSpecified value
     * @return bool|null
     */
    public function getBaseFieldSpecified(): ?bool
    {
        return $this->baseFieldSpecified;
    }
    /**
     * Set baseFieldSpecified value
     * @param bool $baseFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setBaseFieldSpecified(?bool $baseFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($baseFieldSpecified) && !is_bool($baseFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($baseFieldSpecified, true), gettype($baseFieldSpecified)), __LINE__);
        }
        $this->baseFieldSpecified = $baseFieldSpecified;
        
        return $this;
    }
    /**
     * Get currencyCodeField value
     * @return string|null
     */
    public function getCurrencyCodeField(): ?string
    {
        return $this->currencyCodeField;
    }
    /**
     * Set currencyCodeField value
     * @param string $currencyCodeField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setCurrencyCodeField(?string $currencyCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCodeField) && !is_string($currencyCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCodeField, true), gettype($currencyCodeField)), __LINE__);
        }
        $this->currencyCodeField = $currencyCodeField;
        
        return $this;
    }
    /**
     * Get percentageField value
     * @return float|null
     */
    public function getPercentageField(): ?float
    {
        return $this->percentageField;
    }
    /**
     * Set percentageField value
     * @param float $percentageField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setPercentageField(?float $percentageField = null): self
    {
        // validation for constraint: float
        if (!is_null($percentageField) && !(is_float($percentageField) || is_numeric($percentageField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($percentageField, true), gettype($percentageField)), __LINE__);
        }
        $this->percentageField = $percentageField;
        
        return $this;
    }
    /**
     * Get percentageFieldSpecified value
     * @return bool|null
     */
    public function getPercentageFieldSpecified(): ?bool
    {
        return $this->percentageFieldSpecified;
    }
    /**
     * Set percentageFieldSpecified value
     * @param bool $percentageFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setPercentageFieldSpecified(?bool $percentageFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($percentageFieldSpecified) && !is_bool($percentageFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($percentageFieldSpecified, true), gettype($percentageFieldSpecified)), __LINE__);
        }
        $this->percentageFieldSpecified = $percentageFieldSpecified;
        
        return $this;
    }
    /**
     * Get rateField value
     * @return float|null
     */
    public function getRateField(): ?float
    {
        return $this->rateField;
    }
    /**
     * Set rateField value
     * @param float $rateField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setRateField(?float $rateField = null): self
    {
        // validation for constraint: float
        if (!is_null($rateField) && !(is_float($rateField) || is_numeric($rateField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($rateField, true), gettype($rateField)), __LINE__);
        }
        $this->rateField = $rateField;
        
        return $this;
    }
    /**
     * Get rateFieldSpecified value
     * @return bool|null
     */
    public function getRateFieldSpecified(): ?bool
    {
        return $this->rateFieldSpecified;
    }
    /**
     * Set rateFieldSpecified value
     * @param bool $rateFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setRateFieldSpecified(?bool $rateFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($rateFieldSpecified) && !is_bool($rateFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($rateFieldSpecified, true), gettype($rateFieldSpecified)), __LINE__);
        }
        $this->rateFieldSpecified = $rateFieldSpecified;
        
        return $this;
    }
    /**
     * Get roundField value
     * @return float|null
     */
    public function getRoundField(): ?float
    {
        return $this->roundField;
    }
    /**
     * Set roundField value
     * @param float $roundField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setRoundField(?float $roundField = null): self
    {
        // validation for constraint: float
        if (!is_null($roundField) && !(is_float($roundField) || is_numeric($roundField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($roundField, true), gettype($roundField)), __LINE__);
        }
        $this->roundField = $roundField;
        
        return $this;
    }
    /**
     * Get roundFieldSpecified value
     * @return bool|null
     */
    public function getRoundFieldSpecified(): ?bool
    {
        return $this->roundFieldSpecified;
    }
    /**
     * Set roundFieldSpecified value
     * @param bool $roundFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setRoundFieldSpecified(?bool $roundFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($roundFieldSpecified) && !is_bool($roundFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($roundFieldSpecified, true), gettype($roundFieldSpecified)), __LINE__);
        }
        $this->roundFieldSpecified = $roundFieldSpecified;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
    /**
     * Get unitField value
     * @return string|null
     */
    public function getUnitField(): ?string
    {
        return $this->unitField;
    }
    /**
     * Set unitField value
     * @param string $unitField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setUnitField(?string $unitField = null): self
    {
        // validation for constraint: string
        if (!is_null($unitField) && !is_string($unitField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($unitField, true), gettype($unitField)), __LINE__);
        }
        $this->unitField = $unitField;
        
        return $this;
    }
    /**
     * Get valueField value
     * @return float|null
     */
    public function getValueField(): ?float
    {
        return $this->valueField;
    }
    /**
     * Set valueField value
     * @param float $valueField
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setValueField(?float $valueField = null): self
    {
        // validation for constraint: float
        if (!is_null($valueField) && !(is_float($valueField) || is_numeric($valueField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($valueField, true), gettype($valueField)), __LINE__);
        }
        $this->valueField = $valueField;
        
        return $this;
    }
    /**
     * Get valueFieldSpecified value
     * @return bool|null
     */
    public function getValueFieldSpecified(): ?bool
    {
        return $this->valueFieldSpecified;
    }
    /**
     * Set valueFieldSpecified value
     * @param bool $valueFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffBaseType_1
     */
    public function setValueFieldSpecified(?bool $valueFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($valueFieldSpecified) && !is_bool($valueFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($valueFieldSpecified, true), gettype($valueFieldSpecified)), __LINE__);
        }
        $this->valueFieldSpecified = $valueFieldSpecified;
        
        return $this;
    }
}
