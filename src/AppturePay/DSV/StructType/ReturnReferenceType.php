<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ReturnReferenceType StructType
 * @subpackage Structs
 */
class ReturnReferenceType extends AbstractStructBase
{
    /**
     * The referenceText
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $referenceText = null;
    /**
     * The mainFileNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $mainFileNumber = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for ReturnReferenceType
     * @uses ReturnReferenceType::setReferenceText()
     * @uses ReturnReferenceType::setMainFileNumber()
     * @uses ReturnReferenceType::setType()
     * @param string $referenceText
     * @param int $mainFileNumber
     * @param string $type
     */
    public function __construct(?string $referenceText = null, ?int $mainFileNumber = null, ?string $type = null)
    {
        $this
            ->setReferenceText($referenceText)
            ->setMainFileNumber($mainFileNumber)
            ->setType($type);
    }
    /**
     * Get referenceText value
     * @return string|null
     */
    public function getReferenceText(): ?string
    {
        return $this->referenceText;
    }
    /**
     * Set referenceText value
     * @param string $referenceText
     * @return \AppturePay\DSV\StructType\ReturnReferenceType
     */
    public function setReferenceText(?string $referenceText = null): self
    {
        // validation for constraint: string
        if (!is_null($referenceText) && !is_string($referenceText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceText, true), gettype($referenceText)), __LINE__);
        }
        $this->referenceText = $referenceText;
        
        return $this;
    }
    /**
     * Get mainFileNumber value
     * @return int|null
     */
    public function getMainFileNumber(): ?int
    {
        return $this->mainFileNumber;
    }
    /**
     * Set mainFileNumber value
     * @param int $mainFileNumber
     * @return \AppturePay\DSV\StructType\ReturnReferenceType
     */
    public function setMainFileNumber(?int $mainFileNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($mainFileNumber) && !(is_int($mainFileNumber) || ctype_digit($mainFileNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($mainFileNumber, true), gettype($mainFileNumber)), __LINE__);
        }
        $this->mainFileNumber = $mainFileNumber;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\ReturnReferenceType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
