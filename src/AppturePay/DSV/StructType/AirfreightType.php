<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AirfreightType StructType
 * @subpackage Structs
 */
class AirfreightType extends AbstractStructBase
{
    /**
     * The airwayBill
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $airwayBill = null;
    /**
     * The routing
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $routing = null;
    /**
     * The cargoPrepaidCollect
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $cargoPrepaidCollect = null;
    /**
     * The cargoCurrencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $cargoCurrencyCode = null;
    /**
     * The cargoExchangeRate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cargoExchangeRate = null;
    /**
     * The cargoAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cargoAmount = null;
    /**
     * The costsPrepaidCollect
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $costsPrepaidCollect = null;
    /**
     * The costsCurrencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $costsCurrencyCode = null;
    /**
     * The costsExchangeRate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $costsExchangeRate = null;
    /**
     * The costsAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $costsAmount = null;
    /**
     * The carriageAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $carriageAmount = null;
    /**
     * The customsAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $customsAmount = null;
    /**
     * The insuranceAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $insuranceAmount = null;
    /**
     * Constructor method for AirfreightType
     * @uses AirfreightType::setAirwayBill()
     * @uses AirfreightType::setRouting()
     * @uses AirfreightType::setCargoPrepaidCollect()
     * @uses AirfreightType::setCargoCurrencyCode()
     * @uses AirfreightType::setCargoExchangeRate()
     * @uses AirfreightType::setCargoAmount()
     * @uses AirfreightType::setCostsPrepaidCollect()
     * @uses AirfreightType::setCostsCurrencyCode()
     * @uses AirfreightType::setCostsExchangeRate()
     * @uses AirfreightType::setCostsAmount()
     * @uses AirfreightType::setCarriageAmount()
     * @uses AirfreightType::setCustomsAmount()
     * @uses AirfreightType::setInsuranceAmount()
     * @param int $airwayBill
     * @param string $routing
     * @param string $cargoPrepaidCollect
     * @param string $cargoCurrencyCode
     * @param float $cargoExchangeRate
     * @param float $cargoAmount
     * @param string $costsPrepaidCollect
     * @param string $costsCurrencyCode
     * @param float $costsExchangeRate
     * @param float $costsAmount
     * @param float $carriageAmount
     * @param float $customsAmount
     * @param float $insuranceAmount
     */
    public function __construct(?int $airwayBill = null, ?string $routing = null, ?string $cargoPrepaidCollect = null, ?string $cargoCurrencyCode = null, ?float $cargoExchangeRate = null, ?float $cargoAmount = null, ?string $costsPrepaidCollect = null, ?string $costsCurrencyCode = null, ?float $costsExchangeRate = null, ?float $costsAmount = null, ?float $carriageAmount = null, ?float $customsAmount = null, ?float $insuranceAmount = null)
    {
        $this
            ->setAirwayBill($airwayBill)
            ->setRouting($routing)
            ->setCargoPrepaidCollect($cargoPrepaidCollect)
            ->setCargoCurrencyCode($cargoCurrencyCode)
            ->setCargoExchangeRate($cargoExchangeRate)
            ->setCargoAmount($cargoAmount)
            ->setCostsPrepaidCollect($costsPrepaidCollect)
            ->setCostsCurrencyCode($costsCurrencyCode)
            ->setCostsExchangeRate($costsExchangeRate)
            ->setCostsAmount($costsAmount)
            ->setCarriageAmount($carriageAmount)
            ->setCustomsAmount($customsAmount)
            ->setInsuranceAmount($insuranceAmount);
    }
    /**
     * Get airwayBill value
     * @return int|null
     */
    public function getAirwayBill(): ?int
    {
        return $this->airwayBill;
    }
    /**
     * Set airwayBill value
     * @param int $airwayBill
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setAirwayBill(?int $airwayBill = null): self
    {
        // validation for constraint: int
        if (!is_null($airwayBill) && !(is_int($airwayBill) || ctype_digit($airwayBill))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($airwayBill, true), gettype($airwayBill)), __LINE__);
        }
        $this->airwayBill = $airwayBill;
        
        return $this;
    }
    /**
     * Get routing value
     * @return string|null
     */
    public function getRouting(): ?string
    {
        return $this->routing;
    }
    /**
     * Set routing value
     * @param string $routing
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setRouting(?string $routing = null): self
    {
        // validation for constraint: string
        if (!is_null($routing) && !is_string($routing)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routing, true), gettype($routing)), __LINE__);
        }
        $this->routing = $routing;
        
        return $this;
    }
    /**
     * Get cargoPrepaidCollect value
     * @return string|null
     */
    public function getCargoPrepaidCollect(): ?string
    {
        return $this->cargoPrepaidCollect;
    }
    /**
     * Set cargoPrepaidCollect value
     * @uses \AppturePay\DSV\EnumType\CargoPrepaidCollectType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\CargoPrepaidCollectType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $cargoPrepaidCollect
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCargoPrepaidCollect(?string $cargoPrepaidCollect = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\CargoPrepaidCollectType::valueIsValid($cargoPrepaidCollect)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\CargoPrepaidCollectType', is_array($cargoPrepaidCollect) ? implode(', ', $cargoPrepaidCollect) : var_export($cargoPrepaidCollect, true), implode(', ', \AppturePay\DSV\EnumType\CargoPrepaidCollectType::getValidValues())), __LINE__);
        }
        $this->cargoPrepaidCollect = $cargoPrepaidCollect;
        
        return $this;
    }
    /**
     * Get cargoCurrencyCode value
     * @return string|null
     */
    public function getCargoCurrencyCode(): ?string
    {
        return $this->cargoCurrencyCode;
    }
    /**
     * Set cargoCurrencyCode value
     * @param string $cargoCurrencyCode
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCargoCurrencyCode(?string $cargoCurrencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($cargoCurrencyCode) && !is_string($cargoCurrencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cargoCurrencyCode, true), gettype($cargoCurrencyCode)), __LINE__);
        }
        $this->cargoCurrencyCode = $cargoCurrencyCode;
        
        return $this;
    }
    /**
     * Get cargoExchangeRate value
     * @return float|null
     */
    public function getCargoExchangeRate(): ?float
    {
        return $this->cargoExchangeRate;
    }
    /**
     * Set cargoExchangeRate value
     * @param float $cargoExchangeRate
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCargoExchangeRate(?float $cargoExchangeRate = null): self
    {
        // validation for constraint: float
        if (!is_null($cargoExchangeRate) && !(is_float($cargoExchangeRate) || is_numeric($cargoExchangeRate))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cargoExchangeRate, true), gettype($cargoExchangeRate)), __LINE__);
        }
        $this->cargoExchangeRate = $cargoExchangeRate;
        
        return $this;
    }
    /**
     * Get cargoAmount value
     * @return float|null
     */
    public function getCargoAmount(): ?float
    {
        return $this->cargoAmount;
    }
    /**
     * Set cargoAmount value
     * @param float $cargoAmount
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCargoAmount(?float $cargoAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($cargoAmount) && !(is_float($cargoAmount) || is_numeric($cargoAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cargoAmount, true), gettype($cargoAmount)), __LINE__);
        }
        $this->cargoAmount = $cargoAmount;
        
        return $this;
    }
    /**
     * Get costsPrepaidCollect value
     * @return string|null
     */
    public function getCostsPrepaidCollect(): ?string
    {
        return $this->costsPrepaidCollect;
    }
    /**
     * Set costsPrepaidCollect value
     * @uses \AppturePay\DSV\EnumType\CostsPrepaidCollectType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\CostsPrepaidCollectType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $costsPrepaidCollect
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCostsPrepaidCollect(?string $costsPrepaidCollect = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\CostsPrepaidCollectType::valueIsValid($costsPrepaidCollect)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\CostsPrepaidCollectType', is_array($costsPrepaidCollect) ? implode(', ', $costsPrepaidCollect) : var_export($costsPrepaidCollect, true), implode(', ', \AppturePay\DSV\EnumType\CostsPrepaidCollectType::getValidValues())), __LINE__);
        }
        $this->costsPrepaidCollect = $costsPrepaidCollect;
        
        return $this;
    }
    /**
     * Get costsCurrencyCode value
     * @return string|null
     */
    public function getCostsCurrencyCode(): ?string
    {
        return $this->costsCurrencyCode;
    }
    /**
     * Set costsCurrencyCode value
     * @param string $costsCurrencyCode
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCostsCurrencyCode(?string $costsCurrencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($costsCurrencyCode) && !is_string($costsCurrencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($costsCurrencyCode, true), gettype($costsCurrencyCode)), __LINE__);
        }
        $this->costsCurrencyCode = $costsCurrencyCode;
        
        return $this;
    }
    /**
     * Get costsExchangeRate value
     * @return float|null
     */
    public function getCostsExchangeRate(): ?float
    {
        return $this->costsExchangeRate;
    }
    /**
     * Set costsExchangeRate value
     * @param float $costsExchangeRate
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCostsExchangeRate(?float $costsExchangeRate = null): self
    {
        // validation for constraint: float
        if (!is_null($costsExchangeRate) && !(is_float($costsExchangeRate) || is_numeric($costsExchangeRate))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($costsExchangeRate, true), gettype($costsExchangeRate)), __LINE__);
        }
        $this->costsExchangeRate = $costsExchangeRate;
        
        return $this;
    }
    /**
     * Get costsAmount value
     * @return float|null
     */
    public function getCostsAmount(): ?float
    {
        return $this->costsAmount;
    }
    /**
     * Set costsAmount value
     * @param float $costsAmount
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCostsAmount(?float $costsAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($costsAmount) && !(is_float($costsAmount) || is_numeric($costsAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($costsAmount, true), gettype($costsAmount)), __LINE__);
        }
        $this->costsAmount = $costsAmount;
        
        return $this;
    }
    /**
     * Get carriageAmount value
     * @return float|null
     */
    public function getCarriageAmount(): ?float
    {
        return $this->carriageAmount;
    }
    /**
     * Set carriageAmount value
     * @param float $carriageAmount
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCarriageAmount(?float $carriageAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($carriageAmount) && !(is_float($carriageAmount) || is_numeric($carriageAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($carriageAmount, true), gettype($carriageAmount)), __LINE__);
        }
        $this->carriageAmount = $carriageAmount;
        
        return $this;
    }
    /**
     * Get customsAmount value
     * @return float|null
     */
    public function getCustomsAmount(): ?float
    {
        return $this->customsAmount;
    }
    /**
     * Set customsAmount value
     * @param float $customsAmount
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setCustomsAmount(?float $customsAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($customsAmount) && !(is_float($customsAmount) || is_numeric($customsAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($customsAmount, true), gettype($customsAmount)), __LINE__);
        }
        $this->customsAmount = $customsAmount;
        
        return $this;
    }
    /**
     * Get insuranceAmount value
     * @return float|null
     */
    public function getInsuranceAmount(): ?float
    {
        return $this->insuranceAmount;
    }
    /**
     * Set insuranceAmount value
     * @param float $insuranceAmount
     * @return \AppturePay\DSV\StructType\AirfreightType
     */
    public function setInsuranceAmount(?float $insuranceAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($insuranceAmount) && !(is_float($insuranceAmount) || is_numeric($insuranceAmount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($insuranceAmount, true), gettype($insuranceAmount)), __LINE__);
        }
        $this->insuranceAmount = $insuranceAmount;
        
        return $this;
    }
}
