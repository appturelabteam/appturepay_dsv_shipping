<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubQuantityType StructType
 * @subpackage Structs
 */
class SubQuantityType extends AbstractStructBase
{
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The length
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $length = null;
    /**
     * The width
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $width = null;
    /**
     * The height
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $height = null;
    /**
     * The volumeCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $volumeCode = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The cubicMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cubicMeters = null;
    /**
     * The loadingMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadingMeters = null;
    /**
     * The packageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCode = null;
    /**
     * The commodityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $commodityCode = null;
    /**
     * The commodityDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $commodityDescription = null;
    /**
     * The marksNumberLong
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $marksNumberLong = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The netWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $netWeight = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for SubQuantityType
     * @uses SubQuantityType::setQuantity()
     * @uses SubQuantityType::setLength()
     * @uses SubQuantityType::setWidth()
     * @uses SubQuantityType::setHeight()
     * @uses SubQuantityType::setVolumeCode()
     * @uses SubQuantityType::setVolume()
     * @uses SubQuantityType::setCubicMeters()
     * @uses SubQuantityType::setLoadingMeters()
     * @uses SubQuantityType::setPackageCode()
     * @uses SubQuantityType::setCommodityCode()
     * @uses SubQuantityType::setCommodityDescription()
     * @uses SubQuantityType::setMarksNumberLong()
     * @uses SubQuantityType::setGrossWeight()
     * @uses SubQuantityType::setNetWeight()
     * @uses SubQuantityType::setType()
     * @param float $quantity
     * @param float $length
     * @param float $width
     * @param float $height
     * @param string $volumeCode
     * @param float $volume
     * @param float $cubicMeters
     * @param float $loadingMeters
     * @param string $packageCode
     * @param string $commodityCode
     * @param string $commodityDescription
     * @param string $marksNumberLong
     * @param float $grossWeight
     * @param float $netWeight
     * @param string $type
     */
    public function __construct(?float $quantity = null, ?float $length = null, ?float $width = null, ?float $height = null, ?string $volumeCode = null, ?float $volume = null, ?float $cubicMeters = null, ?float $loadingMeters = null, ?string $packageCode = null, ?string $commodityCode = null, ?string $commodityDescription = null, ?string $marksNumberLong = null, ?float $grossWeight = null, ?float $netWeight = null, ?string $type = null)
    {
        $this
            ->setQuantity($quantity)
            ->setLength($length)
            ->setWidth($width)
            ->setHeight($height)
            ->setVolumeCode($volumeCode)
            ->setVolume($volume)
            ->setCubicMeters($cubicMeters)
            ->setLoadingMeters($loadingMeters)
            ->setPackageCode($packageCode)
            ->setCommodityCode($commodityCode)
            ->setCommodityDescription($commodityDescription)
            ->setMarksNumberLong($marksNumberLong)
            ->setGrossWeight($grossWeight)
            ->setNetWeight($netWeight)
            ->setType($type);
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get length value
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }
    /**
     * Set length value
     * @param float $length
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setLength(?float $length = null): self
    {
        // validation for constraint: float
        if (!is_null($length) && !(is_float($length) || is_numeric($length))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($length, true), gettype($length)), __LINE__);
        }
        $this->length = $length;
        
        return $this;
    }
    /**
     * Get width value
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }
    /**
     * Set width value
     * @param float $width
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setWidth(?float $width = null): self
    {
        // validation for constraint: float
        if (!is_null($width) && !(is_float($width) || is_numeric($width))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($width, true), gettype($width)), __LINE__);
        }
        $this->width = $width;
        
        return $this;
    }
    /**
     * Get height value
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }
    /**
     * Set height value
     * @param float $height
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setHeight(?float $height = null): self
    {
        // validation for constraint: float
        if (!is_null($height) && !(is_float($height) || is_numeric($height))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($height, true), gettype($height)), __LINE__);
        }
        $this->height = $height;
        
        return $this;
    }
    /**
     * Get volumeCode value
     * @return string|null
     */
    public function getVolumeCode(): ?string
    {
        return $this->volumeCode;
    }
    /**
     * Set volumeCode value
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $volumeCode
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setVolumeCode(?string $volumeCode = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\VolumeCodeType_1::valueIsValid($volumeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\VolumeCodeType_1', is_array($volumeCode) ? implode(', ', $volumeCode) : var_export($volumeCode, true), implode(', ', \AppturePay\DSV\EnumType\VolumeCodeType_1::getValidValues())), __LINE__);
        }
        $this->volumeCode = $volumeCode;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get cubicMeters value
     * @return float|null
     */
    public function getCubicMeters(): ?float
    {
        return $this->cubicMeters;
    }
    /**
     * Set cubicMeters value
     * @param float $cubicMeters
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setCubicMeters(?float $cubicMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($cubicMeters) && !(is_float($cubicMeters) || is_numeric($cubicMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cubicMeters, true), gettype($cubicMeters)), __LINE__);
        }
        $this->cubicMeters = $cubicMeters;
        
        return $this;
    }
    /**
     * Get loadingMeters value
     * @return float|null
     */
    public function getLoadingMeters(): ?float
    {
        return $this->loadingMeters;
    }
    /**
     * Set loadingMeters value
     * @param float $loadingMeters
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setLoadingMeters(?float $loadingMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($loadingMeters) && !(is_float($loadingMeters) || is_numeric($loadingMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadingMeters, true), gettype($loadingMeters)), __LINE__);
        }
        $this->loadingMeters = $loadingMeters;
        
        return $this;
    }
    /**
     * Get packageCode value
     * @return string|null
     */
    public function getPackageCode(): ?string
    {
        return $this->packageCode;
    }
    /**
     * Set packageCode value
     * @param string $packageCode
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setPackageCode(?string $packageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCode) && !is_string($packageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCode, true), gettype($packageCode)), __LINE__);
        }
        $this->packageCode = $packageCode;
        
        return $this;
    }
    /**
     * Get commodityCode value
     * @return string|null
     */
    public function getCommodityCode(): ?string
    {
        return $this->commodityCode;
    }
    /**
     * Set commodityCode value
     * @param string $commodityCode
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setCommodityCode(?string $commodityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($commodityCode) && !is_string($commodityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($commodityCode, true), gettype($commodityCode)), __LINE__);
        }
        $this->commodityCode = $commodityCode;
        
        return $this;
    }
    /**
     * Get commodityDescription value
     * @return string|null
     */
    public function getCommodityDescription(): ?string
    {
        return $this->commodityDescription;
    }
    /**
     * Set commodityDescription value
     * @param string $commodityDescription
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setCommodityDescription(?string $commodityDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($commodityDescription) && !is_string($commodityDescription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($commodityDescription, true), gettype($commodityDescription)), __LINE__);
        }
        $this->commodityDescription = $commodityDescription;
        
        return $this;
    }
    /**
     * Get marksNumberLong value
     * @return string|null
     */
    public function getMarksNumberLong(): ?string
    {
        return $this->marksNumberLong;
    }
    /**
     * Set marksNumberLong value
     * @param string $marksNumberLong
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setMarksNumberLong(?string $marksNumberLong = null): self
    {
        // validation for constraint: string
        if (!is_null($marksNumberLong) && !is_string($marksNumberLong)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($marksNumberLong, true), gettype($marksNumberLong)), __LINE__);
        }
        $this->marksNumberLong = $marksNumberLong;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get netWeight value
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    /**
     * Set netWeight value
     * @param float $netWeight
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setNetWeight(?float $netWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($netWeight) && !(is_float($netWeight) || is_numeric($netWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($netWeight, true), gettype($netWeight)), __LINE__);
        }
        $this->netWeight = $netWeight;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\SubQuantityType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
