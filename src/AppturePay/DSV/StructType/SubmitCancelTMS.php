<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitCancelTMS StructType
 * @subpackage Structs
 */
class SubmitCancelTMS extends AbstractStructBase
{
    /**
     * The cancel
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\StatusMessage|null
     */
    protected ?\AppturePay\DSV\StructType\StatusMessage $cancel = null;
    /**
     * Constructor method for SubmitCancelTMS
     * @uses SubmitCancelTMS::setCancel()
     * @param \AppturePay\DSV\StructType\StatusMessage $cancel
     */
    public function __construct(?\AppturePay\DSV\StructType\StatusMessage $cancel = null)
    {
        $this
            ->setCancel($cancel);
    }
    /**
     * Get cancel value
     * @return \AppturePay\DSV\StructType\StatusMessage|null
     */
    public function getCancel(): ?\AppturePay\DSV\StructType\StatusMessage
    {
        return $this->cancel;
    }
    /**
     * Set cancel value
     * @param \AppturePay\DSV\StructType\StatusMessage $cancel
     * @return \AppturePay\DSV\StructType\SubmitCancelTMS
     */
    public function setCancel(?\AppturePay\DSV\StructType\StatusMessage $cancel = null): self
    {
        $this->cancel = $cancel;
        
        return $this;
    }
}
