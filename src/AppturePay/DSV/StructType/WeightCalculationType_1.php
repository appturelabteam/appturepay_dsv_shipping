<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WeightCalculationType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:WeightCalculationType
 * @subpackage Structs
 */
class WeightCalculationType_1 extends AbstractStructBase
{
    /**
     * The conversionBase
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $conversionBase = null;
    /**
     * The conversionFactor
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $conversionFactor = null;
    /**
     * The chargeableWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $chargeableWeight = null;
    /**
     * The chargeableWeightField
     * @var float|null
     */
    protected ?float $chargeableWeightField = null;
    /**
     * The chargeableWeightFieldSpecified
     * @var bool|null
     */
    protected ?bool $chargeableWeightFieldSpecified = null;
    /**
     * The conversionBaseField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $conversionBaseField = null;
    /**
     * The conversionFactorField
     * @var float|null
     */
    protected ?float $conversionFactorField = null;
    /**
     * The conversionFactorFieldSpecified
     * @var bool|null
     */
    protected ?bool $conversionFactorFieldSpecified = null;
    /**
     * Constructor method for WeightCalculationType
     * @uses WeightCalculationType_1::setConversionBase()
     * @uses WeightCalculationType_1::setConversionFactor()
     * @uses WeightCalculationType_1::setChargeableWeight()
     * @uses WeightCalculationType_1::setChargeableWeightField()
     * @uses WeightCalculationType_1::setChargeableWeightFieldSpecified()
     * @uses WeightCalculationType_1::setConversionBaseField()
     * @uses WeightCalculationType_1::setConversionFactorField()
     * @uses WeightCalculationType_1::setConversionFactorFieldSpecified()
     * @param string $conversionBase
     * @param float $conversionFactor
     * @param float $chargeableWeight
     * @param float $chargeableWeightField
     * @param bool $chargeableWeightFieldSpecified
     * @param string $conversionBaseField
     * @param float $conversionFactorField
     * @param bool $conversionFactorFieldSpecified
     */
    public function __construct(?string $conversionBase = null, ?float $conversionFactor = null, ?float $chargeableWeight = null, ?float $chargeableWeightField = null, ?bool $chargeableWeightFieldSpecified = null, ?string $conversionBaseField = null, ?float $conversionFactorField = null, ?bool $conversionFactorFieldSpecified = null)
    {
        $this
            ->setConversionBase($conversionBase)
            ->setConversionFactor($conversionFactor)
            ->setChargeableWeight($chargeableWeight)
            ->setChargeableWeightField($chargeableWeightField)
            ->setChargeableWeightFieldSpecified($chargeableWeightFieldSpecified)
            ->setConversionBaseField($conversionBaseField)
            ->setConversionFactorField($conversionFactorField)
            ->setConversionFactorFieldSpecified($conversionFactorFieldSpecified);
    }
    /**
     * Get conversionBase value
     * @return string|null
     */
    public function getConversionBase(): ?string
    {
        return $this->conversionBase;
    }
    /**
     * Set conversionBase value
     * @param string $conversionBase
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setConversionBase(?string $conversionBase = null): self
    {
        // validation for constraint: string
        if (!is_null($conversionBase) && !is_string($conversionBase)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($conversionBase, true), gettype($conversionBase)), __LINE__);
        }
        $this->conversionBase = $conversionBase;
        
        return $this;
    }
    /**
     * Get conversionFactor value
     * @return float|null
     */
    public function getConversionFactor(): ?float
    {
        return $this->conversionFactor;
    }
    /**
     * Set conversionFactor value
     * @param float $conversionFactor
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setConversionFactor(?float $conversionFactor = null): self
    {
        // validation for constraint: float
        if (!is_null($conversionFactor) && !(is_float($conversionFactor) || is_numeric($conversionFactor))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($conversionFactor, true), gettype($conversionFactor)), __LINE__);
        }
        $this->conversionFactor = $conversionFactor;
        
        return $this;
    }
    /**
     * Get chargeableWeight value
     * @return float|null
     */
    public function getChargeableWeight(): ?float
    {
        return $this->chargeableWeight;
    }
    /**
     * Set chargeableWeight value
     * @param float $chargeableWeight
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setChargeableWeight(?float $chargeableWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($chargeableWeight) && !(is_float($chargeableWeight) || is_numeric($chargeableWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($chargeableWeight, true), gettype($chargeableWeight)), __LINE__);
        }
        $this->chargeableWeight = $chargeableWeight;
        
        return $this;
    }
    /**
     * Get chargeableWeightField value
     * @return float|null
     */
    public function getChargeableWeightField(): ?float
    {
        return $this->chargeableWeightField;
    }
    /**
     * Set chargeableWeightField value
     * @param float $chargeableWeightField
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setChargeableWeightField(?float $chargeableWeightField = null): self
    {
        // validation for constraint: float
        if (!is_null($chargeableWeightField) && !(is_float($chargeableWeightField) || is_numeric($chargeableWeightField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($chargeableWeightField, true), gettype($chargeableWeightField)), __LINE__);
        }
        $this->chargeableWeightField = $chargeableWeightField;
        
        return $this;
    }
    /**
     * Get chargeableWeightFieldSpecified value
     * @return bool|null
     */
    public function getChargeableWeightFieldSpecified(): ?bool
    {
        return $this->chargeableWeightFieldSpecified;
    }
    /**
     * Set chargeableWeightFieldSpecified value
     * @param bool $chargeableWeightFieldSpecified
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setChargeableWeightFieldSpecified(?bool $chargeableWeightFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($chargeableWeightFieldSpecified) && !is_bool($chargeableWeightFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($chargeableWeightFieldSpecified, true), gettype($chargeableWeightFieldSpecified)), __LINE__);
        }
        $this->chargeableWeightFieldSpecified = $chargeableWeightFieldSpecified;
        
        return $this;
    }
    /**
     * Get conversionBaseField value
     * @return string|null
     */
    public function getConversionBaseField(): ?string
    {
        return $this->conversionBaseField;
    }
    /**
     * Set conversionBaseField value
     * @param string $conversionBaseField
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setConversionBaseField(?string $conversionBaseField = null): self
    {
        // validation for constraint: string
        if (!is_null($conversionBaseField) && !is_string($conversionBaseField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($conversionBaseField, true), gettype($conversionBaseField)), __LINE__);
        }
        $this->conversionBaseField = $conversionBaseField;
        
        return $this;
    }
    /**
     * Get conversionFactorField value
     * @return float|null
     */
    public function getConversionFactorField(): ?float
    {
        return $this->conversionFactorField;
    }
    /**
     * Set conversionFactorField value
     * @param float $conversionFactorField
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setConversionFactorField(?float $conversionFactorField = null): self
    {
        // validation for constraint: float
        if (!is_null($conversionFactorField) && !(is_float($conversionFactorField) || is_numeric($conversionFactorField))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($conversionFactorField, true), gettype($conversionFactorField)), __LINE__);
        }
        $this->conversionFactorField = $conversionFactorField;
        
        return $this;
    }
    /**
     * Get conversionFactorFieldSpecified value
     * @return bool|null
     */
    public function getConversionFactorFieldSpecified(): ?bool
    {
        return $this->conversionFactorFieldSpecified;
    }
    /**
     * Set conversionFactorFieldSpecified value
     * @param bool $conversionFactorFieldSpecified
     * @return \AppturePay\DSV\StructType\WeightCalculationType_1
     */
    public function setConversionFactorFieldSpecified(?bool $conversionFactorFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($conversionFactorFieldSpecified) && !is_bool($conversionFactorFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($conversionFactorFieldSpecified, true), gettype($conversionFactorFieldSpecified)), __LINE__);
        }
        $this->conversionFactorFieldSpecified = $conversionFactorFieldSpecified;
        
        return $this;
    }
}
