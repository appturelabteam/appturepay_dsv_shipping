<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DateTimeZonesType StructType
 * @subpackage Structs
 */
class DateTimeZonesType extends AbstractStructBase
{
    /**
     * The typeOfDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $typeOfDate = null;
    /**
     * The dateTimeZone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTimeZone = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for DateTimeZonesType
     * @uses DateTimeZonesType::setTypeOfDate()
     * @uses DateTimeZonesType::setDateTimeZone()
     * @uses DateTimeZonesType::setType()
     * @param int $typeOfDate
     * @param string $dateTimeZone
     * @param string $type
     */
    public function __construct(?int $typeOfDate = null, ?string $dateTimeZone = null, ?string $type = null)
    {
        $this
            ->setTypeOfDate($typeOfDate)
            ->setDateTimeZone($dateTimeZone)
            ->setType($type);
    }
    /**
     * Get typeOfDate value
     * @return int|null
     */
    public function getTypeOfDate(): ?int
    {
        return $this->typeOfDate;
    }
    /**
     * Set typeOfDate value
     * @param int $typeOfDate
     * @return \AppturePay\DSV\StructType\DateTimeZonesType
     */
    public function setTypeOfDate(?int $typeOfDate = null): self
    {
        // validation for constraint: int
        if (!is_null($typeOfDate) && !(is_int($typeOfDate) || ctype_digit($typeOfDate))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($typeOfDate, true), gettype($typeOfDate)), __LINE__);
        }
        $this->typeOfDate = $typeOfDate;
        
        return $this;
    }
    /**
     * Get dateTimeZone value
     * @return string|null
     */
    public function getDateTimeZone(): ?string
    {
        return $this->dateTimeZone;
    }
    /**
     * Set dateTimeZone value
     * @param string $dateTimeZone
     * @return \AppturePay\DSV\StructType\DateTimeZonesType
     */
    public function setDateTimeZone(?string $dateTimeZone = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZone) && !is_string($dateTimeZone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZone, true), gettype($dateTimeZone)), __LINE__);
        }
        $this->dateTimeZone = $dateTimeZone;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\DateTimeZonesType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
