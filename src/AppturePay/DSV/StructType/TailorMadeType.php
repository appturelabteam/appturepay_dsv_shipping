<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TailorMadeType StructType
 * @subpackage Structs
 */
class TailorMadeType extends AbstractStructBase
{
    /**
     * The tailorCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tailorCode = null;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $value = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for TailorMadeType
     * @uses TailorMadeType::setTailorCode()
     * @uses TailorMadeType::setValue()
     * @uses TailorMadeType::setType()
     * @param string $tailorCode
     * @param string $value
     * @param string $type
     */
    public function __construct(?string $tailorCode = null, ?string $value = null, ?string $type = null)
    {
        $this
            ->setTailorCode($tailorCode)
            ->setValue($value)
            ->setType($type);
    }
    /**
     * Get tailorCode value
     * @return string|null
     */
    public function getTailorCode(): ?string
    {
        return $this->tailorCode;
    }
    /**
     * Set tailorCode value
     * @param string $tailorCode
     * @return \AppturePay\DSV\StructType\TailorMadeType
     */
    public function setTailorCode(?string $tailorCode = null): self
    {
        // validation for constraint: string
        if (!is_null($tailorCode) && !is_string($tailorCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tailorCode, true), gettype($tailorCode)), __LINE__);
        }
        $this->tailorCode = $tailorCode;
        
        return $this;
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $value
     * @return \AppturePay\DSV\StructType\TailorMadeType
     */
    public function setValue(?string $value = null): self
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TailorMadeType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
