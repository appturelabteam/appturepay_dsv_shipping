<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CodesType StructType
 * @subpackage Structs
 */
class CodesType extends AbstractStructBase
{
    /**
     * The code1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code1 = null;
    /**
     * The code2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code2 = null;
    /**
     * The code3
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code3 = null;
    /**
     * The code4
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code4 = null;
    /**
     * The code5
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code5 = null;
    /**
     * The code6
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code6 = null;
    /**
     * The code7
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code7 = null;
    /**
     * The code8
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $code8 = null;
    /**
     * Constructor method for CodesType
     * @uses CodesType::setCode1()
     * @uses CodesType::setCode2()
     * @uses CodesType::setCode3()
     * @uses CodesType::setCode4()
     * @uses CodesType::setCode5()
     * @uses CodesType::setCode6()
     * @uses CodesType::setCode7()
     * @uses CodesType::setCode8()
     * @param int $code1
     * @param int $code2
     * @param int $code3
     * @param int $code4
     * @param int $code5
     * @param int $code6
     * @param int $code7
     * @param int $code8
     */
    public function __construct(?int $code1 = null, ?int $code2 = null, ?int $code3 = null, ?int $code4 = null, ?int $code5 = null, ?int $code6 = null, ?int $code7 = null, ?int $code8 = null)
    {
        $this
            ->setCode1($code1)
            ->setCode2($code2)
            ->setCode3($code3)
            ->setCode4($code4)
            ->setCode5($code5)
            ->setCode6($code6)
            ->setCode7($code7)
            ->setCode8($code8);
    }
    /**
     * Get code1 value
     * @return int|null
     */
    public function getCode1(): ?int
    {
        return $this->code1;
    }
    /**
     * Set code1 value
     * @param int $code1
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode1(?int $code1 = null): self
    {
        // validation for constraint: int
        if (!is_null($code1) && !(is_int($code1) || ctype_digit($code1))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code1, true), gettype($code1)), __LINE__);
        }
        $this->code1 = $code1;
        
        return $this;
    }
    /**
     * Get code2 value
     * @return int|null
     */
    public function getCode2(): ?int
    {
        return $this->code2;
    }
    /**
     * Set code2 value
     * @param int $code2
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode2(?int $code2 = null): self
    {
        // validation for constraint: int
        if (!is_null($code2) && !(is_int($code2) || ctype_digit($code2))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code2, true), gettype($code2)), __LINE__);
        }
        $this->code2 = $code2;
        
        return $this;
    }
    /**
     * Get code3 value
     * @return int|null
     */
    public function getCode3(): ?int
    {
        return $this->code3;
    }
    /**
     * Set code3 value
     * @param int $code3
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode3(?int $code3 = null): self
    {
        // validation for constraint: int
        if (!is_null($code3) && !(is_int($code3) || ctype_digit($code3))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code3, true), gettype($code3)), __LINE__);
        }
        $this->code3 = $code3;
        
        return $this;
    }
    /**
     * Get code4 value
     * @return int|null
     */
    public function getCode4(): ?int
    {
        return $this->code4;
    }
    /**
     * Set code4 value
     * @param int $code4
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode4(?int $code4 = null): self
    {
        // validation for constraint: int
        if (!is_null($code4) && !(is_int($code4) || ctype_digit($code4))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code4, true), gettype($code4)), __LINE__);
        }
        $this->code4 = $code4;
        
        return $this;
    }
    /**
     * Get code5 value
     * @return int|null
     */
    public function getCode5(): ?int
    {
        return $this->code5;
    }
    /**
     * Set code5 value
     * @param int $code5
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode5(?int $code5 = null): self
    {
        // validation for constraint: int
        if (!is_null($code5) && !(is_int($code5) || ctype_digit($code5))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code5, true), gettype($code5)), __LINE__);
        }
        $this->code5 = $code5;
        
        return $this;
    }
    /**
     * Get code6 value
     * @return int|null
     */
    public function getCode6(): ?int
    {
        return $this->code6;
    }
    /**
     * Set code6 value
     * @param int $code6
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode6(?int $code6 = null): self
    {
        // validation for constraint: int
        if (!is_null($code6) && !(is_int($code6) || ctype_digit($code6))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code6, true), gettype($code6)), __LINE__);
        }
        $this->code6 = $code6;
        
        return $this;
    }
    /**
     * Get code7 value
     * @return int|null
     */
    public function getCode7(): ?int
    {
        return $this->code7;
    }
    /**
     * Set code7 value
     * @param int $code7
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode7(?int $code7 = null): self
    {
        // validation for constraint: int
        if (!is_null($code7) && !(is_int($code7) || ctype_digit($code7))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code7, true), gettype($code7)), __LINE__);
        }
        $this->code7 = $code7;
        
        return $this;
    }
    /**
     * Get code8 value
     * @return int|null
     */
    public function getCode8(): ?int
    {
        return $this->code8;
    }
    /**
     * Set code8 value
     * @param int $code8
     * @return \AppturePay\DSV\StructType\CodesType
     */
    public function setCode8(?int $code8 = null): self
    {
        // validation for constraint: int
        if (!is_null($code8) && !(is_int($code8) || ctype_digit($code8))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code8, true), gettype($code8)), __LINE__);
        }
        $this->code8 = $code8;
        
        return $this;
    }
}
