<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FreightChargeType StructType
 * @subpackage Structs
 */
class FreightChargeType extends AbstractStructBase
{
    /**
     * The sequenceNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $sequenceNumber = null;
    /**
     * The otherChargeCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $otherChargeCode = null;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The currencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currencyCode = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for FreightChargeType
     * @uses FreightChargeType::setSequenceNumber()
     * @uses FreightChargeType::setOtherChargeCode()
     * @uses FreightChargeType::setDescription()
     * @uses FreightChargeType::setCurrencyCode()
     * @uses FreightChargeType::setAmount()
     * @uses FreightChargeType::setType()
     * @param int $sequenceNumber
     * @param string $otherChargeCode
     * @param string $description
     * @param string $currencyCode
     * @param float $amount
     * @param string $type
     */
    public function __construct(?int $sequenceNumber = null, ?string $otherChargeCode = null, ?string $description = null, ?string $currencyCode = null, ?float $amount = null, ?string $type = null)
    {
        $this
            ->setSequenceNumber($sequenceNumber)
            ->setOtherChargeCode($otherChargeCode)
            ->setDescription($description)
            ->setCurrencyCode($currencyCode)
            ->setAmount($amount)
            ->setType($type);
    }
    /**
     * Get sequenceNumber value
     * @return int|null
     */
    public function getSequenceNumber(): ?int
    {
        return $this->sequenceNumber;
    }
    /**
     * Set sequenceNumber value
     * @param int $sequenceNumber
     * @return \AppturePay\DSV\StructType\FreightChargeType
     */
    public function setSequenceNumber(?int $sequenceNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($sequenceNumber) && !(is_int($sequenceNumber) || ctype_digit($sequenceNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($sequenceNumber, true), gettype($sequenceNumber)), __LINE__);
        }
        $this->sequenceNumber = $sequenceNumber;
        
        return $this;
    }
    /**
     * Get otherChargeCode value
     * @return string|null
     */
    public function getOtherChargeCode(): ?string
    {
        return $this->otherChargeCode;
    }
    /**
     * Set otherChargeCode value
     * @param string $otherChargeCode
     * @return \AppturePay\DSV\StructType\FreightChargeType
     */
    public function setOtherChargeCode(?string $otherChargeCode = null): self
    {
        // validation for constraint: string
        if (!is_null($otherChargeCode) && !is_string($otherChargeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($otherChargeCode, true), gettype($otherChargeCode)), __LINE__);
        }
        $this->otherChargeCode = $otherChargeCode;
        
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \AppturePay\DSV\StructType\FreightChargeType
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get currencyCode value
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }
    /**
     * Set currencyCode value
     * @param string $currencyCode
     * @return \AppturePay\DSV\StructType\FreightChargeType
     */
    public function setCurrencyCode(?string $currencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyCode, true), gettype($currencyCode)), __LINE__);
        }
        $this->currencyCode = $currencyCode;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \AppturePay\DSV\StructType\FreightChargeType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FreightChargeType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
