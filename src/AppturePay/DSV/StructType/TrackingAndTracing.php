<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for trackingAndTracing StructType
 * @subpackage Structs
 */
class TrackingAndTracing extends AbstractStructBase
{
    /**
     * The dateTimeZone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dateTimeZone = null;
    /**
     * The code
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $code = null;
    /**
     * The remark_1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $remark_1 = null;
    /**
     * The remark_2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $remark_2 = null;
    /**
     * The details
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $details = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for trackingAndTracing
     * @uses TrackingAndTracing::setDateTimeZone()
     * @uses TrackingAndTracing::setCode()
     * @uses TrackingAndTracing::setRemark_1()
     * @uses TrackingAndTracing::setRemark_2()
     * @uses TrackingAndTracing::setDetails()
     * @uses TrackingAndTracing::setType()
     * @param string $dateTimeZone
     * @param string $code
     * @param string $remark_1
     * @param string $remark_2
     * @param string $details
     * @param string $type
     */
    public function __construct(?string $dateTimeZone = null, ?string $code = null, ?string $remark_1 = null, ?string $remark_2 = null, ?string $details = null, ?string $type = null)
    {
        $this
            ->setDateTimeZone($dateTimeZone)
            ->setCode($code)
            ->setRemark_1($remark_1)
            ->setRemark_2($remark_2)
            ->setDetails($details)
            ->setType($type);
    }
    /**
     * Get dateTimeZone value
     * @return string|null
     */
    public function getDateTimeZone(): ?string
    {
        return $this->dateTimeZone;
    }
    /**
     * Set dateTimeZone value
     * @param string $dateTimeZone
     * @return \AppturePay\DSV\StructType\TrackingAndTracing
     */
    public function setDateTimeZone(?string $dateTimeZone = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeZone) && !is_string($dateTimeZone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeZone, true), gettype($dateTimeZone)), __LINE__);
        }
        $this->dateTimeZone = $dateTimeZone;
        
        return $this;
    }
    /**
     * Get code value
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }
    /**
     * Set code value
     * @param string $code
     * @return \AppturePay\DSV\StructType\TrackingAndTracing
     */
    public function setCode(?string $code = null): self
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->code = $code;
        
        return $this;
    }
    /**
     * Get remark value
     * @return remark
     */
    public function getRemark_1(): ?string
    {
        return $this->remark_1;
    }
    /**
     * Set remark value
     * @param remark $remark
     * @return \AppturePay\DSV\StructType\TrackingAndTracing
     */
    public function setRemark_1(?string $remark_1 = null): self
    {
        // validation for constraint: string
        if (!is_null($remark_1) && !is_string($remark_1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($remark_1, true), gettype($remark_1)), __LINE__);
        }
        $this->remark_1 = $remark_1;
        
        return $this;
    }
    /**
     * Get remark value
     * @return remark
     */
    public function getRemark_2(): ?string
    {
        return $this->remark_2;
    }
    /**
     * Set remark value
     * @param remark $remark
     * @return \AppturePay\DSV\StructType\TrackingAndTracing
     */
    public function setRemark_2(?string $remark_2 = null): self
    {
        // validation for constraint: string
        if (!is_null($remark_2) && !is_string($remark_2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($remark_2, true), gettype($remark_2)), __LINE__);
        }
        $this->remark_2 = $remark_2;
        
        return $this;
    }
    /**
     * Get details value
     * @return string|null
     */
    public function getDetails(): ?string
    {
        return $this->details;
    }
    /**
     * Set details value
     * @param string $details
     * @return \AppturePay\DSV\StructType\TrackingAndTracing
     */
    public function setDetails(?string $details = null): self
    {
        // validation for constraint: string
        if (!is_null($details) && !is_string($details)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($details, true), gettype($details)), __LINE__);
        }
        $this->details = $details;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TrackingAndTracing
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
