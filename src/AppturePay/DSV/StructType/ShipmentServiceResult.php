<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentServiceResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ShipmentServiceResult
 * @subpackage Structs
 */
class ShipmentServiceResult extends AbstractStructBase
{
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1 | 0
     * @var bool
     */
    protected bool $Status;
    /**
     * The ErrorMessage
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ErrorMessage = null;
    /**
     * The ErrorDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ErrorDetails = null;
    /**
     * Constructor method for ShipmentServiceResult
     * @uses ShipmentServiceResult::setStatus()
     * @uses ShipmentServiceResult::setErrorMessage()
     * @uses ShipmentServiceResult::setErrorDetails()
     * @param bool $status
     * @param string $errorMessage
     * @param string $errorDetails
     */
    public function __construct(bool $status, ?string $errorMessage = null, ?string $errorDetails = null)
    {
        $this
            ->setStatus($status)
            ->setErrorMessage($errorMessage)
            ->setErrorDetails($errorDetails);
    }
    /**
     * Get Status value
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param bool $status
     * @return \AppturePay\DSV\StructType\ShipmentServiceResult
     */
    public function setStatus(bool $status): self
    {
        // validation for constraint: boolean
        if (!is_null($status) && !is_bool($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get ErrorMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return isset($this->ErrorMessage) ? $this->ErrorMessage : null;
    }
    /**
     * Set ErrorMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $errorMessage
     * @return \AppturePay\DSV\StructType\ShipmentServiceResult
     */
    public function setErrorMessage(?string $errorMessage = null): self
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorMessage, true), gettype($errorMessage)), __LINE__);
        }
        if (is_null($errorMessage) || (is_array($errorMessage) && empty($errorMessage))) {
            unset($this->ErrorMessage);
        } else {
            $this->ErrorMessage = $errorMessage;
        }
        
        return $this;
    }
    /**
     * Get ErrorDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getErrorDetails(): ?string
    {
        return isset($this->ErrorDetails) ? $this->ErrorDetails : null;
    }
    /**
     * Set ErrorDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $errorDetails
     * @return \AppturePay\DSV\StructType\ShipmentServiceResult
     */
    public function setErrorDetails(?string $errorDetails = null): self
    {
        // validation for constraint: string
        if (!is_null($errorDetails) && !is_string($errorDetails)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorDetails, true), gettype($errorDetails)), __LINE__);
        }
        if (is_null($errorDetails) || (is_array($errorDetails) && empty($errorDetails))) {
            unset($this->ErrorDetails);
        } else {
            $this->ErrorDetails = $errorDetails;
        }
        
        return $this;
    }
}
