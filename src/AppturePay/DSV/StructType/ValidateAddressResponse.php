<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateAddressResponse StructType
 * @subpackage Structs
 */
class ValidateAddressResponse extends AbstractStructBase
{
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * The ErrorMessage
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ErrorMessage = null;
    /**
     * The AddressList
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\ArrayType\ArrayOfValidAddress|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfValidAddress $AddressList = null;
    /**
     * The ValidateAddressResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ValidateAddressResponse|null
     */
    protected ?\AppturePay\DSV\StructType\ValidateAddressResponse $ValidateAddressResult = null;
    /**
     * Constructor method for ValidateAddressResponse
     * @uses ValidateAddressResponse::setStatus()
     * @uses ValidateAddressResponse::setErrorMessage()
     * @uses ValidateAddressResponse::setAddressList()
     * @uses ValidateAddressResponse::setValidateAddressResult()
     * @param string $status
     * @param string $errorMessage
     * @param \AppturePay\DSV\ArrayType\ArrayOfValidAddress $addressList
     * @param \AppturePay\DSV\StructType\ValidateAddressResponse $validateAddressResult
     */
    public function __construct(?string $status = null, ?string $errorMessage = null, ?\AppturePay\DSV\ArrayType\ArrayOfValidAddress $addressList = null, ?\AppturePay\DSV\StructType\ValidateAddressResponse $validateAddressResult = null)
    {
        $this
            ->setStatus($status)
            ->setErrorMessage($errorMessage)
            ->setAddressList($addressList)
            ->setValidateAddressResult($validateAddressResult);
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param string $status
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get ErrorMessage value
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->ErrorMessage;
    }
    /**
     * Set ErrorMessage value
     * @param string $errorMessage
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse
     */
    public function setErrorMessage(?string $errorMessage = null): self
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorMessage, true), gettype($errorMessage)), __LINE__);
        }
        $this->ErrorMessage = $errorMessage;
        
        return $this;
    }
    /**
     * Get AddressList value
     * @return \AppturePay\DSV\ArrayType\ArrayOfValidAddress|null
     */
    public function getAddressList(): ?\AppturePay\DSV\ArrayType\ArrayOfValidAddress
    {
        return $this->AddressList;
    }
    /**
     * Set AddressList value
     * @param \AppturePay\DSV\ArrayType\ArrayOfValidAddress $addressList
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse
     */
    public function setAddressList(?\AppturePay\DSV\ArrayType\ArrayOfValidAddress $addressList = null): self
    {
        $this->AddressList = $addressList;
        
        return $this;
    }
    /**
     * Get ValidateAddressResult value
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse|null
     */
    public function getValidateAddressResult(): ?\AppturePay\DSV\StructType\ValidateAddressResponse
    {
        return $this->ValidateAddressResult;
    }
    /**
     * Set ValidateAddressResult value
     * @param \AppturePay\DSV\StructType\ValidateAddressResponse $validateAddressResult
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse
     */
    public function setValidateAddressResult(?\AppturePay\DSV\StructType\ValidateAddressResponse $validateAddressResult = null): self
    {
        $this->ValidateAddressResult = $validateAddressResult;
        
        return $this;
    }
}
