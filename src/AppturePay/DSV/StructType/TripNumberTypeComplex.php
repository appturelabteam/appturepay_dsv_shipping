<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TripNumberTypeComplex StructType
 * @subpackage Structs
 */
class TripNumberTypeComplex extends AbstractStructBase
{
    /**
     * The _
     * @var int|null
     */
    protected ?int $_ = null;
    /**
     * The tripReference
     * @var string|null
     */
    protected ?string $tripReference = null;
    /**
     * Constructor method for TripNumberTypeComplex
     * @uses TripNumberTypeComplex::set_()
     * @uses TripNumberTypeComplex::setTripReference()
     * @param int $_
     * @param string $tripReference
     */
    public function __construct(?int $_ = null, ?string $tripReference = null)
    {
        $this
            ->set_($_)
            ->setTripReference($tripReference);
    }
    /**
     * Get _ value
     * @return int|null
     */
    public function get_(): ?int
    {
        return $this->_;
    }
    /**
     * Set _ value
     * @param int $_
     * @return \AppturePay\DSV\StructType\TripNumberTypeComplex
     */
    public function set_(?int $_ = null): self
    {
        // validation for constraint: int
        if (!is_null($_) && !(is_int($_) || ctype_digit($_))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($_, true), gettype($_)), __LINE__);
        }
        $this->_ = $_;
        
        return $this;
    }
    /**
     * Get tripReference value
     * @return string|null
     */
    public function getTripReference(): ?string
    {
        return $this->tripReference;
    }
    /**
     * Set tripReference value
     * @param string $tripReference
     * @return \AppturePay\DSV\StructType\TripNumberTypeComplex
     */
    public function setTripReference(?string $tripReference = null): self
    {
        // validation for constraint: string
        if (!is_null($tripReference) && !is_string($tripReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tripReference, true), gettype($tripReference)), __LINE__);
        }
        $this->tripReference = $tripReference;
        
        return $this;
    }
}
