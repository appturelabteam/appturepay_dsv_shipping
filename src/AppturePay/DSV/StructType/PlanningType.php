<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PlanningType StructType
 * @subpackage Structs
 */
class PlanningType extends AbstractStructBase
{
    /**
     * The action
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $action = null;
    /**
     * The tripNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TripNumberTypeComplex|null
     */
    protected ?\AppturePay\DSV\StructType\TripNumberTypeComplex $tripNumber = null;
    /**
     * The equipmentSequence
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $equipmentSequence = null;
    /**
     * The tripSequence
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $tripSequence = null;
    /**
     * The fromDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromDate = null;
    /**
     * The fromTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromTime = null;
    /**
     * The fromHubzone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromHubzone = null;
    /**
     * The toDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $toDate = null;
    /**
     * The toTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $toTime = null;
    /**
     * The toHubzone
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $toHubzone = null;
    /**
     * The plannable
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $plannable = null;
    /**
     * The legReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $legReference = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for PlanningType
     * @uses PlanningType::setAction()
     * @uses PlanningType::setTripNumber()
     * @uses PlanningType::setEquipmentSequence()
     * @uses PlanningType::setTripSequence()
     * @uses PlanningType::setFromDate()
     * @uses PlanningType::setFromTime()
     * @uses PlanningType::setFromHubzone()
     * @uses PlanningType::setToDate()
     * @uses PlanningType::setToTime()
     * @uses PlanningType::setToHubzone()
     * @uses PlanningType::setPlannable()
     * @uses PlanningType::setLegReference()
     * @uses PlanningType::setType()
     * @param int $action
     * @param \AppturePay\DSV\StructType\TripNumberTypeComplex $tripNumber
     * @param int $equipmentSequence
     * @param int $tripSequence
     * @param string $fromDate
     * @param string $fromTime
     * @param string $fromHubzone
     * @param string $toDate
     * @param string $toTime
     * @param string $toHubzone
     * @param string $plannable
     * @param string $legReference
     * @param string $type
     */
    public function __construct(?int $action = null, ?\AppturePay\DSV\StructType\TripNumberTypeComplex $tripNumber = null, ?int $equipmentSequence = null, ?int $tripSequence = null, ?string $fromDate = null, ?string $fromTime = null, ?string $fromHubzone = null, ?string $toDate = null, ?string $toTime = null, ?string $toHubzone = null, ?string $plannable = null, ?string $legReference = null, ?string $type = null)
    {
        $this
            ->setAction($action)
            ->setTripNumber($tripNumber)
            ->setEquipmentSequence($equipmentSequence)
            ->setTripSequence($tripSequence)
            ->setFromDate($fromDate)
            ->setFromTime($fromTime)
            ->setFromHubzone($fromHubzone)
            ->setToDate($toDate)
            ->setToTime($toTime)
            ->setToHubzone($toHubzone)
            ->setPlannable($plannable)
            ->setLegReference($legReference)
            ->setType($type);
    }
    /**
     * Get action value
     * @return int|null
     */
    public function getAction(): ?int
    {
        return $this->action;
    }
    /**
     * Set action value
     * @param int $action
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setAction(?int $action = null): self
    {
        // validation for constraint: int
        if (!is_null($action) && !(is_int($action) || ctype_digit($action))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($action, true), gettype($action)), __LINE__);
        }
        $this->action = $action;
        
        return $this;
    }
    /**
     * Get tripNumber value
     * @return \AppturePay\DSV\StructType\TripNumberTypeComplex|null
     */
    public function getTripNumber(): ?\AppturePay\DSV\StructType\TripNumberTypeComplex
    {
        return $this->tripNumber;
    }
    /**
     * Set tripNumber value
     * @param \AppturePay\DSV\StructType\TripNumberTypeComplex $tripNumber
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setTripNumber(?\AppturePay\DSV\StructType\TripNumberTypeComplex $tripNumber = null): self
    {
        $this->tripNumber = $tripNumber;
        
        return $this;
    }
    /**
     * Get equipmentSequence value
     * @return int|null
     */
    public function getEquipmentSequence(): ?int
    {
        return $this->equipmentSequence;
    }
    /**
     * Set equipmentSequence value
     * @param int $equipmentSequence
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setEquipmentSequence(?int $equipmentSequence = null): self
    {
        // validation for constraint: int
        if (!is_null($equipmentSequence) && !(is_int($equipmentSequence) || ctype_digit($equipmentSequence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($equipmentSequence, true), gettype($equipmentSequence)), __LINE__);
        }
        $this->equipmentSequence = $equipmentSequence;
        
        return $this;
    }
    /**
     * Get tripSequence value
     * @return int|null
     */
    public function getTripSequence(): ?int
    {
        return $this->tripSequence;
    }
    /**
     * Set tripSequence value
     * @param int $tripSequence
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setTripSequence(?int $tripSequence = null): self
    {
        // validation for constraint: int
        if (!is_null($tripSequence) && !(is_int($tripSequence) || ctype_digit($tripSequence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($tripSequence, true), gettype($tripSequence)), __LINE__);
        }
        $this->tripSequence = $tripSequence;
        
        return $this;
    }
    /**
     * Get fromDate value
     * @return string|null
     */
    public function getFromDate(): ?string
    {
        return $this->fromDate;
    }
    /**
     * Set fromDate value
     * @param string $fromDate
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setFromDate(?string $fromDate = null): self
    {
        // validation for constraint: string
        if (!is_null($fromDate) && !is_string($fromDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromDate, true), gettype($fromDate)), __LINE__);
        }
        $this->fromDate = $fromDate;
        
        return $this;
    }
    /**
     * Get fromTime value
     * @return string|null
     */
    public function getFromTime(): ?string
    {
        return $this->fromTime;
    }
    /**
     * Set fromTime value
     * @param string $fromTime
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setFromTime(?string $fromTime = null): self
    {
        // validation for constraint: string
        if (!is_null($fromTime) && !is_string($fromTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromTime, true), gettype($fromTime)), __LINE__);
        }
        $this->fromTime = $fromTime;
        
        return $this;
    }
    /**
     * Get fromHubzone value
     * @return string|null
     */
    public function getFromHubzone(): ?string
    {
        return $this->fromHubzone;
    }
    /**
     * Set fromHubzone value
     * @param string $fromHubzone
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setFromHubzone(?string $fromHubzone = null): self
    {
        // validation for constraint: string
        if (!is_null($fromHubzone) && !is_string($fromHubzone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromHubzone, true), gettype($fromHubzone)), __LINE__);
        }
        $this->fromHubzone = $fromHubzone;
        
        return $this;
    }
    /**
     * Get toDate value
     * @return string|null
     */
    public function getToDate(): ?string
    {
        return $this->toDate;
    }
    /**
     * Set toDate value
     * @param string $toDate
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setToDate(?string $toDate = null): self
    {
        // validation for constraint: string
        if (!is_null($toDate) && !is_string($toDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDate, true), gettype($toDate)), __LINE__);
        }
        $this->toDate = $toDate;
        
        return $this;
    }
    /**
     * Get toTime value
     * @return string|null
     */
    public function getToTime(): ?string
    {
        return $this->toTime;
    }
    /**
     * Set toTime value
     * @param string $toTime
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setToTime(?string $toTime = null): self
    {
        // validation for constraint: string
        if (!is_null($toTime) && !is_string($toTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toTime, true), gettype($toTime)), __LINE__);
        }
        $this->toTime = $toTime;
        
        return $this;
    }
    /**
     * Get toHubzone value
     * @return string|null
     */
    public function getToHubzone(): ?string
    {
        return $this->toHubzone;
    }
    /**
     * Set toHubzone value
     * @param string $toHubzone
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setToHubzone(?string $toHubzone = null): self
    {
        // validation for constraint: string
        if (!is_null($toHubzone) && !is_string($toHubzone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toHubzone, true), gettype($toHubzone)), __LINE__);
        }
        $this->toHubzone = $toHubzone;
        
        return $this;
    }
    /**
     * Get plannable value
     * @return string|null
     */
    public function getPlannable(): ?string
    {
        return $this->plannable;
    }
    /**
     * Set plannable value
     * @param string $plannable
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setPlannable(?string $plannable = null): self
    {
        // validation for constraint: string
        if (!is_null($plannable) && !is_string($plannable)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($plannable, true), gettype($plannable)), __LINE__);
        }
        $this->plannable = $plannable;
        
        return $this;
    }
    /**
     * Get legReference value
     * @return string|null
     */
    public function getLegReference(): ?string
    {
        return $this->legReference;
    }
    /**
     * Set legReference value
     * @param string $legReference
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setLegReference(?string $legReference = null): self
    {
        // validation for constraint: string
        if (!is_null($legReference) && !is_string($legReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($legReference, true), gettype($legReference)), __LINE__);
        }
        $this->legReference = $legReference;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\PlanningType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
