<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LinkType StructType
 * @subpackage Structs
 */
class LinkType extends AbstractStructBase
{
    /**
     * The linkType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $linkType = null;
    /**
     * The linkText
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $linkText = null;
    /**
     * The linkAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $linkAddress = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for LinkType
     * @uses LinkType::setLinkType()
     * @uses LinkType::setLinkText()
     * @uses LinkType::setLinkAddress()
     * @uses LinkType::setType()
     * @param int $linkType
     * @param string $linkText
     * @param string $linkAddress
     * @param string $type
     */
    public function __construct(?int $linkType = null, ?string $linkText = null, ?string $linkAddress = null, ?string $type = null)
    {
        $this
            ->setLinkType($linkType)
            ->setLinkText($linkText)
            ->setLinkAddress($linkAddress)
            ->setType($type);
    }
    /**
     * Get linkType value
     * @return int|null
     */
    public function getLinkType(): ?int
    {
        return $this->linkType;
    }
    /**
     * Set linkType value
     * @param int $linkType
     * @return \AppturePay\DSV\StructType\LinkType
     */
    public function setLinkType(?int $linkType = null): self
    {
        // validation for constraint: int
        if (!is_null($linkType) && !(is_int($linkType) || ctype_digit($linkType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($linkType, true), gettype($linkType)), __LINE__);
        }
        $this->linkType = $linkType;
        
        return $this;
    }
    /**
     * Get linkText value
     * @return string|null
     */
    public function getLinkText(): ?string
    {
        return $this->linkText;
    }
    /**
     * Set linkText value
     * @param string $linkText
     * @return \AppturePay\DSV\StructType\LinkType
     */
    public function setLinkText(?string $linkText = null): self
    {
        // validation for constraint: string
        if (!is_null($linkText) && !is_string($linkText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($linkText, true), gettype($linkText)), __LINE__);
        }
        $this->linkText = $linkText;
        
        return $this;
    }
    /**
     * Get linkAddress value
     * @return string|null
     */
    public function getLinkAddress(): ?string
    {
        return $this->linkAddress;
    }
    /**
     * Set linkAddress value
     * @param string $linkAddress
     * @return \AppturePay\DSV\StructType\LinkType
     */
    public function setLinkAddress(?string $linkAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($linkAddress) && !is_string($linkAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($linkAddress, true), gettype($linkAddress)), __LINE__);
        }
        $this->linkAddress = $linkAddress;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\LinkType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
