<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TariffSpecificationType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:TariffSpecificationType
 * @subpackage Structs
 */
class TariffSpecificationType_1 extends AbstractStructBase
{
    /**
     * The tariffNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $tariffNumber = null;
    /**
     * The validFrom
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $validFrom = null;
    /**
     * The validThru
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $validThru = null;
    /**
     * The relationNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $relationNumber = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The relationNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $relationNumberField = null;
    /**
     * The tariffNumberField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $tariffNumberField = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * The validFromField
     * @var string|null
     */
    protected ?string $validFromField = null;
    /**
     * The validFromStringFieldSpecified
     * @var bool|null
     */
    protected ?bool $validFromStringFieldSpecified = null;
    /**
     * The validThruField
     * @var string|null
     */
    protected ?string $validThruField = null;
    /**
     * The validThruStringFieldSpecified
     * @var bool|null
     */
    protected ?bool $validThruStringFieldSpecified = null;
    /**
     * Constructor method for TariffSpecificationType
     * @uses TariffSpecificationType_1::setTariffNumber()
     * @uses TariffSpecificationType_1::setValidFrom()
     * @uses TariffSpecificationType_1::setValidThru()
     * @uses TariffSpecificationType_1::setRelationNumber()
     * @uses TariffSpecificationType_1::setType()
     * @uses TariffSpecificationType_1::setRelationNumberField()
     * @uses TariffSpecificationType_1::setTariffNumberField()
     * @uses TariffSpecificationType_1::setTypeField()
     * @uses TariffSpecificationType_1::setValidFromField()
     * @uses TariffSpecificationType_1::setValidFromStringFieldSpecified()
     * @uses TariffSpecificationType_1::setValidThruField()
     * @uses TariffSpecificationType_1::setValidThruStringFieldSpecified()
     * @param int $tariffNumber
     * @param string $validFrom
     * @param string $validThru
     * @param int $relationNumber
     * @param string $type
     * @param string $relationNumberField
     * @param string $tariffNumberField
     * @param string $typeField
     * @param string $validFromField
     * @param bool $validFromStringFieldSpecified
     * @param string $validThruField
     * @param bool $validThruStringFieldSpecified
     */
    public function __construct(?int $tariffNumber = null, ?string $validFrom = null, ?string $validThru = null, ?int $relationNumber = null, ?string $type = null, ?string $relationNumberField = null, ?string $tariffNumberField = null, ?string $typeField = null, ?string $validFromField = null, ?bool $validFromStringFieldSpecified = null, ?string $validThruField = null, ?bool $validThruStringFieldSpecified = null)
    {
        $this
            ->setTariffNumber($tariffNumber)
            ->setValidFrom($validFrom)
            ->setValidThru($validThru)
            ->setRelationNumber($relationNumber)
            ->setType($type)
            ->setRelationNumberField($relationNumberField)
            ->setTariffNumberField($tariffNumberField)
            ->setTypeField($typeField)
            ->setValidFromField($validFromField)
            ->setValidFromStringFieldSpecified($validFromStringFieldSpecified)
            ->setValidThruField($validThruField)
            ->setValidThruStringFieldSpecified($validThruStringFieldSpecified);
    }
    /**
     * Get tariffNumber value
     * @return int|null
     */
    public function getTariffNumber(): ?int
    {
        return $this->tariffNumber;
    }
    /**
     * Set tariffNumber value
     * @param int $tariffNumber
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setTariffNumber(?int $tariffNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($tariffNumber) && !(is_int($tariffNumber) || ctype_digit($tariffNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($tariffNumber, true), gettype($tariffNumber)), __LINE__);
        }
        $this->tariffNumber = $tariffNumber;
        
        return $this;
    }
    /**
     * Get validFrom value
     * @return string|null
     */
    public function getValidFrom(): ?string
    {
        return $this->validFrom;
    }
    /**
     * Set validFrom value
     * @param string $validFrom
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setValidFrom(?string $validFrom = null): self
    {
        // validation for constraint: string
        if (!is_null($validFrom) && !is_string($validFrom)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validFrom, true), gettype($validFrom)), __LINE__);
        }
        $this->validFrom = $validFrom;
        
        return $this;
    }
    /**
     * Get validThru value
     * @return string|null
     */
    public function getValidThru(): ?string
    {
        return $this->validThru;
    }
    /**
     * Set validThru value
     * @param string $validThru
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setValidThru(?string $validThru = null): self
    {
        // validation for constraint: string
        if (!is_null($validThru) && !is_string($validThru)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validThru, true), gettype($validThru)), __LINE__);
        }
        $this->validThru = $validThru;
        
        return $this;
    }
    /**
     * Get relationNumber value
     * @return int|null
     */
    public function getRelationNumber(): ?int
    {
        return $this->relationNumber;
    }
    /**
     * Set relationNumber value
     * @param int $relationNumber
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setRelationNumber(?int $relationNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($relationNumber) && !(is_int($relationNumber) || ctype_digit($relationNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relationNumber, true), gettype($relationNumber)), __LINE__);
        }
        $this->relationNumber = $relationNumber;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get relationNumberField value
     * @return string|null
     */
    public function getRelationNumberField(): ?string
    {
        return $this->relationNumberField;
    }
    /**
     * Set relationNumberField value
     * @param string $relationNumberField
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setRelationNumberField(?string $relationNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($relationNumberField) && !is_string($relationNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($relationNumberField, true), gettype($relationNumberField)), __LINE__);
        }
        $this->relationNumberField = $relationNumberField;
        
        return $this;
    }
    /**
     * Get tariffNumberField value
     * @return string|null
     */
    public function getTariffNumberField(): ?string
    {
        return $this->tariffNumberField;
    }
    /**
     * Set tariffNumberField value
     * @param string $tariffNumberField
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setTariffNumberField(?string $tariffNumberField = null): self
    {
        // validation for constraint: string
        if (!is_null($tariffNumberField) && !is_string($tariffNumberField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tariffNumberField, true), gettype($tariffNumberField)), __LINE__);
        }
        $this->tariffNumberField = $tariffNumberField;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
    /**
     * Get validFromField value
     * @return string|null
     */
    public function getValidFromField(): ?string
    {
        return $this->validFromField;
    }
    /**
     * Set validFromField value
     * @param string $validFromField
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setValidFromField(?string $validFromField = null): self
    {
        // validation for constraint: string
        if (!is_null($validFromField) && !is_string($validFromField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validFromField, true), gettype($validFromField)), __LINE__);
        }
        $this->validFromField = $validFromField;
        
        return $this;
    }
    /**
     * Get validFromStringFieldSpecified value
     * @return bool|null
     */
    public function getValidFromStringFieldSpecified(): ?bool
    {
        return $this->validFromStringFieldSpecified;
    }
    /**
     * Set validFromStringFieldSpecified value
     * @param bool $validFromStringFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setValidFromStringFieldSpecified(?bool $validFromStringFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($validFromStringFieldSpecified) && !is_bool($validFromStringFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($validFromStringFieldSpecified, true), gettype($validFromStringFieldSpecified)), __LINE__);
        }
        $this->validFromStringFieldSpecified = $validFromStringFieldSpecified;
        
        return $this;
    }
    /**
     * Get validThruField value
     * @return string|null
     */
    public function getValidThruField(): ?string
    {
        return $this->validThruField;
    }
    /**
     * Set validThruField value
     * @param string $validThruField
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setValidThruField(?string $validThruField = null): self
    {
        // validation for constraint: string
        if (!is_null($validThruField) && !is_string($validThruField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validThruField, true), gettype($validThruField)), __LINE__);
        }
        $this->validThruField = $validThruField;
        
        return $this;
    }
    /**
     * Get validThruStringFieldSpecified value
     * @return bool|null
     */
    public function getValidThruStringFieldSpecified(): ?bool
    {
        return $this->validThruStringFieldSpecified;
    }
    /**
     * Set validThruStringFieldSpecified value
     * @param bool $validThruStringFieldSpecified
     * @return \AppturePay\DSV\StructType\TariffSpecificationType_1
     */
    public function setValidThruStringFieldSpecified(?bool $validThruStringFieldSpecified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($validThruStringFieldSpecified) && !is_bool($validThruStringFieldSpecified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($validThruStringFieldSpecified, true), gettype($validThruStringFieldSpecified)), __LINE__);
        }
        $this->validThruStringFieldSpecified = $validThruStringFieldSpecified;
        
        return $this;
    }
}
