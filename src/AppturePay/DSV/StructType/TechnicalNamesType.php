<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TechnicalNamesType StructType
 * @subpackage Structs
 */
class TechnicalNamesType extends AbstractStructBase
{
    /**
     * The isoLanguageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $isoLanguageCode = null;
    /**
     * The technicalName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $technicalName = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for TechnicalNamesType
     * @uses TechnicalNamesType::setIsoLanguageCode()
     * @uses TechnicalNamesType::setTechnicalName()
     * @uses TechnicalNamesType::setType()
     * @param string $isoLanguageCode
     * @param string $technicalName
     * @param string $type
     */
    public function __construct(?string $isoLanguageCode = null, ?string $technicalName = null, ?string $type = null)
    {
        $this
            ->setIsoLanguageCode($isoLanguageCode)
            ->setTechnicalName($technicalName)
            ->setType($type);
    }
    /**
     * Get isoLanguageCode value
     * @return string|null
     */
    public function getIsoLanguageCode(): ?string
    {
        return $this->isoLanguageCode;
    }
    /**
     * Set isoLanguageCode value
     * @param string $isoLanguageCode
     * @return \AppturePay\DSV\StructType\TechnicalNamesType
     */
    public function setIsoLanguageCode(?string $isoLanguageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($isoLanguageCode) && !is_string($isoLanguageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($isoLanguageCode, true), gettype($isoLanguageCode)), __LINE__);
        }
        $this->isoLanguageCode = $isoLanguageCode;
        
        return $this;
    }
    /**
     * Get technicalName value
     * @return string|null
     */
    public function getTechnicalName(): ?string
    {
        return $this->technicalName;
    }
    /**
     * Set technicalName value
     * @param string $technicalName
     * @return \AppturePay\DSV\StructType\TechnicalNamesType
     */
    public function setTechnicalName(?string $technicalName = null): self
    {
        // validation for constraint: string
        if (!is_null($technicalName) && !is_string($technicalName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($technicalName, true), gettype($technicalName)), __LINE__);
        }
        $this->technicalName = $technicalName;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\TechnicalNamesType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
