<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SeafreightType StructType
 * @subpackage Structs
 */
class SeafreightType extends AbstractStructBase
{
    /**
     * The finalDestinationPlaceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $finalDestinationPlaceCode = null;
    /**
     * The fclLcl
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fclLcl = null;
    /**
     * The originals
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $originals = null;
    /**
     * The copies
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $copies = null;
    /**
     * The vesselCurrencyCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $vesselCurrencyCode = null;
    /**
     * The vesselRate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $vesselRate = null;
    /**
     * The disbursementPrepaidCollect
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $disbursementPrepaidCollect = null;
    /**
     * The placeCodeReceipt
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $placeCodeReceipt = null;
    /**
     * The placeOfReceipt
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $placeOfReceipt = null;
    /**
     * The placeCodeDelivery
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $placeCodeDelivery = null;
    /**
     * The placeOfDelivery
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $placeOfDelivery = null;
    /**
     * Constructor method for SeafreightType
     * @uses SeafreightType::setFinalDestinationPlaceCode()
     * @uses SeafreightType::setFclLcl()
     * @uses SeafreightType::setOriginals()
     * @uses SeafreightType::setCopies()
     * @uses SeafreightType::setVesselCurrencyCode()
     * @uses SeafreightType::setVesselRate()
     * @uses SeafreightType::setDisbursementPrepaidCollect()
     * @uses SeafreightType::setPlaceCodeReceipt()
     * @uses SeafreightType::setPlaceOfReceipt()
     * @uses SeafreightType::setPlaceCodeDelivery()
     * @uses SeafreightType::setPlaceOfDelivery()
     * @param string $finalDestinationPlaceCode
     * @param string $fclLcl
     * @param int $originals
     * @param int $copies
     * @param string $vesselCurrencyCode
     * @param float $vesselRate
     * @param string $disbursementPrepaidCollect
     * @param string $placeCodeReceipt
     * @param string $placeOfReceipt
     * @param string $placeCodeDelivery
     * @param string $placeOfDelivery
     */
    public function __construct(?string $finalDestinationPlaceCode = null, ?string $fclLcl = null, ?int $originals = null, ?int $copies = null, ?string $vesselCurrencyCode = null, ?float $vesselRate = null, ?string $disbursementPrepaidCollect = null, ?string $placeCodeReceipt = null, ?string $placeOfReceipt = null, ?string $placeCodeDelivery = null, ?string $placeOfDelivery = null)
    {
        $this
            ->setFinalDestinationPlaceCode($finalDestinationPlaceCode)
            ->setFclLcl($fclLcl)
            ->setOriginals($originals)
            ->setCopies($copies)
            ->setVesselCurrencyCode($vesselCurrencyCode)
            ->setVesselRate($vesselRate)
            ->setDisbursementPrepaidCollect($disbursementPrepaidCollect)
            ->setPlaceCodeReceipt($placeCodeReceipt)
            ->setPlaceOfReceipt($placeOfReceipt)
            ->setPlaceCodeDelivery($placeCodeDelivery)
            ->setPlaceOfDelivery($placeOfDelivery);
    }
    /**
     * Get finalDestinationPlaceCode value
     * @return string|null
     */
    public function getFinalDestinationPlaceCode(): ?string
    {
        return $this->finalDestinationPlaceCode;
    }
    /**
     * Set finalDestinationPlaceCode value
     * @param string $finalDestinationPlaceCode
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setFinalDestinationPlaceCode(?string $finalDestinationPlaceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($finalDestinationPlaceCode) && !is_string($finalDestinationPlaceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($finalDestinationPlaceCode, true), gettype($finalDestinationPlaceCode)), __LINE__);
        }
        $this->finalDestinationPlaceCode = $finalDestinationPlaceCode;
        
        return $this;
    }
    /**
     * Get fclLcl value
     * @return string|null
     */
    public function getFclLcl(): ?string
    {
        return $this->fclLcl;
    }
    /**
     * Set fclLcl value
     * @param string $fclLcl
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setFclLcl(?string $fclLcl = null): self
    {
        // validation for constraint: string
        if (!is_null($fclLcl) && !is_string($fclLcl)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fclLcl, true), gettype($fclLcl)), __LINE__);
        }
        $this->fclLcl = $fclLcl;
        
        return $this;
    }
    /**
     * Get originals value
     * @return int|null
     */
    public function getOriginals(): ?int
    {
        return $this->originals;
    }
    /**
     * Set originals value
     * @param int $originals
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setOriginals(?int $originals = null): self
    {
        // validation for constraint: int
        if (!is_null($originals) && !(is_int($originals) || ctype_digit($originals))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($originals, true), gettype($originals)), __LINE__);
        }
        $this->originals = $originals;
        
        return $this;
    }
    /**
     * Get copies value
     * @return int|null
     */
    public function getCopies(): ?int
    {
        return $this->copies;
    }
    /**
     * Set copies value
     * @param int $copies
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setCopies(?int $copies = null): self
    {
        // validation for constraint: int
        if (!is_null($copies) && !(is_int($copies) || ctype_digit($copies))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($copies, true), gettype($copies)), __LINE__);
        }
        $this->copies = $copies;
        
        return $this;
    }
    /**
     * Get vesselCurrencyCode value
     * @return string|null
     */
    public function getVesselCurrencyCode(): ?string
    {
        return $this->vesselCurrencyCode;
    }
    /**
     * Set vesselCurrencyCode value
     * @param string $vesselCurrencyCode
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setVesselCurrencyCode(?string $vesselCurrencyCode = null): self
    {
        // validation for constraint: string
        if (!is_null($vesselCurrencyCode) && !is_string($vesselCurrencyCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vesselCurrencyCode, true), gettype($vesselCurrencyCode)), __LINE__);
        }
        $this->vesselCurrencyCode = $vesselCurrencyCode;
        
        return $this;
    }
    /**
     * Get vesselRate value
     * @return float|null
     */
    public function getVesselRate(): ?float
    {
        return $this->vesselRate;
    }
    /**
     * Set vesselRate value
     * @param float $vesselRate
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setVesselRate(?float $vesselRate = null): self
    {
        // validation for constraint: float
        if (!is_null($vesselRate) && !(is_float($vesselRate) || is_numeric($vesselRate))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vesselRate, true), gettype($vesselRate)), __LINE__);
        }
        $this->vesselRate = $vesselRate;
        
        return $this;
    }
    /**
     * Get disbursementPrepaidCollect value
     * @return string|null
     */
    public function getDisbursementPrepaidCollect(): ?string
    {
        return $this->disbursementPrepaidCollect;
    }
    /**
     * Set disbursementPrepaidCollect value
     * @uses \AppturePay\DSV\EnumType\DisbursementPrepaidCollectType::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\DisbursementPrepaidCollectType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $disbursementPrepaidCollect
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setDisbursementPrepaidCollect(?string $disbursementPrepaidCollect = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\DisbursementPrepaidCollectType::valueIsValid($disbursementPrepaidCollect)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\DisbursementPrepaidCollectType', is_array($disbursementPrepaidCollect) ? implode(', ', $disbursementPrepaidCollect) : var_export($disbursementPrepaidCollect, true), implode(', ', \AppturePay\DSV\EnumType\DisbursementPrepaidCollectType::getValidValues())), __LINE__);
        }
        $this->disbursementPrepaidCollect = $disbursementPrepaidCollect;
        
        return $this;
    }
    /**
     * Get placeCodeReceipt value
     * @return string|null
     */
    public function getPlaceCodeReceipt(): ?string
    {
        return $this->placeCodeReceipt;
    }
    /**
     * Set placeCodeReceipt value
     * @param string $placeCodeReceipt
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setPlaceCodeReceipt(?string $placeCodeReceipt = null): self
    {
        // validation for constraint: string
        if (!is_null($placeCodeReceipt) && !is_string($placeCodeReceipt)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($placeCodeReceipt, true), gettype($placeCodeReceipt)), __LINE__);
        }
        $this->placeCodeReceipt = $placeCodeReceipt;
        
        return $this;
    }
    /**
     * Get placeOfReceipt value
     * @return string|null
     */
    public function getPlaceOfReceipt(): ?string
    {
        return $this->placeOfReceipt;
    }
    /**
     * Set placeOfReceipt value
     * @param string $placeOfReceipt
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setPlaceOfReceipt(?string $placeOfReceipt = null): self
    {
        // validation for constraint: string
        if (!is_null($placeOfReceipt) && !is_string($placeOfReceipt)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($placeOfReceipt, true), gettype($placeOfReceipt)), __LINE__);
        }
        $this->placeOfReceipt = $placeOfReceipt;
        
        return $this;
    }
    /**
     * Get placeCodeDelivery value
     * @return string|null
     */
    public function getPlaceCodeDelivery(): ?string
    {
        return $this->placeCodeDelivery;
    }
    /**
     * Set placeCodeDelivery value
     * @param string $placeCodeDelivery
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setPlaceCodeDelivery(?string $placeCodeDelivery = null): self
    {
        // validation for constraint: string
        if (!is_null($placeCodeDelivery) && !is_string($placeCodeDelivery)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($placeCodeDelivery, true), gettype($placeCodeDelivery)), __LINE__);
        }
        $this->placeCodeDelivery = $placeCodeDelivery;
        
        return $this;
    }
    /**
     * Get placeOfDelivery value
     * @return string|null
     */
    public function getPlaceOfDelivery(): ?string
    {
        return $this->placeOfDelivery;
    }
    /**
     * Set placeOfDelivery value
     * @param string $placeOfDelivery
     * @return \AppturePay\DSV\StructType\SeafreightType
     */
    public function setPlaceOfDelivery(?string $placeOfDelivery = null): self
    {
        // validation for constraint: string
        if (!is_null($placeOfDelivery) && !is_string($placeOfDelivery)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($placeOfDelivery, true), gettype($placeOfDelivery)), __LINE__);
        }
        $this->placeOfDelivery = $placeOfDelivery;
        
        return $this;
    }
}
