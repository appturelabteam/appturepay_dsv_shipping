<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CustomsDocumentsType StructType
 * @subpackage Structs
 */
class CustomsDocumentsType extends AbstractStructBase
{
    /**
     * The sequenceNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $sequenceNumber = null;
    /**
     * The documentCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentCode = null;
    /**
     * The documentNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentNumber = null;
    /**
     * The documentItemNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $documentItemNumber = null;
    /**
     * The documentIssuingOffice
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentIssuingOffice = null;
    /**
     * The documentIssuingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentIssuingDate = null;
    /**
     * The documentValidFromDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentValidFromDate = null;
    /**
     * The documentExpiryDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentExpiryDate = null;
    /**
     * The documentStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $documentStatus = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for CustomsDocumentsType
     * @uses CustomsDocumentsType::setSequenceNumber()
     * @uses CustomsDocumentsType::setDocumentCode()
     * @uses CustomsDocumentsType::setDocumentNumber()
     * @uses CustomsDocumentsType::setDocumentItemNumber()
     * @uses CustomsDocumentsType::setDocumentIssuingOffice()
     * @uses CustomsDocumentsType::setDocumentIssuingDate()
     * @uses CustomsDocumentsType::setDocumentValidFromDate()
     * @uses CustomsDocumentsType::setDocumentExpiryDate()
     * @uses CustomsDocumentsType::setDocumentStatus()
     * @uses CustomsDocumentsType::setType()
     * @param int $sequenceNumber
     * @param string $documentCode
     * @param string $documentNumber
     * @param int $documentItemNumber
     * @param string $documentIssuingOffice
     * @param string $documentIssuingDate
     * @param string $documentValidFromDate
     * @param string $documentExpiryDate
     * @param int $documentStatus
     * @param string $type
     */
    public function __construct(?int $sequenceNumber = null, ?string $documentCode = null, ?string $documentNumber = null, ?int $documentItemNumber = null, ?string $documentIssuingOffice = null, ?string $documentIssuingDate = null, ?string $documentValidFromDate = null, ?string $documentExpiryDate = null, ?int $documentStatus = null, ?string $type = null)
    {
        $this
            ->setSequenceNumber($sequenceNumber)
            ->setDocumentCode($documentCode)
            ->setDocumentNumber($documentNumber)
            ->setDocumentItemNumber($documentItemNumber)
            ->setDocumentIssuingOffice($documentIssuingOffice)
            ->setDocumentIssuingDate($documentIssuingDate)
            ->setDocumentValidFromDate($documentValidFromDate)
            ->setDocumentExpiryDate($documentExpiryDate)
            ->setDocumentStatus($documentStatus)
            ->setType($type);
    }
    /**
     * Get sequenceNumber value
     * @return int|null
     */
    public function getSequenceNumber(): ?int
    {
        return $this->sequenceNumber;
    }
    /**
     * Set sequenceNumber value
     * @param int $sequenceNumber
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setSequenceNumber(?int $sequenceNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($sequenceNumber) && !(is_int($sequenceNumber) || ctype_digit($sequenceNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($sequenceNumber, true), gettype($sequenceNumber)), __LINE__);
        }
        $this->sequenceNumber = $sequenceNumber;
        
        return $this;
    }
    /**
     * Get documentCode value
     * @return string|null
     */
    public function getDocumentCode(): ?string
    {
        return $this->documentCode;
    }
    /**
     * Set documentCode value
     * @param string $documentCode
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentCode(?string $documentCode = null): self
    {
        // validation for constraint: string
        if (!is_null($documentCode) && !is_string($documentCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentCode, true), gettype($documentCode)), __LINE__);
        }
        $this->documentCode = $documentCode;
        
        return $this;
    }
    /**
     * Get documentNumber value
     * @return string|null
     */
    public function getDocumentNumber(): ?string
    {
        return $this->documentNumber;
    }
    /**
     * Set documentNumber value
     * @param string $documentNumber
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentNumber(?string $documentNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($documentNumber) && !is_string($documentNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentNumber, true), gettype($documentNumber)), __LINE__);
        }
        $this->documentNumber = $documentNumber;
        
        return $this;
    }
    /**
     * Get documentItemNumber value
     * @return int|null
     */
    public function getDocumentItemNumber(): ?int
    {
        return $this->documentItemNumber;
    }
    /**
     * Set documentItemNumber value
     * @param int $documentItemNumber
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentItemNumber(?int $documentItemNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($documentItemNumber) && !(is_int($documentItemNumber) || ctype_digit($documentItemNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($documentItemNumber, true), gettype($documentItemNumber)), __LINE__);
        }
        $this->documentItemNumber = $documentItemNumber;
        
        return $this;
    }
    /**
     * Get documentIssuingOffice value
     * @return string|null
     */
    public function getDocumentIssuingOffice(): ?string
    {
        return $this->documentIssuingOffice;
    }
    /**
     * Set documentIssuingOffice value
     * @param string $documentIssuingOffice
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentIssuingOffice(?string $documentIssuingOffice = null): self
    {
        // validation for constraint: string
        if (!is_null($documentIssuingOffice) && !is_string($documentIssuingOffice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentIssuingOffice, true), gettype($documentIssuingOffice)), __LINE__);
        }
        $this->documentIssuingOffice = $documentIssuingOffice;
        
        return $this;
    }
    /**
     * Get documentIssuingDate value
     * @return string|null
     */
    public function getDocumentIssuingDate(): ?string
    {
        return $this->documentIssuingDate;
    }
    /**
     * Set documentIssuingDate value
     * @param string $documentIssuingDate
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentIssuingDate(?string $documentIssuingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($documentIssuingDate) && !is_string($documentIssuingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentIssuingDate, true), gettype($documentIssuingDate)), __LINE__);
        }
        $this->documentIssuingDate = $documentIssuingDate;
        
        return $this;
    }
    /**
     * Get documentValidFromDate value
     * @return string|null
     */
    public function getDocumentValidFromDate(): ?string
    {
        return $this->documentValidFromDate;
    }
    /**
     * Set documentValidFromDate value
     * @param string $documentValidFromDate
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentValidFromDate(?string $documentValidFromDate = null): self
    {
        // validation for constraint: string
        if (!is_null($documentValidFromDate) && !is_string($documentValidFromDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentValidFromDate, true), gettype($documentValidFromDate)), __LINE__);
        }
        $this->documentValidFromDate = $documentValidFromDate;
        
        return $this;
    }
    /**
     * Get documentExpiryDate value
     * @return string|null
     */
    public function getDocumentExpiryDate(): ?string
    {
        return $this->documentExpiryDate;
    }
    /**
     * Set documentExpiryDate value
     * @param string $documentExpiryDate
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentExpiryDate(?string $documentExpiryDate = null): self
    {
        // validation for constraint: string
        if (!is_null($documentExpiryDate) && !is_string($documentExpiryDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentExpiryDate, true), gettype($documentExpiryDate)), __LINE__);
        }
        $this->documentExpiryDate = $documentExpiryDate;
        
        return $this;
    }
    /**
     * Get documentStatus value
     * @return int|null
     */
    public function getDocumentStatus(): ?int
    {
        return $this->documentStatus;
    }
    /**
     * Set documentStatus value
     * @param int $documentStatus
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setDocumentStatus(?int $documentStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($documentStatus) && !(is_int($documentStatus) || ctype_digit($documentStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($documentStatus, true), gettype($documentStatus)), __LINE__);
        }
        $this->documentStatus = $documentStatus;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
