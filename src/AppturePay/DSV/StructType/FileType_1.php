<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FileType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:FileType
 * @subpackage Structs
 */
class FileType_1 extends AbstractStructBase
{
    /**
     * The arrivalDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $arrivalDate = null;
    /**
     * The loadingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $loadingDate = null;
    /**
     * The loadingTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $loadingTime = null;
    /**
     * The unloadingDate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $unloadingDate = null;
    /**
     * The unloadingTime
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $unloadingTime = null;
    /**
     * The marksAndNumber
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $marksAndNumber = null;
    /**
     * The waybill
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $waybill = null;
    /**
     * The houseWaybill
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $houseWaybill = null;
    /**
     * The primaryReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $primaryReference = null;
    /**
     * The serviceLevel
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $serviceLevel = null;
    /**
     * The tariffType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tariffType = null;
    /**
     * The deliveryTerm
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $deliveryTerm = null;
    /**
     * The deliveryTermPlace
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $deliveryTermPlace = null;
    /**
     * The originPlaceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $originPlaceCode = null;
    /**
     * The destinationPlaceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $destinationPlaceCode = null;
    /**
     * The codeShedHandling
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $codeShedHandling = null;
    /**
     * The daysOfShedRent
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $daysOfShedRent = null;
    /**
     * The shedLocationBerth
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $shedLocationBerth = null;
    /**
     * The totals
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TotalsType_1|null
     */
    protected ?\AppturePay\DSV\StructType\TotalsType_1 $totals = null;
    /**
     * The dimension
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DimensionType_1|null
     */
    protected ?\AppturePay\DSV\StructType\DimensionType_1 $dimension = null;
    /**
     * The transport
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TransportType|null
     */
    protected ?\AppturePay\DSV\StructType\TransportType $transport = null;
    /**
     * The seafreight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\SeafreightType|null
     */
    protected ?\AppturePay\DSV\StructType\SeafreightType $seafreight = null;
    /**
     * The airfreight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AirfreightType|null
     */
    protected ?\AppturePay\DSV\StructType\AirfreightType $airfreight = null;
    /**
     * The customItems
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CustomItemsType|null
     */
    protected ?\AppturePay\DSV\StructType\CustomItemsType $customItems = null;
    /**
     * The amounts
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AmountsType|null
     */
    protected ?\AppturePay\DSV\StructType\AmountsType $amounts = null;
    /**
     * The codes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CodesType|null
     */
    protected ?\AppturePay\DSV\StructType\CodesType $codes = null;
    /**
     * The parameters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ParametersType_1|null
     */
    protected ?\AppturePay\DSV\StructType\ParametersType_1 $parameters = null;
    /**
     * The extraParameters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ExtraParametersType_1|null
     */
    protected ?\AppturePay\DSV\StructType\ExtraParametersType_1 $extraParameters = null;
    /**
     * The tailorFiller15
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tailorFiller15 = null;
    /**
     * The cashInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CashInfoType[]
     */
    protected ?array $cashInfo = null;
    /**
     * The extraAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ExtraAmountType_1[]
     */
    protected ?array $extraAmount = null;
    /**
     * The freightCharge
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FreightChargeType[]
     */
    protected ?array $freightCharge = null;
    /**
     * The meansOfTransport
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\MeansOfTransportType[]
     */
    protected ?array $meansOfTransport = null;
    /**
     * The textKeys
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TextKeysType_1[]
     */
    protected ?array $textKeys = null;
    /**
     * The dateTimeZones
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DateTimeZonesType[]
     */
    protected ?array $dateTimeZones = null;
    /**
     * The address
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AddressType_1[]
     */
    protected ?array $address = null;
    /**
     * The extraReference
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ExtraReferenceType[]
     */
    protected ?array $extraReference = null;
    /**
     * The returnReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ReturnReferenceType|null
     */
    protected ?\AppturePay\DSV\StructType\ReturnReferenceType $returnReference = null;
    /**
     * The additionalServices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\AdditionalServicesType_1[]
     */
    protected ?array $additionalServices = null;
    /**
     * The conNote
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ConNoteType|null
     */
    protected ?\AppturePay\DSV\StructType\ConNoteType $conNote = null;
    /**
     * The link
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\LinkType[]
     */
    protected ?array $link = null;
    /**
     * The freeText
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\FreeTextType[]
     */
    protected ?array $freeText = null;
    /**
     * The emission
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\EmissionType[]
     */
    protected ?array $emission = null;
    /**
     * The customsDocuments
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CustomsDocumentsType[]
     */
    protected ?array $customsDocuments = null;
    /**
     * The transportInstruction
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TransportInstructionType_1[]
     */
    protected ?array $transportInstruction = null;
    /**
     * The planCharacteristic
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\PlanCharacteristicType_1[]
     */
    protected ?array $planCharacteristic = null;
    /**
     * The planning
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\PlanningType[]
     */
    protected ?array $planning = null;
    /**
     * The shippingUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    protected ?array $shippingUnit = null;
    /**
     * The goodsLine
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\GoodsLineType_1[]
     */
    protected ?array $goodsLine = null;
    /**
     * The container
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ContainerType[]
     */
    protected ?array $container = null;
    /**
     * The costAndCharge
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CostAndChargeType[]
     */
    protected ?array $costAndCharge = null;
    /**
     * The tailorMade
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\TailorMadeType[]
     */
    protected ?array $tailorMade = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for FileType
     * @uses FileType_1::setArrivalDate()
     * @uses FileType_1::setLoadingDate()
     * @uses FileType_1::setLoadingTime()
     * @uses FileType_1::setUnloadingDate()
     * @uses FileType_1::setUnloadingTime()
     * @uses FileType_1::setMarksAndNumber()
     * @uses FileType_1::setWaybill()
     * @uses FileType_1::setHouseWaybill()
     * @uses FileType_1::setPrimaryReference()
     * @uses FileType_1::setServiceLevel()
     * @uses FileType_1::setTariffType()
     * @uses FileType_1::setDeliveryTerm()
     * @uses FileType_1::setDeliveryTermPlace()
     * @uses FileType_1::setOriginPlaceCode()
     * @uses FileType_1::setDestinationPlaceCode()
     * @uses FileType_1::setCodeShedHandling()
     * @uses FileType_1::setDaysOfShedRent()
     * @uses FileType_1::setShedLocationBerth()
     * @uses FileType_1::setTotals()
     * @uses FileType_1::setDimension()
     * @uses FileType_1::setTransport()
     * @uses FileType_1::setSeafreight()
     * @uses FileType_1::setAirfreight()
     * @uses FileType_1::setCustomItems()
     * @uses FileType_1::setAmounts()
     * @uses FileType_1::setCodes()
     * @uses FileType_1::setParameters()
     * @uses FileType_1::setExtraParameters()
     * @uses FileType_1::setTailorFiller15()
     * @uses FileType_1::setCashInfo()
     * @uses FileType_1::setExtraAmount()
     * @uses FileType_1::setFreightCharge()
     * @uses FileType_1::setMeansOfTransport()
     * @uses FileType_1::setTextKeys()
     * @uses FileType_1::setDateTimeZones()
     * @uses FileType_1::setAddress()
     * @uses FileType_1::setExtraReference()
     * @uses FileType_1::setReturnReference()
     * @uses FileType_1::setAdditionalServices()
     * @uses FileType_1::setConNote()
     * @uses FileType_1::setLink()
     * @uses FileType_1::setFreeText()
     * @uses FileType_1::setEmission()
     * @uses FileType_1::setCustomsDocuments()
     * @uses FileType_1::setTransportInstruction()
     * @uses FileType_1::setPlanCharacteristic()
     * @uses FileType_1::setPlanning()
     * @uses FileType_1::setShippingUnit()
     * @uses FileType_1::setGoodsLine()
     * @uses FileType_1::setContainer()
     * @uses FileType_1::setCostAndCharge()
     * @uses FileType_1::setTailorMade()
     * @uses FileType_1::setType()
     * @uses FileType_1::setTypeField()
     * @param string $arrivalDate
     * @param string $loadingDate
     * @param string $loadingTime
     * @param string $unloadingDate
     * @param string $unloadingTime
     * @param string $marksAndNumber
     * @param string $waybill
     * @param string $houseWaybill
     * @param string $primaryReference
     * @param string $serviceLevel
     * @param string $tariffType
     * @param string $deliveryTerm
     * @param string $deliveryTermPlace
     * @param string $originPlaceCode
     * @param string $destinationPlaceCode
     * @param string $codeShedHandling
     * @param int $daysOfShedRent
     * @param string $shedLocationBerth
     * @param \AppturePay\DSV\StructType\TotalsType_1 $totals
     * @param \AppturePay\DSV\StructType\DimensionType_1 $dimension
     * @param \AppturePay\DSV\StructType\TransportType $transport
     * @param \AppturePay\DSV\StructType\SeafreightType $seafreight
     * @param \AppturePay\DSV\StructType\AirfreightType $airfreight
     * @param \AppturePay\DSV\StructType\CustomItemsType $customItems
     * @param \AppturePay\DSV\StructType\AmountsType $amounts
     * @param \AppturePay\DSV\StructType\CodesType $codes
     * @param \AppturePay\DSV\StructType\ParametersType_1 $parameters
     * @param \AppturePay\DSV\StructType\ExtraParametersType_1 $extraParameters
     * @param string $tailorFiller15
     * @param \AppturePay\DSV\StructType\CashInfoType[] $cashInfo
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1[] $extraAmount
     * @param \AppturePay\DSV\StructType\FreightChargeType[] $freightCharge
     * @param \AppturePay\DSV\StructType\MeansOfTransportType[] $meansOfTransport
     * @param \AppturePay\DSV\StructType\TextKeysType_1[] $textKeys
     * @param \AppturePay\DSV\StructType\DateTimeZonesType[] $dateTimeZones
     * @param \AppturePay\DSV\StructType\AddressType_1[] $address
     * @param \AppturePay\DSV\StructType\ExtraReferenceType[] $extraReference
     * @param \AppturePay\DSV\StructType\ReturnReferenceType $returnReference
     * @param \AppturePay\DSV\StructType\AdditionalServicesType_1[] $additionalServices
     * @param \AppturePay\DSV\StructType\ConNoteType $conNote
     * @param \AppturePay\DSV\StructType\LinkType[] $link
     * @param \AppturePay\DSV\StructType\FreeTextType[] $freeText
     * @param \AppturePay\DSV\StructType\EmissionType[] $emission
     * @param \AppturePay\DSV\StructType\CustomsDocumentsType[] $customsDocuments
     * @param \AppturePay\DSV\StructType\TransportInstructionType_1[] $transportInstruction
     * @param \AppturePay\DSV\StructType\PlanCharacteristicType_1[] $planCharacteristic
     * @param \AppturePay\DSV\StructType\PlanningType[] $planning
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnit
     * @param \AppturePay\DSV\StructType\GoodsLineType_1[] $goodsLine
     * @param \AppturePay\DSV\StructType\ContainerType[] $container
     * @param \AppturePay\DSV\StructType\CostAndChargeType[] $costAndCharge
     * @param \AppturePay\DSV\StructType\TailorMadeType[] $tailorMade
     * @param string $type
     * @param string $typeField
     */
    public function __construct(?string $arrivalDate = null, ?string $loadingDate = null, ?string $loadingTime = null, ?string $unloadingDate = null, ?string $unloadingTime = null, ?string $marksAndNumber = null, ?string $waybill = null, ?string $houseWaybill = null, ?string $primaryReference = null, ?string $serviceLevel = null, ?string $tariffType = null, ?string $deliveryTerm = null, ?string $deliveryTermPlace = null, ?string $originPlaceCode = null, ?string $destinationPlaceCode = null, ?string $codeShedHandling = null, ?int $daysOfShedRent = null, ?string $shedLocationBerth = null, ?\AppturePay\DSV\StructType\TotalsType_1 $totals = null, ?\AppturePay\DSV\StructType\DimensionType_1 $dimension = null, ?\AppturePay\DSV\StructType\TransportType $transport = null, ?\AppturePay\DSV\StructType\SeafreightType $seafreight = null, ?\AppturePay\DSV\StructType\AirfreightType $airfreight = null, ?\AppturePay\DSV\StructType\CustomItemsType $customItems = null, ?\AppturePay\DSV\StructType\AmountsType $amounts = null, ?\AppturePay\DSV\StructType\CodesType $codes = null, ?\AppturePay\DSV\StructType\ParametersType_1 $parameters = null, ?\AppturePay\DSV\StructType\ExtraParametersType_1 $extraParameters = null, ?string $tailorFiller15 = null, ?array $cashInfo = null, ?array $extraAmount = null, ?array $freightCharge = null, ?array $meansOfTransport = null, ?array $textKeys = null, ?array $dateTimeZones = null, ?array $address = null, ?array $extraReference = null, ?\AppturePay\DSV\StructType\ReturnReferenceType $returnReference = null, ?array $additionalServices = null, ?\AppturePay\DSV\StructType\ConNoteType $conNote = null, ?array $link = null, ?array $freeText = null, ?array $emission = null, ?array $customsDocuments = null, ?array $transportInstruction = null, ?array $planCharacteristic = null, ?array $planning = null, ?array $shippingUnit = null, ?array $goodsLine = null, ?array $container = null, ?array $costAndCharge = null, ?array $tailorMade = null, ?string $type = null, ?string $typeField = null)
    {
        $this
            ->setArrivalDate($arrivalDate)
            ->setLoadingDate($loadingDate)
            ->setLoadingTime($loadingTime)
            ->setUnloadingDate($unloadingDate)
            ->setUnloadingTime($unloadingTime)
            ->setMarksAndNumber($marksAndNumber)
            ->setWaybill($waybill)
            ->setHouseWaybill($houseWaybill)
            ->setPrimaryReference($primaryReference)
            ->setServiceLevel($serviceLevel)
            ->setTariffType($tariffType)
            ->setDeliveryTerm($deliveryTerm)
            ->setDeliveryTermPlace($deliveryTermPlace)
            ->setOriginPlaceCode($originPlaceCode)
            ->setDestinationPlaceCode($destinationPlaceCode)
            ->setCodeShedHandling($codeShedHandling)
            ->setDaysOfShedRent($daysOfShedRent)
            ->setShedLocationBerth($shedLocationBerth)
            ->setTotals($totals)
            ->setDimension($dimension)
            ->setTransport($transport)
            ->setSeafreight($seafreight)
            ->setAirfreight($airfreight)
            ->setCustomItems($customItems)
            ->setAmounts($amounts)
            ->setCodes($codes)
            ->setParameters($parameters)
            ->setExtraParameters($extraParameters)
            ->setTailorFiller15($tailorFiller15)
            ->setCashInfo($cashInfo)
            ->setExtraAmount($extraAmount)
            ->setFreightCharge($freightCharge)
            ->setMeansOfTransport($meansOfTransport)
            ->setTextKeys($textKeys)
            ->setDateTimeZones($dateTimeZones)
            ->setAddress($address)
            ->setExtraReference($extraReference)
            ->setReturnReference($returnReference)
            ->setAdditionalServices($additionalServices)
            ->setConNote($conNote)
            ->setLink($link)
            ->setFreeText($freeText)
            ->setEmission($emission)
            ->setCustomsDocuments($customsDocuments)
            ->setTransportInstruction($transportInstruction)
            ->setPlanCharacteristic($planCharacteristic)
            ->setPlanning($planning)
            ->setShippingUnit($shippingUnit)
            ->setGoodsLine($goodsLine)
            ->setContainer($container)
            ->setCostAndCharge($costAndCharge)
            ->setTailorMade($tailorMade)
            ->setType($type)
            ->setTypeField($typeField);
    }
    /**
     * Get arrivalDate value
     * @return string|null
     */
    public function getArrivalDate(): ?string
    {
        return $this->arrivalDate;
    }
    /**
     * Set arrivalDate value
     * @param string $arrivalDate
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setArrivalDate(?string $arrivalDate = null): self
    {
        // validation for constraint: string
        if (!is_null($arrivalDate) && !is_string($arrivalDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($arrivalDate, true), gettype($arrivalDate)), __LINE__);
        }
        $this->arrivalDate = $arrivalDate;
        
        return $this;
    }
    /**
     * Get loadingDate value
     * @return string|null
     */
    public function getLoadingDate(): ?string
    {
        return $this->loadingDate;
    }
    /**
     * Set loadingDate value
     * @param string $loadingDate
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setLoadingDate(?string $loadingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($loadingDate) && !is_string($loadingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loadingDate, true), gettype($loadingDate)), __LINE__);
        }
        $this->loadingDate = $loadingDate;
        
        return $this;
    }
    /**
     * Get loadingTime value
     * @return string|null
     */
    public function getLoadingTime(): ?string
    {
        return $this->loadingTime;
    }
    /**
     * Set loadingTime value
     * @param string $loadingTime
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setLoadingTime(?string $loadingTime = null): self
    {
        // validation for constraint: string
        if (!is_null($loadingTime) && !is_string($loadingTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loadingTime, true), gettype($loadingTime)), __LINE__);
        }
        $this->loadingTime = $loadingTime;
        
        return $this;
    }
    /**
     * Get unloadingDate value
     * @return string|null
     */
    public function getUnloadingDate(): ?string
    {
        return $this->unloadingDate;
    }
    /**
     * Set unloadingDate value
     * @param string $unloadingDate
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setUnloadingDate(?string $unloadingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($unloadingDate) && !is_string($unloadingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($unloadingDate, true), gettype($unloadingDate)), __LINE__);
        }
        $this->unloadingDate = $unloadingDate;
        
        return $this;
    }
    /**
     * Get unloadingTime value
     * @return string|null
     */
    public function getUnloadingTime(): ?string
    {
        return $this->unloadingTime;
    }
    /**
     * Set unloadingTime value
     * @param string $unloadingTime
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setUnloadingTime(?string $unloadingTime = null): self
    {
        // validation for constraint: string
        if (!is_null($unloadingTime) && !is_string($unloadingTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($unloadingTime, true), gettype($unloadingTime)), __LINE__);
        }
        $this->unloadingTime = $unloadingTime;
        
        return $this;
    }
    /**
     * Get marksAndNumber value
     * @return string|null
     */
    public function getMarksAndNumber(): ?string
    {
        return $this->marksAndNumber;
    }
    /**
     * Set marksAndNumber value
     * @param string $marksAndNumber
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setMarksAndNumber(?string $marksAndNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($marksAndNumber) && !is_string($marksAndNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($marksAndNumber, true), gettype($marksAndNumber)), __LINE__);
        }
        $this->marksAndNumber = $marksAndNumber;
        
        return $this;
    }
    /**
     * Get waybill value
     * @return string|null
     */
    public function getWaybill(): ?string
    {
        return $this->waybill;
    }
    /**
     * Set waybill value
     * @param string $waybill
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setWaybill(?string $waybill = null): self
    {
        // validation for constraint: string
        if (!is_null($waybill) && !is_string($waybill)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($waybill, true), gettype($waybill)), __LINE__);
        }
        $this->waybill = $waybill;
        
        return $this;
    }
    /**
     * Get houseWaybill value
     * @return string|null
     */
    public function getHouseWaybill(): ?string
    {
        return $this->houseWaybill;
    }
    /**
     * Set houseWaybill value
     * @param string $houseWaybill
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setHouseWaybill(?string $houseWaybill = null): self
    {
        // validation for constraint: string
        if (!is_null($houseWaybill) && !is_string($houseWaybill)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($houseWaybill, true), gettype($houseWaybill)), __LINE__);
        }
        $this->houseWaybill = $houseWaybill;
        
        return $this;
    }
    /**
     * Get primaryReference value
     * @return string|null
     */
    public function getPrimaryReference(): ?string
    {
        return $this->primaryReference;
    }
    /**
     * Set primaryReference value
     * @param string $primaryReference
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setPrimaryReference(?string $primaryReference = null): self
    {
        // validation for constraint: string
        if (!is_null($primaryReference) && !is_string($primaryReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($primaryReference, true), gettype($primaryReference)), __LINE__);
        }
        $this->primaryReference = $primaryReference;
        
        return $this;
    }
    /**
     * Get serviceLevel value
     * @return string|null
     */
    public function getServiceLevel(): ?string
    {
        return $this->serviceLevel;
    }
    /**
     * Set serviceLevel value
     * @param string $serviceLevel
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setServiceLevel(?string $serviceLevel = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceLevel) && !is_string($serviceLevel)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceLevel, true), gettype($serviceLevel)), __LINE__);
        }
        $this->serviceLevel = $serviceLevel;
        
        return $this;
    }
    /**
     * Get tariffType value
     * @return string|null
     */
    public function getTariffType(): ?string
    {
        return $this->tariffType;
    }
    /**
     * Set tariffType value
     * @param string $tariffType
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTariffType(?string $tariffType = null): self
    {
        // validation for constraint: string
        if (!is_null($tariffType) && !is_string($tariffType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tariffType, true), gettype($tariffType)), __LINE__);
        }
        $this->tariffType = $tariffType;
        
        return $this;
    }
    /**
     * Get deliveryTerm value
     * @return string|null
     */
    public function getDeliveryTerm(): ?string
    {
        return $this->deliveryTerm;
    }
    /**
     * Set deliveryTerm value
     * @param string $deliveryTerm
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setDeliveryTerm(?string $deliveryTerm = null): self
    {
        // validation for constraint: string
        if (!is_null($deliveryTerm) && !is_string($deliveryTerm)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryTerm, true), gettype($deliveryTerm)), __LINE__);
        }
        $this->deliveryTerm = $deliveryTerm;
        
        return $this;
    }
    /**
     * Get deliveryTermPlace value
     * @return string|null
     */
    public function getDeliveryTermPlace(): ?string
    {
        return $this->deliveryTermPlace;
    }
    /**
     * Set deliveryTermPlace value
     * @param string $deliveryTermPlace
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setDeliveryTermPlace(?string $deliveryTermPlace = null): self
    {
        // validation for constraint: string
        if (!is_null($deliveryTermPlace) && !is_string($deliveryTermPlace)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryTermPlace, true), gettype($deliveryTermPlace)), __LINE__);
        }
        $this->deliveryTermPlace = $deliveryTermPlace;
        
        return $this;
    }
    /**
     * Get originPlaceCode value
     * @return string|null
     */
    public function getOriginPlaceCode(): ?string
    {
        return $this->originPlaceCode;
    }
    /**
     * Set originPlaceCode value
     * @param string $originPlaceCode
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setOriginPlaceCode(?string $originPlaceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($originPlaceCode) && !is_string($originPlaceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($originPlaceCode, true), gettype($originPlaceCode)), __LINE__);
        }
        $this->originPlaceCode = $originPlaceCode;
        
        return $this;
    }
    /**
     * Get destinationPlaceCode value
     * @return string|null
     */
    public function getDestinationPlaceCode(): ?string
    {
        return $this->destinationPlaceCode;
    }
    /**
     * Set destinationPlaceCode value
     * @param string $destinationPlaceCode
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setDestinationPlaceCode(?string $destinationPlaceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($destinationPlaceCode) && !is_string($destinationPlaceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($destinationPlaceCode, true), gettype($destinationPlaceCode)), __LINE__);
        }
        $this->destinationPlaceCode = $destinationPlaceCode;
        
        return $this;
    }
    /**
     * Get codeShedHandling value
     * @return string|null
     */
    public function getCodeShedHandling(): ?string
    {
        return $this->codeShedHandling;
    }
    /**
     * Set codeShedHandling value
     * @param string $codeShedHandling
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setCodeShedHandling(?string $codeShedHandling = null): self
    {
        // validation for constraint: string
        if (!is_null($codeShedHandling) && !is_string($codeShedHandling)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codeShedHandling, true), gettype($codeShedHandling)), __LINE__);
        }
        $this->codeShedHandling = $codeShedHandling;
        
        return $this;
    }
    /**
     * Get daysOfShedRent value
     * @return int|null
     */
    public function getDaysOfShedRent(): ?int
    {
        return $this->daysOfShedRent;
    }
    /**
     * Set daysOfShedRent value
     * @param int $daysOfShedRent
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setDaysOfShedRent(?int $daysOfShedRent = null): self
    {
        // validation for constraint: int
        if (!is_null($daysOfShedRent) && !(is_int($daysOfShedRent) || ctype_digit($daysOfShedRent))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($daysOfShedRent, true), gettype($daysOfShedRent)), __LINE__);
        }
        $this->daysOfShedRent = $daysOfShedRent;
        
        return $this;
    }
    /**
     * Get shedLocationBerth value
     * @return string|null
     */
    public function getShedLocationBerth(): ?string
    {
        return $this->shedLocationBerth;
    }
    /**
     * Set shedLocationBerth value
     * @param string $shedLocationBerth
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setShedLocationBerth(?string $shedLocationBerth = null): self
    {
        // validation for constraint: string
        if (!is_null($shedLocationBerth) && !is_string($shedLocationBerth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shedLocationBerth, true), gettype($shedLocationBerth)), __LINE__);
        }
        $this->shedLocationBerth = $shedLocationBerth;
        
        return $this;
    }
    /**
     * Get totals value
     * @return \AppturePay\DSV\StructType\TotalsType_1|null
     */
    public function getTotals(): ?\AppturePay\DSV\StructType\TotalsType_1
    {
        return $this->totals;
    }
    /**
     * Set totals value
     * @param \AppturePay\DSV\StructType\TotalsType_1 $totals
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTotals(?\AppturePay\DSV\StructType\TotalsType_1 $totals = null): self
    {
        $this->totals = $totals;
        
        return $this;
    }
    /**
     * Get dimension value
     * @return \AppturePay\DSV\StructType\DimensionType_1|null
     */
    public function getDimension(): ?\AppturePay\DSV\StructType\DimensionType_1
    {
        return $this->dimension;
    }
    /**
     * Set dimension value
     * @param \AppturePay\DSV\StructType\DimensionType_1 $dimension
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setDimension(?\AppturePay\DSV\StructType\DimensionType_1 $dimension = null): self
    {
        $this->dimension = $dimension;
        
        return $this;
    }
    /**
     * Get transport value
     * @return \AppturePay\DSV\StructType\TransportType|null
     */
    public function getTransport(): ?\AppturePay\DSV\StructType\TransportType
    {
        return $this->transport;
    }
    /**
     * Set transport value
     * @param \AppturePay\DSV\StructType\TransportType $transport
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTransport(?\AppturePay\DSV\StructType\TransportType $transport = null): self
    {
        $this->transport = $transport;
        
        return $this;
    }
    /**
     * Get seafreight value
     * @return \AppturePay\DSV\StructType\SeafreightType|null
     */
    public function getSeafreight(): ?\AppturePay\DSV\StructType\SeafreightType
    {
        return $this->seafreight;
    }
    /**
     * Set seafreight value
     * @param \AppturePay\DSV\StructType\SeafreightType $seafreight
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setSeafreight(?\AppturePay\DSV\StructType\SeafreightType $seafreight = null): self
    {
        $this->seafreight = $seafreight;
        
        return $this;
    }
    /**
     * Get airfreight value
     * @return \AppturePay\DSV\StructType\AirfreightType|null
     */
    public function getAirfreight(): ?\AppturePay\DSV\StructType\AirfreightType
    {
        return $this->airfreight;
    }
    /**
     * Set airfreight value
     * @param \AppturePay\DSV\StructType\AirfreightType $airfreight
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setAirfreight(?\AppturePay\DSV\StructType\AirfreightType $airfreight = null): self
    {
        $this->airfreight = $airfreight;
        
        return $this;
    }
    /**
     * Get customItems value
     * @return \AppturePay\DSV\StructType\CustomItemsType|null
     */
    public function getCustomItems(): ?\AppturePay\DSV\StructType\CustomItemsType
    {
        return $this->customItems;
    }
    /**
     * Set customItems value
     * @param \AppturePay\DSV\StructType\CustomItemsType $customItems
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setCustomItems(?\AppturePay\DSV\StructType\CustomItemsType $customItems = null): self
    {
        $this->customItems = $customItems;
        
        return $this;
    }
    /**
     * Get amounts value
     * @return \AppturePay\DSV\StructType\AmountsType|null
     */
    public function getAmounts(): ?\AppturePay\DSV\StructType\AmountsType
    {
        return $this->amounts;
    }
    /**
     * Set amounts value
     * @param \AppturePay\DSV\StructType\AmountsType $amounts
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setAmounts(?\AppturePay\DSV\StructType\AmountsType $amounts = null): self
    {
        $this->amounts = $amounts;
        
        return $this;
    }
    /**
     * Get codes value
     * @return \AppturePay\DSV\StructType\CodesType|null
     */
    public function getCodes(): ?\AppturePay\DSV\StructType\CodesType
    {
        return $this->codes;
    }
    /**
     * Set codes value
     * @param \AppturePay\DSV\StructType\CodesType $codes
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setCodes(?\AppturePay\DSV\StructType\CodesType $codes = null): self
    {
        $this->codes = $codes;
        
        return $this;
    }
    /**
     * Get parameters value
     * @return \AppturePay\DSV\StructType\ParametersType_1|null
     */
    public function getParameters(): ?\AppturePay\DSV\StructType\ParametersType_1
    {
        return $this->parameters;
    }
    /**
     * Set parameters value
     * @param \AppturePay\DSV\StructType\ParametersType_1 $parameters
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setParameters(?\AppturePay\DSV\StructType\ParametersType_1 $parameters = null): self
    {
        $this->parameters = $parameters;
        
        return $this;
    }
    /**
     * Get extraParameters value
     * @return \AppturePay\DSV\StructType\ExtraParametersType_1|null
     */
    public function getExtraParameters(): ?\AppturePay\DSV\StructType\ExtraParametersType_1
    {
        return $this->extraParameters;
    }
    /**
     * Set extraParameters value
     * @param \AppturePay\DSV\StructType\ExtraParametersType_1 $extraParameters
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setExtraParameters(?\AppturePay\DSV\StructType\ExtraParametersType_1 $extraParameters = null): self
    {
        $this->extraParameters = $extraParameters;
        
        return $this;
    }
    /**
     * Get tailorFiller15 value
     * @return string|null
     */
    public function getTailorFiller15(): ?string
    {
        return $this->tailorFiller15;
    }
    /**
     * Set tailorFiller15 value
     * @param string $tailorFiller15
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTailorFiller15(?string $tailorFiller15 = null): self
    {
        // validation for constraint: string
        if (!is_null($tailorFiller15) && !is_string($tailorFiller15)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tailorFiller15, true), gettype($tailorFiller15)), __LINE__);
        }
        $this->tailorFiller15 = $tailorFiller15;
        
        return $this;
    }
    /**
     * Get cashInfo value
     * @return \AppturePay\DSV\StructType\CashInfoType[]
     */
    public function getCashInfo(): ?array
    {
        return $this->cashInfo;
    }
    /**
     * This method is responsible for validating the values passed to the setCashInfo method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCashInfo method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCashInfoForArrayConstraintsFromSetCashInfo(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeCashInfoItem) {
            // validation for constraint: itemType
            if (!$fileTypeCashInfoItem instanceof \AppturePay\DSV\StructType\CashInfoType) {
                $invalidValues[] = is_object($fileTypeCashInfoItem) ? get_class($fileTypeCashInfoItem) : sprintf('%s(%s)', gettype($fileTypeCashInfoItem), var_export($fileTypeCashInfoItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The cashInfo property can only contain items of type \AppturePay\DSV\StructType\CashInfoType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set cashInfo value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CashInfoType[] $cashInfo
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setCashInfo(?array $cashInfo = null): self
    {
        // validation for constraint: array
        if ('' !== ($cashInfoArrayErrorMessage = self::validateCashInfoForArrayConstraintsFromSetCashInfo($cashInfo))) {
            throw new InvalidArgumentException($cashInfoArrayErrorMessage, __LINE__);
        }
        $this->cashInfo = $cashInfo;
        
        return $this;
    }
    /**
     * Add item to cashInfo value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CashInfoType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToCashInfo(\AppturePay\DSV\StructType\CashInfoType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\CashInfoType) {
            throw new InvalidArgumentException(sprintf('The cashInfo property can only contain items of type \AppturePay\DSV\StructType\CashInfoType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->cashInfo[] = $item;
        
        return $this;
    }
    /**
     * Get extraAmount value
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1[]
     */
    public function getExtraAmount(): ?array
    {
        return $this->extraAmount;
    }
    /**
     * This method is responsible for validating the values passed to the setExtraAmount method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtraAmount method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtraAmountForArrayConstraintsFromSetExtraAmount(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeExtraAmountItem) {
            // validation for constraint: itemType
            if (!$fileTypeExtraAmountItem instanceof \AppturePay\DSV\StructType\ExtraAmountType_1) {
                $invalidValues[] = is_object($fileTypeExtraAmountItem) ? get_class($fileTypeExtraAmountItem) : sprintf('%s(%s)', gettype($fileTypeExtraAmountItem), var_export($fileTypeExtraAmountItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The extraAmount property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set extraAmount value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1[] $extraAmount
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setExtraAmount(?array $extraAmount = null): self
    {
        // validation for constraint: array
        if ('' !== ($extraAmountArrayErrorMessage = self::validateExtraAmountForArrayConstraintsFromSetExtraAmount($extraAmount))) {
            throw new InvalidArgumentException($extraAmountArrayErrorMessage, __LINE__);
        }
        $this->extraAmount = $extraAmount;
        
        return $this;
    }
    /**
     * Add item to extraAmount value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToExtraAmount(\AppturePay\DSV\StructType\ExtraAmountType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ExtraAmountType_1) {
            throw new InvalidArgumentException(sprintf('The extraAmount property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->extraAmount[] = $item;
        
        return $this;
    }
    /**
     * Get freightCharge value
     * @return \AppturePay\DSV\StructType\FreightChargeType[]
     */
    public function getFreightCharge(): ?array
    {
        return $this->freightCharge;
    }
    /**
     * This method is responsible for validating the values passed to the setFreightCharge method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFreightCharge method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFreightChargeForArrayConstraintsFromSetFreightCharge(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeFreightChargeItem) {
            // validation for constraint: itemType
            if (!$fileTypeFreightChargeItem instanceof \AppturePay\DSV\StructType\FreightChargeType) {
                $invalidValues[] = is_object($fileTypeFreightChargeItem) ? get_class($fileTypeFreightChargeItem) : sprintf('%s(%s)', gettype($fileTypeFreightChargeItem), var_export($fileTypeFreightChargeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The freightCharge property can only contain items of type \AppturePay\DSV\StructType\FreightChargeType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set freightCharge value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\FreightChargeType[] $freightCharge
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setFreightCharge(?array $freightCharge = null): self
    {
        // validation for constraint: array
        if ('' !== ($freightChargeArrayErrorMessage = self::validateFreightChargeForArrayConstraintsFromSetFreightCharge($freightCharge))) {
            throw new InvalidArgumentException($freightChargeArrayErrorMessage, __LINE__);
        }
        $this->freightCharge = $freightCharge;
        
        return $this;
    }
    /**
     * Add item to freightCharge value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\FreightChargeType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToFreightCharge(\AppturePay\DSV\StructType\FreightChargeType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\FreightChargeType) {
            throw new InvalidArgumentException(sprintf('The freightCharge property can only contain items of type \AppturePay\DSV\StructType\FreightChargeType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->freightCharge[] = $item;
        
        return $this;
    }
    /**
     * Get meansOfTransport value
     * @return \AppturePay\DSV\StructType\MeansOfTransportType[]
     */
    public function getMeansOfTransport(): ?array
    {
        return $this->meansOfTransport;
    }
    /**
     * This method is responsible for validating the values passed to the setMeansOfTransport method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMeansOfTransport method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMeansOfTransportForArrayConstraintsFromSetMeansOfTransport(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeMeansOfTransportItem) {
            // validation for constraint: itemType
            if (!$fileTypeMeansOfTransportItem instanceof \AppturePay\DSV\StructType\MeansOfTransportType) {
                $invalidValues[] = is_object($fileTypeMeansOfTransportItem) ? get_class($fileTypeMeansOfTransportItem) : sprintf('%s(%s)', gettype($fileTypeMeansOfTransportItem), var_export($fileTypeMeansOfTransportItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The meansOfTransport property can only contain items of type \AppturePay\DSV\StructType\MeansOfTransportType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set meansOfTransport value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\MeansOfTransportType[] $meansOfTransport
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setMeansOfTransport(?array $meansOfTransport = null): self
    {
        // validation for constraint: array
        if ('' !== ($meansOfTransportArrayErrorMessage = self::validateMeansOfTransportForArrayConstraintsFromSetMeansOfTransport($meansOfTransport))) {
            throw new InvalidArgumentException($meansOfTransportArrayErrorMessage, __LINE__);
        }
        $this->meansOfTransport = $meansOfTransport;
        
        return $this;
    }
    /**
     * Add item to meansOfTransport value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\MeansOfTransportType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToMeansOfTransport(\AppturePay\DSV\StructType\MeansOfTransportType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\MeansOfTransportType) {
            throw new InvalidArgumentException(sprintf('The meansOfTransport property can only contain items of type \AppturePay\DSV\StructType\MeansOfTransportType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->meansOfTransport[] = $item;
        
        return $this;
    }
    /**
     * Get textKeys value
     * @return \AppturePay\DSV\StructType\TextKeysType_1[]
     */
    public function getTextKeys(): ?array
    {
        return $this->textKeys;
    }
    /**
     * This method is responsible for validating the values passed to the setTextKeys method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTextKeys method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTextKeysForArrayConstraintsFromSetTextKeys(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeTextKeysItem) {
            // validation for constraint: itemType
            if (!$fileTypeTextKeysItem instanceof \AppturePay\DSV\StructType\TextKeysType_1) {
                $invalidValues[] = is_object($fileTypeTextKeysItem) ? get_class($fileTypeTextKeysItem) : sprintf('%s(%s)', gettype($fileTypeTextKeysItem), var_export($fileTypeTextKeysItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The textKeys property can only contain items of type \AppturePay\DSV\StructType\TextKeysType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set textKeys value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TextKeysType_1[] $textKeys
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTextKeys(?array $textKeys = null): self
    {
        // validation for constraint: array
        if ('' !== ($textKeysArrayErrorMessage = self::validateTextKeysForArrayConstraintsFromSetTextKeys($textKeys))) {
            throw new InvalidArgumentException($textKeysArrayErrorMessage, __LINE__);
        }
        $this->textKeys = $textKeys;
        
        return $this;
    }
    /**
     * Add item to textKeys value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TextKeysType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToTextKeys(\AppturePay\DSV\StructType\TextKeysType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TextKeysType_1) {
            throw new InvalidArgumentException(sprintf('The textKeys property can only contain items of type \AppturePay\DSV\StructType\TextKeysType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->textKeys[] = $item;
        
        return $this;
    }
    /**
     * Get dateTimeZones value
     * @return \AppturePay\DSV\StructType\DateTimeZonesType[]
     */
    public function getDateTimeZones(): ?array
    {
        return $this->dateTimeZones;
    }
    /**
     * This method is responsible for validating the values passed to the setDateTimeZones method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDateTimeZones method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDateTimeZonesForArrayConstraintsFromSetDateTimeZones(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeDateTimeZonesItem) {
            // validation for constraint: itemType
            if (!$fileTypeDateTimeZonesItem instanceof \AppturePay\DSV\StructType\DateTimeZonesType) {
                $invalidValues[] = is_object($fileTypeDateTimeZonesItem) ? get_class($fileTypeDateTimeZonesItem) : sprintf('%s(%s)', gettype($fileTypeDateTimeZonesItem), var_export($fileTypeDateTimeZonesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The dateTimeZones property can only contain items of type \AppturePay\DSV\StructType\DateTimeZonesType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set dateTimeZones value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\DateTimeZonesType[] $dateTimeZones
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setDateTimeZones(?array $dateTimeZones = null): self
    {
        // validation for constraint: array
        if ('' !== ($dateTimeZonesArrayErrorMessage = self::validateDateTimeZonesForArrayConstraintsFromSetDateTimeZones($dateTimeZones))) {
            throw new InvalidArgumentException($dateTimeZonesArrayErrorMessage, __LINE__);
        }
        $this->dateTimeZones = $dateTimeZones;
        
        return $this;
    }
    /**
     * Add item to dateTimeZones value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\DateTimeZonesType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToDateTimeZones(\AppturePay\DSV\StructType\DateTimeZonesType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\DateTimeZonesType) {
            throw new InvalidArgumentException(sprintf('The dateTimeZones property can only contain items of type \AppturePay\DSV\StructType\DateTimeZonesType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->dateTimeZones[] = $item;
        
        return $this;
    }
    /**
     * Get address value
     * @return \AppturePay\DSV\StructType\AddressType_1[]
     */
    public function getAddress(): ?array
    {
        return $this->address;
    }
    /**
     * This method is responsible for validating the values passed to the setAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAddress method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAddressForArrayConstraintsFromSetAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeAddressItem) {
            // validation for constraint: itemType
            if (!$fileTypeAddressItem instanceof \AppturePay\DSV\StructType\AddressType_1) {
                $invalidValues[] = is_object($fileTypeAddressItem) ? get_class($fileTypeAddressItem) : sprintf('%s(%s)', gettype($fileTypeAddressItem), var_export($fileTypeAddressItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The address property can only contain items of type \AppturePay\DSV\StructType\AddressType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set address value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AddressType_1[] $address
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setAddress(?array $address = null): self
    {
        // validation for constraint: array
        if ('' !== ($addressArrayErrorMessage = self::validateAddressForArrayConstraintsFromSetAddress($address))) {
            throw new InvalidArgumentException($addressArrayErrorMessage, __LINE__);
        }
        $this->address = $address;
        
        return $this;
    }
    /**
     * Add item to address value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AddressType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToAddress(\AppturePay\DSV\StructType\AddressType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\AddressType_1) {
            throw new InvalidArgumentException(sprintf('The address property can only contain items of type \AppturePay\DSV\StructType\AddressType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->address[] = $item;
        
        return $this;
    }
    /**
     * Get extraReference value
     * @return \AppturePay\DSV\StructType\ExtraReferenceType[]
     */
    public function getExtraReference(): ?array
    {
        return $this->extraReference;
    }
    /**
     * This method is responsible for validating the values passed to the setExtraReference method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtraReference method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtraReferenceForArrayConstraintsFromSetExtraReference(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeExtraReferenceItem) {
            // validation for constraint: itemType
            if (!$fileTypeExtraReferenceItem instanceof \AppturePay\DSV\StructType\ExtraReferenceType) {
                $invalidValues[] = is_object($fileTypeExtraReferenceItem) ? get_class($fileTypeExtraReferenceItem) : sprintf('%s(%s)', gettype($fileTypeExtraReferenceItem), var_export($fileTypeExtraReferenceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The extraReference property can only contain items of type \AppturePay\DSV\StructType\ExtraReferenceType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set extraReference value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraReferenceType[] $extraReference
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setExtraReference(?array $extraReference = null): self
    {
        // validation for constraint: array
        if ('' !== ($extraReferenceArrayErrorMessage = self::validateExtraReferenceForArrayConstraintsFromSetExtraReference($extraReference))) {
            throw new InvalidArgumentException($extraReferenceArrayErrorMessage, __LINE__);
        }
        $this->extraReference = $extraReference;
        
        return $this;
    }
    /**
     * Add item to extraReference value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraReferenceType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToExtraReference(\AppturePay\DSV\StructType\ExtraReferenceType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ExtraReferenceType) {
            throw new InvalidArgumentException(sprintf('The extraReference property can only contain items of type \AppturePay\DSV\StructType\ExtraReferenceType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->extraReference[] = $item;
        
        return $this;
    }
    /**
     * Get returnReference value
     * @return \AppturePay\DSV\StructType\ReturnReferenceType|null
     */
    public function getReturnReference(): ?\AppturePay\DSV\StructType\ReturnReferenceType
    {
        return $this->returnReference;
    }
    /**
     * Set returnReference value
     * @param \AppturePay\DSV\StructType\ReturnReferenceType $returnReference
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setReturnReference(?\AppturePay\DSV\StructType\ReturnReferenceType $returnReference = null): self
    {
        $this->returnReference = $returnReference;
        
        return $this;
    }
    /**
     * Get additionalServices value
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1[]
     */
    public function getAdditionalServices(): ?array
    {
        return $this->additionalServices;
    }
    /**
     * This method is responsible for validating the values passed to the setAdditionalServices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAdditionalServices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAdditionalServicesForArrayConstraintsFromSetAdditionalServices(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeAdditionalServicesItem) {
            // validation for constraint: itemType
            if (!$fileTypeAdditionalServicesItem instanceof \AppturePay\DSV\StructType\AdditionalServicesType_1) {
                $invalidValues[] = is_object($fileTypeAdditionalServicesItem) ? get_class($fileTypeAdditionalServicesItem) : sprintf('%s(%s)', gettype($fileTypeAdditionalServicesItem), var_export($fileTypeAdditionalServicesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The additionalServices property can only contain items of type \AppturePay\DSV\StructType\AdditionalServicesType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set additionalServices value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AdditionalServicesType_1[] $additionalServices
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setAdditionalServices(?array $additionalServices = null): self
    {
        // validation for constraint: array
        if ('' !== ($additionalServicesArrayErrorMessage = self::validateAdditionalServicesForArrayConstraintsFromSetAdditionalServices($additionalServices))) {
            throw new InvalidArgumentException($additionalServicesArrayErrorMessage, __LINE__);
        }
        $this->additionalServices = $additionalServices;
        
        return $this;
    }
    /**
     * Add item to additionalServices value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\AdditionalServicesType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToAdditionalServices(\AppturePay\DSV\StructType\AdditionalServicesType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\AdditionalServicesType_1) {
            throw new InvalidArgumentException(sprintf('The additionalServices property can only contain items of type \AppturePay\DSV\StructType\AdditionalServicesType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->additionalServices[] = $item;
        
        return $this;
    }
    /**
     * Get conNote value
     * @return \AppturePay\DSV\StructType\ConNoteType|null
     */
    public function getConNote(): ?\AppturePay\DSV\StructType\ConNoteType
    {
        return $this->conNote;
    }
    /**
     * Set conNote value
     * @param \AppturePay\DSV\StructType\ConNoteType $conNote
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setConNote(?\AppturePay\DSV\StructType\ConNoteType $conNote = null): self
    {
        $this->conNote = $conNote;
        
        return $this;
    }
    /**
     * Get link value
     * @return \AppturePay\DSV\StructType\LinkType[]
     */
    public function getLink(): ?array
    {
        return $this->link;
    }
    /**
     * This method is responsible for validating the values passed to the setLink method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLink method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLinkForArrayConstraintsFromSetLink(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeLinkItem) {
            // validation for constraint: itemType
            if (!$fileTypeLinkItem instanceof \AppturePay\DSV\StructType\LinkType) {
                $invalidValues[] = is_object($fileTypeLinkItem) ? get_class($fileTypeLinkItem) : sprintf('%s(%s)', gettype($fileTypeLinkItem), var_export($fileTypeLinkItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The link property can only contain items of type \AppturePay\DSV\StructType\LinkType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set link value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LinkType[] $link
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setLink(?array $link = null): self
    {
        // validation for constraint: array
        if ('' !== ($linkArrayErrorMessage = self::validateLinkForArrayConstraintsFromSetLink($link))) {
            throw new InvalidArgumentException($linkArrayErrorMessage, __LINE__);
        }
        $this->link = $link;
        
        return $this;
    }
    /**
     * Add item to link value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LinkType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToLink(\AppturePay\DSV\StructType\LinkType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\LinkType) {
            throw new InvalidArgumentException(sprintf('The link property can only contain items of type \AppturePay\DSV\StructType\LinkType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->link[] = $item;
        
        return $this;
    }
    /**
     * Get freeText value
     * @return \AppturePay\DSV\StructType\FreeTextType[]
     */
    public function getFreeText(): ?array
    {
        return $this->freeText;
    }
    /**
     * This method is responsible for validating the values passed to the setFreeText method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFreeText method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFreeTextForArrayConstraintsFromSetFreeText(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeFreeTextItem) {
            // validation for constraint: itemType
            if (!$fileTypeFreeTextItem instanceof \AppturePay\DSV\StructType\FreeTextType) {
                $invalidValues[] = is_object($fileTypeFreeTextItem) ? get_class($fileTypeFreeTextItem) : sprintf('%s(%s)', gettype($fileTypeFreeTextItem), var_export($fileTypeFreeTextItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The freeText property can only contain items of type \AppturePay\DSV\StructType\FreeTextType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set freeText value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\FreeTextType[] $freeText
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setFreeText(?array $freeText = null): self
    {
        // validation for constraint: array
        if ('' !== ($freeTextArrayErrorMessage = self::validateFreeTextForArrayConstraintsFromSetFreeText($freeText))) {
            throw new InvalidArgumentException($freeTextArrayErrorMessage, __LINE__);
        }
        $this->freeText = $freeText;
        
        return $this;
    }
    /**
     * Add item to freeText value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\FreeTextType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToFreeText(\AppturePay\DSV\StructType\FreeTextType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\FreeTextType) {
            throw new InvalidArgumentException(sprintf('The freeText property can only contain items of type \AppturePay\DSV\StructType\FreeTextType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->freeText[] = $item;
        
        return $this;
    }
    /**
     * Get emission value
     * @return \AppturePay\DSV\StructType\EmissionType[]
     */
    public function getEmission(): ?array
    {
        return $this->emission;
    }
    /**
     * This method is responsible for validating the values passed to the setEmission method
     * This method is willingly generated in order to preserve the one-line inline validation within the setEmission method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateEmissionForArrayConstraintsFromSetEmission(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeEmissionItem) {
            // validation for constraint: itemType
            if (!$fileTypeEmissionItem instanceof \AppturePay\DSV\StructType\EmissionType) {
                $invalidValues[] = is_object($fileTypeEmissionItem) ? get_class($fileTypeEmissionItem) : sprintf('%s(%s)', gettype($fileTypeEmissionItem), var_export($fileTypeEmissionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The emission property can only contain items of type \AppturePay\DSV\StructType\EmissionType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set emission value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\EmissionType[] $emission
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setEmission(?array $emission = null): self
    {
        // validation for constraint: array
        if ('' !== ($emissionArrayErrorMessage = self::validateEmissionForArrayConstraintsFromSetEmission($emission))) {
            throw new InvalidArgumentException($emissionArrayErrorMessage, __LINE__);
        }
        $this->emission = $emission;
        
        return $this;
    }
    /**
     * Add item to emission value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\EmissionType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToEmission(\AppturePay\DSV\StructType\EmissionType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\EmissionType) {
            throw new InvalidArgumentException(sprintf('The emission property can only contain items of type \AppturePay\DSV\StructType\EmissionType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->emission[] = $item;
        
        return $this;
    }
    /**
     * Get customsDocuments value
     * @return \AppturePay\DSV\StructType\CustomsDocumentsType[]
     */
    public function getCustomsDocuments(): ?array
    {
        return $this->customsDocuments;
    }
    /**
     * This method is responsible for validating the values passed to the setCustomsDocuments method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCustomsDocuments method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCustomsDocumentsForArrayConstraintsFromSetCustomsDocuments(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeCustomsDocumentsItem) {
            // validation for constraint: itemType
            if (!$fileTypeCustomsDocumentsItem instanceof \AppturePay\DSV\StructType\CustomsDocumentsType) {
                $invalidValues[] = is_object($fileTypeCustomsDocumentsItem) ? get_class($fileTypeCustomsDocumentsItem) : sprintf('%s(%s)', gettype($fileTypeCustomsDocumentsItem), var_export($fileTypeCustomsDocumentsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The customsDocuments property can only contain items of type \AppturePay\DSV\StructType\CustomsDocumentsType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set customsDocuments value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CustomsDocumentsType[] $customsDocuments
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setCustomsDocuments(?array $customsDocuments = null): self
    {
        // validation for constraint: array
        if ('' !== ($customsDocumentsArrayErrorMessage = self::validateCustomsDocumentsForArrayConstraintsFromSetCustomsDocuments($customsDocuments))) {
            throw new InvalidArgumentException($customsDocumentsArrayErrorMessage, __LINE__);
        }
        $this->customsDocuments = $customsDocuments;
        
        return $this;
    }
    /**
     * Add item to customsDocuments value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CustomsDocumentsType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToCustomsDocuments(\AppturePay\DSV\StructType\CustomsDocumentsType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\CustomsDocumentsType) {
            throw new InvalidArgumentException(sprintf('The customsDocuments property can only contain items of type \AppturePay\DSV\StructType\CustomsDocumentsType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->customsDocuments[] = $item;
        
        return $this;
    }
    /**
     * Get transportInstruction value
     * @return \AppturePay\DSV\StructType\TransportInstructionType_1[]
     */
    public function getTransportInstruction(): ?array
    {
        return $this->transportInstruction;
    }
    /**
     * This method is responsible for validating the values passed to the setTransportInstruction method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTransportInstruction method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTransportInstructionForArrayConstraintsFromSetTransportInstruction(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeTransportInstructionItem) {
            // validation for constraint: itemType
            if (!$fileTypeTransportInstructionItem instanceof \AppturePay\DSV\StructType\TransportInstructionType_1) {
                $invalidValues[] = is_object($fileTypeTransportInstructionItem) ? get_class($fileTypeTransportInstructionItem) : sprintf('%s(%s)', gettype($fileTypeTransportInstructionItem), var_export($fileTypeTransportInstructionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The transportInstruction property can only contain items of type \AppturePay\DSV\StructType\TransportInstructionType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set transportInstruction value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TransportInstructionType_1[] $transportInstruction
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTransportInstruction(?array $transportInstruction = null): self
    {
        // validation for constraint: array
        if ('' !== ($transportInstructionArrayErrorMessage = self::validateTransportInstructionForArrayConstraintsFromSetTransportInstruction($transportInstruction))) {
            throw new InvalidArgumentException($transportInstructionArrayErrorMessage, __LINE__);
        }
        $this->transportInstruction = $transportInstruction;
        
        return $this;
    }
    /**
     * Add item to transportInstruction value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TransportInstructionType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToTransportInstruction(\AppturePay\DSV\StructType\TransportInstructionType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TransportInstructionType_1) {
            throw new InvalidArgumentException(sprintf('The transportInstruction property can only contain items of type \AppturePay\DSV\StructType\TransportInstructionType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->transportInstruction[] = $item;
        
        return $this;
    }
    /**
     * Get planCharacteristic value
     * @return \AppturePay\DSV\StructType\PlanCharacteristicType_1[]
     */
    public function getPlanCharacteristic(): ?array
    {
        return $this->planCharacteristic;
    }
    /**
     * This method is responsible for validating the values passed to the setPlanCharacteristic method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPlanCharacteristic method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePlanCharacteristicForArrayConstraintsFromSetPlanCharacteristic(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypePlanCharacteristicItem) {
            // validation for constraint: itemType
            if (!$fileTypePlanCharacteristicItem instanceof \AppturePay\DSV\StructType\PlanCharacteristicType_1) {
                $invalidValues[] = is_object($fileTypePlanCharacteristicItem) ? get_class($fileTypePlanCharacteristicItem) : sprintf('%s(%s)', gettype($fileTypePlanCharacteristicItem), var_export($fileTypePlanCharacteristicItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The planCharacteristic property can only contain items of type \AppturePay\DSV\StructType\PlanCharacteristicType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set planCharacteristic value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\PlanCharacteristicType_1[] $planCharacteristic
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setPlanCharacteristic(?array $planCharacteristic = null): self
    {
        // validation for constraint: array
        if ('' !== ($planCharacteristicArrayErrorMessage = self::validatePlanCharacteristicForArrayConstraintsFromSetPlanCharacteristic($planCharacteristic))) {
            throw new InvalidArgumentException($planCharacteristicArrayErrorMessage, __LINE__);
        }
        $this->planCharacteristic = $planCharacteristic;
        
        return $this;
    }
    /**
     * Add item to planCharacteristic value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\PlanCharacteristicType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToPlanCharacteristic(\AppturePay\DSV\StructType\PlanCharacteristicType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\PlanCharacteristicType_1) {
            throw new InvalidArgumentException(sprintf('The planCharacteristic property can only contain items of type \AppturePay\DSV\StructType\PlanCharacteristicType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->planCharacteristic[] = $item;
        
        return $this;
    }
    /**
     * Get planning value
     * @return \AppturePay\DSV\StructType\PlanningType[]
     */
    public function getPlanning(): ?array
    {
        return $this->planning;
    }
    /**
     * This method is responsible for validating the values passed to the setPlanning method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPlanning method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePlanningForArrayConstraintsFromSetPlanning(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypePlanningItem) {
            // validation for constraint: itemType
            if (!$fileTypePlanningItem instanceof \AppturePay\DSV\StructType\PlanningType) {
                $invalidValues[] = is_object($fileTypePlanningItem) ? get_class($fileTypePlanningItem) : sprintf('%s(%s)', gettype($fileTypePlanningItem), var_export($fileTypePlanningItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The planning property can only contain items of type \AppturePay\DSV\StructType\PlanningType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set planning value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\PlanningType[] $planning
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setPlanning(?array $planning = null): self
    {
        // validation for constraint: array
        if ('' !== ($planningArrayErrorMessage = self::validatePlanningForArrayConstraintsFromSetPlanning($planning))) {
            throw new InvalidArgumentException($planningArrayErrorMessage, __LINE__);
        }
        $this->planning = $planning;
        
        return $this;
    }
    /**
     * Add item to planning value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\PlanningType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToPlanning(\AppturePay\DSV\StructType\PlanningType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\PlanningType) {
            throw new InvalidArgumentException(sprintf('The planning property can only contain items of type \AppturePay\DSV\StructType\PlanningType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->planning[] = $item;
        
        return $this;
    }
    /**
     * Get shippingUnit value
     * @return \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    public function getShippingUnit(): ?array
    {
        return $this->shippingUnit;
    }
    /**
     * This method is responsible for validating the values passed to the setShippingUnit method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShippingUnit method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShippingUnitForArrayConstraintsFromSetShippingUnit(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeShippingUnitItem) {
            // validation for constraint: itemType
            if (!$fileTypeShippingUnitItem instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
                $invalidValues[] = is_object($fileTypeShippingUnitItem) ? get_class($fileTypeShippingUnitItem) : sprintf('%s(%s)', gettype($fileTypeShippingUnitItem), var_export($fileTypeShippingUnitItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shippingUnit property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set shippingUnit value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnit
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setShippingUnit(?array $shippingUnit = null): self
    {
        // validation for constraint: array
        if ('' !== ($shippingUnitArrayErrorMessage = self::validateShippingUnitForArrayConstraintsFromSetShippingUnit($shippingUnit))) {
            throw new InvalidArgumentException($shippingUnitArrayErrorMessage, __LINE__);
        }
        $this->shippingUnit = $shippingUnit;
        
        return $this;
    }
    /**
     * Add item to shippingUnit value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToShippingUnit(\AppturePay\DSV\StructType\ShippingUnitType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
            throw new InvalidArgumentException(sprintf('The shippingUnit property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shippingUnit[] = $item;
        
        return $this;
    }
    /**
     * Get goodsLine value
     * @return \AppturePay\DSV\StructType\GoodsLineType_1[]
     */
    public function getGoodsLine(): ?array
    {
        return $this->goodsLine;
    }
    /**
     * This method is responsible for validating the values passed to the setGoodsLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGoodsLine method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGoodsLineForArrayConstraintsFromSetGoodsLine(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeGoodsLineItem) {
            // validation for constraint: itemType
            if (!$fileTypeGoodsLineItem instanceof \AppturePay\DSV\StructType\GoodsLineType_1) {
                $invalidValues[] = is_object($fileTypeGoodsLineItem) ? get_class($fileTypeGoodsLineItem) : sprintf('%s(%s)', gettype($fileTypeGoodsLineItem), var_export($fileTypeGoodsLineItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The goodsLine property can only contain items of type \AppturePay\DSV\StructType\GoodsLineType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set goodsLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\GoodsLineType_1[] $goodsLine
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setGoodsLine(?array $goodsLine = null): self
    {
        // validation for constraint: array
        if ('' !== ($goodsLineArrayErrorMessage = self::validateGoodsLineForArrayConstraintsFromSetGoodsLine($goodsLine))) {
            throw new InvalidArgumentException($goodsLineArrayErrorMessage, __LINE__);
        }
        $this->goodsLine = $goodsLine;
        
        return $this;
    }
    /**
     * Add item to goodsLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\GoodsLineType_1 $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToGoodsLine(\AppturePay\DSV\StructType\GoodsLineType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\GoodsLineType_1) {
            throw new InvalidArgumentException(sprintf('The goodsLine property can only contain items of type \AppturePay\DSV\StructType\GoodsLineType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->goodsLine[] = $item;
        
        return $this;
    }
    /**
     * Get container value
     * @return \AppturePay\DSV\StructType\ContainerType[]
     */
    public function getContainer(): ?array
    {
        return $this->container;
    }
    /**
     * This method is responsible for validating the values passed to the setContainer method
     * This method is willingly generated in order to preserve the one-line inline validation within the setContainer method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateContainerForArrayConstraintsFromSetContainer(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeContainerItem) {
            // validation for constraint: itemType
            if (!$fileTypeContainerItem instanceof \AppturePay\DSV\StructType\ContainerType) {
                $invalidValues[] = is_object($fileTypeContainerItem) ? get_class($fileTypeContainerItem) : sprintf('%s(%s)', gettype($fileTypeContainerItem), var_export($fileTypeContainerItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The container property can only contain items of type \AppturePay\DSV\StructType\ContainerType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set container value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ContainerType[] $container
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setContainer(?array $container = null): self
    {
        // validation for constraint: array
        if ('' !== ($containerArrayErrorMessage = self::validateContainerForArrayConstraintsFromSetContainer($container))) {
            throw new InvalidArgumentException($containerArrayErrorMessage, __LINE__);
        }
        $this->container = $container;
        
        return $this;
    }
    /**
     * Add item to container value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ContainerType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToContainer(\AppturePay\DSV\StructType\ContainerType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ContainerType) {
            throw new InvalidArgumentException(sprintf('The container property can only contain items of type \AppturePay\DSV\StructType\ContainerType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->container[] = $item;
        
        return $this;
    }
    /**
     * Get costAndCharge value
     * @return \AppturePay\DSV\StructType\CostAndChargeType[]
     */
    public function getCostAndCharge(): ?array
    {
        return $this->costAndCharge;
    }
    /**
     * This method is responsible for validating the values passed to the setCostAndCharge method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCostAndCharge method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCostAndChargeForArrayConstraintsFromSetCostAndCharge(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeCostAndChargeItem) {
            // validation for constraint: itemType
            if (!$fileTypeCostAndChargeItem instanceof \AppturePay\DSV\StructType\CostAndChargeType) {
                $invalidValues[] = is_object($fileTypeCostAndChargeItem) ? get_class($fileTypeCostAndChargeItem) : sprintf('%s(%s)', gettype($fileTypeCostAndChargeItem), var_export($fileTypeCostAndChargeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The costAndCharge property can only contain items of type \AppturePay\DSV\StructType\CostAndChargeType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set costAndCharge value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CostAndChargeType[] $costAndCharge
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setCostAndCharge(?array $costAndCharge = null): self
    {
        // validation for constraint: array
        if ('' !== ($costAndChargeArrayErrorMessage = self::validateCostAndChargeForArrayConstraintsFromSetCostAndCharge($costAndCharge))) {
            throw new InvalidArgumentException($costAndChargeArrayErrorMessage, __LINE__);
        }
        $this->costAndCharge = $costAndCharge;
        
        return $this;
    }
    /**
     * Add item to costAndCharge value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\CostAndChargeType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToCostAndCharge(\AppturePay\DSV\StructType\CostAndChargeType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\CostAndChargeType) {
            throw new InvalidArgumentException(sprintf('The costAndCharge property can only contain items of type \AppturePay\DSV\StructType\CostAndChargeType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->costAndCharge[] = $item;
        
        return $this;
    }
    /**
     * Get tailorMade value
     * @return \AppturePay\DSV\StructType\TailorMadeType[]
     */
    public function getTailorMade(): ?array
    {
        return $this->tailorMade;
    }
    /**
     * This method is responsible for validating the values passed to the setTailorMade method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTailorMade method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTailorMadeForArrayConstraintsFromSetTailorMade(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileTypeTailorMadeItem) {
            // validation for constraint: itemType
            if (!$fileTypeTailorMadeItem instanceof \AppturePay\DSV\StructType\TailorMadeType) {
                $invalidValues[] = is_object($fileTypeTailorMadeItem) ? get_class($fileTypeTailorMadeItem) : sprintf('%s(%s)', gettype($fileTypeTailorMadeItem), var_export($fileTypeTailorMadeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The tailorMade property can only contain items of type \AppturePay\DSV\StructType\TailorMadeType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set tailorMade value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TailorMadeType[] $tailorMade
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTailorMade(?array $tailorMade = null): self
    {
        // validation for constraint: array
        if ('' !== ($tailorMadeArrayErrorMessage = self::validateTailorMadeForArrayConstraintsFromSetTailorMade($tailorMade))) {
            throw new InvalidArgumentException($tailorMadeArrayErrorMessage, __LINE__);
        }
        $this->tailorMade = $tailorMade;
        
        return $this;
    }
    /**
     * Add item to tailorMade value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\TailorMadeType $item
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function addToTailorMade(\AppturePay\DSV\StructType\TailorMadeType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\TailorMadeType) {
            throw new InvalidArgumentException(sprintf('The tailorMade property can only contain items of type \AppturePay\DSV\StructType\TailorMadeType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->tailorMade[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\FileType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
