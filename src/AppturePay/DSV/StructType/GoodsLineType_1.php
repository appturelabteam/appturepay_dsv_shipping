<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GoodsLineType StructType
 * @subpackage Structs
 */
class GoodsLineType_1 extends AbstractStructBase
{
    /**
     * The articleCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $articleCode = null;
    /**
     * The primaryReference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $primaryReference = null;
    /**
     * The quantity
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $quantity = null;
    /**
     * The packageCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCode = null;
    /**
     * The commodityCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $commodityCode = null;
    /**
     * The grossWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $grossWeight = null;
    /**
     * The netWeight
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $netWeight = null;
    /**
     * The volumeCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $volumeCode = null;
    /**
     * The dimension
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DimensionType_1|null
     */
    protected ?\AppturePay\DSV\StructType\DimensionType_1 $dimension = null;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $volume = null;
    /**
     * The cubicMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $cubicMeters = null;
    /**
     * The loadingMeters
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadingMeters = null;
    /**
     * The goodsValueCurrency
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $goodsValueCurrency = null;
    /**
     * The goodsValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $goodsValue = null;
    /**
     * The loadIndex
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $loadIndex = null;
    /**
     * The quantityOfShippingUnits
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $quantityOfShippingUnits = null;
    /**
     * The packageCodeOfShippingUnits
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $packageCodeOfShippingUnits = null;
    /**
     * The dangerousGoods
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DangerousGoodsType_1|null
     */
    protected ?\AppturePay\DSV\StructType\DangerousGoodsType_1 $dangerousGoods = null;
    /**
     * The customLineItems
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\CustomLineItemsTms|null
     */
    protected ?\AppturePay\DSV\StructType\CustomLineItemsTms $customLineItems = null;
    /**
     * The tailorFiller20
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tailorFiller20 = null;
    /**
     * The subQuantity
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\SubQuantityType[]
     */
    protected ?array $subQuantity = null;
    /**
     * The marksAndNumbers
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\MarksAndNumbersType[]
     */
    protected ?array $marksAndNumbers = null;
    /**
     * The goodsDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\GoodsDescriptionType[]
     */
    protected ?array $goodsDescription = null;
    /**
     * The dangerousGoodsSpecification
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\DangerousGoodsSpecificationType[]
     */
    protected ?array $dangerousGoodsSpecification = null;
    /**
     * The container
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ContainerType[]
     */
    protected ?array $container = null;
    /**
     * The shippingUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    protected ?array $shippingUnit = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for GoodsLineType
     * @uses GoodsLineType_1::setArticleCode()
     * @uses GoodsLineType_1::setPrimaryReference()
     * @uses GoodsLineType_1::setQuantity()
     * @uses GoodsLineType_1::setPackageCode()
     * @uses GoodsLineType_1::setCommodityCode()
     * @uses GoodsLineType_1::setGrossWeight()
     * @uses GoodsLineType_1::setNetWeight()
     * @uses GoodsLineType_1::setVolumeCode()
     * @uses GoodsLineType_1::setDimension()
     * @uses GoodsLineType_1::setVolume()
     * @uses GoodsLineType_1::setCubicMeters()
     * @uses GoodsLineType_1::setLoadingMeters()
     * @uses GoodsLineType_1::setGoodsValueCurrency()
     * @uses GoodsLineType_1::setGoodsValue()
     * @uses GoodsLineType_1::setLoadIndex()
     * @uses GoodsLineType_1::setQuantityOfShippingUnits()
     * @uses GoodsLineType_1::setPackageCodeOfShippingUnits()
     * @uses GoodsLineType_1::setDangerousGoods()
     * @uses GoodsLineType_1::setCustomLineItems()
     * @uses GoodsLineType_1::setTailorFiller20()
     * @uses GoodsLineType_1::setSubQuantity()
     * @uses GoodsLineType_1::setMarksAndNumbers()
     * @uses GoodsLineType_1::setGoodsDescription()
     * @uses GoodsLineType_1::setDangerousGoodsSpecification()
     * @uses GoodsLineType_1::setContainer()
     * @uses GoodsLineType_1::setShippingUnit()
     * @uses GoodsLineType_1::setType()
     * @param string $articleCode
     * @param string $primaryReference
     * @param float $quantity
     * @param string $packageCode
     * @param string $commodityCode
     * @param float $grossWeight
     * @param float $netWeight
     * @param string $volumeCode
     * @param \AppturePay\DSV\StructType\DimensionType_1 $dimension
     * @param float $volume
     * @param float $cubicMeters
     * @param float $loadingMeters
     * @param string $goodsValueCurrency
     * @param float $goodsValue
     * @param float $loadIndex
     * @param int $quantityOfShippingUnits
     * @param string $packageCodeOfShippingUnits
     * @param \AppturePay\DSV\StructType\DangerousGoodsType_1 $dangerousGoods
     * @param \AppturePay\DSV\StructType\CustomLineItemsTms $customLineItems
     * @param string $tailorFiller20
     * @param \AppturePay\DSV\StructType\SubQuantityType[] $subQuantity
     * @param \AppturePay\DSV\StructType\MarksAndNumbersType[] $marksAndNumbers
     * @param \AppturePay\DSV\StructType\GoodsDescriptionType[] $goodsDescription
     * @param \AppturePay\DSV\StructType\DangerousGoodsSpecificationType[] $dangerousGoodsSpecification
     * @param \AppturePay\DSV\StructType\ContainerType[] $container
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnit
     * @param string $type
     */
    public function __construct(?string $articleCode = null, ?string $primaryReference = null, ?float $quantity = null, ?string $packageCode = null, ?string $commodityCode = null, ?float $grossWeight = null, ?float $netWeight = null, ?string $volumeCode = null, ?\AppturePay\DSV\StructType\DimensionType_1 $dimension = null, ?float $volume = null, ?float $cubicMeters = null, ?float $loadingMeters = null, ?string $goodsValueCurrency = null, ?float $goodsValue = null, ?float $loadIndex = null, ?int $quantityOfShippingUnits = null, ?string $packageCodeOfShippingUnits = null, ?\AppturePay\DSV\StructType\DangerousGoodsType_1 $dangerousGoods = null, ?\AppturePay\DSV\StructType\CustomLineItemsTms $customLineItems = null, ?string $tailorFiller20 = null, ?array $subQuantity = null, ?array $marksAndNumbers = null, ?array $goodsDescription = null, ?array $dangerousGoodsSpecification = null, ?array $container = null, ?array $shippingUnit = null, ?string $type = null)
    {
        $this
            ->setArticleCode($articleCode)
            ->setPrimaryReference($primaryReference)
            ->setQuantity($quantity)
            ->setPackageCode($packageCode)
            ->setCommodityCode($commodityCode)
            ->setGrossWeight($grossWeight)
            ->setNetWeight($netWeight)
            ->setVolumeCode($volumeCode)
            ->setDimension($dimension)
            ->setVolume($volume)
            ->setCubicMeters($cubicMeters)
            ->setLoadingMeters($loadingMeters)
            ->setGoodsValueCurrency($goodsValueCurrency)
            ->setGoodsValue($goodsValue)
            ->setLoadIndex($loadIndex)
            ->setQuantityOfShippingUnits($quantityOfShippingUnits)
            ->setPackageCodeOfShippingUnits($packageCodeOfShippingUnits)
            ->setDangerousGoods($dangerousGoods)
            ->setCustomLineItems($customLineItems)
            ->setTailorFiller20($tailorFiller20)
            ->setSubQuantity($subQuantity)
            ->setMarksAndNumbers($marksAndNumbers)
            ->setGoodsDescription($goodsDescription)
            ->setDangerousGoodsSpecification($dangerousGoodsSpecification)
            ->setContainer($container)
            ->setShippingUnit($shippingUnit)
            ->setType($type);
    }
    /**
     * Get articleCode value
     * @return string|null
     */
    public function getArticleCode(): ?string
    {
        return $this->articleCode;
    }
    /**
     * Set articleCode value
     * @param string $articleCode
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setArticleCode(?string $articleCode = null): self
    {
        // validation for constraint: string
        if (!is_null($articleCode) && !is_string($articleCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($articleCode, true), gettype($articleCode)), __LINE__);
        }
        $this->articleCode = $articleCode;
        
        return $this;
    }
    /**
     * Get primaryReference value
     * @return string|null
     */
    public function getPrimaryReference(): ?string
    {
        return $this->primaryReference;
    }
    /**
     * Set primaryReference value
     * @param string $primaryReference
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setPrimaryReference(?string $primaryReference = null): self
    {
        // validation for constraint: string
        if (!is_null($primaryReference) && !is_string($primaryReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($primaryReference, true), gettype($primaryReference)), __LINE__);
        }
        $this->primaryReference = $primaryReference;
        
        return $this;
    }
    /**
     * Get quantity value
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param float $quantity
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setQuantity(?float $quantity = null): self
    {
        // validation for constraint: float
        if (!is_null($quantity) && !(is_float($quantity) || is_numeric($quantity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->quantity = $quantity;
        
        return $this;
    }
    /**
     * Get packageCode value
     * @return string|null
     */
    public function getPackageCode(): ?string
    {
        return $this->packageCode;
    }
    /**
     * Set packageCode value
     * @param string $packageCode
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setPackageCode(?string $packageCode = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCode) && !is_string($packageCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCode, true), gettype($packageCode)), __LINE__);
        }
        $this->packageCode = $packageCode;
        
        return $this;
    }
    /**
     * Get commodityCode value
     * @return string|null
     */
    public function getCommodityCode(): ?string
    {
        return $this->commodityCode;
    }
    /**
     * Set commodityCode value
     * @param string $commodityCode
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setCommodityCode(?string $commodityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($commodityCode) && !is_string($commodityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($commodityCode, true), gettype($commodityCode)), __LINE__);
        }
        $this->commodityCode = $commodityCode;
        
        return $this;
    }
    /**
     * Get grossWeight value
     * @return float|null
     */
    public function getGrossWeight(): ?float
    {
        return $this->grossWeight;
    }
    /**
     * Set grossWeight value
     * @param float $grossWeight
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setGrossWeight(?float $grossWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($grossWeight) && !(is_float($grossWeight) || is_numeric($grossWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($grossWeight, true), gettype($grossWeight)), __LINE__);
        }
        $this->grossWeight = $grossWeight;
        
        return $this;
    }
    /**
     * Get netWeight value
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    /**
     * Set netWeight value
     * @param float $netWeight
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setNetWeight(?float $netWeight = null): self
    {
        // validation for constraint: float
        if (!is_null($netWeight) && !(is_float($netWeight) || is_numeric($netWeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($netWeight, true), gettype($netWeight)), __LINE__);
        }
        $this->netWeight = $netWeight;
        
        return $this;
    }
    /**
     * Get volumeCode value
     * @return string|null
     */
    public function getVolumeCode(): ?string
    {
        return $this->volumeCode;
    }
    /**
     * Set volumeCode value
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType_1::valueIsValid()
     * @uses \AppturePay\DSV\EnumType\VolumeCodeType_1::getValidValues()
     * @throws InvalidArgumentException
     * @param string $volumeCode
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setVolumeCode(?string $volumeCode = null): self
    {
        // validation for constraint: enumeration
        if (!\AppturePay\DSV\EnumType\VolumeCodeType_1::valueIsValid($volumeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \AppturePay\DSV\EnumType\VolumeCodeType_1', is_array($volumeCode) ? implode(', ', $volumeCode) : var_export($volumeCode, true), implode(', ', \AppturePay\DSV\EnumType\VolumeCodeType_1::getValidValues())), __LINE__);
        }
        $this->volumeCode = $volumeCode;
        
        return $this;
    }
    /**
     * Get dimension value
     * @return \AppturePay\DSV\StructType\DimensionType_1|null
     */
    public function getDimension(): ?\AppturePay\DSV\StructType\DimensionType_1
    {
        return $this->dimension;
    }
    /**
     * Set dimension value
     * @param \AppturePay\DSV\StructType\DimensionType_1 $dimension
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setDimension(?\AppturePay\DSV\StructType\DimensionType_1 $dimension = null): self
    {
        $this->dimension = $dimension;
        
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setVolume(?float $volume = null): self
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        
        return $this;
    }
    /**
     * Get cubicMeters value
     * @return float|null
     */
    public function getCubicMeters(): ?float
    {
        return $this->cubicMeters;
    }
    /**
     * Set cubicMeters value
     * @param float $cubicMeters
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setCubicMeters(?float $cubicMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($cubicMeters) && !(is_float($cubicMeters) || is_numeric($cubicMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cubicMeters, true), gettype($cubicMeters)), __LINE__);
        }
        $this->cubicMeters = $cubicMeters;
        
        return $this;
    }
    /**
     * Get loadingMeters value
     * @return float|null
     */
    public function getLoadingMeters(): ?float
    {
        return $this->loadingMeters;
    }
    /**
     * Set loadingMeters value
     * @param float $loadingMeters
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setLoadingMeters(?float $loadingMeters = null): self
    {
        // validation for constraint: float
        if (!is_null($loadingMeters) && !(is_float($loadingMeters) || is_numeric($loadingMeters))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadingMeters, true), gettype($loadingMeters)), __LINE__);
        }
        $this->loadingMeters = $loadingMeters;
        
        return $this;
    }
    /**
     * Get goodsValueCurrency value
     * @return string|null
     */
    public function getGoodsValueCurrency(): ?string
    {
        return $this->goodsValueCurrency;
    }
    /**
     * Set goodsValueCurrency value
     * @param string $goodsValueCurrency
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setGoodsValueCurrency(?string $goodsValueCurrency = null): self
    {
        // validation for constraint: string
        if (!is_null($goodsValueCurrency) && !is_string($goodsValueCurrency)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($goodsValueCurrency, true), gettype($goodsValueCurrency)), __LINE__);
        }
        $this->goodsValueCurrency = $goodsValueCurrency;
        
        return $this;
    }
    /**
     * Get goodsValue value
     * @return float|null
     */
    public function getGoodsValue(): ?float
    {
        return $this->goodsValue;
    }
    /**
     * Set goodsValue value
     * @param float $goodsValue
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setGoodsValue(?float $goodsValue = null): self
    {
        // validation for constraint: float
        if (!is_null($goodsValue) && !(is_float($goodsValue) || is_numeric($goodsValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsValue, true), gettype($goodsValue)), __LINE__);
        }
        $this->goodsValue = $goodsValue;
        
        return $this;
    }
    /**
     * Get loadIndex value
     * @return float|null
     */
    public function getLoadIndex(): ?float
    {
        return $this->loadIndex;
    }
    /**
     * Set loadIndex value
     * @param float $loadIndex
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setLoadIndex(?float $loadIndex = null): self
    {
        // validation for constraint: float
        if (!is_null($loadIndex) && !(is_float($loadIndex) || is_numeric($loadIndex))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($loadIndex, true), gettype($loadIndex)), __LINE__);
        }
        $this->loadIndex = $loadIndex;
        
        return $this;
    }
    /**
     * Get quantityOfShippingUnits value
     * @return int|null
     */
    public function getQuantityOfShippingUnits(): ?int
    {
        return $this->quantityOfShippingUnits;
    }
    /**
     * Set quantityOfShippingUnits value
     * @param int $quantityOfShippingUnits
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setQuantityOfShippingUnits(?int $quantityOfShippingUnits = null): self
    {
        // validation for constraint: int
        if (!is_null($quantityOfShippingUnits) && !(is_int($quantityOfShippingUnits) || ctype_digit($quantityOfShippingUnits))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($quantityOfShippingUnits, true), gettype($quantityOfShippingUnits)), __LINE__);
        }
        $this->quantityOfShippingUnits = $quantityOfShippingUnits;
        
        return $this;
    }
    /**
     * Get packageCodeOfShippingUnits value
     * @return string|null
     */
    public function getPackageCodeOfShippingUnits(): ?string
    {
        return $this->packageCodeOfShippingUnits;
    }
    /**
     * Set packageCodeOfShippingUnits value
     * @param string $packageCodeOfShippingUnits
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setPackageCodeOfShippingUnits(?string $packageCodeOfShippingUnits = null): self
    {
        // validation for constraint: string
        if (!is_null($packageCodeOfShippingUnits) && !is_string($packageCodeOfShippingUnits)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageCodeOfShippingUnits, true), gettype($packageCodeOfShippingUnits)), __LINE__);
        }
        $this->packageCodeOfShippingUnits = $packageCodeOfShippingUnits;
        
        return $this;
    }
    /**
     * Get dangerousGoods value
     * @return \AppturePay\DSV\StructType\DangerousGoodsType_1|null
     */
    public function getDangerousGoods(): ?\AppturePay\DSV\StructType\DangerousGoodsType_1
    {
        return $this->dangerousGoods;
    }
    /**
     * Set dangerousGoods value
     * @param \AppturePay\DSV\StructType\DangerousGoodsType_1 $dangerousGoods
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setDangerousGoods(?\AppturePay\DSV\StructType\DangerousGoodsType_1 $dangerousGoods = null): self
    {
        $this->dangerousGoods = $dangerousGoods;
        
        return $this;
    }
    /**
     * Get customLineItems value
     * @return \AppturePay\DSV\StructType\CustomLineItemsTms|null
     */
    public function getCustomLineItems(): ?\AppturePay\DSV\StructType\CustomLineItemsTms
    {
        return $this->customLineItems;
    }
    /**
     * Set customLineItems value
     * @param \AppturePay\DSV\StructType\CustomLineItemsTms $customLineItems
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setCustomLineItems(?\AppturePay\DSV\StructType\CustomLineItemsTms $customLineItems = null): self
    {
        $this->customLineItems = $customLineItems;
        
        return $this;
    }
    /**
     * Get tailorFiller20 value
     * @return string|null
     */
    public function getTailorFiller20(): ?string
    {
        return $this->tailorFiller20;
    }
    /**
     * Set tailorFiller20 value
     * @param string $tailorFiller20
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setTailorFiller20(?string $tailorFiller20 = null): self
    {
        // validation for constraint: string
        if (!is_null($tailorFiller20) && !is_string($tailorFiller20)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tailorFiller20, true), gettype($tailorFiller20)), __LINE__);
        }
        $this->tailorFiller20 = $tailorFiller20;
        
        return $this;
    }
    /**
     * Get subQuantity value
     * @return \AppturePay\DSV\StructType\SubQuantityType[]
     */
    public function getSubQuantity(): ?array
    {
        return $this->subQuantity;
    }
    /**
     * This method is responsible for validating the values passed to the setSubQuantity method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSubQuantity method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSubQuantityForArrayConstraintsFromSetSubQuantity(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $goodsLineTypeSubQuantityItem) {
            // validation for constraint: itemType
            if (!$goodsLineTypeSubQuantityItem instanceof \AppturePay\DSV\StructType\SubQuantityType) {
                $invalidValues[] = is_object($goodsLineTypeSubQuantityItem) ? get_class($goodsLineTypeSubQuantityItem) : sprintf('%s(%s)', gettype($goodsLineTypeSubQuantityItem), var_export($goodsLineTypeSubQuantityItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The subQuantity property can only contain items of type \AppturePay\DSV\StructType\SubQuantityType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set subQuantity value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\SubQuantityType[] $subQuantity
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setSubQuantity(?array $subQuantity = null): self
    {
        // validation for constraint: array
        if ('' !== ($subQuantityArrayErrorMessage = self::validateSubQuantityForArrayConstraintsFromSetSubQuantity($subQuantity))) {
            throw new InvalidArgumentException($subQuantityArrayErrorMessage, __LINE__);
        }
        $this->subQuantity = $subQuantity;
        
        return $this;
    }
    /**
     * Add item to subQuantity value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\SubQuantityType $item
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function addToSubQuantity(\AppturePay\DSV\StructType\SubQuantityType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\SubQuantityType) {
            throw new InvalidArgumentException(sprintf('The subQuantity property can only contain items of type \AppturePay\DSV\StructType\SubQuantityType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->subQuantity[] = $item;
        
        return $this;
    }
    /**
     * Get marksAndNumbers value
     * @return \AppturePay\DSV\StructType\MarksAndNumbersType[]
     */
    public function getMarksAndNumbers(): ?array
    {
        return $this->marksAndNumbers;
    }
    /**
     * This method is responsible for validating the values passed to the setMarksAndNumbers method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMarksAndNumbers method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMarksAndNumbersForArrayConstraintsFromSetMarksAndNumbers(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $goodsLineTypeMarksAndNumbersItem) {
            // validation for constraint: itemType
            if (!$goodsLineTypeMarksAndNumbersItem instanceof \AppturePay\DSV\StructType\MarksAndNumbersType) {
                $invalidValues[] = is_object($goodsLineTypeMarksAndNumbersItem) ? get_class($goodsLineTypeMarksAndNumbersItem) : sprintf('%s(%s)', gettype($goodsLineTypeMarksAndNumbersItem), var_export($goodsLineTypeMarksAndNumbersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The marksAndNumbers property can only contain items of type \AppturePay\DSV\StructType\MarksAndNumbersType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set marksAndNumbers value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\MarksAndNumbersType[] $marksAndNumbers
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setMarksAndNumbers(?array $marksAndNumbers = null): self
    {
        // validation for constraint: array
        if ('' !== ($marksAndNumbersArrayErrorMessage = self::validateMarksAndNumbersForArrayConstraintsFromSetMarksAndNumbers($marksAndNumbers))) {
            throw new InvalidArgumentException($marksAndNumbersArrayErrorMessage, __LINE__);
        }
        $this->marksAndNumbers = $marksAndNumbers;
        
        return $this;
    }
    /**
     * Add item to marksAndNumbers value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\MarksAndNumbersType $item
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function addToMarksAndNumbers(\AppturePay\DSV\StructType\MarksAndNumbersType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\MarksAndNumbersType) {
            throw new InvalidArgumentException(sprintf('The marksAndNumbers property can only contain items of type \AppturePay\DSV\StructType\MarksAndNumbersType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->marksAndNumbers[] = $item;
        
        return $this;
    }
    /**
     * Get goodsDescription value
     * @return \AppturePay\DSV\StructType\GoodsDescriptionType[]
     */
    public function getGoodsDescription(): ?array
    {
        return $this->goodsDescription;
    }
    /**
     * This method is responsible for validating the values passed to the setGoodsDescription method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGoodsDescription method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGoodsDescriptionForArrayConstraintsFromSetGoodsDescription(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $goodsLineTypeGoodsDescriptionItem) {
            // validation for constraint: itemType
            if (!$goodsLineTypeGoodsDescriptionItem instanceof \AppturePay\DSV\StructType\GoodsDescriptionType) {
                $invalidValues[] = is_object($goodsLineTypeGoodsDescriptionItem) ? get_class($goodsLineTypeGoodsDescriptionItem) : sprintf('%s(%s)', gettype($goodsLineTypeGoodsDescriptionItem), var_export($goodsLineTypeGoodsDescriptionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The goodsDescription property can only contain items of type \AppturePay\DSV\StructType\GoodsDescriptionType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set goodsDescription value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\GoodsDescriptionType[] $goodsDescription
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setGoodsDescription(?array $goodsDescription = null): self
    {
        // validation for constraint: array
        if ('' !== ($goodsDescriptionArrayErrorMessage = self::validateGoodsDescriptionForArrayConstraintsFromSetGoodsDescription($goodsDescription))) {
            throw new InvalidArgumentException($goodsDescriptionArrayErrorMessage, __LINE__);
        }
        $this->goodsDescription = $goodsDescription;
        
        return $this;
    }
    /**
     * Add item to goodsDescription value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\GoodsDescriptionType $item
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function addToGoodsDescription(\AppturePay\DSV\StructType\GoodsDescriptionType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\GoodsDescriptionType) {
            throw new InvalidArgumentException(sprintf('The goodsDescription property can only contain items of type \AppturePay\DSV\StructType\GoodsDescriptionType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->goodsDescription[] = $item;
        
        return $this;
    }
    /**
     * Get dangerousGoodsSpecification value
     * @return \AppturePay\DSV\StructType\DangerousGoodsSpecificationType[]
     */
    public function getDangerousGoodsSpecification(): ?array
    {
        return $this->dangerousGoodsSpecification;
    }
    /**
     * This method is responsible for validating the values passed to the setDangerousGoodsSpecification method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDangerousGoodsSpecification method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDangerousGoodsSpecificationForArrayConstraintsFromSetDangerousGoodsSpecification(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $goodsLineTypeDangerousGoodsSpecificationItem) {
            // validation for constraint: itemType
            if (!$goodsLineTypeDangerousGoodsSpecificationItem instanceof \AppturePay\DSV\StructType\DangerousGoodsSpecificationType) {
                $invalidValues[] = is_object($goodsLineTypeDangerousGoodsSpecificationItem) ? get_class($goodsLineTypeDangerousGoodsSpecificationItem) : sprintf('%s(%s)', gettype($goodsLineTypeDangerousGoodsSpecificationItem), var_export($goodsLineTypeDangerousGoodsSpecificationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The dangerousGoodsSpecification property can only contain items of type \AppturePay\DSV\StructType\DangerousGoodsSpecificationType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set dangerousGoodsSpecification value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\DangerousGoodsSpecificationType[] $dangerousGoodsSpecification
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setDangerousGoodsSpecification(?array $dangerousGoodsSpecification = null): self
    {
        // validation for constraint: array
        if ('' !== ($dangerousGoodsSpecificationArrayErrorMessage = self::validateDangerousGoodsSpecificationForArrayConstraintsFromSetDangerousGoodsSpecification($dangerousGoodsSpecification))) {
            throw new InvalidArgumentException($dangerousGoodsSpecificationArrayErrorMessage, __LINE__);
        }
        $this->dangerousGoodsSpecification = $dangerousGoodsSpecification;
        
        return $this;
    }
    /**
     * Add item to dangerousGoodsSpecification value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\DangerousGoodsSpecificationType $item
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function addToDangerousGoodsSpecification(\AppturePay\DSV\StructType\DangerousGoodsSpecificationType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\DangerousGoodsSpecificationType) {
            throw new InvalidArgumentException(sprintf('The dangerousGoodsSpecification property can only contain items of type \AppturePay\DSV\StructType\DangerousGoodsSpecificationType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->dangerousGoodsSpecification[] = $item;
        
        return $this;
    }
    /**
     * Get container value
     * @return \AppturePay\DSV\StructType\ContainerType[]
     */
    public function getContainer(): ?array
    {
        return $this->container;
    }
    /**
     * This method is responsible for validating the values passed to the setContainer method
     * This method is willingly generated in order to preserve the one-line inline validation within the setContainer method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateContainerForArrayConstraintsFromSetContainer(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $goodsLineTypeContainerItem) {
            // validation for constraint: itemType
            if (!$goodsLineTypeContainerItem instanceof \AppturePay\DSV\StructType\ContainerType) {
                $invalidValues[] = is_object($goodsLineTypeContainerItem) ? get_class($goodsLineTypeContainerItem) : sprintf('%s(%s)', gettype($goodsLineTypeContainerItem), var_export($goodsLineTypeContainerItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The container property can only contain items of type \AppturePay\DSV\StructType\ContainerType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set container value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ContainerType[] $container
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setContainer(?array $container = null): self
    {
        // validation for constraint: array
        if ('' !== ($containerArrayErrorMessage = self::validateContainerForArrayConstraintsFromSetContainer($container))) {
            throw new InvalidArgumentException($containerArrayErrorMessage, __LINE__);
        }
        $this->container = $container;
        
        return $this;
    }
    /**
     * Add item to container value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ContainerType $item
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function addToContainer(\AppturePay\DSV\StructType\ContainerType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ContainerType) {
            throw new InvalidArgumentException(sprintf('The container property can only contain items of type \AppturePay\DSV\StructType\ContainerType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->container[] = $item;
        
        return $this;
    }
    /**
     * Get shippingUnit value
     * @return \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    public function getShippingUnit(): ?array
    {
        return $this->shippingUnit;
    }
    /**
     * This method is responsible for validating the values passed to the setShippingUnit method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShippingUnit method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShippingUnitForArrayConstraintsFromSetShippingUnit(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $goodsLineTypeShippingUnitItem) {
            // validation for constraint: itemType
            if (!$goodsLineTypeShippingUnitItem instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
                $invalidValues[] = is_object($goodsLineTypeShippingUnitItem) ? get_class($goodsLineTypeShippingUnitItem) : sprintf('%s(%s)', gettype($goodsLineTypeShippingUnitItem), var_export($goodsLineTypeShippingUnitItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shippingUnit property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set shippingUnit value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnit
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setShippingUnit(?array $shippingUnit = null): self
    {
        // validation for constraint: array
        if ('' !== ($shippingUnitArrayErrorMessage = self::validateShippingUnitForArrayConstraintsFromSetShippingUnit($shippingUnit))) {
            throw new InvalidArgumentException($shippingUnitArrayErrorMessage, __LINE__);
        }
        $this->shippingUnit = $shippingUnit;
        
        return $this;
    }
    /**
     * Add item to shippingUnit value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType $item
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function addToShippingUnit(\AppturePay\DSV\StructType\ShippingUnitType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
            throw new InvalidArgumentException(sprintf('The shippingUnit property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shippingUnit[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\GoodsLineType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
