<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CTcoordinatesType StructType
 * @subpackage Structs
 */
class CTcoordinatesType extends AbstractStructBase
{
    /**
     * The coordinatesType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $coordinatesType = null;
    /**
     * The xCoordinate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $xCoordinate = null;
    /**
     * The yCoordinate
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $yCoordinate = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for CTcoordinatesType
     * @uses CTcoordinatesType::setCoordinatesType()
     * @uses CTcoordinatesType::setXCoordinate()
     * @uses CTcoordinatesType::setYCoordinate()
     * @uses CTcoordinatesType::setType()
     * @param string $coordinatesType
     * @param string $xCoordinate
     * @param string $yCoordinate
     * @param string $type
     */
    public function __construct(?string $coordinatesType = null, ?string $xCoordinate = null, ?string $yCoordinate = null, ?string $type = null)
    {
        $this
            ->setCoordinatesType($coordinatesType)
            ->setXCoordinate($xCoordinate)
            ->setYCoordinate($yCoordinate)
            ->setType($type);
    }
    /**
     * Get coordinatesType value
     * @return string|null
     */
    public function getCoordinatesType(): ?string
    {
        return $this->coordinatesType;
    }
    /**
     * Set coordinatesType value
     * @param string $coordinatesType
     * @return \AppturePay\DSV\StructType\CTcoordinatesType
     */
    public function setCoordinatesType(?string $coordinatesType = null): self
    {
        // validation for constraint: string
        if (!is_null($coordinatesType) && !is_string($coordinatesType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($coordinatesType, true), gettype($coordinatesType)), __LINE__);
        }
        $this->coordinatesType = $coordinatesType;
        
        return $this;
    }
    /**
     * Get xCoordinate value
     * @return string|null
     */
    public function getXCoordinate(): ?string
    {
        return $this->xCoordinate;
    }
    /**
     * Set xCoordinate value
     * @param string $xCoordinate
     * @return \AppturePay\DSV\StructType\CTcoordinatesType
     */
    public function setXCoordinate(?string $xCoordinate = null): self
    {
        // validation for constraint: string
        if (!is_null($xCoordinate) && !is_string($xCoordinate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xCoordinate, true), gettype($xCoordinate)), __LINE__);
        }
        $this->xCoordinate = $xCoordinate;
        
        return $this;
    }
    /**
     * Get yCoordinate value
     * @return string|null
     */
    public function getYCoordinate(): ?string
    {
        return $this->yCoordinate;
    }
    /**
     * Set yCoordinate value
     * @param string $yCoordinate
     * @return \AppturePay\DSV\StructType\CTcoordinatesType
     */
    public function setYCoordinate(?string $yCoordinate = null): self
    {
        // validation for constraint: string
        if (!is_null($yCoordinate) && !is_string($yCoordinate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($yCoordinate, true), gettype($yCoordinate)), __LINE__);
        }
        $this->yCoordinate = $yCoordinate;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\CTcoordinatesType
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
