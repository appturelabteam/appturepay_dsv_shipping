<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AdditionalServicesType StructType
 * @subpackage Structs
 */
class AdditionalServicesType_1 extends AbstractStructBase
{
    /**
     * The serviceCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $serviceCode = null;
    /**
     * The tariffBaseValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $tariffBaseValue = null;
    /**
     * The conditionValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $conditionValue = null;
    /**
     * The minValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $minValue = null;
    /**
     * The maxValue
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $maxValue = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for AdditionalServicesType
     * @uses AdditionalServicesType_1::setServiceCode()
     * @uses AdditionalServicesType_1::setTariffBaseValue()
     * @uses AdditionalServicesType_1::setConditionValue()
     * @uses AdditionalServicesType_1::setMinValue()
     * @uses AdditionalServicesType_1::setMaxValue()
     * @uses AdditionalServicesType_1::setType()
     * @param string $serviceCode
     * @param float $tariffBaseValue
     * @param string $conditionValue
     * @param string $minValue
     * @param string $maxValue
     * @param string $type
     */
    public function __construct(?string $serviceCode = null, ?float $tariffBaseValue = null, ?string $conditionValue = null, ?string $minValue = null, ?string $maxValue = null, ?string $type = null)
    {
        $this
            ->setServiceCode($serviceCode)
            ->setTariffBaseValue($tariffBaseValue)
            ->setConditionValue($conditionValue)
            ->setMinValue($minValue)
            ->setMaxValue($maxValue)
            ->setType($type);
    }
    /**
     * Get serviceCode value
     * @return string|null
     */
    public function getServiceCode(): ?string
    {
        return $this->serviceCode;
    }
    /**
     * Set serviceCode value
     * @param string $serviceCode
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1
     */
    public function setServiceCode(?string $serviceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceCode) && !is_string($serviceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceCode, true), gettype($serviceCode)), __LINE__);
        }
        $this->serviceCode = $serviceCode;
        
        return $this;
    }
    /**
     * Get tariffBaseValue value
     * @return float|null
     */
    public function getTariffBaseValue(): ?float
    {
        return $this->tariffBaseValue;
    }
    /**
     * Set tariffBaseValue value
     * @param float $tariffBaseValue
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1
     */
    public function setTariffBaseValue(?float $tariffBaseValue = null): self
    {
        // validation for constraint: float
        if (!is_null($tariffBaseValue) && !(is_float($tariffBaseValue) || is_numeric($tariffBaseValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($tariffBaseValue, true), gettype($tariffBaseValue)), __LINE__);
        }
        $this->tariffBaseValue = $tariffBaseValue;
        
        return $this;
    }
    /**
     * Get conditionValue value
     * @return string|null
     */
    public function getConditionValue(): ?string
    {
        return $this->conditionValue;
    }
    /**
     * Set conditionValue value
     * @param string $conditionValue
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1
     */
    public function setConditionValue(?string $conditionValue = null): self
    {
        // validation for constraint: string
        if (!is_null($conditionValue) && !is_string($conditionValue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($conditionValue, true), gettype($conditionValue)), __LINE__);
        }
        $this->conditionValue = $conditionValue;
        
        return $this;
    }
    /**
     * Get minValue value
     * @return string|null
     */
    public function getMinValue(): ?string
    {
        return $this->minValue;
    }
    /**
     * Set minValue value
     * @param string $minValue
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1
     */
    public function setMinValue(?string $minValue = null): self
    {
        // validation for constraint: string
        if (!is_null($minValue) && !is_string($minValue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($minValue, true), gettype($minValue)), __LINE__);
        }
        $this->minValue = $minValue;
        
        return $this;
    }
    /**
     * Get maxValue value
     * @return string|null
     */
    public function getMaxValue(): ?string
    {
        return $this->maxValue;
    }
    /**
     * Set maxValue value
     * @param string $maxValue
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1
     */
    public function setMaxValue(?string $maxValue = null): self
    {
        // validation for constraint: string
        if (!is_null($maxValue) && !is_string($maxValue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($maxValue, true), gettype($maxValue)), __LINE__);
        }
        $this->maxValue = $maxValue;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\AdditionalServicesType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
