<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AmountsType StructType
 * @subpackage Structs
 */
class AmountsType extends AbstractStructBase
{
    /**
     * The amount1
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount1 = null;
    /**
     * The amount2
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount2 = null;
    /**
     * The amount3
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount3 = null;
    /**
     * The amount4
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount4 = null;
    /**
     * The amount5
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount5 = null;
    /**
     * The amount6
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount6 = null;
    /**
     * The amount7
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount7 = null;
    /**
     * The amount8
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount8 = null;
    /**
     * The amount9
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount9 = null;
    /**
     * Constructor method for AmountsType
     * @uses AmountsType::setAmount1()
     * @uses AmountsType::setAmount2()
     * @uses AmountsType::setAmount3()
     * @uses AmountsType::setAmount4()
     * @uses AmountsType::setAmount5()
     * @uses AmountsType::setAmount6()
     * @uses AmountsType::setAmount7()
     * @uses AmountsType::setAmount8()
     * @uses AmountsType::setAmount9()
     * @param float $amount1
     * @param float $amount2
     * @param float $amount3
     * @param float $amount4
     * @param float $amount5
     * @param float $amount6
     * @param float $amount7
     * @param float $amount8
     * @param float $amount9
     */
    public function __construct(?float $amount1 = null, ?float $amount2 = null, ?float $amount3 = null, ?float $amount4 = null, ?float $amount5 = null, ?float $amount6 = null, ?float $amount7 = null, ?float $amount8 = null, ?float $amount9 = null)
    {
        $this
            ->setAmount1($amount1)
            ->setAmount2($amount2)
            ->setAmount3($amount3)
            ->setAmount4($amount4)
            ->setAmount5($amount5)
            ->setAmount6($amount6)
            ->setAmount7($amount7)
            ->setAmount8($amount8)
            ->setAmount9($amount9);
    }
    /**
     * Get amount1 value
     * @return float|null
     */
    public function getAmount1(): ?float
    {
        return $this->amount1;
    }
    /**
     * Set amount1 value
     * @param float $amount1
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount1(?float $amount1 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount1) && !(is_float($amount1) || is_numeric($amount1))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount1, true), gettype($amount1)), __LINE__);
        }
        $this->amount1 = $amount1;
        
        return $this;
    }
    /**
     * Get amount2 value
     * @return float|null
     */
    public function getAmount2(): ?float
    {
        return $this->amount2;
    }
    /**
     * Set amount2 value
     * @param float $amount2
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount2(?float $amount2 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount2) && !(is_float($amount2) || is_numeric($amount2))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount2, true), gettype($amount2)), __LINE__);
        }
        $this->amount2 = $amount2;
        
        return $this;
    }
    /**
     * Get amount3 value
     * @return float|null
     */
    public function getAmount3(): ?float
    {
        return $this->amount3;
    }
    /**
     * Set amount3 value
     * @param float $amount3
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount3(?float $amount3 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount3) && !(is_float($amount3) || is_numeric($amount3))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount3, true), gettype($amount3)), __LINE__);
        }
        $this->amount3 = $amount3;
        
        return $this;
    }
    /**
     * Get amount4 value
     * @return float|null
     */
    public function getAmount4(): ?float
    {
        return $this->amount4;
    }
    /**
     * Set amount4 value
     * @param float $amount4
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount4(?float $amount4 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount4) && !(is_float($amount4) || is_numeric($amount4))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount4, true), gettype($amount4)), __LINE__);
        }
        $this->amount4 = $amount4;
        
        return $this;
    }
    /**
     * Get amount5 value
     * @return float|null
     */
    public function getAmount5(): ?float
    {
        return $this->amount5;
    }
    /**
     * Set amount5 value
     * @param float $amount5
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount5(?float $amount5 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount5) && !(is_float($amount5) || is_numeric($amount5))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount5, true), gettype($amount5)), __LINE__);
        }
        $this->amount5 = $amount5;
        
        return $this;
    }
    /**
     * Get amount6 value
     * @return float|null
     */
    public function getAmount6(): ?float
    {
        return $this->amount6;
    }
    /**
     * Set amount6 value
     * @param float $amount6
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount6(?float $amount6 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount6) && !(is_float($amount6) || is_numeric($amount6))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount6, true), gettype($amount6)), __LINE__);
        }
        $this->amount6 = $amount6;
        
        return $this;
    }
    /**
     * Get amount7 value
     * @return float|null
     */
    public function getAmount7(): ?float
    {
        return $this->amount7;
    }
    /**
     * Set amount7 value
     * @param float $amount7
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount7(?float $amount7 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount7) && !(is_float($amount7) || is_numeric($amount7))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount7, true), gettype($amount7)), __LINE__);
        }
        $this->amount7 = $amount7;
        
        return $this;
    }
    /**
     * Get amount8 value
     * @return float|null
     */
    public function getAmount8(): ?float
    {
        return $this->amount8;
    }
    /**
     * Set amount8 value
     * @param float $amount8
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount8(?float $amount8 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount8) && !(is_float($amount8) || is_numeric($amount8))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount8, true), gettype($amount8)), __LINE__);
        }
        $this->amount8 = $amount8;
        
        return $this;
    }
    /**
     * Get amount9 value
     * @return float|null
     */
    public function getAmount9(): ?float
    {
        return $this->amount9;
    }
    /**
     * Set amount9 value
     * @param float $amount9
     * @return \AppturePay\DSV\StructType\AmountsType
     */
    public function setAmount9(?float $amount9 = null): self
    {
        // validation for constraint: float
        if (!is_null($amount9) && !(is_float($amount9) || is_numeric($amount9))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount9, true), gettype($amount9)), __LINE__);
        }
        $this->amount9 = $amount9;
        
        return $this;
    }
}
