<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LoggingType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:LoggingType
 * @subpackage Structs
 */
class LoggingType_1 extends AbstractStructBase
{
    /**
     * The tableName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $tableName = null;
    /**
     * The errorCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $errorCode = null;
    /**
     * The indicationNotOk
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $indicationNotOk = null;
    /**
     * The errorDescription
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorDescription = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The errorCodeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $errorCodeField = null;
    /**
     * The errorDescriptionField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $errorDescriptionField = null;
    /**
     * The indicationNotOkField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $indicationNotOkField = null;
    /**
     * The tableNameField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $tableNameField = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for LoggingType
     * @uses LoggingType_1::setTableName()
     * @uses LoggingType_1::setErrorCode()
     * @uses LoggingType_1::setIndicationNotOk()
     * @uses LoggingType_1::setErrorDescription()
     * @uses LoggingType_1::setType()
     * @uses LoggingType_1::setErrorCodeField()
     * @uses LoggingType_1::setErrorDescriptionField()
     * @uses LoggingType_1::setIndicationNotOkField()
     * @uses LoggingType_1::setTableNameField()
     * @uses LoggingType_1::setTypeField()
     * @param string $tableName
     * @param int $errorCode
     * @param string $indicationNotOk
     * @param string $errorDescription
     * @param string $type
     * @param string $errorCodeField
     * @param string $errorDescriptionField
     * @param string $indicationNotOkField
     * @param string $tableNameField
     * @param string $typeField
     */
    public function __construct(?string $tableName = null, ?int $errorCode = null, ?string $indicationNotOk = null, ?string $errorDescription = null, ?string $type = null, ?string $errorCodeField = null, ?string $errorDescriptionField = null, ?string $indicationNotOkField = null, ?string $tableNameField = null, ?string $typeField = null)
    {
        $this
            ->setTableName($tableName)
            ->setErrorCode($errorCode)
            ->setIndicationNotOk($indicationNotOk)
            ->setErrorDescription($errorDescription)
            ->setType($type)
            ->setErrorCodeField($errorCodeField)
            ->setErrorDescriptionField($errorDescriptionField)
            ->setIndicationNotOkField($indicationNotOkField)
            ->setTableNameField($tableNameField)
            ->setTypeField($typeField);
    }
    /**
     * Get tableName value
     * @return string|null
     */
    public function getTableName(): ?string
    {
        return $this->tableName;
    }
    /**
     * Set tableName value
     * @param string $tableName
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setTableName(?string $tableName = null): self
    {
        // validation for constraint: string
        if (!is_null($tableName) && !is_string($tableName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tableName, true), gettype($tableName)), __LINE__);
        }
        $this->tableName = $tableName;
        
        return $this;
    }
    /**
     * Get errorCode value
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }
    /**
     * Set errorCode value
     * @param int $errorCode
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setErrorCode(?int $errorCode = null): self
    {
        // validation for constraint: int
        if (!is_null($errorCode) && !(is_int($errorCode) || ctype_digit($errorCode))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($errorCode, true), gettype($errorCode)), __LINE__);
        }
        $this->errorCode = $errorCode;
        
        return $this;
    }
    /**
     * Get indicationNotOk value
     * @return string|null
     */
    public function getIndicationNotOk(): ?string
    {
        return $this->indicationNotOk;
    }
    /**
     * Set indicationNotOk value
     * @param string $indicationNotOk
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setIndicationNotOk(?string $indicationNotOk = null): self
    {
        // validation for constraint: string
        if (!is_null($indicationNotOk) && !is_string($indicationNotOk)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($indicationNotOk, true), gettype($indicationNotOk)), __LINE__);
        }
        $this->indicationNotOk = $indicationNotOk;
        
        return $this;
    }
    /**
     * Get errorDescription value
     * @return string|null
     */
    public function getErrorDescription(): ?string
    {
        return $this->errorDescription;
    }
    /**
     * Set errorDescription value
     * @param string $errorDescription
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setErrorDescription(?string $errorDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($errorDescription) && !is_string($errorDescription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorDescription, true), gettype($errorDescription)), __LINE__);
        }
        $this->errorDescription = $errorDescription;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get errorCodeField value
     * @return string|null
     */
    public function getErrorCodeField(): ?string
    {
        return $this->errorCodeField;
    }
    /**
     * Set errorCodeField value
     * @param string $errorCodeField
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setErrorCodeField(?string $errorCodeField = null): self
    {
        // validation for constraint: string
        if (!is_null($errorCodeField) && !is_string($errorCodeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorCodeField, true), gettype($errorCodeField)), __LINE__);
        }
        $this->errorCodeField = $errorCodeField;
        
        return $this;
    }
    /**
     * Get errorDescriptionField value
     * @return string|null
     */
    public function getErrorDescriptionField(): ?string
    {
        return $this->errorDescriptionField;
    }
    /**
     * Set errorDescriptionField value
     * @param string $errorDescriptionField
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setErrorDescriptionField(?string $errorDescriptionField = null): self
    {
        // validation for constraint: string
        if (!is_null($errorDescriptionField) && !is_string($errorDescriptionField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorDescriptionField, true), gettype($errorDescriptionField)), __LINE__);
        }
        $this->errorDescriptionField = $errorDescriptionField;
        
        return $this;
    }
    /**
     * Get indicationNotOkField value
     * @return string|null
     */
    public function getIndicationNotOkField(): ?string
    {
        return $this->indicationNotOkField;
    }
    /**
     * Set indicationNotOkField value
     * @param string $indicationNotOkField
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setIndicationNotOkField(?string $indicationNotOkField = null): self
    {
        // validation for constraint: string
        if (!is_null($indicationNotOkField) && !is_string($indicationNotOkField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($indicationNotOkField, true), gettype($indicationNotOkField)), __LINE__);
        }
        $this->indicationNotOkField = $indicationNotOkField;
        
        return $this;
    }
    /**
     * Get tableNameField value
     * @return string|null
     */
    public function getTableNameField(): ?string
    {
        return $this->tableNameField;
    }
    /**
     * Set tableNameField value
     * @param string $tableNameField
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setTableNameField(?string $tableNameField = null): self
    {
        // validation for constraint: string
        if (!is_null($tableNameField) && !is_string($tableNameField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tableNameField, true), gettype($tableNameField)), __LINE__);
        }
        $this->tableNameField = $tableNameField;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\LoggingType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
