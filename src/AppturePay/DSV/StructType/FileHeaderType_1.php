<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FileHeaderType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:FileHeaderType
 * @subpackage Structs
 */
class FileHeaderType_1 extends AbstractStructBase
{
    /**
     * The extraAmount
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ExtraAmountType_1[]
     */
    protected ?array $extraAmount = null;
    /**
     * The invoiceLine
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\InvoiceLineType_1[]
     */
    protected ?array $invoiceLine = null;
    /**
     * The shippingUnit
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    protected ?array $shippingUnit = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The extraAmountField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfExtraAmountType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfExtraAmountType $extraAmountField = null;
    /**
     * The invoiceLineField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType $invoiceLineField = null;
    /**
     * The shippingUnitField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \AppturePay\DSV\ArrayType\ArrayOfShippingUnitType|null
     */
    protected ?\AppturePay\DSV\ArrayType\ArrayOfShippingUnitType $shippingUnitField = null;
    /**
     * The typeField
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $typeField = null;
    /**
     * Constructor method for FileHeaderType
     * @uses FileHeaderType_1::setExtraAmount()
     * @uses FileHeaderType_1::setInvoiceLine()
     * @uses FileHeaderType_1::setShippingUnit()
     * @uses FileHeaderType_1::setType()
     * @uses FileHeaderType_1::setExtraAmountField()
     * @uses FileHeaderType_1::setInvoiceLineField()
     * @uses FileHeaderType_1::setShippingUnitField()
     * @uses FileHeaderType_1::setTypeField()
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1[] $extraAmount
     * @param \AppturePay\DSV\StructType\InvoiceLineType_1[] $invoiceLine
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnit
     * @param string $type
     * @param \AppturePay\DSV\ArrayType\ArrayOfExtraAmountType $extraAmountField
     * @param \AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType $invoiceLineField
     * @param \AppturePay\DSV\ArrayType\ArrayOfShippingUnitType $shippingUnitField
     * @param string $typeField
     */
    public function __construct(?array $extraAmount = null, ?array $invoiceLine = null, ?array $shippingUnit = null, ?string $type = null, ?\AppturePay\DSV\ArrayType\ArrayOfExtraAmountType $extraAmountField = null, ?\AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType $invoiceLineField = null, ?\AppturePay\DSV\ArrayType\ArrayOfShippingUnitType $shippingUnitField = null, ?string $typeField = null)
    {
        $this
            ->setExtraAmount($extraAmount)
            ->setInvoiceLine($invoiceLine)
            ->setShippingUnit($shippingUnit)
            ->setType($type)
            ->setExtraAmountField($extraAmountField)
            ->setInvoiceLineField($invoiceLineField)
            ->setShippingUnitField($shippingUnitField)
            ->setTypeField($typeField);
    }
    /**
     * Get extraAmount value
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1[]
     */
    public function getExtraAmount(): ?array
    {
        return $this->extraAmount;
    }
    /**
     * This method is responsible for validating the values passed to the setExtraAmount method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtraAmount method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtraAmountForArrayConstraintsFromSetExtraAmount(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileHeaderTypeExtraAmountItem) {
            // validation for constraint: itemType
            if (!$fileHeaderTypeExtraAmountItem instanceof \AppturePay\DSV\StructType\ExtraAmountType_1) {
                $invalidValues[] = is_object($fileHeaderTypeExtraAmountItem) ? get_class($fileHeaderTypeExtraAmountItem) : sprintf('%s(%s)', gettype($fileHeaderTypeExtraAmountItem), var_export($fileHeaderTypeExtraAmountItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The extraAmount property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set extraAmount value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1[] $extraAmount
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setExtraAmount(?array $extraAmount = null): self
    {
        // validation for constraint: array
        if ('' !== ($extraAmountArrayErrorMessage = self::validateExtraAmountForArrayConstraintsFromSetExtraAmount($extraAmount))) {
            throw new InvalidArgumentException($extraAmountArrayErrorMessage, __LINE__);
        }
        $this->extraAmount = $extraAmount;
        
        return $this;
    }
    /**
     * Add item to extraAmount value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1 $item
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function addToExtraAmount(\AppturePay\DSV\StructType\ExtraAmountType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ExtraAmountType_1) {
            throw new InvalidArgumentException(sprintf('The extraAmount property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->extraAmount[] = $item;
        
        return $this;
    }
    /**
     * Get invoiceLine value
     * @return \AppturePay\DSV\StructType\InvoiceLineType_1[]
     */
    public function getInvoiceLine(): ?array
    {
        return $this->invoiceLine;
    }
    /**
     * This method is responsible for validating the values passed to the setInvoiceLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInvoiceLine method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInvoiceLineForArrayConstraintsFromSetInvoiceLine(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileHeaderTypeInvoiceLineItem) {
            // validation for constraint: itemType
            if (!$fileHeaderTypeInvoiceLineItem instanceof \AppturePay\DSV\StructType\InvoiceLineType_1) {
                $invalidValues[] = is_object($fileHeaderTypeInvoiceLineItem) ? get_class($fileHeaderTypeInvoiceLineItem) : sprintf('%s(%s)', gettype($fileHeaderTypeInvoiceLineItem), var_export($fileHeaderTypeInvoiceLineItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The invoiceLine property can only contain items of type \AppturePay\DSV\StructType\InvoiceLineType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set invoiceLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\InvoiceLineType_1[] $invoiceLine
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setInvoiceLine(?array $invoiceLine = null): self
    {
        // validation for constraint: array
        if ('' !== ($invoiceLineArrayErrorMessage = self::validateInvoiceLineForArrayConstraintsFromSetInvoiceLine($invoiceLine))) {
            throw new InvalidArgumentException($invoiceLineArrayErrorMessage, __LINE__);
        }
        $this->invoiceLine = $invoiceLine;
        
        return $this;
    }
    /**
     * Add item to invoiceLine value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\InvoiceLineType_1 $item
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function addToInvoiceLine(\AppturePay\DSV\StructType\InvoiceLineType_1 $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\InvoiceLineType_1) {
            throw new InvalidArgumentException(sprintf('The invoiceLine property can only contain items of type \AppturePay\DSV\StructType\InvoiceLineType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->invoiceLine[] = $item;
        
        return $this;
    }
    /**
     * Get shippingUnit value
     * @return \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    public function getShippingUnit(): ?array
    {
        return $this->shippingUnit;
    }
    /**
     * This method is responsible for validating the values passed to the setShippingUnit method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShippingUnit method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShippingUnitForArrayConstraintsFromSetShippingUnit(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fileHeaderTypeShippingUnitItem) {
            // validation for constraint: itemType
            if (!$fileHeaderTypeShippingUnitItem instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
                $invalidValues[] = is_object($fileHeaderTypeShippingUnitItem) ? get_class($fileHeaderTypeShippingUnitItem) : sprintf('%s(%s)', gettype($fileHeaderTypeShippingUnitItem), var_export($fileHeaderTypeShippingUnitItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shippingUnit property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set shippingUnit value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnit
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setShippingUnit(?array $shippingUnit = null): self
    {
        // validation for constraint: array
        if ('' !== ($shippingUnitArrayErrorMessage = self::validateShippingUnitForArrayConstraintsFromSetShippingUnit($shippingUnit))) {
            throw new InvalidArgumentException($shippingUnitArrayErrorMessage, __LINE__);
        }
        $this->shippingUnit = $shippingUnit;
        
        return $this;
    }
    /**
     * Add item to shippingUnit value
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType $item
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function addToShippingUnit(\AppturePay\DSV\StructType\ShippingUnitType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
            throw new InvalidArgumentException(sprintf('The shippingUnit property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shippingUnit[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get extraAmountField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfExtraAmountType|null
     */
    public function getExtraAmountField(): ?\AppturePay\DSV\ArrayType\ArrayOfExtraAmountType
    {
        return $this->extraAmountField;
    }
    /**
     * Set extraAmountField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfExtraAmountType $extraAmountField
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setExtraAmountField(?\AppturePay\DSV\ArrayType\ArrayOfExtraAmountType $extraAmountField = null): self
    {
        $this->extraAmountField = $extraAmountField;
        
        return $this;
    }
    /**
     * Get invoiceLineField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType|null
     */
    public function getInvoiceLineField(): ?\AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType
    {
        return $this->invoiceLineField;
    }
    /**
     * Set invoiceLineField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType $invoiceLineField
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setInvoiceLineField(?\AppturePay\DSV\ArrayType\ArrayOfInvoiceLineType $invoiceLineField = null): self
    {
        $this->invoiceLineField = $invoiceLineField;
        
        return $this;
    }
    /**
     * Get shippingUnitField value
     * @return \AppturePay\DSV\ArrayType\ArrayOfShippingUnitType|null
     */
    public function getShippingUnitField(): ?\AppturePay\DSV\ArrayType\ArrayOfShippingUnitType
    {
        return $this->shippingUnitField;
    }
    /**
     * Set shippingUnitField value
     * @param \AppturePay\DSV\ArrayType\ArrayOfShippingUnitType $shippingUnitField
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setShippingUnitField(?\AppturePay\DSV\ArrayType\ArrayOfShippingUnitType $shippingUnitField = null): self
    {
        $this->shippingUnitField = $shippingUnitField;
        
        return $this;
    }
    /**
     * Get typeField value
     * @return string|null
     */
    public function getTypeField(): ?string
    {
        return $this->typeField;
    }
    /**
     * Set typeField value
     * @param string $typeField
     * @return \AppturePay\DSV\StructType\FileHeaderType_1
     */
    public function setTypeField(?string $typeField = null): self
    {
        // validation for constraint: string
        if (!is_null($typeField) && !is_string($typeField)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeField, true), gettype($typeField)), __LINE__);
        }
        $this->typeField = $typeField;
        
        return $this;
    }
}
