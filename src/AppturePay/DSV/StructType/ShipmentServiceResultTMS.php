<?php

declare(strict_types=1);

namespace AppturePay\DSV\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentServiceResultTMS StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ShipmentServiceResultTMS
 * @subpackage Structs
 */
class ShipmentServiceResultTMS extends ShipmentServiceResult
{
    /**
     * The TMSServiceError
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ServiceMessage|null
     */
    protected ?\AppturePay\DSV\StructType\ServiceMessage $TMSServiceError = null;
    /**
     * The TMSResponse
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\EdiParametersType_1|null
     */
    protected ?\AppturePay\DSV\StructType\EdiParametersType_1 $TMSResponse = null;
    /**
     * Constructor method for ShipmentServiceResultTMS
     * @uses ShipmentServiceResultTMS::setTMSServiceError()
     * @uses ShipmentServiceResultTMS::setTMSResponse()
     * @param \AppturePay\DSV\StructType\ServiceMessage $tMSServiceError
     * @param \AppturePay\DSV\StructType\EdiParametersType_1 $tMSResponse
     */
    public function __construct(?\AppturePay\DSV\StructType\ServiceMessage $tMSServiceError = null, ?\AppturePay\DSV\StructType\EdiParametersType_1 $tMSResponse = null)
    {
        $this
            ->setTMSServiceError($tMSServiceError)
            ->setTMSResponse($tMSResponse);
    }
    /**
     * Get TMSServiceError value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\ServiceMessage|null
     */
    public function getTMSServiceError(): ?\AppturePay\DSV\StructType\ServiceMessage
    {
        return isset($this->TMSServiceError) ? $this->TMSServiceError : null;
    }
    /**
     * Set TMSServiceError value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \AppturePay\DSV\StructType\ServiceMessage $tMSServiceError
     * @return \AppturePay\DSV\StructType\ShipmentServiceResultTMS
     */
    public function setTMSServiceError(?\AppturePay\DSV\StructType\ServiceMessage $tMSServiceError = null): self
    {
        if (is_null($tMSServiceError) || (is_array($tMSServiceError) && empty($tMSServiceError))) {
            unset($this->TMSServiceError);
        } else {
            $this->TMSServiceError = $tMSServiceError;
        }
        
        return $this;
    }
    /**
     * Get TMSResponse value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\EdiParametersType_1|null
     */
    public function getTMSResponse(): ?\AppturePay\DSV\StructType\EdiParametersType_1
    {
        return isset($this->TMSResponse) ? $this->TMSResponse : null;
    }
    /**
     * Set TMSResponse value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \AppturePay\DSV\StructType\EdiParametersType_1 $tMSResponse
     * @return \AppturePay\DSV\StructType\ShipmentServiceResultTMS
     */
    public function setTMSResponse(?\AppturePay\DSV\StructType\EdiParametersType_1 $tMSResponse = null): self
    {
        if (is_null($tMSResponse) || (is_array($tMSResponse) && empty($tMSResponse))) {
            unset($this->TMSResponse);
        } else {
            $this->TMSResponse = $tMSResponse;
        }
        
        return $this;
    }
}
