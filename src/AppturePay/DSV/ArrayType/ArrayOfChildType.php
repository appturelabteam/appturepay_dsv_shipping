<?php

declare(strict_types=1);

namespace AppturePay\DSV\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfChildType ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfChildType
 * @subpackage Arrays
 */
class ArrayOfChildType extends AbstractStructArrayBase
{
    /**
     * The ChildType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ChildType_1[]
     */
    protected ?array $ChildType = null;
    /**
     * Constructor method for ArrayOfChildType
     * @uses ArrayOfChildType::setChildType()
     * @param \AppturePay\DSV\StructType\ChildType_1[] $childType
     */
    public function __construct(?array $childType = null)
    {
        $this
            ->setChildType($childType);
    }
    /**
     * Get ChildType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\ChildType_1[]
     */
    public function getChildType(): ?array
    {
        return isset($this->ChildType) ? $this->ChildType : null;
    }
    /**
     * This method is responsible for validating the values passed to the setChildType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setChildType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateChildTypeForArrayConstraintsFromSetChildType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfChildTypeChildTypeItem) {
            // validation for constraint: itemType
            if (!$arrayOfChildTypeChildTypeItem instanceof \AppturePay\DSV\StructType\ChildType_1) {
                $invalidValues[] = is_object($arrayOfChildTypeChildTypeItem) ? get_class($arrayOfChildTypeChildTypeItem) : sprintf('%s(%s)', gettype($arrayOfChildTypeChildTypeItem), var_export($arrayOfChildTypeChildTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ChildType property can only contain items of type \AppturePay\DSV\StructType\ChildType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set ChildType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ChildType_1[] $childType
     * @return \AppturePay\DSV\ArrayType\ArrayOfChildType
     */
    public function setChildType(?array $childType = null): self
    {
        // validation for constraint: array
        if ('' !== ($childTypeArrayErrorMessage = self::validateChildTypeForArrayConstraintsFromSetChildType($childType))) {
            throw new InvalidArgumentException($childTypeArrayErrorMessage, __LINE__);
        }
        if (is_null($childType) || (is_array($childType) && empty($childType))) {
            unset($this->ChildType);
        } else {
            $this->ChildType = $childType;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \AppturePay\DSV\StructType\ChildType_1|null
     */
    public function current(): ?\AppturePay\DSV\StructType\ChildType_1
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \AppturePay\DSV\StructType\ChildType_1|null
     */
    public function item($index): ?\AppturePay\DSV\StructType\ChildType_1
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \AppturePay\DSV\StructType\ChildType_1|null
     */
    public function first(): ?\AppturePay\DSV\StructType\ChildType_1
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \AppturePay\DSV\StructType\ChildType_1|null
     */
    public function last(): ?\AppturePay\DSV\StructType\ChildType_1
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \AppturePay\DSV\StructType\ChildType_1|null
     */
    public function offsetGet($offset): ?\AppturePay\DSV\StructType\ChildType_1
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ChildType_1 $item
     * @return \AppturePay\DSV\ArrayType\ArrayOfChildType
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ChildType_1) {
            throw new InvalidArgumentException(sprintf('The ChildType property can only contain items of type \AppturePay\DSV\StructType\ChildType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ChildType
     */
    public function getAttributeName(): string
    {
        return 'ChildType';
    }
}
