<?php

declare(strict_types=1);

namespace AppturePay\DSV\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfExtraAmountType ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfExtraAmountType
 * @subpackage Arrays
 */
class ArrayOfExtraAmountType extends AbstractStructArrayBase
{
    /**
     * The ExtraAmountType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ExtraAmountType_1[]
     */
    protected ?array $ExtraAmountType = null;
    /**
     * Constructor method for ArrayOfExtraAmountType
     * @uses ArrayOfExtraAmountType::setExtraAmountType()
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1[] $extraAmountType
     */
    public function __construct(?array $extraAmountType = null)
    {
        $this
            ->setExtraAmountType($extraAmountType);
    }
    /**
     * Get ExtraAmountType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1[]
     */
    public function getExtraAmountType(): ?array
    {
        return isset($this->ExtraAmountType) ? $this->ExtraAmountType : null;
    }
    /**
     * This method is responsible for validating the values passed to the setExtraAmountType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtraAmountType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtraAmountTypeForArrayConstraintsFromSetExtraAmountType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfExtraAmountTypeExtraAmountTypeItem) {
            // validation for constraint: itemType
            if (!$arrayOfExtraAmountTypeExtraAmountTypeItem instanceof \AppturePay\DSV\StructType\ExtraAmountType_1) {
                $invalidValues[] = is_object($arrayOfExtraAmountTypeExtraAmountTypeItem) ? get_class($arrayOfExtraAmountTypeExtraAmountTypeItem) : sprintf('%s(%s)', gettype($arrayOfExtraAmountTypeExtraAmountTypeItem), var_export($arrayOfExtraAmountTypeExtraAmountTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ExtraAmountType property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set ExtraAmountType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1[] $extraAmountType
     * @return \AppturePay\DSV\ArrayType\ArrayOfExtraAmountType
     */
    public function setExtraAmountType(?array $extraAmountType = null): self
    {
        // validation for constraint: array
        if ('' !== ($extraAmountTypeArrayErrorMessage = self::validateExtraAmountTypeForArrayConstraintsFromSetExtraAmountType($extraAmountType))) {
            throw new InvalidArgumentException($extraAmountTypeArrayErrorMessage, __LINE__);
        }
        if (is_null($extraAmountType) || (is_array($extraAmountType) && empty($extraAmountType))) {
            unset($this->ExtraAmountType);
        } else {
            $this->ExtraAmountType = $extraAmountType;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1|null
     */
    public function current(): ?\AppturePay\DSV\StructType\ExtraAmountType_1
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1|null
     */
    public function item($index): ?\AppturePay\DSV\StructType\ExtraAmountType_1
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1|null
     */
    public function first(): ?\AppturePay\DSV\StructType\ExtraAmountType_1
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1|null
     */
    public function last(): ?\AppturePay\DSV\StructType\ExtraAmountType_1
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \AppturePay\DSV\StructType\ExtraAmountType_1|null
     */
    public function offsetGet($offset): ?\AppturePay\DSV\StructType\ExtraAmountType_1
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ExtraAmountType_1 $item
     * @return \AppturePay\DSV\ArrayType\ArrayOfExtraAmountType
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ExtraAmountType_1) {
            throw new InvalidArgumentException(sprintf('The ExtraAmountType property can only contain items of type \AppturePay\DSV\StructType\ExtraAmountType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ExtraAmountType
     */
    public function getAttributeName(): string
    {
        return 'ExtraAmountType';
    }
}
