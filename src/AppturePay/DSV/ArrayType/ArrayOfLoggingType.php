<?php

declare(strict_types=1);

namespace AppturePay\DSV\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfLoggingType ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfLoggingType
 * @subpackage Arrays
 */
class ArrayOfLoggingType extends AbstractStructArrayBase
{
    /**
     * The LoggingType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\LoggingType_1[]
     */
    protected ?array $LoggingType = null;
    /**
     * Constructor method for ArrayOfLoggingType
     * @uses ArrayOfLoggingType::setLoggingType()
     * @param \AppturePay\DSV\StructType\LoggingType_1[] $loggingType
     */
    public function __construct(?array $loggingType = null)
    {
        $this
            ->setLoggingType($loggingType);
    }
    /**
     * Get LoggingType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\LoggingType_1[]
     */
    public function getLoggingType(): ?array
    {
        return isset($this->LoggingType) ? $this->LoggingType : null;
    }
    /**
     * This method is responsible for validating the values passed to the setLoggingType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLoggingType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLoggingTypeForArrayConstraintsFromSetLoggingType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfLoggingTypeLoggingTypeItem) {
            // validation for constraint: itemType
            if (!$arrayOfLoggingTypeLoggingTypeItem instanceof \AppturePay\DSV\StructType\LoggingType_1) {
                $invalidValues[] = is_object($arrayOfLoggingTypeLoggingTypeItem) ? get_class($arrayOfLoggingTypeLoggingTypeItem) : sprintf('%s(%s)', gettype($arrayOfLoggingTypeLoggingTypeItem), var_export($arrayOfLoggingTypeLoggingTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The LoggingType property can only contain items of type \AppturePay\DSV\StructType\LoggingType_1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set LoggingType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType_1[] $loggingType
     * @return \AppturePay\DSV\ArrayType\ArrayOfLoggingType
     */
    public function setLoggingType(?array $loggingType = null): self
    {
        // validation for constraint: array
        if ('' !== ($loggingTypeArrayErrorMessage = self::validateLoggingTypeForArrayConstraintsFromSetLoggingType($loggingType))) {
            throw new InvalidArgumentException($loggingTypeArrayErrorMessage, __LINE__);
        }
        if (is_null($loggingType) || (is_array($loggingType) && empty($loggingType))) {
            unset($this->LoggingType);
        } else {
            $this->LoggingType = $loggingType;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \AppturePay\DSV\StructType\LoggingType_1|null
     */
    public function current(): ?\AppturePay\DSV\StructType\LoggingType_1
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \AppturePay\DSV\StructType\LoggingType_1|null
     */
    public function item($index): ?\AppturePay\DSV\StructType\LoggingType_1
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \AppturePay\DSV\StructType\LoggingType_1|null
     */
    public function first(): ?\AppturePay\DSV\StructType\LoggingType_1
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \AppturePay\DSV\StructType\LoggingType_1|null
     */
    public function last(): ?\AppturePay\DSV\StructType\LoggingType_1
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \AppturePay\DSV\StructType\LoggingType_1|null
     */
    public function offsetGet($offset): ?\AppturePay\DSV\StructType\LoggingType_1
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\LoggingType_1 $item
     * @return \AppturePay\DSV\ArrayType\ArrayOfLoggingType
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\LoggingType_1) {
            throw new InvalidArgumentException(sprintf('The LoggingType property can only contain items of type \AppturePay\DSV\StructType\LoggingType_1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string LoggingType
     */
    public function getAttributeName(): string
    {
        return 'LoggingType';
    }
}
