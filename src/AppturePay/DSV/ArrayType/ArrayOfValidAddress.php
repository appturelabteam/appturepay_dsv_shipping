<?php

declare(strict_types=1);

namespace AppturePay\DSV\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfValidAddress ArrayType
 * @subpackage Arrays
 */
class ArrayOfValidAddress extends AbstractStructArrayBase
{
    /**
     * The ValidAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ValidAddress[]
     */
    protected ?array $ValidAddress = null;
    /**
     * Constructor method for ArrayOfValidAddress
     * @uses ArrayOfValidAddress::setValidAddress()
     * @param \AppturePay\DSV\StructType\ValidAddress[] $validAddress
     */
    public function __construct(?array $validAddress = null)
    {
        $this
            ->setValidAddress($validAddress);
    }
    /**
     * Get ValidAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\ValidAddress[]
     */
    public function getValidAddress(): ?array
    {
        return isset($this->ValidAddress) ? $this->ValidAddress : null;
    }
    /**
     * This method is responsible for validating the values passed to the setValidAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setValidAddress method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateValidAddressForArrayConstraintsFromSetValidAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfValidAddressValidAddressItem) {
            // validation for constraint: itemType
            if (!$arrayOfValidAddressValidAddressItem instanceof \AppturePay\DSV\StructType\ValidAddress) {
                $invalidValues[] = is_object($arrayOfValidAddressValidAddressItem) ? get_class($arrayOfValidAddressValidAddressItem) : sprintf('%s(%s)', gettype($arrayOfValidAddressValidAddressItem), var_export($arrayOfValidAddressValidAddressItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ValidAddress property can only contain items of type \AppturePay\DSV\StructType\ValidAddress, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set ValidAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ValidAddress[] $validAddress
     * @return \AppturePay\DSV\ArrayType\ArrayOfValidAddress
     */
    public function setValidAddress(?array $validAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($validAddressArrayErrorMessage = self::validateValidAddressForArrayConstraintsFromSetValidAddress($validAddress))) {
            throw new InvalidArgumentException($validAddressArrayErrorMessage, __LINE__);
        }
        if (is_null($validAddress) || (is_array($validAddress) && empty($validAddress))) {
            unset($this->ValidAddress);
        } else {
            $this->ValidAddress = $validAddress;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \AppturePay\DSV\StructType\ValidAddress|null
     */
    public function current(): ?\AppturePay\DSV\StructType\ValidAddress
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \AppturePay\DSV\StructType\ValidAddress|null
     */
    public function item($index): ?\AppturePay\DSV\StructType\ValidAddress
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \AppturePay\DSV\StructType\ValidAddress|null
     */
    public function first(): ?\AppturePay\DSV\StructType\ValidAddress
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \AppturePay\DSV\StructType\ValidAddress|null
     */
    public function last(): ?\AppturePay\DSV\StructType\ValidAddress
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \AppturePay\DSV\StructType\ValidAddress|null
     */
    public function offsetGet($offset): ?\AppturePay\DSV\StructType\ValidAddress
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ValidAddress $item
     * @return \AppturePay\DSV\ArrayType\ArrayOfValidAddress
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ValidAddress) {
            throw new InvalidArgumentException(sprintf('The ValidAddress property can only contain items of type \AppturePay\DSV\StructType\ValidAddress, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ValidAddress
     */
    public function getAttributeName(): string
    {
        return 'ValidAddress';
    }
}
