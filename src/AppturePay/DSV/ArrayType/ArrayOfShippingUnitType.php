<?php

declare(strict_types=1);

namespace AppturePay\DSV\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfShippingUnitType ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfShippingUnitType
 * @subpackage Arrays
 */
class ArrayOfShippingUnitType extends AbstractStructArrayBase
{
    /**
     * The ShippingUnitType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    protected ?array $ShippingUnitType = null;
    /**
     * Constructor method for ArrayOfShippingUnitType
     * @uses ArrayOfShippingUnitType::setShippingUnitType()
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnitType
     */
    public function __construct(?array $shippingUnitType = null)
    {
        $this
            ->setShippingUnitType($shippingUnitType);
    }
    /**
     * Get ShippingUnitType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \AppturePay\DSV\StructType\ShippingUnitType[]
     */
    public function getShippingUnitType(): ?array
    {
        return isset($this->ShippingUnitType) ? $this->ShippingUnitType : null;
    }
    /**
     * This method is responsible for validating the values passed to the setShippingUnitType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShippingUnitType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShippingUnitTypeForArrayConstraintsFromSetShippingUnitType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfShippingUnitTypeShippingUnitTypeItem) {
            // validation for constraint: itemType
            if (!$arrayOfShippingUnitTypeShippingUnitTypeItem instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
                $invalidValues[] = is_object($arrayOfShippingUnitTypeShippingUnitTypeItem) ? get_class($arrayOfShippingUnitTypeShippingUnitTypeItem) : sprintf('%s(%s)', gettype($arrayOfShippingUnitTypeShippingUnitTypeItem), var_export($arrayOfShippingUnitTypeShippingUnitTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ShippingUnitType property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set ShippingUnitType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType[] $shippingUnitType
     * @return \AppturePay\DSV\ArrayType\ArrayOfShippingUnitType
     */
    public function setShippingUnitType(?array $shippingUnitType = null): self
    {
        // validation for constraint: array
        if ('' !== ($shippingUnitTypeArrayErrorMessage = self::validateShippingUnitTypeForArrayConstraintsFromSetShippingUnitType($shippingUnitType))) {
            throw new InvalidArgumentException($shippingUnitTypeArrayErrorMessage, __LINE__);
        }
        if (is_null($shippingUnitType) || (is_array($shippingUnitType) && empty($shippingUnitType))) {
            unset($this->ShippingUnitType);
        } else {
            $this->ShippingUnitType = $shippingUnitType;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \AppturePay\DSV\StructType\ShippingUnitType|null
     */
    public function current(): ?\AppturePay\DSV\StructType\ShippingUnitType
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \AppturePay\DSV\StructType\ShippingUnitType|null
     */
    public function item($index): ?\AppturePay\DSV\StructType\ShippingUnitType
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \AppturePay\DSV\StructType\ShippingUnitType|null
     */
    public function first(): ?\AppturePay\DSV\StructType\ShippingUnitType
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \AppturePay\DSV\StructType\ShippingUnitType|null
     */
    public function last(): ?\AppturePay\DSV\StructType\ShippingUnitType
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \AppturePay\DSV\StructType\ShippingUnitType|null
     */
    public function offsetGet($offset): ?\AppturePay\DSV\StructType\ShippingUnitType
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \AppturePay\DSV\StructType\ShippingUnitType $item
     * @return \AppturePay\DSV\ArrayType\ArrayOfShippingUnitType
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \AppturePay\DSV\StructType\ShippingUnitType) {
            throw new InvalidArgumentException(sprintf('The ShippingUnitType property can only contain items of type \AppturePay\DSV\StructType\ShippingUnitType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ShippingUnitType
     */
    public function getAttributeName(): string
    {
        return 'ShippingUnitType';
    }
}
