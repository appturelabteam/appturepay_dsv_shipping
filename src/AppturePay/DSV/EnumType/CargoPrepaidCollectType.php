<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CargoPrepaidCollectType EnumType
 * @subpackage Enumerations
 */
class CargoPrepaidCollectType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'P'
     * @return string 'P'
     */
    const VALUE_P = 'P';
    /**
     * Constant for value 'C'
     * @return string 'C'
     */
    const VALUE_C = 'C';
    /**
     * Return allowed values
     * @uses self::VALUE_P
     * @uses self::VALUE_C
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_P,
            self::VALUE_C,
        ];
    }
}
