<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for MotModeType EnumType
 * @subpackage Enumerations
 */
class MotModeType extends AbstractStructEnumBase
{
    /**
     * Constant for value '1'
     * @return string '1'
     */
    const VALUE_1 = '1';
    /**
     * Constant for value '3'
     * @return string '3'
     */
    const VALUE_3 = '3';
    /**
     * Constant for value '4'
     * @return string '4'
     */
    const VALUE_4 = '4';
    /**
     * Constant for value '10'
     * @return string '10'
     */
    const VALUE_10 = '10';
    /**
     * Constant for value '30'
     * @return string '30'
     */
    const VALUE_30 = '30';
    /**
     * Constant for value '40'
     * @return string '40'
     */
    const VALUE_40 = '40';
    /**
     * Return allowed values
     * @uses self::VALUE_1
     * @uses self::VALUE_3
     * @uses self::VALUE_4
     * @uses self::VALUE_10
     * @uses self::VALUE_30
     * @uses self::VALUE_40
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_1,
            self::VALUE_3,
            self::VALUE_4,
            self::VALUE_10,
            self::VALUE_30,
            self::VALUE_40,
        ];
    }
}
