<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CodSequenceNumber EnumType
 * @subpackage Enumerations
 */
class CodSequenceNumber extends AbstractStructEnumBase
{
    /**
     * Constant for value '0'
     * @return string '0'
     */
    const VALUE_0 = '0';
    /**
     * Constant for value '98'
     * @return string '98'
     */
    const VALUE_98 = '98';
    /**
     * Constant for value '99'
     * @return string '99'
     */
    const VALUE_99 = '99';
    /**
     * Return allowed values
     * @uses self::VALUE_0
     * @uses self::VALUE_98
     * @uses self::VALUE_99
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_0,
            self::VALUE_98,
            self::VALUE_99,
        ];
    }
}
