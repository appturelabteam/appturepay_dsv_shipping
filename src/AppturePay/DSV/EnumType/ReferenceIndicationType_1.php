<?php

declare(strict_types=1);

namespace AppturePay\DSV\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for ReferenceIndicationType EnumType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ReferenceIndicationType
 * @subpackage Enumerations
 */
class ReferenceIndicationType_1 extends AbstractStructEnumBase
{
    /**
     * Constant for value '0'
     * @return string '0'
     */
    const VALUE_0_1 = '0';
    /**
     * Constant for value '1'
     * @return string '1'
     */
    const VALUE_1_1 = '1';
    /**
     * Constant for value '9'
     * @return string '9'
     */
    const VALUE_9_1 = '9';
    /**
     * Constant for value 'Item0'
     * @return string 'Item0'
     */
    const VALUE_ITEM_0 = 'Item0';
    /**
     * Constant for value 'Item1'
     * @return string 'Item1'
     */
    const VALUE_ITEM_1 = 'Item1';
    /**
     * Constant for value 'Item9'
     * @return string 'Item9'
     */
    const VALUE_ITEM_9 = 'Item9';
    /**
     * Return allowed values
     * @uses self::VALUE_0_1
     * @uses self::VALUE_1_1
     * @uses self::VALUE_9_1
     * @uses self::VALUE_ITEM_0
     * @uses self::VALUE_ITEM_1
     * @uses self::VALUE_ITEM_9
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_0_1,
            self::VALUE_1_1,
            self::VALUE_9_1,
            self::VALUE_ITEM_0,
            self::VALUE_ITEM_1,
            self::VALUE_ITEM_9,
        ];
    }
}
