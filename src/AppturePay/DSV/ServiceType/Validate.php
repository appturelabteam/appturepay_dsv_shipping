<?php

declare(strict_types=1);

namespace AppturePay\DSV\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Validate ServiceType
 * @subpackage Services
 */
class Validate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ValidateAddress
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \AppturePay\DSV\StructType\ValidateAddress $parameters
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse|bool
     */
    public function ValidateAddress(\AppturePay\DSV\StructType\ValidateAddress $parameters)
    {
        try {
            $this->setResult($resultValidateAddress = $this->getSoapClient()->__soapCall('ValidateAddress', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultValidateAddress;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \AppturePay\DSV\StructType\ValidateAddressResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
