<?php

declare(strict_types=1);

namespace AppturePay\DSV\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Submit ServiceType
 * @subpackage Services
 */
class Submit extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SubmitShipmentTMS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \AppturePay\DSV\StructType\SubmitShipmentTMS $parameters
     * @return \AppturePay\DSV\StructType\SubmitShipmentTMSResponse|bool
     */
    public function SubmitShipmentTMS(\AppturePay\DSV\StructType\SubmitShipmentTMS $parameters)
    {
        try {
            $this->setResult($resultSubmitShipmentTMS = $this->getSoapClient()->__soapCall('SubmitShipmentTMS', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSubmitShipmentTMS;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitCancelTMS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \AppturePay\DSV\StructType\SubmitCancelTMS $parameters
     * @return \AppturePay\DSV\StructType\SubmitCancelTMSResponse|bool
     */
    public function SubmitCancelTMS(\AppturePay\DSV\StructType\SubmitCancelTMS $parameters)
    {
        try {
            $this->setResult($resultSubmitCancelTMS = $this->getSoapClient()->__soapCall('SubmitCancelTMS', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSubmitCancelTMS;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \AppturePay\DSV\StructType\SubmitCancelTMSResponse|\AppturePay\DSV\StructType\SubmitShipmentTMSResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
