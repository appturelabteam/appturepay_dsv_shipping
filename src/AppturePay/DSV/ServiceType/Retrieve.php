<?php

declare(strict_types=1);

namespace AppturePay\DSV\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Retrieve ServiceType
 * @subpackage Services
 */
class Retrieve extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named RetrieveShipmentLabelsTMS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMS $parameters
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse|bool
     */
    public function RetrieveShipmentLabelsTMS(\AppturePay\DSV\StructType\RetrieveShipmentLabelsTMS $parameters)
    {
        try {
            $this->setResult($resultRetrieveShipmentLabelsTMS = $this->getSoapClient()->__soapCall('RetrieveShipmentLabelsTMS', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRetrieveShipmentLabelsTMS;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \AppturePay\DSV\StructType\RetrieveShipmentLabelsTMSResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
